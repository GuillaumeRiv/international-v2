-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mar. 03 mars 2020 à 14:02
-- Version du serveur :  5.7.26
-- Version de PHP :  7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `projetinternational`
--

-- --------------------------------------------------------

--
-- Structure de la table `bilan_docs`
--

DROP TABLE IF EXISTS `bilan_docs`;
CREATE TABLE IF NOT EXISTS `bilan_docs` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(100) NOT NULL,
  `path` varchar(100) NOT NULL,
  `text` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `bilan_docs`
--

INSERT INTO `bilan_docs` (`ID`, `nom`, `path`, `text`) VALUES
(1, 'CTR_Terre.jpg', 'docs', 'merci de complÃ©ter le bilan dâ€™apprÃ©ciation de votre sÃ©jour en France. ');

-- --------------------------------------------------------

--
-- Structure de la table `choix_reponse`
--

DROP TABLE IF EXISTS `choix_reponse`;
CREATE TABLE IF NOT EXISTS `choix_reponse` (
  `id_choix_reponse` int(11) NOT NULL AUTO_INCREMENT,
  `id_question` int(11) NOT NULL,
  `choix_reponse` varchar(50) NOT NULL,
  PRIMARY KEY (`id_choix_reponse`),
  KEY `id_question` (`id_question`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `choix_reponse`
--

INSERT INTO `choix_reponse` (`id_choix_reponse`, `id_question`, `choix_reponse`) VALUES
(17, 46, 'Choix #1'),
(18, 46, 'Choix #2');

-- --------------------------------------------------------

--
-- Structure de la table `demandes`
--

DROP TABLE IF EXISTS `demandes`;
CREATE TABLE IF NOT EXISTS `demandes` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `projetDocsID` int(11) DEFAULT NULL,
  `Nom` varchar(50) NOT NULL,
  `Prenom` varchar(50) NOT NULL,
  `adresse` varchar(100) NOT NULL,
  `programme` varchar(150) NOT NULL,
  `destination` varchar(50) NOT NULL,
  `dateD` varchar(15) NOT NULL,
  `dateR` varchar(15) NOT NULL,
  `precisionDestination` varchar(50) NOT NULL,
  `titre` varchar(100) NOT NULL,
  `categorie` varchar(40) NOT NULL,
  `description` text NOT NULL,
  `etudiants` text NOT NULL,
  `liberation` tinyint(1) NOT NULL,
  `financement` tinyint(1) NOT NULL,
  `recrutement` tinyint(1) NOT NULL,
  `strategies` text NOT NULL,
  `date_creation` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `etat` int(11) NOT NULL,
  `raison_refus` varchar(250) DEFAULT NULL,
  `date_changement_etat` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`),
  UNIQUE KEY `ID_2` (`ID`),
  UNIQUE KEY `ID_3` (`ID`),
  KEY `fk_demandes_projdocs` (`projetDocsID`),
  KEY `fk_demandes_user` (`userID`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `demandes`
--

INSERT INTO `demandes` (`ID`, `userID`, `projetDocsID`, `Nom`, `Prenom`, `adresse`, `programme`, `destination`, `dateD`, `dateR`, `precisionDestination`, `titre`, `categorie`, `description`, `etudiants`, `liberation`, `financement`, `recrutement`, `strategies`, `date_creation`, `etat`, `raison_refus`, `date_changement_etat`) VALUES
(24, 16, 8, 'Lampron', 'Jeremy', 'jeremy_lampron@hotmail.com', 'DEC-Bac en informatique', 'Barbade', '2019-12-20', '2020-01-10', 'Barbade Central', 'Barbade 2020', 'Projet de session', 'Nous allons faire un voyage humanitaire en Barbade.', 'Ã‰tudiants de dec bac en informatique.', 1, 0, 0, 'Aucune stratÃ©gie particuliÃ¨rement', '2019-12-06 08:18:48', 1, '', NULL),
(29, 16, 6, 'Lemieux', 'Alexis', 'AlexisLemieux@hotmail.com', 'Technologie de la mécanique du bâtiment (Génie du bâtiment)', 'Afrique du Sud', '2020-12-13', '2020-12-30', 'Angola', 'Afrique Angola 2020', 'Projet de session', 'Nous allons construire des batiments en afrique.', 'Ã‰tudiants en Technologie de la mÃ©canique du bÃ¢timent (GÃ©nie du bÃ¢timent)', 1, 1, 1, '', '2019-12-06 09:42:37', 1, '', NULL),
(30, 16, NULL, 'DemandeTest', 'DemandeTest', 'test123@gmail.com', 'Techniques dâ€™hygiÃ¨ne dentaire', 'Aruba', '2019-12-11', '2019-12-26', 'no', 'France 2021', 'Projet de session', '123', '123', 1, 1, 1, '', '2019-12-11 03:10:55', 2, 'Raison de refus ', NULL),
(31, 16, 9, 'DemandeTest', 'DemandeTest', 'test123@gmail.com', 'Techniques de diÃ©tÃ©tique', 'Canada', '2019-12-12', '2019-12-24', 'no', 'DemandeTest', 'Projet de session', 'Description de projet', 'Tous les Ã©tudiants du cÃ©gep', 1, 1, 1, '', '2019-12-11 08:43:43', 1, '', NULL),
(32, 27, 10, 'Caron-Royer', 'Julie', 'julie.caron.royer@cegeptr.qc.ca', 'Techniques de la documentation', 'Allemagne', '2021-03-14', '2021-03-21', 'Berlin', 'visite culturelle', 'Projet de session', 'Documentation et archives - voir traitement de l\'information en Allemagne', 'Documentation et langues', 1, 1, 0, 'cours - loft - ', '2020-01-07 10:12:56', 1, '', NULL),
(33, 16, NULL, 'Caron-Royer', 'Julie', 'julie.caron.royer@cegeptr.qc.ca', 'Techniques de travail social', 'Argentine', '2021-01-03', '2021-01-10', 'Buenos Aires', 'Argentine 2021', 'Projet de session', 'test', 'TTS', 1, 1, 0, 'cours', '2020-01-07 11:14:26', 1, '', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `demandes_accompagnateurs`
--

DROP TABLE IF EXISTS `demandes_accompagnateurs`;
CREATE TABLE IF NOT EXISTS `demandes_accompagnateurs` (
  `id_acc` int(11) NOT NULL AUTO_INCREMENT,
  `id_demande` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `telephone` varchar(15) NOT NULL,
  `courriel` varchar(50) NOT NULL,
  PRIMARY KEY (`id_acc`),
  KEY `fk_demandes_accompagnateurs` (`id_demande`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `demandes_accompagnateurs`
--

INSERT INTO `demandes_accompagnateurs` (`id_acc`, `id_demande`, `nom`, `prenom`, `telephone`, `courriel`) VALUES
(14, 24, 'Doe', 'John', '(123)321-5531', 'johndoe@Cegeptr.qc.ca'),
(15, 24, 'Poirier', 'Michel', '(642)651-6928', 'michelpoirier@hotmail.com'),
(24, 29, 'Duchesne', 'Robert', '(321)681-4712', 'RobertDus@hotmail.com'),
(25, 29, 'Blanchard', 'Yves', '(313)652-7632', 'YvesBlanchard12@mail.com'),
(26, 30, 'Gadoua', 'Roger', '(111)111-1111', 'roger@hotmail.com'),
(27, 30, 'Graton', 'Bob', '(999)999-9999', 'bobgraton@gmail.com'),
(29, 32, 'Trudel', 'Janie', '(819)376-1721', 'janie@cegeptr.qc.ca'),
(30, 32, 'Gaulin ', 'Joyce', '(819)376-1721', 'joyce@cegeptr.qc.ca'),
(31, 33, 'Trudel', 'Janie ', '(819)376-1721 ', 'janie.trudel@cegeptr.qc.ca');

-- --------------------------------------------------------

--
-- Structure de la table `demandes_activites`
--

DROP TABLE IF EXISTS `demandes_activites`;
CREATE TABLE IF NOT EXISTS `demandes_activites` (
  `id_activite` int(11) NOT NULL AUTO_INCREMENT,
  `id_demande` int(11) NOT NULL,
  `activites` varchar(100) NOT NULL,
  `dates` varchar(50) NOT NULL,
  PRIMARY KEY (`id_activite`),
  KEY `fk_demande_activite` (`id_demande`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `demandes_activites`
--

INSERT INTO `demandes_activites` (`id_activite`, `id_demande`, `activites`, `dates`) VALUES
(17, 24, 'Nettoyage d\'un parc', '25 decembre 2019'),
(18, 24, 'Tour de la ville', '26 decembre 2019'),
(19, 24, 'Aide dans un centre communautaire', '29 decembre 2019'),
(26, 29, 'confection des batiments au sol', '15 decembre 2020'),
(27, 30, 'Activites1', '11/27/2019'),
(28, 31, 'Activites1', '11/27/2019'),
(29, 32, 'ArrivÃ©e Ã  Berlin', '15 mars 2021 '),
(30, 32, 'Retour au Canada', '21 mars 2021'),
(31, 33, 'arrivÃ©e en Argentine ', '3 janvier 2021'),
(32, 33, 'Visite quartier spectacle', '4 janvier 2021'),
(33, 33, 'thÃ©Ã¢tre', '7 janvier 2021'),
(34, 33, 'visite OSBL ', '5 janvier 2021');

-- --------------------------------------------------------

--
-- Structure de la table `demandes_docs`
--

DROP TABLE IF EXISTS `demandes_docs`;
CREATE TABLE IF NOT EXISTS `demandes_docs` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(100) NOT NULL,
  `path` varchar(150) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `demandes_docs`
--

INSERT INTO `demandes_docs` (`ID`, `nom`, `path`) VALUES
(1, 'test', '/test'),
(2, 'test2', '/test2'),
(6, 'FichierAfrique2020.zip', 'uploads'),
(8, 'yeet.zip', 'uploads'),
(9, 'BilanJournalierGabriel.docx', 'uploads'),
(10, 'Perfectionnement.zip', 'uploads');

-- --------------------------------------------------------

--
-- Structure de la table `demandes_vaccins`
--

DROP TABLE IF EXISTS `demandes_vaccins`;
CREATE TABLE IF NOT EXISTS `demandes_vaccins` (
  `id_vaccin` int(11) NOT NULL,
  `id_demande` int(11) NOT NULL,
  KEY `fk_demandes_vaccins` (`id_demande`),
  KEY `fk_vaccins` (`id_vaccin`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `etat_demande`
--

DROP TABLE IF EXISTS `etat_demande`;
CREATE TABLE IF NOT EXISTS `etat_demande` (
  `id_demande` int(11) NOT NULL AUTO_INCREMENT,
  `id_formulaire` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `destination` varchar(100) NOT NULL,
  `raison_refus` varchar(1000) DEFAULT NULL,
  `date_depart` timestamp NOT NULL,
  `date_retour` timestamp NOT NULL,
  `etat` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_demande`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `etat_demande`
--

INSERT INTO `etat_demande` (`id_demande`, `id_formulaire`, `id_user`, `destination`, `raison_refus`, `date_depart`, `date_retour`, `etat`) VALUES
(5, 23, 16, 'kkk', '', '2020-02-02 05:00:00', '2020-02-28 05:00:00', 1),
(10, 23, 16, 'hehe', NULL, '2020-03-02 05:00:00', '2020-03-23 04:00:00', 0),
(9, 23, 16, 'top of', NULL, '2020-03-02 05:00:00', '2020-03-25 04:00:00', 0);

-- --------------------------------------------------------

--
-- Structure de la table `formulaire`
--

DROP TABLE IF EXISTS `formulaire`;
CREATE TABLE IF NOT EXISTS `formulaire` (
  `id_formulaire` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `type` varchar(20) DEFAULT NULL,
  `selected` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `utilized` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_formulaire`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `formulaire`
--

INSERT INTO `formulaire` (`id_formulaire`, `title`, `description`, `type`, `selected`, `deleted`, `created_at`, `updated_at`, `deleted_at`, `utilized`) VALUES
(22, 'Demande de voyage #1', '', 'Demande', 0, 0, '2020-02-28 19:47:09', '2020-02-28 19:47:09', NULL, 1),
(23, 'Formulaire Checkbox', '', 'Demande', 1, 0, '2020-02-28 20:05:11', '2020-02-28 20:05:11', NULL, 1),
(24, 'Coucou', 'hehehe', 'Demande', 0, 1, '2020-03-02 17:19:45', '2020-03-02 17:19:45', NULL, 0),
(25, 'test', '', 'Demande', 0, 1, '2020-03-02 17:36:28', '2020-03-02 17:36:28', NULL, 0),
(26, 'test', '', 'Demande', 0, 1, '2020-03-02 17:36:29', '2020-03-02 17:36:29', NULL, 0),
(27, 'test', '', 'Demande', 0, 1, '2020-03-02 17:36:30', '2020-03-02 17:36:30', NULL, 0),
(28, 'testtt', '', 'Demande', 0, 1, '2020-03-02 17:36:42', '2020-03-02 17:36:42', NULL, 0),
(29, 'tess', '', 'Demande', 0, 1, '2020-03-02 17:37:32', '2020-03-02 17:37:32', NULL, 0);

-- --------------------------------------------------------

--
-- Structure de la table `formulaires_etudiants`
--

DROP TABLE IF EXISTS `formulaires_etudiants`;
CREATE TABLE IF NOT EXISTS `formulaires_etudiants` (
  `id_formulaire` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `projetid` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `courriel` varchar(100) NOT NULL,
  `sexe` varchar(10) NOT NULL,
  `datenaissance` varchar(15) NOT NULL,
  `date_creation` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_formulaire`),
  KEY `fk_formulaires_etudiants` (`userid`),
  KEY `fk_formulaires_projet` (`projetid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `formulaires_etudiants`
--

INSERT INTO `formulaires_etudiants` (`id_formulaire`, `userid`, `projetid`, `nom`, `prenom`, `courriel`, `sexe`, `datenaissance`, `date_creation`) VALUES
(1, 32, 11, 'Jetson', 'Arold', 'Jetson@gmail.com', 'Homme', '1999-02-08', '2020-02-11 13:08:55'),
(2, 32, 11, 'Jetson', 'Arold', 'Jetson@gmail.com', 'Homme', '2020-02-11', '2020-02-11 13:11:36'),
(3, 32, 11, 'Jetson', 'Arold', 'Jetson@gmail.com', 'Homme', '2020-02-11', '2020-02-11 13:13:01');

-- --------------------------------------------------------

--
-- Structure de la table `formulaire_projet`
--

DROP TABLE IF EXISTS `formulaire_projet`;
CREATE TABLE IF NOT EXISTS `formulaire_projet` (
  `id_formulaire` int(11) NOT NULL,
  `id_projet` int(11) NOT NULL,
  `actif` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `formulaire_projet`
--

INSERT INTO `formulaire_projet` (`id_formulaire`, `id_projet`, `actif`) VALUES
(3, 7, 1),
(7, 11, 1);

-- --------------------------------------------------------

--
-- Structure de la table `form_amb`
--

DROP TABLE IF EXISTS `form_amb`;
CREATE TABLE IF NOT EXISTS `form_amb` (
  `id_formulaire` int(11) NOT NULL,
  `adresse` varchar(75) NOT NULL,
  `tel` varchar(15) NOT NULL,
  `courriel` varchar(50) NOT NULL,
  KEY `fk_idformulaire_amb` (`id_formulaire`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `form_amb`
--

INSERT INTO `form_amb` (`id_formulaire`, `adresse`, `tel`, `courriel`) VALUES
(1, '111', '(819)123-4567', 'Jetson@gmail.com'),
(2, '111', '(111)111-1111', 'Jetson@gmail.com'),
(3, '111', '(111)111-1111', 'Jetson@gmail.com');

-- --------------------------------------------------------

--
-- Structure de la table `form_ass_bag`
--

DROP TABLE IF EXISTS `form_ass_bag`;
CREATE TABLE IF NOT EXISTS `form_ass_bag` (
  `id_formulaire` int(11) NOT NULL,
  `nom` varchar(75) NOT NULL,
  `num` varchar(25) NOT NULL,
  `adresse` varchar(75) NOT NULL,
  `tel` varchar(15) NOT NULL,
  `courriel` varchar(50) NOT NULL,
  KEY `fk_idformulaire_bag` (`id_formulaire`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `form_ass_bag`
--

INSERT INTO `form_ass_bag` (`id_formulaire`, `nom`, `num`, `adresse`, `tel`, `courriel`) VALUES
(1, 'test', '111', '111', '(819)123-4567', 'Jetson@gmail.com'),
(2, 'test', '111', '111', '(111)111-1111', 'Jetson@gmail.com'),
(3, 'test', '111', '111', '(111)111-1111', 'Jetson@gmail.com');

-- --------------------------------------------------------

--
-- Structure de la table `form_ass_mal`
--

DROP TABLE IF EXISTS `form_ass_mal`;
CREATE TABLE IF NOT EXISTS `form_ass_mal` (
  `id_formulaire` int(11) NOT NULL,
  `nom` varchar(75) NOT NULL,
  `num` varchar(25) NOT NULL,
  `adresse` varchar(75) NOT NULL,
  `tel` varchar(15) NOT NULL,
  `telurg` varchar(15) NOT NULL,
  `courriel` varchar(50) NOT NULL,
  KEY `fk_idformulaire_mal` (`id_formulaire`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `form_ass_mal`
--

INSERT INTO `form_ass_mal` (`id_formulaire`, `nom`, `num`, `adresse`, `tel`, `telurg`, `courriel`) VALUES
(1, 'test', '111', '111', '(819)123-4567', '(819)123-4567', 'Jetson@gmail.com'),
(2, 'test', '111', '111', '(111)111-1111', '(111)111-1111', 'Jetson@gmail.com'),
(3, 'test', '111', '111', '(111)111-1111', '(111)111-1111', 'Jetson@gmail.com');

-- --------------------------------------------------------

--
-- Structure de la table `form_coor_etu`
--

DROP TABLE IF EXISTS `form_coor_etu`;
CREATE TABLE IF NOT EXISTS `form_coor_etu` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `id_formulaire` int(11) NOT NULL,
  `dateDu` varchar(50) NOT NULL,
  `dateAu` varchar(50) NOT NULL,
  `adresse` varchar(75) NOT NULL,
  `telephone` varchar(15) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_idformulaire_etu` (`id_formulaire`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `form_coor_etu`
--

INSERT INTO `form_coor_etu` (`ID`, `id_formulaire`, `dateDu`, `dateAu`, `adresse`, `telephone`) VALUES
(1, 1, '5', '16', '111', '(819)123-4567'),
(2, 2, '5', '16', '111', '(111)111-1111'),
(3, 3, '5', '16', '111', '(111)111-1111');

-- --------------------------------------------------------

--
-- Structure de la table `form_coor_pro`
--

DROP TABLE IF EXISTS `form_coor_pro`;
CREATE TABLE IF NOT EXISTS `form_coor_pro` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `id_formulaire` int(11) NOT NULL,
  `dateDu` varchar(50) NOT NULL,
  `dateAu` varchar(50) NOT NULL,
  `adresse` varchar(75) NOT NULL,
  `telephone` varchar(15) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_idformulaire_pro` (`id_formulaire`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `form_coor_pro`
--

INSERT INTO `form_coor_pro` (`ID`, `id_formulaire`, `dateDu`, `dateAu`, `adresse`, `telephone`) VALUES
(1, 1, '5', '16', '111', '(819)123-4567'),
(2, 2, '5', '16', '111', '(111)111-1111'),
(3, 3, '5', '16', '111', '(111)111-1111');

-- --------------------------------------------------------

--
-- Structure de la table `form_coor_res`
--

DROP TABLE IF EXISTS `form_coor_res`;
CREATE TABLE IF NOT EXISTS `form_coor_res` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `id_formulaire` int(11) NOT NULL,
  `dateDu` varchar(50) NOT NULL,
  `dateAu` varchar(50) NOT NULL,
  `adresse` varchar(75) NOT NULL,
  `telephone` varchar(15) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_idformulaire_res` (`id_formulaire`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `form_coor_res`
--

INSERT INTO `form_coor_res` (`ID`, `id_formulaire`, `dateDu`, `dateAu`, `adresse`, `telephone`) VALUES
(1, 1, '', '', '111', '(819)123-4567'),
(2, 2, '', '', '111', '(111)111-1111'),
(3, 3, '', '', '111', '(111)111-1111');

-- --------------------------------------------------------

--
-- Structure de la table `form_eng`
--

DROP TABLE IF EXISTS `form_eng`;
CREATE TABLE IF NOT EXISTS `form_eng` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `id_formulaire` int(11) NOT NULL,
  `reglements` tinyint(1) NOT NULL,
  `politiques` tinyint(1) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_idformulaire_eng` (`id_formulaire`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `form_eng`
--

INSERT INTO `form_eng` (`ID`, `id_formulaire`, `reglements`, `politiques`) VALUES
(1, 1, 1, 1),
(2, 2, 1, 1),
(3, 3, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `form_sante`
--

DROP TABLE IF EXISTS `form_sante`;
CREATE TABLE IF NOT EXISTS `form_sante` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `id_formulaire` int(11) NOT NULL,
  `etat_sante` text NOT NULL,
  `medications` text NOT NULL,
  `allergies` text NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_idformularie_sante` (`id_formulaire`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `form_sante`
--

INSERT INTO `form_sante` (`ID`, `id_formulaire`, `etat_sante`, `medications`, `allergies`) VALUES
(1, 1, 'Aucun', 'Aucune', 'Aucune'),
(2, 2, 'Aucun', 'Aucune', 'Aucune'),
(3, 3, 'Aucun', 'Aucune', 'Aucune');

-- --------------------------------------------------------

--
-- Structure de la table `form_vaccins`
--

DROP TABLE IF EXISTS `form_vaccins`;
CREATE TABLE IF NOT EXISTS `form_vaccins` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `id_formulaire` int(11) NOT NULL,
  `id_vaccin` int(11) NOT NULL,
  `valeur_vaccin` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_idformulaire_vaccin` (`id_formulaire`),
  KEY `fk_idvaccin_vaccin` (`id_vaccin`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `pays`
--

DROP TABLE IF EXISTS `pays`;
CREATE TABLE IF NOT EXISTS `pays` (
  `id_pays` int(11) NOT NULL AUTO_INCREMENT,
  `code_pays` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `fr` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `en` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_pays`)
) ENGINE=InnoDB AUTO_INCREMENT=239 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `pays`
--

INSERT INTO `pays` (`id_pays`, `code_pays`, `fr`, `en`) VALUES
(1, 'AF', 'Afghanistan', 'Afghanistan'),
(2, 'ZA', 'Afrique du Sud', 'South Africa'),
(3, 'AL', 'Albanie', 'Albania'),
(4, 'DZ', 'Algérie', 'Algeria'),
(5, 'DE', 'Allemagne', 'Germany'),
(6, 'AD', 'Andorre', 'Andorra'),
(7, 'AO', 'Angola', 'Angola'),
(8, 'AI', 'Anguilla', 'Anguilla'),
(9, 'AQ', 'Antarctique', 'Antarctica'),
(10, 'AG', 'Antigua-et-Barbuda', 'Antigua & Barbuda'),
(11, 'AN', 'Antilles néerlandaises', 'Netherlands Antilles'),
(12, 'SA', 'Arabie saoudite', 'Saudi Arabia'),
(13, 'AR', 'Argentine', 'Argentina'),
(14, 'AM', 'Arménie', 'Armenia'),
(15, 'AW', 'Aruba', 'Aruba'),
(16, 'AU', 'Australie', 'Australia'),
(17, 'AT', 'Autriche', 'Austria'),
(18, 'AZ', 'Azerbaïdjan', 'Azerbaijan'),
(19, 'BJ', 'Bénin', 'Benin'),
(20, 'BS', 'Bahamas', 'Bahamas, The'),
(21, 'BH', 'Bahreïn', 'Bahrain'),
(22, 'BD', 'Bangladesh', 'Bangladesh'),
(23, 'BB', 'Barbade', 'Barbados'),
(24, 'PW', 'Belau', 'Palau'),
(25, 'BE', 'Belgique', 'Belgium'),
(26, 'BZ', 'Belize', 'Belize'),
(27, 'BM', 'Bermudes', 'Bermuda'),
(28, 'BT', 'Bhoutan', 'Bhutan'),
(29, 'BY', 'Biélorussie', 'Belarus'),
(30, 'MM', 'Birmanie', 'Myanmar (ex-Burma)'),
(31, 'BO', 'Bolivie', 'Bolivia'),
(32, 'BA', 'Bosnie-Herzégovine', 'Bosnia and Herzegovina'),
(33, 'BW', 'Botswana', 'Botswana'),
(34, 'BR', 'Brésil', 'Brazil'),
(35, 'BN', 'Brunei', 'Brunei Darussalam'),
(36, 'BG', 'Bulgarie', 'Bulgaria'),
(37, 'BF', 'Burkina Faso', 'Burkina Faso'),
(38, 'BI', 'Burundi', 'Burundi'),
(39, 'CI', 'Côte d\'Ivoire', 'Ivory Coast (see Cote d\'Ivoire)'),
(40, 'KH', 'Cambodge', 'Cambodia'),
(41, 'CM', 'Cameroun', 'Cameroon'),
(42, 'CA', 'Canada', 'Canada'),
(43, 'CV', 'Cap-Vert', 'Cape Verde'),
(44, 'CL', 'Chili', 'Chile'),
(45, 'CN', 'Chine', 'China'),
(46, 'CY', 'Chypre', 'Cyprus'),
(47, 'CO', 'Colombie', 'Colombia'),
(48, 'KM', 'Comores', 'Comoros'),
(49, 'CG', 'Congo', 'Congo'),
(50, 'KP', 'Corée du Nord', 'Korea, Demo. People\'s Rep. of'),
(51, 'KR', 'Corée du Sud', 'Korea, (South) Republic of'),
(52, 'CR', 'Costa Rica', 'Costa Rica'),
(53, 'HR', 'Croatie', 'Croatia'),
(54, 'CU', 'Cuba', 'Cuba'),
(55, 'DK', 'Danemark', 'Denmark'),
(56, 'DJ', 'Djibouti', 'Djibouti'),
(57, 'DM', 'Dominique', 'Dominica'),
(58, 'EG', 'Égypte', 'Egypt'),
(59, 'AE', 'Émirats arabes unis', 'United Arab Emirates'),
(60, 'EC', 'Équateur', 'Ecuador'),
(61, 'ER', 'Érythrée', 'Eritrea'),
(62, 'ES', 'Espagne', 'Spain'),
(63, 'EE', 'Estonie', 'Estonia'),
(64, 'US', 'États-Unis', 'United States'),
(65, 'ET', 'Éthiopie', 'Ethiopia'),
(66, 'FI', 'Finlande', 'Finland'),
(67, 'FR', 'France', 'France'),
(68, 'GE', 'Géorgie', 'Georgia'),
(69, 'GA', 'Gabon', 'Gabon'),
(70, 'GM', 'Gambie', 'Gambia, the'),
(71, 'GH', 'Ghana', 'Ghana'),
(72, 'GI', 'Gibraltar', 'Gibraltar'),
(73, 'GR', 'Grèce', 'Greece'),
(74, 'GD', 'Grenade', 'Grenada'),
(75, 'GL', 'Groenland', 'Greenland'),
(76, 'GP', 'Guadeloupe', 'Guinea, Equatorial'),
(77, 'GU', 'Guam', 'Guam'),
(78, 'GT', 'Guatemala', 'Guatemala'),
(79, 'GN', 'Guinée', 'Guinea'),
(80, 'GQ', 'Guinée équatoriale', 'Equatorial Guinea'),
(81, 'GW', 'Guinée-Bissao', 'Guinea-Bissau'),
(82, 'GY', 'Guyana', 'Guyana'),
(83, 'GF', 'Guyane française', 'Guiana, French'),
(84, 'HT', 'Haïti', 'Haiti'),
(85, 'HN', 'Honduras', 'Honduras'),
(86, 'HK', 'Hong Kong', 'Hong Kong, (China)'),
(87, 'HU', 'Hongrie', 'Hungary'),
(88, 'BV', 'Ile Bouvet', 'Bouvet Island'),
(89, 'CX', 'Ile Christmas', 'Christmas Island'),
(90, 'NF', 'Ile Norfolk', 'Norfolk Island'),
(91, 'KY', 'Iles Cayman', 'Cayman Islands'),
(92, 'CK', 'Iles Cook', 'Cook Islands'),
(93, 'FO', 'Iles Féroé', 'Faroe Islands'),
(94, 'FK', 'Iles Falkland', 'Falkland Islands (Malvinas)'),
(95, 'FJ', 'Iles Fidji', 'Fiji'),
(96, 'GS', 'Iles Géorgie du Sud et Sandwich du Sud', 'S. Georgia and S. Sandwich Is.'),
(97, 'HM', 'Iles Heard et McDonald', 'Heard and McDonald Islands'),
(98, 'MH', 'Iles Marshall', 'Marshall Islands'),
(99, 'PN', 'Iles Pitcairn', 'Pitcairn Island'),
(100, 'SB', 'Iles Salomon', 'Solomon Islands'),
(101, 'SJ', 'Iles Svalbard et Jan Mayen', 'Svalbard and Jan Mayen Islands'),
(102, 'TC', 'Iles Turks-et-Caicos', 'Turks and Caicos Islands'),
(103, 'VI', 'Iles Vierges américaines', 'Virgin Islands, U.S.'),
(104, 'VG', 'Iles Vierges britanniques', 'Virgin Islands, British'),
(105, 'CC', 'Iles des Cocos (Keeling)', 'Cocos (Keeling) Islands'),
(106, 'UM', 'Iles mineures éloignées des États-Unis', 'US Minor Outlying Islands'),
(107, 'IN', 'Inde', 'India'),
(108, 'ID', 'Indonésie', 'Indonesia'),
(109, 'IR', 'Iran', 'Iran, Islamic Republic of'),
(110, 'IQ', 'Iraq', 'Iraq'),
(111, 'IE', 'Irlande', 'Ireland'),
(112, 'IS', 'Islande', 'Iceland'),
(113, 'IL', 'Israël', 'Israel'),
(114, 'IT', 'Italie', 'Italy'),
(115, 'JM', 'Jamaïque', 'Jamaica'),
(116, 'JP', 'Japon', 'Japan'),
(117, 'JO', 'Jordanie', 'Jordan'),
(118, 'KZ', 'Kazakhstan', 'Kazakhstan'),
(119, 'KE', 'Kenya', 'Kenya'),
(120, 'KG', 'Kirghizistan', 'Kyrgyzstan'),
(121, 'KI', 'Kiribati', 'Kiribati'),
(122, 'KW', 'Koweït', 'Kuwait'),
(123, 'LA', 'Laos', 'Lao People\'s Democratic Republic'),
(124, 'LS', 'Lesotho', 'Lesotho'),
(125, 'LV', 'Lettonie', 'Latvia'),
(126, 'LB', 'Liban', 'Lebanon'),
(127, 'LR', 'Liberia', 'Liberia'),
(128, 'LY', 'Libye', 'Libyan Arab Jamahiriya'),
(129, 'LI', 'Liechtenstein', 'Liechtenstein'),
(130, 'LT', 'Lituanie', 'Lithuania'),
(131, 'LU', 'Luxembourg', 'Luxembourg'),
(132, 'MO', 'Macao', 'Macao, (China)'),
(133, 'MG', 'Madagascar', 'Madagascar'),
(134, 'MY', 'Malaisie', 'Malaysia'),
(135, 'MW', 'Malawi', 'Malawi'),
(136, 'MV', 'Maldives', 'Maldives'),
(137, 'ML', 'Mali', 'Mali'),
(138, 'MT', 'Malte', 'Malta'),
(139, 'MP', 'Mariannes du Nord', 'Northern Mariana Islands'),
(140, 'MA', 'Maroc', 'Morocco'),
(141, 'MQ', 'Martinique', 'Martinique'),
(142, 'MU', 'Maurice', 'Mauritius'),
(143, 'MR', 'Mauritanie', 'Mauritania'),
(144, 'YT', 'Mayotte', 'Mayotte'),
(145, 'MX', 'Mexique', 'Mexico'),
(146, 'FM', 'Micronésie', 'Micronesia, Federated States of'),
(147, 'MD', 'Moldavie', 'Moldova, Republic of'),
(148, 'MC', 'Monaco', 'Monaco'),
(149, 'MN', 'Mongolie', 'Mongolia'),
(150, 'MS', 'Montserrat', 'Montserrat'),
(151, 'MZ', 'Mozambique', 'Mozambique'),
(152, 'NP', 'Népal', 'Nepal'),
(153, 'NA', 'Namibie', 'Namibia'),
(154, 'NR', 'Nauru', 'Nauru'),
(155, 'NI', 'Nicaragua', 'Nicaragua'),
(156, 'NE', 'Niger', 'Niger'),
(157, 'NG', 'Nigeria', 'Nigeria'),
(158, 'NU', 'Nioué', 'Niue'),
(159, 'NO', 'Norvège', 'Norway'),
(160, 'NC', 'Nouvelle-Calédonie', 'New Caledonia'),
(161, 'NZ', 'Nouvelle-Zélande', 'New Zealand'),
(162, 'OM', 'Oman', 'Oman'),
(163, 'UG', 'Ouganda', 'Uganda'),
(164, 'UZ', 'Ouzbékistan', 'Uzbekistan'),
(165, 'PE', 'Pérou', 'Peru'),
(166, 'PK', 'Pakistan', 'Pakistan'),
(167, 'PA', 'Panama', 'Panama'),
(168, 'PG', 'Papouasie-Nouvelle-Guinée', 'Papua New Guinea'),
(169, 'PY', 'Paraguay', 'Paraguay'),
(170, 'NL', 'Pays-Bas', 'Netherlands'),
(171, 'PH', 'Philippines', 'Philippines'),
(172, 'PL', 'Pologne', 'Poland'),
(173, 'PF', 'Polynésie française', 'French Polynesia'),
(174, 'PR', 'Porto Rico', 'Puerto Rico'),
(175, 'PT', 'Portugal', 'Portugal'),
(176, 'QA', 'Qatar', 'Qatar'),
(177, 'CF', 'République centrafricaine', 'Central African Republic'),
(178, 'CD', 'République démocratique du Congo', 'Congo, Democratic Rep. of the'),
(179, 'DO', 'République dominicaine', 'Dominican Republic'),
(180, 'CZ', 'République tchèque', 'Czech Republic'),
(181, 'RE', 'Réunion', 'Reunion'),
(182, 'RO', 'Roumanie', 'Romania'),
(183, 'GB', 'Royaume-Uni', 'Saint Pierre and Miquelon'),
(184, 'RU', 'Russie', 'Russia (Russian Federation)'),
(185, 'RW', 'Rwanda', 'Rwanda'),
(186, 'SN', 'Sénégal', 'Senegal'),
(187, 'EH', 'Sahara occidental', 'Western Sahara'),
(188, 'KN', 'Saint-Christophe-et-Niévès', 'Saint Kitts and Nevis'),
(189, 'SM', 'Saint-Marin', 'San Marino'),
(190, 'PM', 'Saint-Pierre-et-Miquelon', 'Saint Pierre and Miquelon'),
(191, 'VA', 'Saint-Siège ', 'Vatican City State (Holy See)'),
(192, 'VC', 'Saint-Vincent-et-les-Grenadines', 'Saint Vincent and the Grenadines'),
(193, 'SH', 'Sainte-Hélène', 'Saint Helena'),
(194, 'LC', 'Sainte-Lucie', 'Saint Lucia'),
(195, 'SV', 'Salvador', 'El Salvador'),
(196, 'WS', 'Samoa', 'Samoa'),
(197, 'AS', 'Samoa américaines', 'American Samoa'),
(198, 'ST', 'Sao Tomé-et-Principe', 'Sao Tome and Principe'),
(199, 'SC', 'Seychelles', 'Seychelles'),
(200, 'SL', 'Sierra Leone', 'Sierra Leone'),
(201, 'SG', 'Singapour', 'Singapore'),
(202, 'SI', 'Slovénie', 'Slovenia'),
(203, 'SK', 'Slovaquie', 'Slovakia'),
(204, 'SO', 'Somalie', 'Somalia'),
(205, 'SD', 'Soudan', 'Sudan'),
(206, 'LK', 'Sri Lanka', 'Sri Lanka (ex-Ceilan)'),
(207, 'SE', 'Suède', 'Sweden'),
(208, 'CH', 'Suisse', 'Switzerland'),
(209, 'SR', 'Suriname', 'Suriname'),
(210, 'SZ', 'Swaziland', 'Swaziland'),
(211, 'SY', 'Syrie', 'Syrian Arab Republic'),
(212, 'TW', 'Taïwan', 'Taiwan'),
(213, 'TJ', 'Tadjikistan', 'Tajikistan'),
(214, 'TZ', 'Tanzanie', 'Tanzania, United Republic of'),
(215, 'TD', 'Tchad', 'Chad'),
(216, 'TF', 'Terres australes françaises', 'French Southern Territories - TF'),
(217, 'IO', 'Territoire britannique de l\'Océan Indien', 'British Indian Ocean Territory'),
(218, 'TH', 'Thaïlande', 'Thailand'),
(219, 'TL', 'Timor Oriental', 'Timor-Leste (East Timor)'),
(220, 'TG', 'Togo', 'Togo'),
(221, 'TK', 'Tokélaou', 'Tokelau'),
(222, 'TO', 'Tonga', 'Tonga'),
(223, 'TT', 'Trinité-et-Tobago', 'Trinidad & Tobago'),
(224, 'TN', 'Tunisie', 'Tunisia'),
(225, 'TM', 'Turkménistan', 'Turkmenistan'),
(226, 'TR', 'Turquie', 'Turkey'),
(227, 'TV', 'Tuvalu', 'Tuvalu'),
(228, 'UA', 'Ukraine', 'Ukraine'),
(229, 'UY', 'Uruguay', 'Uruguay'),
(230, 'VU', 'Vanuatu', 'Vanuatu'),
(231, 'VE', 'Venezuela', 'Venezuela'),
(232, 'VN', 'ViÃªt Nam', 'Viet Nam'),
(233, 'WF', 'Wallis-et-Futuna', 'Wallis and Futuna'),
(234, 'YE', 'Yémen', 'Yemen'),
(235, 'YU', 'Yougoslavie', 'Saint Pierre and Miquelon'),
(236, 'ZM', 'Zambie', 'Zambia'),
(237, 'ZW', 'Zimbabwe', 'Zimbabwe'),
(238, 'MK', 'ex-République yougoslave de Macédoine', 'Macedonia, TFYR');

-- --------------------------------------------------------

--
-- Structure de la table `projets`
--

DROP TABLE IF EXISTS `projets`;
CREATE TABLE IF NOT EXISTS `projets` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `id_demandes` int(11) NOT NULL,
  `nom_projet` varchar(60) NOT NULL,
  `codeProj` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_id_demande` (`id_demandes`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `projets`
--

INSERT INTO `projets` (`ID`, `id_demandes`, `nom_projet`, `codeProj`) VALUES
(11, 29, 'Afrique Angola 2020', 'Dh74kePz'),
(12, 32, 'visite culturelle', 'IhrYMmoU'),
(13, 33, 'Argentine 2021', 'pZJ3(ypT'),
(14, 31, 'DemandeTest', 'oaXSLVIC/eY'),
(15, 24, 'Barbade 2020', 'jmLdpF'),
(16, 5, 'kkk', '1qrDQE');

-- --------------------------------------------------------

--
-- Structure de la table `proj_vaccins`
--

DROP TABLE IF EXISTS `proj_vaccins`;
CREATE TABLE IF NOT EXISTS `proj_vaccins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proj_id` int(11) NOT NULL,
  `vaccin_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_proj` (`proj_id`),
  KEY `fk_vaccin` (`vaccin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `proj_vaccins`
--

INSERT INTO `proj_vaccins` (`id`, `proj_id`, `vaccin_id`) VALUES
(46, 11, 2),
(47, 11, 3),
(48, 11, 4);

-- --------------------------------------------------------

--
-- Structure de la table `question`
--

DROP TABLE IF EXISTS `question`;
CREATE TABLE IF NOT EXISTS `question` (
  `id_question` int(11) NOT NULL AUTO_INCREMENT,
  `id_formulaire` int(11) NOT NULL,
  `question` varchar(300) NOT NULL,
  `type` varchar(50) NOT NULL,
  `fileUploaded` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_question`),
  KEY `id_formulaire` (`id_formulaire`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `question`
--

INSERT INTO `question` (`id_question`, `id_formulaire`, `question`, `type`, `fileUploaded`) VALUES
(45, 22, 'Bonsoir?', 'ShortText', NULL),
(46, 23, 'ccc', 'Checkbox', NULL),
(47, 24, 'hehe', 'ShortText', NULL),
(48, 25, 'test', 'ShortText', NULL),
(49, 26, 'test', 'ShortText', NULL),
(50, 27, 'test', 'ShortText', NULL),
(51, 28, 'test', 'ShortText', NULL),
(52, 29, 'test', 'ShortText', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `usr_info`
--

DROP TABLE IF EXISTS `usr_info`;
CREATE TABLE IF NOT EXISTS `usr_info` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `nom` varchar(64) NOT NULL,
  `prenom` varchar(64) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_usr_usrInfo` (`userID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `usr_info`
--

INSERT INTO `usr_info` (`ID`, `userID`, `nom`, `prenom`) VALUES
(1, 30, 'Lampron', 'Jeremy');

-- --------------------------------------------------------

--
-- Structure de la table `usr_projet_info`
--

DROP TABLE IF EXISTS `usr_projet_info`;
CREATE TABLE IF NOT EXISTS `usr_projet_info` (
  `userID` int(11) NOT NULL,
  `projetID` int(11) NOT NULL,
  PRIMARY KEY (`userID`,`projetID`),
  KEY `fk_projInfo` (`projetID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `usr_projet_info`
--

INSERT INTO `usr_projet_info` (`userID`, `projetID`) VALUES
(15, 11),
(16, 11),
(31, 11),
(32, 11),
(15, 12),
(15, 15);

-- --------------------------------------------------------

--
-- Structure de la table `utilisateurs`
--

DROP TABLE IF EXISTS `utilisateurs`;
CREATE TABLE IF NOT EXISTS `utilisateurs` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `courriel` varchar(75) NOT NULL,
  `nom` varchar(75) NOT NULL,
  `prenom` varchar(75) NOT NULL,
  `password` varchar(64) NOT NULL,
  `type` varchar(25) NOT NULL,
  `actif` tinyint(1) NOT NULL DEFAULT '1',
  `participant` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `utilisateurs`
--

INSERT INTO `utilisateurs` (`ID`, `courriel`, `nom`, `prenom`, `password`, `type`, `actif`, `participant`) VALUES
(15, 'etu@mail.com', 'etu', 'etu', 'ca3bbd5c1fe06bd6ca97927a6b7b2a7cd1081f42367ffad446e88de80db016a3', 'Etudiant', 1, 0),
(16, 'prof@mail.com', 'prof', 'prof', '51d1e6a398acbda7e15b687de747e7dfe95fa13154dcb40aa8ab37f1e2b393a0', 'Enseignant', 1, 0),
(17, 'admin@mail.com', 'admin', 'admin', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', 'Administrateur', 1, 0),
(21, 'etu1@mail.com', 'etu1', 'etu1', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', 'Etudiant', 1, 0),
(23, 'etu2@mail.com', 'etu2', 'etu2', '7a67517ea57a3f768283426abdfdc1c6e5144ce8c56cde69d38ca048703e75d5', 'Etudiant', 1, 0),
(24, 'nouveau_prof@mail.com', 'nouveau_prof', 'nouveau_prof', '40f02e0d889fdbc489f9099adf4606328635e68ffaf0858f398bc2ed1d7f35f6', 'Enseignant', 1, 0),
(25, 'test_admin@mail.com', 'test_admin', 'test_admin', 'bfdea7828517f603998813c0f6d5720b19c1d73b22c31699937e120d3a2510df', 'Administrateur', 1, 0),
(26, 'daisy_2000@mail.com', 'daisy_2000', 'daisy_2000', 'c775e7b757ede630cd0aa1113bd102661ab38829ca52a6422ab782862f268646', 'Etudiant', 1, 0),
(27, 'julieallemand@mail.com', 'allemand', 'julie', '2bcdb4aa2edb048ac1f441cb3657764d1fa95d573d1a3f47c0560d6a0d87b9f0', 'Enseignant', 1, 0),
(28, 'etu123@mail.com', 'etu123', 'etu123', '612b65360845261803ad809086cff726f5c4dff0e12fc9b157569879f0ab3953', 'Etudiant', 1, 0),
(30, 'jeremy.lampron.01@edu.cegeptr.qc.ca', 'Lampron', 'Jeremy', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', 'Etudiant', 1, 0),
(31, 'jawda@wadwa.com', 'etu6', 'etu6', 'a337cc8958e7d2c1e256364a22ac3d6b9e4404e656a2899aee2abb4f7487d1ef', 'Etudiant', 1, 0),
(32, 'Jetson@gmail.com', 'Jetson@gmail.com', 'Arold', '03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4', 'Etudiant', 1, 0),
(33, 't@mail.com', 't', 't', '4d1b541d24b3be8a8538b045d7e63446d99c6431133b339147ca39a2a2cee9ad', 'Administrateur', 1, 0);

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur_reponse`
--

DROP TABLE IF EXISTS `utilisateur_reponse`;
CREATE TABLE IF NOT EXISTS `utilisateur_reponse` (
  `id_user` int(11) NOT NULL,
  `id_question` int(11) NOT NULL,
  `id_demande` int(11) NOT NULL DEFAULT '0',
  `reponse` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `utilisateur_reponse`
--

INSERT INTO `utilisateur_reponse` (`id_user`, `id_question`, `id_demande`, `reponse`) VALUES
(16, 46, 8, '[\"Choix #1\"]'),
(16, 46, 9, '[\"Choix #1\"]'),
(16, 46, 10, '[\"Choix #1\"]');

-- --------------------------------------------------------

--
-- Structure de la table `vaccins`
--

DROP TABLE IF EXISTS `vaccins`;
CREATE TABLE IF NOT EXISTS `vaccins` (
  `id_vaccin` int(11) NOT NULL AUTO_INCREMENT,
  `vaccin` varchar(50) NOT NULL,
  PRIMARY KEY (`id_vaccin`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `vaccins`
--

INSERT INTO `vaccins` (`id_vaccin`, `vaccin`) VALUES
(2, 'h1n1'),
(3, 'Vaccin contre la rage'),
(4, 'Grippe'),
(5, 'AnyVaccin'),
(6, 'Vaccin 2204');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `demandes`
--
ALTER TABLE `demandes`
  ADD CONSTRAINT `fk_demandes_projdocs` FOREIGN KEY (`projetDocsID`) REFERENCES `demandes_docs` (`ID`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_demandes_user` FOREIGN KEY (`userID`) REFERENCES `utilisateurs` (`ID`) ON UPDATE CASCADE;

--
-- Contraintes pour la table `demandes_accompagnateurs`
--
ALTER TABLE `demandes_accompagnateurs`
  ADD CONSTRAINT `fk_demandes_accompagnateurs` FOREIGN KEY (`id_demande`) REFERENCES `demandes` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `demandes_activites`
--
ALTER TABLE `demandes_activites`
  ADD CONSTRAINT `fk_demande_activite` FOREIGN KEY (`id_demande`) REFERENCES `demandes` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `demandes_vaccins`
--
ALTER TABLE `demandes_vaccins`
  ADD CONSTRAINT `fk_demandes_vaccins` FOREIGN KEY (`id_demande`) REFERENCES `demandes` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_vaccins` FOREIGN KEY (`id_vaccin`) REFERENCES `vaccins` (`id_vaccin`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `formulaires_etudiants`
--
ALTER TABLE `formulaires_etudiants`
  ADD CONSTRAINT `fk_formulaires_etudiants` FOREIGN KEY (`userid`) REFERENCES `usr_projet_info` (`userID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_formulaires_projet` FOREIGN KEY (`projetid`) REFERENCES `usr_projet_info` (`projetID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `form_amb`
--
ALTER TABLE `form_amb`
  ADD CONSTRAINT `fk_idformulaire_amb` FOREIGN KEY (`id_formulaire`) REFERENCES `formulaires_etudiants` (`id_formulaire`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `form_ass_bag`
--
ALTER TABLE `form_ass_bag`
  ADD CONSTRAINT `fk_idformulaire_bag` FOREIGN KEY (`id_formulaire`) REFERENCES `formulaires_etudiants` (`id_formulaire`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `form_ass_mal`
--
ALTER TABLE `form_ass_mal`
  ADD CONSTRAINT `fk_idformulaire_mal` FOREIGN KEY (`id_formulaire`) REFERENCES `formulaires_etudiants` (`id_formulaire`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `form_coor_etu`
--
ALTER TABLE `form_coor_etu`
  ADD CONSTRAINT `fk_idformulaire_etu` FOREIGN KEY (`id_formulaire`) REFERENCES `formulaires_etudiants` (`id_formulaire`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `form_coor_pro`
--
ALTER TABLE `form_coor_pro`
  ADD CONSTRAINT `fk_idformulaire_pro` FOREIGN KEY (`id_formulaire`) REFERENCES `formulaires_etudiants` (`id_formulaire`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `form_coor_res`
--
ALTER TABLE `form_coor_res`
  ADD CONSTRAINT `fk_idformulaire_res` FOREIGN KEY (`id_formulaire`) REFERENCES `formulaires_etudiants` (`id_formulaire`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `form_eng`
--
ALTER TABLE `form_eng`
  ADD CONSTRAINT `fk_idformulaire_eng` FOREIGN KEY (`id_formulaire`) REFERENCES `formulaires_etudiants` (`id_formulaire`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `form_sante`
--
ALTER TABLE `form_sante`
  ADD CONSTRAINT `fk_idformularie_sante` FOREIGN KEY (`id_formulaire`) REFERENCES `formulaires_etudiants` (`id_formulaire`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `form_vaccins`
--
ALTER TABLE `form_vaccins`
  ADD CONSTRAINT `fk_idformulaire_vaccin` FOREIGN KEY (`id_formulaire`) REFERENCES `formulaires_etudiants` (`id_formulaire`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_idvaccin_vaccin` FOREIGN KEY (`id_vaccin`) REFERENCES `vaccins` (`id_vaccin`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `proj_vaccins`
--
ALTER TABLE `proj_vaccins`
  ADD CONSTRAINT `fk_proj` FOREIGN KEY (`proj_id`) REFERENCES `projets` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_vaccin` FOREIGN KEY (`vaccin_id`) REFERENCES `vaccins` (`id_vaccin`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `usr_info`
--
ALTER TABLE `usr_info`
  ADD CONSTRAINT `fk_usr_usrInfo` FOREIGN KEY (`userID`) REFERENCES `utilisateurs` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `usr_projet_info`
--
ALTER TABLE `usr_projet_info`
  ADD CONSTRAINT `fk_projInfo` FOREIGN KEY (`projetID`) REFERENCES `projets` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_usrInfo` FOREIGN KEY (`userID`) REFERENCES `utilisateurs` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
