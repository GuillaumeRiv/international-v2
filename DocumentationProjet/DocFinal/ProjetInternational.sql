-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Mer 11 Décembre 2019 à 03:58
-- Version du serveur :  5.7.28-0ubuntu0.18.04.4
-- Version de PHP :  7.2.24-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `ProjetInternational`
--

-- --------------------------------------------------------

--
-- Structure de la table `bilan_docs`
--

CREATE TABLE `bilan_docs` (
  `ID` int(11) NOT NULL,
  `nom` varchar(100) NOT NULL,
  `path` varchar(100) NOT NULL,
  `text` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `bilan_docs`
--

INSERT INTO `bilan_docs` (`ID`, `nom`, `path`, `text`) VALUES
(1, 'Bilans (1).zip', 'docs', 'Ã€ la fin de votre sÃ©jour, veuillez remplir ce bilan du sÃ©jour et d\'apprÃ©ciation afin de donner votre avis aux organisateurs et aux responsables du voyage.');

-- --------------------------------------------------------

--
-- Structure de la table `demandes`
--

CREATE TABLE `demandes` (
  `ID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `projetDocsID` int(11) DEFAULT NULL,
  `Nom` varchar(50) NOT NULL,
  `Prenom` varchar(50) NOT NULL,
  `adresse` varchar(100) NOT NULL,
  `programme` varchar(150) NOT NULL,
  `destination` varchar(50) NOT NULL,
  `dateD` varchar(15) NOT NULL,
  `dateR` varchar(15) NOT NULL,
  `precisionDestination` varchar(50) NOT NULL,
  `titre` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `etudiants` text NOT NULL,
  `liberation` tinyint(1) NOT NULL,
  `financement` tinyint(1) NOT NULL,
  `recrutement` tinyint(1) NOT NULL,
  `strategies` text NOT NULL,
  `date_creation` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `etat` int(11) NOT NULL,
  `raison_refus` varchar(250) DEFAULT NULL,
  `date_changement_etat` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `demandes`
--

INSERT INTO `demandes` (`ID`, `userID`, `projetDocsID`, `Nom`, `Prenom`, `adresse`, `programme`, `destination`, `dateD`, `dateR`, `precisionDestination`, `titre`, `description`, `etudiants`, `liberation`, `financement`, `recrutement`, `strategies`, `date_creation`, `etat`, `raison_refus`, `date_changement_etat`) VALUES
(24, 16, 8, 'Lampron', 'Jeremy', 'jeremy_lampron@hotmail.com', 'DEC-Bac en informatique', 'Barbade', '2019-12-20', '2020-01-10', 'Barbade Central', 'Barbade 2020', 'Nous allons faire un voyage humanitaire en Barbade.', 'Ã‰tudiants de dec bac en informatique.', 1, 0, 0, 'Aucune stratÃ©gie particuliÃ¨rement', '2019-12-06 08:18:48', 2, 'Une Autre Raison de refus', NULL),
(29, 16, 6, 'Lemieux', 'Alexis', 'AlexisLemieux@hotmail.com', 'Technologie de la mÃ©canique du bÃ¢timent (GÃ©nie du bÃ¢timent)', 'Afrique du Sud', '2020-12-13', '2020-12-30', 'Angola', 'Afrique Angola 2020', 'Nous allons construire des batiments en afrique.', 'Ã‰tudiants en Technologie de la mÃ©canique du bÃ¢timent (GÃ©nie du bÃ¢timent)', 1, 1, 1, '', '2019-12-06 09:42:37', 1, '', NULL),
(30, 16, NULL, 'DemandeTest', 'DemandeTest', 'test123@gmail.com', 'Techniques dâ€™hygiÃ¨ne dentaire', 'Aruba', '2019-12-11', '2019-12-26', 'no', 'DemandeTest', '123', '123', 1, 1, 1, '', '2019-12-11 03:10:55', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `demandes_accompagnateurs`
--

CREATE TABLE `demandes_accompagnateurs` (
  `id_acc` int(11) NOT NULL,
  `id_demande` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `telephone` varchar(15) NOT NULL,
  `courriel` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `demandes_accompagnateurs`
--

INSERT INTO `demandes_accompagnateurs` (`id_acc`, `id_demande`, `nom`, `prenom`, `telephone`, `courriel`) VALUES
(14, 24, 'Doe', 'John', '(123)321-5531', 'johndoe@Cegeptr.qc.ca'),
(15, 24, 'Poirier', 'Michel', '(642)651-6928', 'michelpoirier@hotmail.com'),
(24, 29, 'Duchesne', 'Robert', '(321)681-4712', 'RobertDus@hotmail.com'),
(25, 29, 'Blanchard', 'Yves', '(313)652-7632', 'YvesBlanchard12@mail.com'),
(26, 30, 'Gadoua', 'Roger', '(111)111-1111', 'roger@hotmail.com'),
(27, 30, 'Graton', 'Bob', '(999)999-9999', 'bobgraton@gmail.com');

-- --------------------------------------------------------

--
-- Structure de la table `demandes_activites`
--

CREATE TABLE `demandes_activites` (
  `id_activite` int(11) NOT NULL,
  `id_demande` int(11) NOT NULL,
  `activites` varchar(100) NOT NULL,
  `dates` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `demandes_activites`
--

INSERT INTO `demandes_activites` (`id_activite`, `id_demande`, `activites`, `dates`) VALUES
(17, 24, 'Nettoyage d\'un parc', '25 decembre 2019'),
(18, 24, 'Tour de la ville', '26 decembre 2019'),
(19, 24, 'Aide dans un centre communautaire', '29 decembre 2019'),
(26, 29, 'confection des batiments au sol', '15 decembre 2020'),
(27, 30, 'Activites1', '11/27/2019');

-- --------------------------------------------------------

--
-- Structure de la table `demandes_docs`
--

CREATE TABLE `demandes_docs` (
  `ID` int(11) NOT NULL,
  `nom` varchar(100) NOT NULL,
  `path` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `demandes_docs`
--

INSERT INTO `demandes_docs` (`ID`, `nom`, `path`) VALUES
(1, 'test', '/test'),
(2, 'test2', '/test2'),
(6, 'FichierAfrique2020.zip', 'uploads'),
(8, 'yeet.zip', 'uploads');

-- --------------------------------------------------------

--
-- Structure de la table `demandes_vaccins`
--

CREATE TABLE `demandes_vaccins` (
  `id_vaccin` int(11) NOT NULL,
  `id_demande` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `formulaires_etudiants`
--

CREATE TABLE `formulaires_etudiants` (
  `id_formulaire` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `projetid` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `courriel` varchar(100) NOT NULL,
  `sexe` varchar(10) NOT NULL,
  `datenaissance` varchar(15) NOT NULL,
  `date_creation` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `formulaires_etudiants`
--

INSERT INTO `formulaires_etudiants` (`id_formulaire`, `userid`, `projetid`, `nom`, `prenom`, `courriel`, `sexe`, `datenaissance`, `date_creation`) VALUES
(3, 15, 1, 'awd', 'awd', 'awdW@mail.com', 'Aucun', '2019-01-01', '2019-12-10 21:27:21');

-- --------------------------------------------------------

--
-- Structure de la table `form_amb`
--

CREATE TABLE `form_amb` (
  `id_formulaire` int(11) NOT NULL,
  `adresse` varchar(75) NOT NULL,
  `tel` varchar(15) NOT NULL,
  `courriel` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `form_amb`
--

INSERT INTO `form_amb` (`id_formulaire`, `adresse`, `tel`, `courriel`) VALUES
(3, 'a', '(132)312-2311', 'awd@mail.com');

-- --------------------------------------------------------

--
-- Structure de la table `form_ass_bag`
--

CREATE TABLE `form_ass_bag` (
  `id_formulaire` int(11) NOT NULL,
  `nom` varchar(75) NOT NULL,
  `num` varchar(25) NOT NULL,
  `adresse` varchar(75) NOT NULL,
  `tel` varchar(15) NOT NULL,
  `courriel` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `form_ass_bag`
--

INSERT INTO `form_ass_bag` (`id_formulaire`, `nom`, `num`, `adresse`, `tel`, `courriel`) VALUES
(3, 'ad', 'ada', 'dad', '(132)312-2311', 'aawd@mail.com');

-- --------------------------------------------------------

--
-- Structure de la table `form_ass_mal`
--

CREATE TABLE `form_ass_mal` (
  `id_formulaire` int(11) NOT NULL,
  `nom` varchar(75) NOT NULL,
  `num` varchar(25) NOT NULL,
  `adresse` varchar(75) NOT NULL,
  `tel` varchar(15) NOT NULL,
  `telurg` varchar(15) NOT NULL,
  `courriel` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `form_ass_mal`
--

INSERT INTO `form_ass_mal` (`id_formulaire`, `nom`, `num`, `adresse`, `tel`, `telurg`, `courriel`) VALUES
(3, 'ad', 'ada', 'da', '(132)312-2311', '(132)312-2311', 'awdw@mail.com');

-- --------------------------------------------------------

--
-- Structure de la table `form_coor_etu`
--

CREATE TABLE `form_coor_etu` (
  `ID` int(11) NOT NULL,
  `id_formulaire` int(11) NOT NULL,
  `dateDu` varchar(50) NOT NULL,
  `dateAu` varchar(50) NOT NULL,
  `adresse` varchar(75) NOT NULL,
  `telephone` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `form_coor_etu`
--

INSERT INTO `form_coor_etu` (`ID`, `id_formulaire`, `dateDu`, `dateAu`, `adresse`, `telephone`) VALUES
(3, 3, 'ad', 'ad', 'ad', '(132)312-2311'),
(4, 3, 'te', 'te', 'ewte', '(321)465-4984');

-- --------------------------------------------------------

--
-- Structure de la table `form_coor_pro`
--

CREATE TABLE `form_coor_pro` (
  `ID` int(11) NOT NULL,
  `id_formulaire` int(11) NOT NULL,
  `dateDu` varchar(50) NOT NULL,
  `dateAu` varchar(50) NOT NULL,
  `adresse` varchar(75) NOT NULL,
  `telephone` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `form_coor_pro`
--

INSERT INTO `form_coor_pro` (`ID`, `id_formulaire`, `dateDu`, `dateAu`, `adresse`, `telephone`) VALUES
(3, 3, 'da', 'ad', 'ada', '(132)312-2311');

-- --------------------------------------------------------

--
-- Structure de la table `form_coor_res`
--

CREATE TABLE `form_coor_res` (
  `ID` int(11) NOT NULL,
  `id_formulaire` int(11) NOT NULL,
  `dateDu` varchar(50) NOT NULL,
  `dateAu` varchar(50) NOT NULL,
  `adresse` varchar(75) NOT NULL,
  `telephone` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `form_coor_res`
--

INSERT INTO `form_coor_res` (`ID`, `id_formulaire`, `dateDu`, `dateAu`, `adresse`, `telephone`) VALUES
(3, 3, '45', '89', 'ad', '(132)312-2311');

-- --------------------------------------------------------

--
-- Structure de la table `form_eng`
--

CREATE TABLE `form_eng` (
  `ID` int(11) NOT NULL,
  `id_formulaire` int(11) NOT NULL,
  `reglements` tinyint(1) NOT NULL,
  `politiques` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `form_eng`
--

INSERT INTO `form_eng` (`ID`, `id_formulaire`, `reglements`, `politiques`) VALUES
(3, 3, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `form_sante`
--

CREATE TABLE `form_sante` (
  `ID` int(11) NOT NULL,
  `id_formulaire` int(11) NOT NULL,
  `etat_sante` text NOT NULL,
  `medications` text NOT NULL,
  `allergies` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `form_sante`
--

INSERT INTO `form_sante` (`ID`, `id_formulaire`, `etat_sante`, `medications`, `allergies`) VALUES
(16, 3, 'a', 'Aucune', 'a');

-- --------------------------------------------------------

--
-- Structure de la table `form_vaccins`
--

CREATE TABLE `form_vaccins` (
  `ID` int(11) NOT NULL,
  `id_formulaire` int(11) NOT NULL,
  `id_vaccin` int(11) NOT NULL,
  `valeur_vaccin` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `form_vaccins`
--

INSERT INTO `form_vaccins` (`ID`, `id_formulaire`, `id_vaccin`, `valeur_vaccin`) VALUES
(2, 3, 2, 1),
(3, 3, 3, 1);

-- --------------------------------------------------------

--
-- Structure de la table `pays`
--

CREATE TABLE `pays` (
  `id_pays` int(11) NOT NULL,
  `code_pays` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `fr` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `en` varchar(200) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `pays`
--

INSERT INTO `pays` (`id_pays`, `code_pays`, `fr`, `en`) VALUES
(1, 'AF', 'Afghanistan', 'Afghanistan'),
(2, 'ZA', 'Afrique du Sud', 'South Africa'),
(3, 'AL', 'Albanie', 'Albania'),
(4, 'DZ', 'Algérie', 'Algeria'),
(5, 'DE', 'Allemagne', 'Germany'),
(6, 'AD', 'Andorre', 'Andorra'),
(7, 'AO', 'Angola', 'Angola'),
(8, 'AI', 'Anguilla', 'Anguilla'),
(9, 'AQ', 'Antarctique', 'Antarctica'),
(10, 'AG', 'Antigua-et-Barbuda', 'Antigua & Barbuda'),
(11, 'AN', 'Antilles néerlandaises', 'Netherlands Antilles'),
(12, 'SA', 'Arabie saoudite', 'Saudi Arabia'),
(13, 'AR', 'Argentine', 'Argentina'),
(14, 'AM', 'Arménie', 'Armenia'),
(15, 'AW', 'Aruba', 'Aruba'),
(16, 'AU', 'Australie', 'Australia'),
(17, 'AT', 'Autriche', 'Austria'),
(18, 'AZ', 'Azerbaïdjan', 'Azerbaijan'),
(19, 'BJ', 'Bénin', 'Benin'),
(20, 'BS', 'Bahamas', 'Bahamas, The'),
(21, 'BH', 'Bahreïn', 'Bahrain'),
(22, 'BD', 'Bangladesh', 'Bangladesh'),
(23, 'BB', 'Barbade', 'Barbados'),
(24, 'PW', 'Belau', 'Palau'),
(25, 'BE', 'Belgique', 'Belgium'),
(26, 'BZ', 'Belize', 'Belize'),
(27, 'BM', 'Bermudes', 'Bermuda'),
(28, 'BT', 'Bhoutan', 'Bhutan'),
(29, 'BY', 'Biélorussie', 'Belarus'),
(30, 'MM', 'Birmanie', 'Myanmar (ex-Burma)'),
(31, 'BO', 'Bolivie', 'Bolivia'),
(32, 'BA', 'Bosnie-Herzégovine', 'Bosnia and Herzegovina'),
(33, 'BW', 'Botswana', 'Botswana'),
(34, 'BR', 'Brésil', 'Brazil'),
(35, 'BN', 'Brunei', 'Brunei Darussalam'),
(36, 'BG', 'Bulgarie', 'Bulgaria'),
(37, 'BF', 'Burkina Faso', 'Burkina Faso'),
(38, 'BI', 'Burundi', 'Burundi'),
(39, 'CI', 'Côte d\'Ivoire', 'Ivory Coast (see Cote d\'Ivoire)'),
(40, 'KH', 'Cambodge', 'Cambodia'),
(41, 'CM', 'Cameroun', 'Cameroon'),
(42, 'CA', 'Canada', 'Canada'),
(43, 'CV', 'Cap-Vert', 'Cape Verde'),
(44, 'CL', 'Chili', 'Chile'),
(45, 'CN', 'Chine', 'China'),
(46, 'CY', 'Chypre', 'Cyprus'),
(47, 'CO', 'Colombie', 'Colombia'),
(48, 'KM', 'Comores', 'Comoros'),
(49, 'CG', 'Congo', 'Congo'),
(50, 'KP', 'Corée du Nord', 'Korea, Demo. People\'s Rep. of'),
(51, 'KR', 'Corée du Sud', 'Korea, (South) Republic of'),
(52, 'CR', 'Costa Rica', 'Costa Rica'),
(53, 'HR', 'Croatie', 'Croatia'),
(54, 'CU', 'Cuba', 'Cuba'),
(55, 'DK', 'Danemark', 'Denmark'),
(56, 'DJ', 'Djibouti', 'Djibouti'),
(57, 'DM', 'Dominique', 'Dominica'),
(58, 'EG', 'Égypte', 'Egypt'),
(59, 'AE', 'Émirats arabes unis', 'United Arab Emirates'),
(60, 'EC', 'Équateur', 'Ecuador'),
(61, 'ER', 'Érythrée', 'Eritrea'),
(62, 'ES', 'Espagne', 'Spain'),
(63, 'EE', 'Estonie', 'Estonia'),
(64, 'US', 'États-Unis', 'United States'),
(65, 'ET', 'Éthiopie', 'Ethiopia'),
(66, 'FI', 'Finlande', 'Finland'),
(67, 'FR', 'France', 'France'),
(68, 'GE', 'Géorgie', 'Georgia'),
(69, 'GA', 'Gabon', 'Gabon'),
(70, 'GM', 'Gambie', 'Gambia, the'),
(71, 'GH', 'Ghana', 'Ghana'),
(72, 'GI', 'Gibraltar', 'Gibraltar'),
(73, 'GR', 'Grèce', 'Greece'),
(74, 'GD', 'Grenade', 'Grenada'),
(75, 'GL', 'Groenland', 'Greenland'),
(76, 'GP', 'Guadeloupe', 'Guinea, Equatorial'),
(77, 'GU', 'Guam', 'Guam'),
(78, 'GT', 'Guatemala', 'Guatemala'),
(79, 'GN', 'Guinée', 'Guinea'),
(80, 'GQ', 'Guinée équatoriale', 'Equatorial Guinea'),
(81, 'GW', 'Guinée-Bissao', 'Guinea-Bissau'),
(82, 'GY', 'Guyana', 'Guyana'),
(83, 'GF', 'Guyane française', 'Guiana, French'),
(84, 'HT', 'Haïti', 'Haiti'),
(85, 'HN', 'Honduras', 'Honduras'),
(86, 'HK', 'Hong Kong', 'Hong Kong, (China)'),
(87, 'HU', 'Hongrie', 'Hungary'),
(88, 'BV', 'Ile Bouvet', 'Bouvet Island'),
(89, 'CX', 'Ile Christmas', 'Christmas Island'),
(90, 'NF', 'Ile Norfolk', 'Norfolk Island'),
(91, 'KY', 'Iles Cayman', 'Cayman Islands'),
(92, 'CK', 'Iles Cook', 'Cook Islands'),
(93, 'FO', 'Iles Féroé', 'Faroe Islands'),
(94, 'FK', 'Iles Falkland', 'Falkland Islands (Malvinas)'),
(95, 'FJ', 'Iles Fidji', 'Fiji'),
(96, 'GS', 'Iles Géorgie du Sud et Sandwich du Sud', 'S. Georgia and S. Sandwich Is.'),
(97, 'HM', 'Iles Heard et McDonald', 'Heard and McDonald Islands'),
(98, 'MH', 'Iles Marshall', 'Marshall Islands'),
(99, 'PN', 'Iles Pitcairn', 'Pitcairn Island'),
(100, 'SB', 'Iles Salomon', 'Solomon Islands'),
(101, 'SJ', 'Iles Svalbard et Jan Mayen', 'Svalbard and Jan Mayen Islands'),
(102, 'TC', 'Iles Turks-et-Caicos', 'Turks and Caicos Islands'),
(103, 'VI', 'Iles Vierges américaines', 'Virgin Islands, U.S.'),
(104, 'VG', 'Iles Vierges britanniques', 'Virgin Islands, British'),
(105, 'CC', 'Iles des Cocos (Keeling)', 'Cocos (Keeling) Islands'),
(106, 'UM', 'Iles mineures éloignées des États-Unis', 'US Minor Outlying Islands'),
(107, 'IN', 'Inde', 'India'),
(108, 'ID', 'Indonésie', 'Indonesia'),
(109, 'IR', 'Iran', 'Iran, Islamic Republic of'),
(110, 'IQ', 'Iraq', 'Iraq'),
(111, 'IE', 'Irlande', 'Ireland'),
(112, 'IS', 'Islande', 'Iceland'),
(113, 'IL', 'Israël', 'Israel'),
(114, 'IT', 'Italie', 'Italy'),
(115, 'JM', 'Jamaïque', 'Jamaica'),
(116, 'JP', 'Japon', 'Japan'),
(117, 'JO', 'Jordanie', 'Jordan'),
(118, 'KZ', 'Kazakhstan', 'Kazakhstan'),
(119, 'KE', 'Kenya', 'Kenya'),
(120, 'KG', 'Kirghizistan', 'Kyrgyzstan'),
(121, 'KI', 'Kiribati', 'Kiribati'),
(122, 'KW', 'Koweït', 'Kuwait'),
(123, 'LA', 'Laos', 'Lao People\'s Democratic Republic'),
(124, 'LS', 'Lesotho', 'Lesotho'),
(125, 'LV', 'Lettonie', 'Latvia'),
(126, 'LB', 'Liban', 'Lebanon'),
(127, 'LR', 'Liberia', 'Liberia'),
(128, 'LY', 'Libye', 'Libyan Arab Jamahiriya'),
(129, 'LI', 'Liechtenstein', 'Liechtenstein'),
(130, 'LT', 'Lituanie', 'Lithuania'),
(131, 'LU', 'Luxembourg', 'Luxembourg'),
(132, 'MO', 'Macao', 'Macao, (China)'),
(133, 'MG', 'Madagascar', 'Madagascar'),
(134, 'MY', 'Malaisie', 'Malaysia'),
(135, 'MW', 'Malawi', 'Malawi'),
(136, 'MV', 'Maldives', 'Maldives'),
(137, 'ML', 'Mali', 'Mali'),
(138, 'MT', 'Malte', 'Malta'),
(139, 'MP', 'Mariannes du Nord', 'Northern Mariana Islands'),
(140, 'MA', 'Maroc', 'Morocco'),
(141, 'MQ', 'Martinique', 'Martinique'),
(142, 'MU', 'Maurice', 'Mauritius'),
(143, 'MR', 'Mauritanie', 'Mauritania'),
(144, 'YT', 'Mayotte', 'Mayotte'),
(145, 'MX', 'Mexique', 'Mexico'),
(146, 'FM', 'Micronésie', 'Micronesia, Federated States of'),
(147, 'MD', 'Moldavie', 'Moldova, Republic of'),
(148, 'MC', 'Monaco', 'Monaco'),
(149, 'MN', 'Mongolie', 'Mongolia'),
(150, 'MS', 'Montserrat', 'Montserrat'),
(151, 'MZ', 'Mozambique', 'Mozambique'),
(152, 'NP', 'Népal', 'Nepal'),
(153, 'NA', 'Namibie', 'Namibia'),
(154, 'NR', 'Nauru', 'Nauru'),
(155, 'NI', 'Nicaragua', 'Nicaragua'),
(156, 'NE', 'Niger', 'Niger'),
(157, 'NG', 'Nigeria', 'Nigeria'),
(158, 'NU', 'Nioué', 'Niue'),
(159, 'NO', 'Norvège', 'Norway'),
(160, 'NC', 'Nouvelle-Calédonie', 'New Caledonia'),
(161, 'NZ', 'Nouvelle-Zélande', 'New Zealand'),
(162, 'OM', 'Oman', 'Oman'),
(163, 'UG', 'Ouganda', 'Uganda'),
(164, 'UZ', 'Ouzbékistan', 'Uzbekistan'),
(165, 'PE', 'Pérou', 'Peru'),
(166, 'PK', 'Pakistan', 'Pakistan'),
(167, 'PA', 'Panama', 'Panama'),
(168, 'PG', 'Papouasie-Nouvelle-Guinée', 'Papua New Guinea'),
(169, 'PY', 'Paraguay', 'Paraguay'),
(170, 'NL', 'Pays-Bas', 'Netherlands'),
(171, 'PH', 'Philippines', 'Philippines'),
(172, 'PL', 'Pologne', 'Poland'),
(173, 'PF', 'Polynésie française', 'French Polynesia'),
(174, 'PR', 'Porto Rico', 'Puerto Rico'),
(175, 'PT', 'Portugal', 'Portugal'),
(176, 'QA', 'Qatar', 'Qatar'),
(177, 'CF', 'République centrafricaine', 'Central African Republic'),
(178, 'CD', 'République démocratique du Congo', 'Congo, Democratic Rep. of the'),
(179, 'DO', 'République dominicaine', 'Dominican Republic'),
(180, 'CZ', 'République tchèque', 'Czech Republic'),
(181, 'RE', 'Réunion', 'Reunion'),
(182, 'RO', 'Roumanie', 'Romania'),
(183, 'GB', 'Royaume-Uni', 'Saint Pierre and Miquelon'),
(184, 'RU', 'Russie', 'Russia (Russian Federation)'),
(185, 'RW', 'Rwanda', 'Rwanda'),
(186, 'SN', 'Sénégal', 'Senegal'),
(187, 'EH', 'Sahara occidental', 'Western Sahara'),
(188, 'KN', 'Saint-Christophe-et-Niévès', 'Saint Kitts and Nevis'),
(189, 'SM', 'Saint-Marin', 'San Marino'),
(190, 'PM', 'Saint-Pierre-et-Miquelon', 'Saint Pierre and Miquelon'),
(191, 'VA', 'Saint-Siège ', 'Vatican City State (Holy See)'),
(192, 'VC', 'Saint-Vincent-et-les-Grenadines', 'Saint Vincent and the Grenadines'),
(193, 'SH', 'Sainte-Hélène', 'Saint Helena'),
(194, 'LC', 'Sainte-Lucie', 'Saint Lucia'),
(195, 'SV', 'Salvador', 'El Salvador'),
(196, 'WS', 'Samoa', 'Samoa'),
(197, 'AS', 'Samoa américaines', 'American Samoa'),
(198, 'ST', 'Sao Tomé-et-Principe', 'Sao Tome and Principe'),
(199, 'SC', 'Seychelles', 'Seychelles'),
(200, 'SL', 'Sierra Leone', 'Sierra Leone'),
(201, 'SG', 'Singapour', 'Singapore'),
(202, 'SI', 'Slovénie', 'Slovenia'),
(203, 'SK', 'Slovaquie', 'Slovakia'),
(204, 'SO', 'Somalie', 'Somalia'),
(205, 'SD', 'Soudan', 'Sudan'),
(206, 'LK', 'Sri Lanka', 'Sri Lanka (ex-Ceilan)'),
(207, 'SE', 'Suède', 'Sweden'),
(208, 'CH', 'Suisse', 'Switzerland'),
(209, 'SR', 'Suriname', 'Suriname'),
(210, 'SZ', 'Swaziland', 'Swaziland'),
(211, 'SY', 'Syrie', 'Syrian Arab Republic'),
(212, 'TW', 'Taïwan', 'Taiwan'),
(213, 'TJ', 'Tadjikistan', 'Tajikistan'),
(214, 'TZ', 'Tanzanie', 'Tanzania, United Republic of'),
(215, 'TD', 'Tchad', 'Chad'),
(216, 'TF', 'Terres australes françaises', 'French Southern Territories - TF'),
(217, 'IO', 'Territoire britannique de l\'Océan Indien', 'British Indian Ocean Territory'),
(218, 'TH', 'Thaïlande', 'Thailand'),
(219, 'TL', 'Timor Oriental', 'Timor-Leste (East Timor)'),
(220, 'TG', 'Togo', 'Togo'),
(221, 'TK', 'Tokélaou', 'Tokelau'),
(222, 'TO', 'Tonga', 'Tonga'),
(223, 'TT', 'Trinité-et-Tobago', 'Trinidad & Tobago'),
(224, 'TN', 'Tunisie', 'Tunisia'),
(225, 'TM', 'Turkménistan', 'Turkmenistan'),
(226, 'TR', 'Turquie', 'Turkey'),
(227, 'TV', 'Tuvalu', 'Tuvalu'),
(228, 'UA', 'Ukraine', 'Ukraine'),
(229, 'UY', 'Uruguay', 'Uruguay'),
(230, 'VU', 'Vanuatu', 'Vanuatu'),
(231, 'VE', 'Venezuela', 'Venezuela'),
(232, 'VN', 'ViÃªt Nam', 'Viet Nam'),
(233, 'WF', 'Wallis-et-Futuna', 'Wallis and Futuna'),
(234, 'YE', 'Yémen', 'Yemen'),
(235, 'YU', 'Yougoslavie', 'Saint Pierre and Miquelon'),
(236, 'ZM', 'Zambie', 'Zambia'),
(237, 'ZW', 'Zimbabwe', 'Zimbabwe'),
(238, 'MK', 'ex-République yougoslave de Macédoine', 'Macedonia, TFYR');

-- --------------------------------------------------------

--
-- Structure de la table `projets`
--

CREATE TABLE `projets` (
  `ID` int(11) NOT NULL,
  `nom_projet` varchar(60) NOT NULL,
  `codeProj` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `projets`
--

INSERT INTO `projets` (`ID`, `nom_projet`, `codeProj`) VALUES
(1, 'ProjetTest', 'qwerty123'),
(2, 'test', '12345'),
(3, 'ProjetTest', 'yE6E6cLY'),
(4, 'Antarctique Igloo 2019', '1e#Y0VJJN'),
(5, 'Voyage culturel mykonos 2020', 'yg6z0%k@(x'),
(6, 'ProjetTest', '0fL!F&kOQU90'),
(7, 'Voyage culturel mykonos 2020', '9ATNvmmOy1'),
(8, 'New Projet', '7T3g5DD0BXG'),
(9, 'Antarctique Igloo 2019', '!GUTD/aXnGS'),
(10, 'DemandeTest', 'mVyL3ZsxAu0'),
(11, 'Afrique Angola 2020', 'Dh74k%ePz&');

-- --------------------------------------------------------

--
-- Structure de la table `proj_vaccins`
--

CREATE TABLE `proj_vaccins` (
  `id` int(11) NOT NULL,
  `proj_id` int(11) NOT NULL,
  `vaccin_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `proj_vaccins`
--

INSERT INTO `proj_vaccins` (`id`, `proj_id`, `vaccin_id`) VALUES
(25, 4, 3),
(26, 4, 2),
(27, 5, 3),
(30, 1, 3);

-- --------------------------------------------------------

--
-- Structure de la table `usr_info`
--

CREATE TABLE `usr_info` (
  `ID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `nom` varchar(64) NOT NULL,
  `prenom` varchar(64) NOT NULL,
  `age` int(2) NOT NULL,
  `sexe` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `usr_projet_info`
--

CREATE TABLE `usr_projet_info` (
  `userID` int(11) NOT NULL,
  `projetID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `usr_projet_info`
--

INSERT INTO `usr_projet_info` (`userID`, `projetID`) VALUES
(15, 1),
(16, 1),
(20, 1),
(21, 1),
(16, 2),
(19, 2);

-- --------------------------------------------------------

--
-- Structure de la table `utilisateurs`
--

CREATE TABLE `utilisateurs` (
  `ID` int(11) NOT NULL,
  `username` varchar(64) NOT NULL,
  `password` varchar(64) NOT NULL,
  `type` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `utilisateurs`
--

INSERT INTO `utilisateurs` (`ID`, `username`, `password`, `type`) VALUES
(15, 'etu', 'ca3bbd5c1fe06bd6ca97927a6b7b2a7cd1081f42367ffad446e88de80db016a3', 'E'),
(16, 'prof', '51d1e6a398acbda7e15b687de747e7dfe95fa13154dcb40aa8ab37f1e2b393a0', 'P'),
(17, 'admin', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', 'A'),
(19, '123', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', 'E'),
(20, '222', '9b871512327c09ce91dd649b3f96a63b7408ef267c8cc5710114e629730cb61f', 'E'),
(21, 'etu1', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', 'E'),
(22, 'grobinson', '5f83e69a0953792eb7446b92f9a4653a4c222853d0fe374ee79d26d2ae5f0e6e', 'P');

-- --------------------------------------------------------

--
-- Structure de la table `vaccins`
--

CREATE TABLE `vaccins` (
  `id_vaccin` int(11) NOT NULL,
  `vaccin` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `vaccins`
--

INSERT INTO `vaccins` (`id_vaccin`, `vaccin`) VALUES
(2, 'h1n1'),
(3, 'Vaccin contre la rage'),
(4, 'Grippe');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `bilan_docs`
--
ALTER TABLE `bilan_docs`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `demandes`
--
ALTER TABLE `demandes`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `ID` (`ID`),
  ADD UNIQUE KEY `ID_2` (`ID`),
  ADD UNIQUE KEY `ID_3` (`ID`),
  ADD KEY `fk_demandes_projdocs` (`projetDocsID`),
  ADD KEY `fk_demandes_user` (`userID`);

--
-- Index pour la table `demandes_accompagnateurs`
--
ALTER TABLE `demandes_accompagnateurs`
  ADD PRIMARY KEY (`id_acc`),
  ADD KEY `fk_demandes_accompagnateurs` (`id_demande`);

--
-- Index pour la table `demandes_activites`
--
ALTER TABLE `demandes_activites`
  ADD PRIMARY KEY (`id_activite`),
  ADD KEY `fk_demande_activite` (`id_demande`);

--
-- Index pour la table `demandes_docs`
--
ALTER TABLE `demandes_docs`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `demandes_vaccins`
--
ALTER TABLE `demandes_vaccins`
  ADD KEY `fk_demandes_vaccins` (`id_demande`),
  ADD KEY `fk_vaccins` (`id_vaccin`);

--
-- Index pour la table `formulaires_etudiants`
--
ALTER TABLE `formulaires_etudiants`
  ADD PRIMARY KEY (`id_formulaire`),
  ADD KEY `fk_formulaires_etudiants` (`userid`),
  ADD KEY `fk_formulaires_projet` (`projetid`);

--
-- Index pour la table `form_amb`
--
ALTER TABLE `form_amb`
  ADD KEY `fk_idformulaire_amb` (`id_formulaire`);

--
-- Index pour la table `form_ass_bag`
--
ALTER TABLE `form_ass_bag`
  ADD KEY `fk_idformulaire_bag` (`id_formulaire`);

--
-- Index pour la table `form_ass_mal`
--
ALTER TABLE `form_ass_mal`
  ADD KEY `fk_idformulaire_mal` (`id_formulaire`);

--
-- Index pour la table `form_coor_etu`
--
ALTER TABLE `form_coor_etu`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_idformulaire_etu` (`id_formulaire`);

--
-- Index pour la table `form_coor_pro`
--
ALTER TABLE `form_coor_pro`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_idformulaire_pro` (`id_formulaire`);

--
-- Index pour la table `form_coor_res`
--
ALTER TABLE `form_coor_res`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_idformulaire_res` (`id_formulaire`);

--
-- Index pour la table `form_eng`
--
ALTER TABLE `form_eng`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_idformulaire_eng` (`id_formulaire`);

--
-- Index pour la table `form_sante`
--
ALTER TABLE `form_sante`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_idformularie_sante` (`id_formulaire`);

--
-- Index pour la table `form_vaccins`
--
ALTER TABLE `form_vaccins`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_idformulaire_vaccin` (`id_formulaire`),
  ADD KEY `fk_idvaccin_vaccin` (`id_vaccin`);

--
-- Index pour la table `pays`
--
ALTER TABLE `pays`
  ADD PRIMARY KEY (`id_pays`);

--
-- Index pour la table `projets`
--
ALTER TABLE `projets`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `proj_vaccins`
--
ALTER TABLE `proj_vaccins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_proj` (`proj_id`),
  ADD KEY `fk_vaccin` (`vaccin_id`);

--
-- Index pour la table `usr_info`
--
ALTER TABLE `usr_info`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_usr_usrInfo` (`userID`);

--
-- Index pour la table `usr_projet_info`
--
ALTER TABLE `usr_projet_info`
  ADD PRIMARY KEY (`userID`,`projetID`),
  ADD KEY `fk_projInfo` (`projetID`);

--
-- Index pour la table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Index pour la table `vaccins`
--
ALTER TABLE `vaccins`
  ADD PRIMARY KEY (`id_vaccin`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `bilan_docs`
--
ALTER TABLE `bilan_docs`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `demandes`
--
ALTER TABLE `demandes`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT pour la table `demandes_accompagnateurs`
--
ALTER TABLE `demandes_accompagnateurs`
  MODIFY `id_acc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT pour la table `demandes_activites`
--
ALTER TABLE `demandes_activites`
  MODIFY `id_activite` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT pour la table `demandes_docs`
--
ALTER TABLE `demandes_docs`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT pour la table `formulaires_etudiants`
--
ALTER TABLE `formulaires_etudiants`
  MODIFY `id_formulaire` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `form_coor_etu`
--
ALTER TABLE `form_coor_etu`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `form_coor_pro`
--
ALTER TABLE `form_coor_pro`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `form_coor_res`
--
ALTER TABLE `form_coor_res`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `form_eng`
--
ALTER TABLE `form_eng`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `form_sante`
--
ALTER TABLE `form_sante`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT pour la table `form_vaccins`
--
ALTER TABLE `form_vaccins`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `pays`
--
ALTER TABLE `pays`
  MODIFY `id_pays` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=239;
--
-- AUTO_INCREMENT pour la table `projets`
--
ALTER TABLE `projets`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT pour la table `proj_vaccins`
--
ALTER TABLE `proj_vaccins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT pour la table `usr_info`
--
ALTER TABLE `usr_info`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT pour la table `vaccins`
--
ALTER TABLE `vaccins`
  MODIFY `id_vaccin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `demandes`
--
ALTER TABLE `demandes`
  ADD CONSTRAINT `fk_demandes_projdocs` FOREIGN KEY (`projetDocsID`) REFERENCES `demandes_docs` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_demandes_user` FOREIGN KEY (`userID`) REFERENCES `utilisateurs` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Contraintes pour la table `demandes_accompagnateurs`
--
ALTER TABLE `demandes_accompagnateurs`
  ADD CONSTRAINT `fk_demandes_accompagnateurs` FOREIGN KEY (`id_demande`) REFERENCES `demandes` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `demandes_activites`
--
ALTER TABLE `demandes_activites`
  ADD CONSTRAINT `fk_demande_activite` FOREIGN KEY (`id_demande`) REFERENCES `demandes` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `demandes_vaccins`
--
ALTER TABLE `demandes_vaccins`
  ADD CONSTRAINT `fk_demandes_vaccins` FOREIGN KEY (`id_demande`) REFERENCES `demandes` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_vaccins` FOREIGN KEY (`id_vaccin`) REFERENCES `vaccins` (`id_vaccin`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `formulaires_etudiants`
--
ALTER TABLE `formulaires_etudiants`
  ADD CONSTRAINT `fk_formulaires_etudiants` FOREIGN KEY (`userid`) REFERENCES `usr_projet_info` (`userID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_formulaires_projet` FOREIGN KEY (`projetid`) REFERENCES `usr_projet_info` (`projetID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `form_amb`
--
ALTER TABLE `form_amb`
  ADD CONSTRAINT `fk_idformulaire_amb` FOREIGN KEY (`id_formulaire`) REFERENCES `formulaires_etudiants` (`id_formulaire`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `form_ass_bag`
--
ALTER TABLE `form_ass_bag`
  ADD CONSTRAINT `fk_idformulaire_bag` FOREIGN KEY (`id_formulaire`) REFERENCES `formulaires_etudiants` (`id_formulaire`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `form_ass_mal`
--
ALTER TABLE `form_ass_mal`
  ADD CONSTRAINT `fk_idformulaire_mal` FOREIGN KEY (`id_formulaire`) REFERENCES `formulaires_etudiants` (`id_formulaire`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `form_coor_etu`
--
ALTER TABLE `form_coor_etu`
  ADD CONSTRAINT `fk_idformulaire_etu` FOREIGN KEY (`id_formulaire`) REFERENCES `formulaires_etudiants` (`id_formulaire`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `form_coor_pro`
--
ALTER TABLE `form_coor_pro`
  ADD CONSTRAINT `fk_idformulaire_pro` FOREIGN KEY (`id_formulaire`) REFERENCES `formulaires_etudiants` (`id_formulaire`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `form_coor_res`
--
ALTER TABLE `form_coor_res`
  ADD CONSTRAINT `fk_idformulaire_res` FOREIGN KEY (`id_formulaire`) REFERENCES `formulaires_etudiants` (`id_formulaire`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `form_eng`
--
ALTER TABLE `form_eng`
  ADD CONSTRAINT `fk_idformulaire_eng` FOREIGN KEY (`id_formulaire`) REFERENCES `formulaires_etudiants` (`id_formulaire`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `form_sante`
--
ALTER TABLE `form_sante`
  ADD CONSTRAINT `fk_idformularie_sante` FOREIGN KEY (`id_formulaire`) REFERENCES `formulaires_etudiants` (`id_formulaire`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `form_vaccins`
--
ALTER TABLE `form_vaccins`
  ADD CONSTRAINT `fk_idformulaire_vaccin` FOREIGN KEY (`id_formulaire`) REFERENCES `formulaires_etudiants` (`id_formulaire`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_idvaccin_vaccin` FOREIGN KEY (`id_vaccin`) REFERENCES `vaccins` (`id_vaccin`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `proj_vaccins`
--
ALTER TABLE `proj_vaccins`
  ADD CONSTRAINT `fk_proj` FOREIGN KEY (`proj_id`) REFERENCES `projets` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_vaccin` FOREIGN KEY (`vaccin_id`) REFERENCES `vaccins` (`id_vaccin`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `usr_info`
--
ALTER TABLE `usr_info`
  ADD CONSTRAINT `fk_usr_usrInfo` FOREIGN KEY (`userID`) REFERENCES `utilisateurs` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `usr_projet_info`
--
ALTER TABLE `usr_projet_info`
  ADD CONSTRAINT `fk_projInfo` FOREIGN KEY (`projetID`) REFERENCES `projets` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_usrInfo` FOREIGN KEY (`userID`) REFERENCES `utilisateurs` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
