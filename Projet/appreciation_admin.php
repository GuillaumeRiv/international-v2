<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href= "css/apperciation_admin.css" type="text/css">
</head>
<body>
<button id="btRetour"type="button" class="btn btn-secondary col-md-1"  onclick="window.location.href='main.php'"><i class="fas fa-arrow-left"></i> Retour</button>

<div class="midSectAppreciation col-md-8 text-center">
    <br>
    <h1 class="centerText">Bilan d'appréciation</h1>
    <hr>
    <br>
    <h2 class="headTxt" >Veuillez entrez le texte à afficher à l'étudiant</h2>
    <br>
    <br>
    <textarea name="txtInfo" class="form-control" id="txtInfo" rows="3"></textarea>
      <br>
      <br>
      <h2 class="headTxt" >Veuillez importer le fichier à remettre à l'étudiant</h2>
      <br>
      <br>
      <br>
          <div>
              <input id="fileUpload" class="btn btn-secondary" type="file">
            </div>
            <br>
      <br>
      <br>

            <div>
              <input id="save" class="btn btn-secondary col-md-8" type="button" value="Sauvegarder">
            </div>

      <br>
    <br>
</div>
<div id="overlay"></div>
<script src="js/appreciation_admin.js"></script>
</body>
