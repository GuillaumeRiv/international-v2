<table id="proftable">
  <tr>
    <td class="proftd">
    <a href="?page=formulaire">
      <button class="sectionP">
        <img class="iconSection" src="img/formIcon.png" alt="IconeFormulaire">
        <span>Formulaire</span>
      </button>
      </a>
    </td>
    <td class="proftd">
      <a href="?page=demande">
        <button class="sectionP">
          <img class="iconSection" src="img/demandeIcon.png" alt="IconeFormulaire">
          <span>Demandes</span>
        </button>
      </a>
    </td>
  </tr>
  <tr>
    <td class="proftd">
      <a href="?page=profil">
        <button class="sectionP">
          <img class="iconSection" src="img/profileIcon.png" alt="IconeProfileS">
          <span>Profil</span>
        </button>
      </a>
    </td>
    <td class="proftd">
      <a href="?page=projets">
        <button class="sectionP">
          <img class="iconSection" src="img/projetIcon.png" alt="IconeProfileS">
          <span>Projets</span>
        </button>
      </a>
    </td>
  </tr>

  <tr>
    <td class="proftd">
      <a href="?page=appreciation">
        <button class="sectionP">
          <img class="iconSection" src="img/appreciationIcon.png" alt="IconeAppreciation">
          <span>Bilan du séjour</span>
        </button>
      </a>
    </td>
    <td class="proftd">
      <a href="?page=gestion_users">
        <button class="sectionP">
          <img class="iconSection" src="img/groupIcon.png" alt="IconePréférences">
          <span>Utilisateurs</span>
        </button>
      </a>
    </td>
  </tr>
  <tr>
    <td class="proftd">
      <a href="?page=tableau_de_bord">
        <button class="sectionP">
          <img class="iconTableau" src="img/iconTableau.png" alt="IconeTableau">
          <span>Tableau de bord</span>
        </button>
      </a>
    </td>
  </tr>
</table>
<div id="overlay"></div>
<script type="text/javascript">
if ($(window).width() < 880) {
  $(".sectionP").find("span").css('font-size', '18px');
} else {
  $(".sectionP").find("span").css('font-size', '');
}

if (matchMedia) {
  const mq = window.matchMedia("(min-width: 880px)");
  mq.addListener(WidthChange);
  WidthChange(mq);
}


function WidthChange(mq) {
  if (mq.matches) {
    $(".sectionP").find("span").css('font-size', '');
  } else {
    $(".sectionP").find("span").css('font-size', '18px');
  }

}
</script>
