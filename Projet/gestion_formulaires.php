<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="css/gestion_formulaires.css">
</head>
<body>
  <button id="btRetour"type="button" class="btn btn-secondary col-md-1"  onclick="window.location.href='main.php'"><i id="iRetour"class="fa fa-arrow-left"></i> Retour</button><?php
  if (isset($_SESSION['type']) && ($_SESSION['type'] == "Administrateur")||($_SESSION['type'] == "Enseignant")) {?>
    <div class="midSection">

    <h1 class="text-white">Gestion des formulaires</h1>
    <br><br>
    <input type="button" id="affDemande" class="btAffichage btn btn-secondary" value="Proposition de voyage"></input>
    <input type="button" id="affMobilite" class="btSelectAffichage btn btn-secondary" value="Mobilité étudiante"></input>

    <table id="tblFormulaires" class="table table-striped table-bordered">
      <thead class="thead-dark">
        <th scope="col">#</th>
        <th scope="col">Titre</th>
        <th scope="col">Description</th>
        <th scope="col">Modifiable</th>
        <th scope="col">Actions</th>
      </thead>
        <tbody class="table-light">
      </tbody>
    </table>
    <table id="tblMobilite" class="table table-striped table-bordered">
      <thead class="thead-dark">
        <th scope="col">#</th>
        <th scope="col">Titre</th>
        <th scope="col">Description</th>
        <th scope="col">Modifiable</th>
        <th scope="col">Actions</th>
      </thead>
        <tbody class="table-light">
      </tbody>
    </table>
    <input id="addFormulaire" class="btn btn-primary float-right BTAdd" type="button" value="Ajouter un formulaire" data-toggle="modal" data-target='#confirmModal'><?php
  } else {
    echo "<br><br><h3>Vous n'avez pas les autorisations nécessaires pour accéder à cette page.</h3>";
}?>

    <div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="confirmModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                    <button id="confirmBT" type="button" class="btn btn-danger" data-dismiss="modal">Oui</button>
                </div>
            </div>
        </div>
    </div>
    <div id="overlay"></div>

<div id="overlay"></div>
<script src="js/gestion_formulaires.js" charset="utf-8"></script>
</body>
</html>
