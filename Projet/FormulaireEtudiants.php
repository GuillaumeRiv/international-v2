<!Doctype html>
<html lang="eng">
<head>
  <meta charset="UTF-8">
  <title>Cégep International</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link rel="stylesheet" href="css/FormulaireEtudiants.css">
</head>

<body>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script><?php

  require 'php/BD.inc.php';

  if (session_status() == PHP_SESSION_NONE) {
      session_start();
  }?>

  <button id="btRetour"type="button" class="btn btn-secondary col-md-1"  onclick="window.location.href='main.php?page=main.php'"><i id="iRetour"class="fa fa-arrow-left"></i> Retour</button>

  <div class="container">

    <form method="POST" id="form" class="needs-validation" novalidate>
      <h1>Formulaire d'engagement de l'étudiant</h1>
      <br>
      <div class="grayzone" style="background-color: #E0E0E0;" >
        <div style="margin-left:20px;">
          <h2>Informations de l'étudiant</h2>
        </div>

        <div class="form-group row" style="margin-left:20px; margin-right:20px">
          <div class="col-md-6">
            <label for="tbNom">Nom</label>
            <input id="tbNom" type="text" size="40%" placeholder="Nom" class="form-control" required>
            <div class="invalid-feedback">
              Veuillez entrer un nom en utilisant des lettres ou ( caractères acceptés: (espace) - . , ')
            </div>
          </div>
          <div class="col-md-6">
            <label for="tbPrenom">Prénom</label>
            <input id="tbPrenom" type="text" size="40%" placeholder="Prénom" class="form-control" required>
            <div class="invalid-feedback">
              Veuillez entrer un prénom en utilisant des lettres ou ( caractères acceptés: (espace) - . , ')
            </div>
          </div>

        </div>
        <div class="form-group row" style="margin-left:20px; margin-right:20px">
                  <div class="col-md-12">
                    <label for="tbCourriel">Courriel</label>
                    <input id="tbCourriel" type="text" size="40%" placeholder="Adresse courriel" class="form-control" required>
                    <div class="invalid-feedback">
                      Veuillez entrer une adresse courriel valide
                    </div>
                  </div>

        </div>
        <div class="form-group row" style="margin-left:20px; margin-right:20px">
          <div class="col-md-6">
            <label for="tbSexe">Sexe</label>
            <select id="tbSexe" class="form-control" required>
              <option value="Aucun">Autre</option>
              <option value="Femme">Femme</option>
              <option value="Homme">Homme</option>
            </select>
            </div>
          <div class="col-md-6">
            <label for="tbDateNaissance">Date de naissance</label>
            <input id="tbDateNaissance" type="date" size="40%" max="2099-01-01" class="form-control" required>
            <div class="invalid-feedback">
              Veuillez entrer une date de naissance valide
            </div>
          </div>
            <br>
        </div>
  <br>
</div>
<br>

      <div class="grayzone" style="background-color: #E0E0E0;">
        <div style="margin-left:20px;margin-right:20px;">
          <h2>Engagement et politiques</h2>
          <p>  Je m’engage à respecter ces règlements, sans quoi les responsables de l’activité peuvent m’exclure du
            projet ou forcer mon retour au pays dans les plus brefs délais.</p>
            <input type="button" value="Lire les règlements et les politiques" class="form-control" data-toggle="modal" data-target="#ficheReglements">
            <br>
          </div>

          <div class="modal fade" id="ficheReglements" tabindex="-1" role="dialog" aria-labelledby="Titre" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="Titre">Règlements</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">

                  <p>
                    <h5><b><u>Je m’engage au Québec comme à l’étranger</u></b></h5><br>
                    <b>Pour les séjours de groupe :</b><br>
                    - À assister aux réunions ;<br>
                    - À consacrer le nombre d’heures par semaine que les responsables du projet me demandent;<br>
                    - À participer aux activités de groupe<br>
                    - À participer en même temps que le reste du groupe à toutes les activités (incluant celles de
                    financement), même si elles m’intéressent moins;<br>
                    - À aviser les responsables du projet de tous les éléments susceptibles de compromettre le stage
                    ou ma participation au projet;<br>
                    - À régler mes différends avec les autres, sinon à venir en discuter avec les responsables;<br>
                    - À respecter le fait que les accompagnateurs ont toujours le dernier mot, et ce, pour mon bien;<br>
                    - À partager mes commentaires de manière civilisée et courtoise;<br>
                    <br><b>Pour tous les séjours de mobilité:</b><br>
                    - Je m’engage à participer à la formation pré-départ obligatoire ET à toutes autres formations pour
                    mon projet de mobilité étudiante ;<br>
                    - À recevoir les vaccins nécessaires;<br>
                    - À détenir un passeport valide<br>
                    - À respecter les dates et les montants des divers paiements<br>
                    - À contribuer à l’assurance obligatoire qui sera contractée par le collège pour tous les participants
                    du projet. Le collège veillera à ce que les assurances couvrent les participants pour le type
                    d’activités en rapport au projet de mobilité (exemples : escalade, plongée en apnée ou sousmarine,
                    bungee, kayak, baignade en mer, etc.) ;<br>
                    - À fournir à l’assureur les renseignements véridiques quant à mon état de santé;<br>
                    - À informer le collège de toute situation concernant mon état de santé physique et mentale qui
                    pourrait avoir un impact sur ma sécurité et ma capacité d'intégration dans le milieu hôte (incluant
                    la prise de médicament).<br>
                    - À permettre le partage de photos et de textes à des fins didactiques et professionnelles.<br>
                    <br><h5><b>À l’étranger :</b></h5>
                    <b>Pour tous les séjours</b><br>
                    - À voir à la réalisation des objectifs du voyage;<br>
                    - À ne jamais me promener seul (e), ni aller dans un coin reculé seul (e);<br>
                    - À toujours aviser quelqu’un de mes déplacements;<br>
                    - À maintenir une hygiène corporelle rigoureuse;<br>
                    - À adopter une tenue vestimentaire appropriée;<br>
                    - À respecter les citoyens et leurs croyances;<br>
                    - À respecter les croyances et coutumes des gens du pays d’accueil;<br>
                    - À ne pas acheter ou consommer de drogues ou autres produits illicites;<br>
                    - À ne pas être en état d’ébriété et à cesser de consommer de l’alcool si un des responsables du
                    projet le juge nécessaire, ne serait-ce que par précaution;<br>
                    - À ne pas conduire de véhicule motorisé;<br>
                    - À être en tout temps responsable de mes papiers et bagages et à les avoir bien identifiés;
                    - À ne pas amener d’étrangers dans ma chambre;<br>
                    - À ne pas porter, par prudence, de bijoux qui ont une grande valeur monétaire et sentimentale;<br>
                    - À surveiller et à dissimuler mes objets personnels (un vol affectera tout le groupe) ;<br>
                    - À garder une belle ouverture d’esprit et le sourire!<br>
                    - À rencontrer le responsable de la mobilité étudiante à mon retour pour faire un bilan du projet;<br>
                    <br><b>Pour les séjours de groupe</b><br>
                    - À respecter les consignes et directives des responsables du projet;<br>
                    - À suivre le groupe dans les activités obligatoires indiquées sur l’itinéraire et pour lesquelles je
                    participe au projet de mobilité;<br>
                    - À toujours me déplacer en grands groupes, avec un garçon par groupe si possible;<br>
                    - À avertir les autres si je vois une personne de notre groupe qui est seule, afin que les autres
                    attendent cette personne;<br>
                    - À faire attention au matériel commun;<br>
                    - À comprendre et aider ceux qui sont en choc culturel;<br>
                    - À penser au groupe avant moi, au bien commun avant ma satisfaction individuelle;<br>
                    - À respecter le couvre-feu et l’horaire qu’on aura établi pour ne pas que les responsables, ou que
                    tout le groupe, ne m’attendent;<br>
                    - À permettre l’administration de soins médicaux jugés adéquats lorsqu’urgence il y a et lorsqu’on
                    ne joint pas les parents;<br>
                    <br><b>Pour les séjours dans des familles d’accueil</b><br>
                    - À ne pas inviter des gens sans la permission de mes hôtes;<br>
                    - À faire preuve de politesse, de reconnaissance et de savoir-vivre avec nos hôtes;<br>
                    <br><b>Pour les séjours de solidarité internationale /coopération</b><br>
                    - À avoir en tête que nous allons aider des gens; donnez-vous pleinement;<br>
                    J’ai lu et je comprends les implications de ma participation dans le projet de mobilité étudiante.<br>
                    Je comprends que je ne dois pas apporter dans mes bagages des objets ou des substances interdites
                    (comme des drogues par exemple). Je suis conscient que je ne pourrais passer aux douanes, que mon
                    voyage ne serait pas remboursé et que je devrais retourner chez moi par mes propres moyens
                    J’ai pris connaissance des règles présentées dans ce document et je m’engage à les respecter. Je suis
                    conscient que le non-respect de l’une ou l’autre de ces règles pourrait entraîner des sanctions ou la
                    résiliation du contrat de participation et mon rapatriement au Canada, et ce, à mes frais.<br>

                  </p>

                  <p>
                    <h5><b><u>Politique d’exclusion pour les projets de mobilité étudiante</u></b></h5><br>
                    Si un élève ne se conforme pas aux exigences qui lui sont demandées, ou s’il commet une faute grave,
                    il se verra exclu du projet. Ces exigences sont répertoriées dans le présent document, intitulé « Je
                    m’engage… »<br>
                    <br><b>La procédure d’exclusion va ainsi:</b><br>
                    Un avertissement verbal sera émis à l’individu fautif de la part d’un ou des responsables du projet qui
                    mentionnera explicitement qu’il s’agit d’un premier avis. Aucune sanction n’est prévue, excepté dans le
                    cas d’une faute jugée grave par les responsables du projet.<br>
                    <br>Au deuxième avertissement, l’élève est automatiquement exclu du projet.<br>
                    <br><b>Si l’élève est exclu lors du voyage, la procédure se fera comme suit :</b><br>
                    - Un responsable du projet prend contact avec la personne responsable du service de recrutement,
                    mobilité & partenariats institutionnels et internationaux pour l’avertir de la procédure en cours;<br>
                    - Les parents ou tuteurs de l’élève fautif sont contactés pour les aviser de la décision;<br>
                    - Un ou les responsables accompagnent l’élève fautif jusqu’à l’aéroport qui, dès lors, ne sont plus
                    responsables de celui-ci.<br>
                  </p>

                  <p>
                    <h5><b><u>Politique de remboursement</u></b></h5><br>
                    <b>Je m’engage à :</b><br>
                    <br>À rembourser le Cégep ou mes collègues si j’abandonne ou si je suis exclu (e) du projet pour
                    toutes les sommes qui leur sont dues. La redevance de ce que je dois, étant donné que j’ai
                    participé à des activités de financement de groupe, et ce, au nom du Cégep de Trois-Rivières, est
                    à la discrétion des professeurs organisateurs du stage.<br>
                    <br>Un premier versement correspondant à 250 $ ou à la totalité des frais engagés en date du 15 septembre
                    devra être fait par chacun des étudiants pour leur projet de mobilité étudiante, avant le 30 septembre. Il
                    est entendu que ce montant représente la contribution personnelle de chacun. (La date peut être
                    modifiées selon la nature du projet)<br>
                    <br>Dans le cas où l’étudiant abandonnerait le projet, cette somme sera gardée par le Service
                    recrutement, mobilité & partenariats institutionnels et internationaux pour couvrir les pertes
                    associées au départ de cet étudiant.<br>
                    <br>‐ Pour les frais engagés tels que le transporteur aérien, le lieu d’hébergement, les assurances et
                    tous les autres frais encourus au projet, le collège fera les représentations nécessaires, de
                    concert avec les accompagnateurs impliqués, afin de connaître les possibilités de crédit de la part
                    de ces fournisseurs.<br>
                    <br>‐ Nous émettrons, dans les meilleurs délais, une partie ou la totalité des dépôts, après confirmation
                    des montants qui pourront nous être crédités à la suite du désistement.<br>
                    <br>‐ Il arrive que des rabais de groupes aient été consentis pour l’achat de billets d’avion, forfaits,
                    etc. Si le désistement d’un étudiant, après le mois de septembre, n’occasionne aucuns frais
                    supplémentaires pour le groupe et ne pénalise en aucune façon les autres membres du
                    groupe, l’étudiant sera remboursé à 100 %. Dans le cas contraire, le dépôt servira à couvrir ces
                    dépenses.<br>
                    <br>‐ Veuillez noter que les sommes retenues par le collège seront réparties dans le compte du
                    groupe tout comme les commandites, et ne reviennent d’aucune façon au collège.
                  </p>
                  <input id="cbReglements" type="checkbox" onchange="checkEngagement()" required><span>Je m’engage à respecter ces règlements, sans quoi les responsables de l’activité peuvent m’exclure du
                    projet ou forcer mon retour au pays dans les plus brefs délais.</span>
                    <div class="invalid-feedback">
                      Veuillez cocher la case.
                    </div>
                    <br>
                    <input id="cbPolitiques" type="checkbox" onchange="checkEngagement()" required><span>J’ai lu et j’accepte les termes de la politique d’exclusion et les termes de la politique de remboursement.</span>
                    <div class="invalid-feedback">
                      Veuillez cocher la case.
                    </div>
                    </div>
                    <div class="modal-footer">
                      <input type="button" class="btn btn-secondary" data-dismiss="modal" value="Annuler" onClick="uncheckEngagement()" id="btAnnulerEngagement">
                      <input type="button" class="btn btn-primary" data-dismiss="modal" value="Confirmer" id="btConfirmerEngagement"  disabled>
                    </div>
                  </div>
                </div>
              </div>

            </div>

            <br>

              <div class="grayzone" style="background-color: #E0E0E0;" >

                            <div style="margin-left:20px;margin-right:20px;">
                              <h2>Coordonnées d'urgences de l'étudiant</h2>

                              <table id="tablecooretu" style="width:100%; background-color:white;" class="table">
                                <tbody id="TBODYcooretu">

                                <tr>
                                  <th style="width:25%">Du</th><th style="width:25%">Au</th><th style="width:25%">Adresse</th><th style="width:25%">Telephone</th>
                                </tr>
                                <tr id="TRcooretu">
                                  <td style="width:25%;"> <input id="tbCoorEtuDu1" type="text" style="width:100%;" class="inputtable form-control CoorEtuDu" required><div class="invalid-feedback">Veuillez entrer une date ou supprimer la ligne</div></td>
                                  <td style="width:25%"><input id="tbCoorEtuAu1" type="text" style="width:100%"  class="inputtable form-control CoorEtuAu" required><div class="invalid-feedback">Veuillez entrer une date ou supprimer la ligne</div> </td>
                                  <td style="width:25%"><input id="tbAdresseEtu1" type="text" style="width:100%"  class="inputtable form-control AdresseEtu" required><div class="invalid-feedback">Veuillez entrer une adresse ou supprimer la ligne</div> </td>
                                  <td style="width:25%"><input id="tbTelephoneEtu1" type="text" style="width:100%"  class="inputtable form-control TelephoneEtu" placeholder="(123)456-7890" required><div class="invalid-feedback">Veuillez entrer un numéro de téléphone ou supprimer la ligne</div> </td>

                                </tr>


                                </tbody>

                              </table>

                              <input id="btAjoutCoorEtu" class="form-control col-md-6" type="button" value="+"  style="display: inline-block;" onclick="ajouterCoorEtu()"><input id="btSuppCoorEtu" class="form-control col-md-6" type="button" value="-" style="display: inline-block;"  onclick="supprimerCoorEtu()">

                            </div>
                            <br>
              </div>

              <br>

              <div class="grayzone" style="background-color: #E0E0E0;" >

                            <div style="margin-left:20px;margin-right:20px;">
                              <h2>Coordonnées d'urgences du responsable de l'étudiant</h2>

                              <table id="tablecoorresp" style="width:100%; background-color:white;" class="table">
                                <tbody id="TBODYcoorresp">

                                <tr>
                                  <th style="width:25%">Du</th><th style="width:25%">Au</th><th style="width:25%">Adresse</th><th style="width:25%">Telephone</th>
                                </tr>
                                <tr id="TRcoorresp">
                                  <td style="width:25%;"> <input id="tbCoorRespDu1" type="text" style="width:100%;" class="inputtable form-control CoorRespDu" required><div class="invalid-feedback">Veuillez entrer une date ou supprimer la ligne</div></td>
                                  <td style="width:25%"><input id="tbCoorRespAu1" type="text" style="width:100%"  class="inputtable form-control CoorRespAu" required><div class="invalid-feedback">Veuillez entrer une date ou supprimer la ligne</div> </td>
                                  <td style="width:25%"><input id="tbAdresseResp1" type="text" style="width:100%"  class="inputtable form-control AdresseResp" required><div class="invalid-feedback">Veuillez entrer une adresse ou supprimer la ligne</div> </td>
                                  <td style="width:25%"><input id="tbTelephoneResp1" type="text" style="width:100%"  class="inputtable form-control TelephoneEtu" placeholder="(123)456-7890" required><div class="invalid-feedback">Veuillez entrer un numéro de téléphone ou supprimer la ligne</div> </td>

                                </tr>


                                </tbody>

                              </table>

                              <input id="btAjoutCoorResp" class="form-control col-md-6" type="button" value="+"  style="display: inline-block;" onclick="ajouterCoorResp()"><input id="btSuppCoorResp" class="form-control col-md-6" type="button" value="-" style="display: inline-block;"  onclick="supprimerCoorResp()">

                            </div>
                            <br>
              </div>

              <br>

              <div class="grayzone" style="background-color: #E0E0E0;" >

                            <div style="margin-left:20px;margin-right:20px;">
                              <h2>Coordonnées d'urgences d'un proche de l'étudiant</h2>

                              <table id="tablecoorproche" style="width:100%; background-color:white;" class="table">
                                <tbody id="TBODYcoorproche">

                                <tr>
                                  <th style="width:25%">Du</th><th style="width:25%">Au</th><th style="width:25%">Adresse</th><th style="width:25%">Telephone</th>
                                </tr>
                                <tr id="TRcoorresp">
                                  <td style="width:25%;"> <input id="tbCoorProDu1" type="text" style="width:100%;" class="inputtable form-control CoorProDu" required><div class="invalid-feedback">Veuillez entrer une date ou supprimer la ligne</div></td>
                                  <td style="width:25%"><input id="tbCoorProAu1" type="text" style="width:100%"  class="inputtable form-control CoorProAu" required><div class="invalid-feedback">Veuillez entrer une date ou supprimer la ligne</div> </td>
                                  <td style="width:25%"><input id="tbAdressePro1" type="text" style="width:100%"  class="inputtable form-control AdressePro" required><div class="invalid-feedback">Veuillez entrer une adresse ou supprimer la ligne</div> </td>
                                  <td style="width:25%"><input id="tbTelephonePro1" type="text" style="width:100%"  class="inputtable form-control TelephonePro" placeholder="(123)456-7890" required><div class="invalid-feedback">Veuillez entrer un numéro de téléphone ou supprimer la ligne</div> </td>

                                </tr>


                                </tbody>

                              </table>

                              <input id="btAjoutCoorProche" class="form-control col-md-6" type="button" value="+"  style="display: inline-block;" onclick="ajouterCoorProche()"><input id="btSuppCoorProche" class="form-control col-md-6" type="button" value="-" style="display: inline-block;"  onclick="supprimerCoorProche()">

                            </div>
                            <br>
              </div>
              <br>

              <div class="grayzone" style="background-color: #E0E0E0;" >

                            <div style="margin-left:20px;margin-right:20px;">
                              <h2>Coordonnées de la compagnie d'assurance maladie et rapatriement</h2>
                              <label>Compagnie d'assurances</label>
                              <input id="compagnieassmal" type="text" placeholder="Nom de la compagnie" class="form-control" required>
                              <div class="invalid-feedback">
                                Veuillez entrer un nom de compagnie d'assurance valide
                              </div>
                              <br>
                              <label>Numéro de la police d'assurance</label>
                              <input id="numassmal" type="text" placeholder="Numéro de la police" class="form-control" required>
                              <div class="invalid-feedback">
                                Veuillez entrer un numéro de police d'assurance valide
                              </div>
                              <br>
                              <label>Adresse</label>
                              <input id="adressecompagniemal" type="text" placeholder="Adresse" class="form-control" required>
                              <div class="invalid-feedback">
                                Veuillez entrer une adresse valide
                              </div>
                              <br>
                              <label>Téléphone</label>
                              <input id="telcompagniemal" type="text" placeholder="(123)456-7890" class="form-control" required>
                              <div class="invalid-feedback">
                                Veuillez entrer un numéro de téléphone dans le format (123)456-7890
                              </div>
                              <br>
                              <label>Téléphone en cas d'urgence</label>
                              <input id="telurgcompagniemal" type="text" placeholder="(123)456-7890" class="form-control" required>
                              <div class="invalid-feedback">
                                Veuillez entrer un numéro de téléphone dans le format (123)456-7890
                              </div>
                              <br>
                              <label>Courriel</label>
                              <input id="courrielcompagniemal" type="text" placeholder="exemple@courriel.ca" class="form-control" required>
                              <div class="invalid-feedback">
                                Veuillez entrer une adresse courriel
                              </div>

                              <b>- Couverture d’un minimum de 1 000 000$ pour<br>
                                    - hospitalisation;<br>
                                    - rapatriement d’urgence;<br>
                                    - rapatriement en cas de décès.<br>
                                    N.B. 1: Certains pays exigent aussi le carnet de santé et une preuve d’immunisation à<br>
                                    la grippe A H1N1.<br>
                                    N.B. 2 : Dans certains pays, il est nécessaire d’avoir un visa de touriste. Informez-vous
                                    auprès du ministère des affaires étrangères du Canada
                                    (<a href="http://www.international.gc.ca">http://www.international.gc.ca/</a>)</b>
                            </div>
                            <br>
              </div>
              <br>
              <div class="grayzone" style="background-color: #E0E0E0;" >

                            <div style="margin-left:20px;margin-right:20px;">
                              <h2>Coordonnées de la compagnie d'assurances bagages</h2>
                              <label>Compagnie d'assurances</label>
                              <input id="compagnieassbag" type="text" placeholder="Nom de la compagnie" class="form-control" required>
                              <div class="invalid-feedback">
                                Veuillez entrer un nom de compagnie d'assurance valide
                              </div>
                              <br>
                              <label>Numéro de la police d'assurance</label>
                              <input id="numassbag" type="text" placeholder="Numéro de la police" class="form-control" required>
                              <div class="invalid-feedback">
                                Veuillez entrer un numéro de police d'assurance valide
                              </div>
                              <br>
                              <label>Adresse</label>
                              <input id="adressecompagniebag" type="text" placeholder="Adresse" class="form-control" required>
                              <div class="invalid-feedback">
                                Veuillez entrer une adresse valide
                              </div>
                              <br>
                              <label>Téléphone</label>
                              <input id="telcompagniebag" type="text" placeholder="(123)456-7890" class="form-control" required>
                              <div class="invalid-feedback">
                                Veuillez entrer un numéro de téléphone dans le format (123)456-7890
                              </div>
                              <br>
                              <label>Courriel</label>
                              <input id="courrielcompagniebag" type="text" placeholder="exemple@courriel.ca" class="form-control" required>
                              <div class="invalid-feedback">
                                Veuillez entrer une adresse courriel
                              </div>
                            </div>
                            <br>
              </div>
              <br>
              <div class="grayzone" style="background-color: #E0E0E0;" >

                            <div style="margin-left:20px;margin-right:20px;">
                              <h2>Coordonnées de l'ambassade du Canada</h2>
                              <label>Adresse</label>
                              <input id="adresseamb" type="text" placeholder="Adresse" class="form-control" required>
                              <div class="invalid-feedback">
                                Veuillez entrer une adresse valide
                              </div>
                              <br>
                              <label>Téléphone</label>
                              <input id="telamb" type="text" placeholder="(123)456-7890" class="form-control" required>
                              <div class="invalid-feedback">
                                Veuillez entrer un numéro de téléphone dans le format (123)456-7890
                              </div>
                              <br>
                              <label>Courriel</label>
                              <input id="courrielamb" type="text" placeholder="exemple@courriel.ca" class="form-control" required>
                              <div class="invalid-feedback">
                                Veuillez entrer une adresse courriel valide
                              </div>
                            </div>
                            <br>
              </div>
              <br>
              <div class="grayzone" style="background-color: #E0E0E0;">
                <div style="margin-left:20px;margin-right:20px;">
                <h2>Informations sur la santé de l'étudiant</h2>
                <p>*Les champs État de santé, Médication et Allergies ne sont pas obligatoires.</p>
                <div class="col-md-12">
                        <label for="taSante">État de santé</label>
                        <textarea id="taSante" rows="" cols="" style="width:100%; height:100px" class="form-control"></textarea>
                            <br>
              </div>
              <div class="col-md-12">
                      <label for="taMedication">Médications</label>
                      <textarea id="taMedications" rows="" cols="" style="width:100%; height:100px" class="form-control"></textarea>
                          <br>
            </div>
            <div class="col-md-12">
                    <label for="taAllergies">Allergies</label>
                    <textarea id="taAllergies" rows="" cols="" style="width:100%; height:100px" class="form-control"></textarea>
                        <br>
          </div>
          <div class="col-md-12">
                  <label>Vaccins</label><br><?php
                  $result = array();

                  $sql = "SELECT * FROM proj_vaccins WHERE proj_id = ".$_SESSION['idprojet'].";";
                  $projVaccins = $conn->query($sql);

                  while ($vaccin = $projVaccins->fetch()){
                    $sql = "SELECT * FROM vaccins where id_vaccin = ". $vaccin['vaccin_id'] .";";
                    $temp = $conn->query($sql);
                    array_push($result, ...$temp);
                  }

                  /*$sql = "SELECT * FROM vaccins;";
                  $result = $conn->query($sql);*/

                  if(count($result) > 0){?><table style="width:100%; margin-left:7%"><?php
                    $ctr=0;
                    foreach ($result as $row) {
                      $ctr++;
                      echo "<tr><td>".$row['vaccin']."</td>";
                      echo "<td><input type='radio' id='vaccin".$ctr."Oui' name='".$row['id_vaccin']."' value='1' required>Oui</td>";
                      echo "<td><input type='radio' id='vaccin".$ctr."Non' name='".$row['id_vaccin']."' value='0' checked required>Non</td>";
                      echo"</tr>";

                    }?></table><?php
                  }?>
                  <input type="text" id="nbrvaccin" value="<?php echo $ctr; ?>" hidden>
                      <br>
        </div>
          </div>
          </div>
              <br>

              <input type="submit" id="submit" class="btn-primary form-control" disabled>

            </form>

            <script>
            // Example starter JavaScript for disabling form submissions if there are invalid fields
            (function() {
              'use strict';
              window.addEventListener('load', function() {
                // Fetch all the forms we want to apply custom Bootstrap validation styles to
                var forms = document.getElementsByClassName('needs-validation');
                // Loop over them and prevent submission
                var validation = Array.prototype.filter.call(forms, function(form) {
                  form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                      event.preventDefault();
                      event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                  }, false);
                });
              }, false);
            })();
            </script>

            <script type="text/javascript">
            $('#inputGroupFile01').change(function(){
              $('#labelGroupFile01').text($(this)[0].files[0].name);
            });
            </script>


          <script src="js/FormulaireEtudiants.js"></script><?php
          $conn=null;
          if((($_SESSION["type"] == 'Administrateur')||($_SESSION["type"] == 'Enseignant')&&$_GET['page']=="viewformulaire")){?>
            <script type="text/javascript">

              setTimeout(function(){
                $("#tbNom").val(localStorage.getItem('formNom'));
                $("#tbPrenom").val(localStorage.getItem('formPrenom'));
                $("#tbCourriel").val(localStorage.getItem('formCourriel'));
                $("#tbSexe").val(localStorage.getItem('formSexe'));
                $("#tbDateNaissance").val(localStorage.getItem('formDateN'));
                var formEtuDu = localStorage.getItem('formEtuDu').split(',');
                var formEtuAu = localStorage.getItem('formEtuAu').split(',');
                var formEtuAd = localStorage.getItem("formEtuAd").split(',');
                var formEtuTel = localStorage.getItem("formEtuTel").split(',');
                var formResDu = localStorage.getItem("formResDu").split(',');
                var formResAu = localStorage.getItem("formResAu").split(',');
                var formResAd = localStorage.getItem("formResAd").split(',');
                var formResTel = localStorage.getItem("formResTel").split(',');
                var formProDu = localStorage.getItem("formProDu").split(',');
                var formProAu = localStorage.getItem("formProAu").split(',');
                var formProAd = localStorage.getItem("formProAd").split(',');
                var formProTel = localStorage.getItem("formProTel").split(',');


                if(formEtuDu.length > 1){
                  for(var i = 1; i <= formEtuDu.length; i++){
                    if (i != 1){
                      ajouterCoorEtu();
                    }
                    $("#tbCoorEtuDu"+i).val(formEtuDu[i-1]);
                    $("#tbCoorEtuAu"+i).val(formEtuAu[i-1]);
                    $("#tbAdresseEtu"+i).val(formEtuAd[i-1]);
                    $("#tbTelephoneEtu"+i).val(formEtuTel[i-1]);
                  }
                } else {
                  $("#tbCoorEtuDu1").val(formEtuDu[0]);
                  $("#tbCoorEtuAu1").val(formEtuAu[0]);
                  $("#tbAdresseEtu1").val(formEtuAd[0]);
                  $("#tbTelephoneEtu1").val(formEtuTel[0]);
                }

                if(formResDu.length > 1){
                  for(var i = 1; i <= formResDu.length; i++){
                    if (i != 1){
                      ajouterCoorEtu();
                    }
                    $("#tbCoorRespDu"+i).val(formResDu[i-1]);
                    $("#tbCoorRespAu"+i).val(formResAu[i-1]);
                    $("#tbAdresseResp"+i).val(formResAd[i-1]);
                    $("#tbTelephoneResp"+i).val(formResTel[i-1]);
                  }
                } else {
                  $("#tbCoorRespDu1").val(formResDu[0]);
                  $("#tbCoorRespAu1").val(formResAu[0]);
                  $("#tbAdresseResp1").val(formResAd[0]);
                  $("#tbTelephoneResp1").val(formResTel[0]);
                }


                if(formProDu.length > 1){
                  for(var i = 1; i <= formProDu.length; i++){
                    if (i != 1){
                      ajouterCoorEtu();
                    }
                    $("#tbCoorProDu"+i).val(formProDu[i-1]);
                    $("#tbCoorProAu"+i).val(formProAu[i-1]);
                    $("#tbAdressePro"+i).val(formProAd[i-1]);
                    $("#tbTelephonePro"+i).val(formProTel[i-1]);
                  }
                } else {
                  $("#tbCoorProDu1").val(formProDu[0]);
                  $("#tbCoorProAu1").val(formProAu[0]);
                  $("#tbAdressePro1").val(formProAd[0]);
                  $("#tbTelephonePro1").val(formProTel[0]);
                }


                $("#compagnieassmal").val(localStorage.getItem('formAssMalNom'));
                $("#numassmal").val(localStorage.getItem('formAssMalNum'));
                $("#adressecompagniemal").val(localStorage.getItem('formAssMalAd'));
                $("#telcompagniemal").val(localStorage.getItem('formAssMalTel'));
                $("#telurgcompagniemal").val(localStorage.getItem('formAssMalTelU'));
                $("#courrielcompagniemal").val(localStorage.getItem('formAssMalCourriel'));

                $("#compagnieassbag").val(localStorage.getItem('formAssBagNom'));
                $("#numassbag").val(localStorage.getItem('formAssBagNum'));
                $("#adressecompagniebag").val(localStorage.getItem('formAssBagAd'));
                $("#telcompagniebag").val(localStorage.getItem('formAssBagTel'));
                $("#courrielcompagniebag").val(localStorage.getItem('formAssBagCourriel'));

                $("#adresseamb").val(localStorage.getItem('formAmbAd'));
                $("#telamb").val(localStorage.getItem('formAmbTel'));
                $("#courrielamb").val(localStorage.getItem('formAmbCourriel'));

                $("#taSante").val(localStorage.getItem('formSanteEtat'));
                $("#taMedications").val(localStorage.getItem('formSanteMed'));
                $("#taAllergies").val(localStorage.getItem('formSanteAl'));


                var formVaccinsId = localStorage.getItem("formVaccinsId").split(',');
                var formVaccinsVal = localStorage.getItem("formVaccinsVal").split(',');

                if(formVaccinsId[0] != ""){
                for(var i = 0; i < formVaccinsId.length; i++){
                  if ($("input[name="+formVaccinsId[i]+"]").length){
                    $("input[name="+formVaccinsId[i]+"][value="+formVaccinsVal[i]+"]").prop('checked', true);
                  }
                }
                }

                  $("#submit").hide();
                  $("input, select, textarea").prop('disabled', true);
                  $("#btRetour").prop('disabled', false);
              }, 1000);

            </script>
<?php } ?>
        </body>


        </html>
