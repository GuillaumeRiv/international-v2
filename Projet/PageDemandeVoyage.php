<!Doctype html>
<html lang="eng">
<head>
  <meta charset="UTF-8">
  <title>Cégep International</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link rel="stylesheet" href="./css/PageDemandeVoyage.css">
</head>

<body>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>


<?php
require 'php/BD.inc.php';
?>
  <button id="btRetour"type="button" class="btn btn-secondary col-md-1"  onclick="window.location.href='main.php?page=demande'"><i class="fas fa-arrow-left"></i> Retour</button>
  <div class="container">
    <h1 class="lbh1">Proposer un projet de mobilité étudiante</h1>
    <form method="POST" id="form" class="needs-validation">

      <div class="grayzone" style="background-color: #E0E0E0;" >
        <div style="margin-left:20px;">
          <h2>Informations sur le projet</h2>
        </div>

        <div class="form-group row" style="margin-left:20px; margin-right:20px">
          <div class="col-md-6">
            <label for="tbNom">Nom</label>
            <input id="tbNom" type="text" size="40%" placeholder="Nom" class="form-control" required>
            <div class="invalid-feedback">
              Veuillez entrer un nom en utilisant des lettres ou ( caractères acceptés: (espace) - . , ')
            </div>
          </div>
          <div class="col-md-6">
            <label for="tbPrenom">Prénom</label>
            <input id="tbPrenom" type="text" size="40%" placeholder="Prénom" class="form-control" required>
            <div class="invalid-feedback">
              Veuillez entrer un prénom en utilisant des lettres ou ( caractères acceptés: (espace) - . , ')
            </div>
          </div>

        </div>

        <div class="form-group row" style="margin-left:20px; margin-right:20px">
          <div class="col-md-6">
            <label for="tbAdresse">Adresse Courriel</label>
            <input id="tbAdresse" type="text"  placeholder="xyz@courriel.ca" class="form-control" required>
            <div class="invalid-feedback">
              Veuillez entrer une adresse courriel valide
            </div>

          </div>
          <div class="col-md-6">

            <label for="lsProgramme">Programme d'étude</label>
            <input list="listeProgramme" id="lsProgramme" class="form-control" onload="loadProgrammes()" required>
            <datalist id="listeProgramme">
            </datalist>
            <div class="invalid-feedback">
              Veuillez choisir un programme d'étude
            </div>
          </div>

        </div>

        <div class="form-group row" style="margin-left:20px; margin-right:20px">

          <div class="col-md-6">
            <label for="lsDestination">Destination du voyage</label>
            <input list="listeDestination" id="lsDestination" class="form-control" onload="loadDestination()" required>
            <datalist id="listeDestination">  </datalist>
            <div class="invalid-feedback">
              Veuillez choisir un pays de destination
            </div>
          </div>

          <div class="col-md-3">
            <div style="display:inline-block">
              <label for="tbDateDepart">Date de départ</label>
              <input id="tbDateDepart" type="date" class="form-control" min='2016-05-07'  required>
              <div class="invalid-feedback">
                Veuillez choisir une date de départ
              </div>
            </div>
          </div>

          <div class="col-md-3">
            <div style="display: inline-block; margin-left:18px">
              <label for="tbDateRetour"> Date de retour</label>
              <input id="tbDateRetour" type="date" class="form-control" min='2016-05-08' required>
              <div class="invalid-feedback">
                Veuillez choisir une date de retour
              </div>
            </div>
          </div>
        </div>

        <div class="form-group row" style="margin-left:20px; margin-right:20px">
                      <div class="col-md-6">

                          <label for="tbDestinationPrecision"> Précision sur la destination du voyage</label>
                          <input id="tbDestinationPrecision" type="text" class="form-control" required >
                          <div class="invalid-feedback">
                            Veuillez préciser la destination du voyage en utilisant des lettres ou ( caractères acceptés: (espace) - . , ')
                          </div>
                      </div>

                      <div class="col-md-6">

                                  <label for="tbTitreProjet"> Titre du projet</label>
                                  <input id="tbTitreProjet" type="text" class="form-control" required >
                                  <div class="invalid-feedback">
                                    Veuillez préciser le titre du projet en utilisant des lettres ou ( caractères acceptés: (espace) - . , ')
                                  </div>
                              </div>

                              <div class="col-md-6">

                                  <label for="tbTypeProjet" > Type de projet</label>
                                  <input id="tbTypeProjet" type="text" class="form-control" required >
                                  <div class="invalid-feedback">
                                    Veuillez préciser le type du projet en utilisant des lettres ou ( caractères acceptés: (espace) - . , ')
                                  </div>
                              </div>

                              

                           


                </div>

                    <div style="margin:20px;" class="col-md-12">
                    <p class=gras>
                      Pour les programmes internationalisés :
                    </p>
                    <p class=gras>
                      - Dans la description du projet, mentionner les particularités ou ajouts aux projets antérieurs.
                    </p>
                    <p class=gras>
                      Pour les projets culturels et parascolaires :
                    </p>
                    <p class=gras>
                      - Dans la description du projet, mentionner les activités à réaliser.
                    </p>
                  </div>
            <div style="margin:20px;" class="col-md-12">
                    <label for="taDescription">Description du projet</label>
                    <textarea id="taDescription" rows="" cols="" style="width:96%; height:100px" class="form-control" required></textarea>
                    <div class="invalid-feedback">
                      Veuillez entrer une brève description du projet
                    </div>


                    <div >
                      <label for="taEtudiants" class=gras> À quels étudiants s'adresse ce projet?</label>
                      <textarea id="taEtudiants" rows="" cols="" style="width:96%" class="form-control" required></textarea>
                      <div class="invalid-feedback">
                        Veuillez entrer à quels étudiants s'adresse ce projet
                      </div>

                    </div>
        </div>

        <br>
      </div>
      <br>
      <div class="grayzone" style="background-color: #E0E0E0;">
        <div style="margin-left:20px; margin-right:20px;">
          <h2>Activités</h2>
          <br>
          <table id="tableActivite" style="width:100%; background-color:white;" class="table">
            <tbody id="TBODYactivite">

            <tr>
              <th id ="ligneActivite"style="width:75%">Activités à l'étranger</th><th style="width:25%">Dates ou périodes</th>
            </tr>
            <tr id="TRActivite">
              <td style="width:75%;"> <input id="tbActivite1" type="text" style="width:100%;" class="inputtable form-control activite" required><div class="invalid-feedback">Veuillez entrer une activité ou supprimer la ligne</div></td><td style="width:25%"><input id="tbDateActivite1" type="text" style="width:100%"  class="inputtable form-control actdate" required><div class="invalid-feedback">Veuillez entrer une date ou supprimer la ligne</div> </td>
            </tr>


            </tbody>

          </table>

          <input id="btAjoutActivite" class="form-control col-md-9" type="button" value="+"  style="display: inline-block;" onclick="ajouterLigneActivite()"><input id="btSupprimerActivite" class="form-control col-md-3" type="button" value="-" style="display: inline-block;"  onclick="supprimerLigneActivite()">
          <br>
          <br>


        </div>
      </div>


      <br>

      <div  class="form-group grayzone" style="border: 1px solid gray;">
        <div style="margin:20px">
          <p> Si vous avez des questions, n’hésitez pas à contacter la chargée de projets- Mobilité par téléphone
            (poste 2136) ou par courriel à mobilite.etudiante@cegeptr.qc.ca .
          </p>

          <p>*Pour les programmes/ départements /services qui déposent dans le cadre du Programme de
            mobilité étudiante, la chargée de projets - Mobilité prendra contact avec vous, à la réception des
            documents, afin de vous accompagner dans la rédaction du ME-2.<br>
            *Pour les programmes/ départements /services qui déposent dans le cadre d’un projet parascolaire
            et/ou culturel, la direction des Études donnera suite à votre intention de projet en regard de
            l’approbation ou non de celui-ci.
          </p>
        </div>
      </div>
      <br>


      <div class="Sousform">
        <br>

        <div class="grayzone" style="background-color: #E0E0E0;">
          <div style="margin-right:20px; margin-left:20px;">
            <h2>Financement du projet</h2>
            <br>
            <div>
              <label class=gras>Demandez-vous une libération pour ce projet?</label><br>
              <input id="radioLiberationOui" type="radio" class="form-control radiobut" name="liberation" value="1" required>Oui
              <input id="radioLiberationNon" type="radio" class="form-control radiobut" name="liberation" value="0" required>Non
              <div class="invalid-feedback">
                Veuillez cocher un des deux choix
              </div>
              <br>
                <p>Les libérations seront accordées selon les disponibilités budgétaires.</p>
            </div>

            <div>

              <label class=gras>Dans le cas où le projet était appuyé, mais non financé, faute de budgets suffisants, envisagez-vous de faire tout de même le séjour ?</label><br>
              <input id="radioFinanceOui" type="radio" class="form-control radiobut" style="display:inline-block" name="acceptation" value="1"required> Oui
              <input id="radioFinanceNon" type="radio" class="form-control radiobut" style="display:inline-block" name="acceptation" value="0"required> Non
              <div class="invalid-feedback">
                Veuillez cocher un des deux choix
              </div>
              <br>
            </div>

            <div>
              <br>
              <label class=gras>À ce jour, avez-vous recruté les étudiants ?</label><br>
              <input id="radioEtudiantOui" type="radio" class="form-control radiobut" name="recrutement" value="1" required> Oui
              <input id="radioEtudiantNon" type="radio" class="form-control radiobut" name="recrutement" value="0" required> Non
              <div class="invalid-feedback">
                Veuillez cocher un des deux choix
              </div>
              <br>
            </div>

            <div>
              <p> Si non, veuillez présenter vos stratégies de recrutement pour atteindre vos objectifs.</p>
              <textarea id="taRecrutement" rows="" cols="" style="width:100%; border-radius:5px;" ></textarea>
              <br>
              <br>
            </div>
          </div>
        </div>
        <br>
        <div class="grayzone" style="background-color: #E0E0E0;">
          <div style="margin-left:20px;">
            <h2>Accompagnateurs</h2>
          </div>
          <div style="margin-left:20px; margin-right:20px;">
            <table id="tableAccompagnateur" style="width:100%; background-color:white;" class="table">
              <tbody id="TBODYaccompagnateur">
              <tr>
                <th style="width:25%">Nom</th><th style="width:25%">Prénom</th><th style="width:25%">Téléphone</th><th style="width:25%">Courriel</th>
              </tr>
              <tr id="TRAccom">
                <td style="width:25%"><input id="tbAccNom1" type="text" style="width:100%" class="inputtable form-control nom" required><div class="invalid-feedback">Veuillez entrer un nom ou supprimer la ligne</div></td><td style="width:25%"><input id="tbAccPrenom1" type="text" style="width:100%" class="inputtable form-control prenom" required><div class="invalid-feedback">Veuillez entrer un prenom ou supprimer la ligne</div></td><td style="width:25%"><input id="tbAccTelephone1" type="text" style="width:100%"  placeholder="(123)456-7890" class="inputtable form-control telephone" required><div class="invalid-feedback">Veuillez entrer un numéro de téléphone ou supprimer la ligne</div></td><td style="width:25%"><input id="tbAccCourriel1" type="text" style="width:100%" class="inputtable form-control courriel" required><div class="invalid-feedback">Veuillez entrer une adresse courriel ou supprimer la ligne</div></td>
              </tr>
              <tr>
                <td style="width:25%"><input id="tbAccNom2" type="text" style="width:100%" class="inputtable form-control nom" required><div class="invalid-feedback">Veuillez entrer un nom ou supprimer la ligne</div></td><td style="width:25%"><input id="tbAccPrenom2" type="text" style="width:100%" class="inputtable form-control prenom" required><div class="invalid-feedback">Veuillez entrer un prénom ou supprimer la ligne</div></td><td style="width:25%"><input id="tbAccTelephone2" type="text" style="width:100%" placeholder="(123)456-7890" class="inputtable form-control telephone" required><div class="invalid-feedback">Veuillez entrer un numéro de téléphone ou supprimer la ligne</div></td><td style="width:25%"><input id="tbAccCourriel2" type="text" style="width:100%" class="inputtable form-control courriel" required><div class="invalid-feedback">Veuillez entrer une adresse courriel ou supprimer la ligne</div></td>
              </tr>

            </tbody>
            </table>
            <input id="btAjoutAccompagnateur" class="form-control col-md-6" type="button" value="+"  style="display: inline-block;" onclick="ajouterLigneAccompagnateur()"><input id="btSupprimerAccompagnateur" class="form-control col-md-6" type="button" value="-" style="display: inline-block;"  onclick="supprimerLigneAccompagnateur()">


          </div>
          <br>
          <h6 class="tagH6">NB, Le coordonnateur départemental, le responsable du programme ou le responsable de
            service atteste que les accompagnateurs impliqués dans la réalisation du projet de mobilité
            seront à l’emploi du collège au moment du séjour.</h6>
            <br>
          </div>

          <br>
          <div>
<?php if(isset($_GET['page']) && $_GET['page'] == "viewdemande") {?>
          <div class="d-none">
            <div>
            <p class=gras>- Résolution départementale pour les projets liés à un cours spécifique au sein d’un programme.</p>
            <p class=gras>- Résolution du comité programme pour les projets liés au programme ou pour les projets qui touchent l’épreuve synthèse de programme.</p>
            <p class=gras>- Document Excel sur les prévisions budgétaires.</p>
            </div>
            <a href="" target="_blank">
              <input id="fileDownload" class="btn btn-secondary float-left" type="button" value="Télécharger le fichier">
            </a>
          </div>

            <script type="text/javascript">
              if (localStorage.getItem("projetDocsID") != "null") {
                $("#fileDownload").parent("a").parent("div").removeClass('d-none');

                $.ajax({
                  url: 'php/gestion_demandes/getprojdocpath.php',
                  type: 'POST',
                  data: {
                    docid: localStorage.getItem("projetDocsID")
                  },
                  success: function(data) {
                    if (data != "error")
                      $("#fileDownload").parent("a").attr('href', data);
                    else {
                      $("#fileDownload").parent("a").parent("div").addClass('d-none');
                      alert("Erreur lors de la récupération du fichier lié à la demande");
                    }
                  }
                });
              }
            </script>

<?php } else {?>
            <hr>
            <p class=gras> Veuillez joindre à votre demande les documents suivant (dans un dossier compressé): </p>
            <p class=gras>- Résolution départementale pour les projets liés à un cours spécifique au sein d’un programme.</p>
            <p class=gras>- Résolution du comité programme pour les projets liés au programme ou pour les projets qui touchent l’épreuve synthèse de programme.</p>
            <p class=gras>- Document Excel sur les prévisions budgétaires.</p>
            <div>
              <input id="fileUpload" class="btn btn-secondary" type="file">
            </div>

<?php } ?>


            <br>
            <br>
            <hr>
          </div>
          <br>

        </div>
        <br>
        <input type="submit" id="submit" value="Envoyer"class="btn-primary form-control">

      </form>

      <script>
      // Example starter JavaScript for disabling form submissions if there are invalid fields
      (function() {
        'use strict';
        window.addEventListener('load', function() {
          // Fetch all the forms we want to apply custom Bootstrap validation styles to
          var forms = document.getElementsByClassName('needs-validation');
          // Loop over them and prevent submission
          var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
              if (form.checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
              }
              form.classList.add('was-validated');
            }, false);
          });
        }, false);
      })();
      </script>

      <script type="text/javascript">
      $('#inputGroupFile01').change(function(){
        $('#labelGroupFile01').text($(this)[0].files[0].name);
      });
      </script>

    </div>
    <script src="js/PageDemandeVoyage.js"></script>


<?php
      if((($_SESSION["type"] == 'Administrateur')||($_SESSION["type"] == 'Enseignant') && $_GET['page']=="viewdemande"))
      {
?>
      <script>

          $("#tbNom").val(localStorage.getItem('nom'));
          $("#tbPrenom").val(localStorage.getItem('prenom'));
          $("#tbAdresse").val(localStorage.getItem('adresse'));
          $("#lsProgramme").val(localStorage.getItem('programme'));
          $("#lsDestination").val(localStorage.getItem('destination'));
          $("#tbDateDepart").val(localStorage.getItem('dateD'));
          $("#tbDateRetour").val(localStorage.getItem('dateR'));
          $("#tbDestinationPrecision").val(localStorage.getItem('precisionDestination'));
           $("#tbTitreProjet").val(localStorage.getItem('titre'));
           $("#tbTypeProjet").val(localStorage.getItem('type'));
          $("#taDescription").val(localStorage.getItem('description'));
          $("#taEtudiants").val(localStorage.getItem('etudiants'));

          if(localStorage.getItem('liberation') == 0)
          {
            $("#radioLiberationOui").prop("checked",false);
            $("#radioLiberationNon").prop("checked",true);
          }
          else
          {
            $("#radioLiberationOui").prop("checked",true);
            $("#radioLiberationNon").prop("checked",false);
          }

          if(localStorage.getItem('financement') == 0)
          {
            $("#radioFinanceOui").prop("checked",false);
            $("#radioFinanceNon").prop("checked",true);
          }
          else
          {
            $("#radioFinanceOui").prop("checked",true);
            $("#radioFinanceNon").prop("checked",false);
          }

          if(localStorage.getItem('recrutement') == 0)
          {
            $("#radioEtudiantOui").prop("checked",false);
            $("#radioEtudiantNon").prop("checked",true);
          }
          else
          {
            $("#radioEtudiantOui").prop("checked",true);
            $("#radioEtudiantNon").prop("checked",false);
          }
          $("#taRecrutement").val(localStorage.getItem('strategies'));

          if((localStorage.getItem("activites")!=null)&&(localStorage.getItem("dateactivites")!=null))
          {
            var activites = localStorage.getItem("activites").split(",");
            var dateactivites = localStorage.getItem("dateactivites").split(",");

            if(activites.length == 1)
            {
              $("#tbActivite1").val(localStorage.getItem("activites"));
              $("#tbDateActivite1").val(localStorage.getItem("dateactivites"));
            }
            else if(activites.length > 1)
            {
              $("#tbActivite1").val(activites[0]);
              $("#tbDateActivite1").val(dateactivites[0]);
              for(var i=1; i < activites.length ; i++ )
              {
                var activiteID = i+1;
                ajouterLigneActivite();
                $("#tbActivite"+activiteID).val(activites[i]);
                $("#tbDateActivite"+activiteID).val(dateactivites[i]);
              }
            }
        }


        if((localStorage.getItem("acc_nom")!=null)&&(localStorage.getItem("acc_prenom")!=null)&&(localStorage.getItem("acc_telephone")!=null)&&(localStorage.getItem("acc_courriel")!=null))
          {
            var acc_nom = localStorage.getItem("acc_nom").split(",");
            var acc_prenom = localStorage.getItem("acc_prenom").split(",");
            var acc_telephone = localStorage.getItem("acc_telephone").split(",");
            var acc_courriel = localStorage.getItem("acc_courriel").split(",");

              for(var i=0; i<acc_nom.length; i++)
              {
                var idAcc = i+1;

                if(i>1)
                {
                  ajouterLigneAccompagnateur();
                }

                $("#tbAccNom"+idAcc).val(acc_nom[i]);
                $("#tbAccPrenom"+idAcc).val(acc_prenom[i]);
                $("#tbAccTelephone"+idAcc).val(acc_telephone[i]);
                $("#tbAccCourriel"+idAcc).val(acc_courriel[i]);
              }
        }
          $('input').prop('disabled', true);
          $("#fileDownload").prop('disabled', false);
          $('textarea').prop('disabled', true);
          $('#btDeconnexion').prop('disabled', false);
          $('#submit').hide();
          $('#btAjoutAccompagnateur').hide();
          $('#btSupprimerAccompagnateur').hide();
          $('#btAjoutActivite').hide();
          $('#btSupprimerActivite').hide();

      </script>
<?php
      }
      else if($_GET['page']!="viewdemande")
      {
?>
      <script>
         $('input').prop('disabled', false);
         $('textarea').prop('disabled', false);
         $('#submit').show();
         $('#btAjoutAccompagnateur').show();
         $('#btSupprimerAccompagnateur').show();
         $('#btAjoutActivite').show();
         $('#btSupprimerActivite').show();
      </script>

<?php
      }

?>


<?php
    $conn=null;
?>
  </body>


  </html>
