<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge" >

    <title></title>

         <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
          <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
          <link rel="stylesheet" href="css/gestion_users.css">

    <button id="btRetour"type="button" class="btn btn-secondary col-md-1"  onclick="window.location.href='main.php'"><i class="fas fa-arrow-left"></i> Retour</button>
</head>
<body>

<script src="js/gestion_users.js" charset="utf-8"></script><?php
if (isset($_SESSION['type']) && ($_SESSION['type'] == "Administrateur" || $_SESSION['type'] == "Enseignant")) {?>



<div class="midSection">
    <h1 class="text-white">Gestion des utilisateurs</h1>
    <br><br>
    <input type="button" id="affProjet" class="btAffichage btn btn-secondary" value="Afficher les utilisateurs du projet sélectionné" onclick="pullusers('projet')"></input>
    <input type="button" id="affTous" class="btSelectAffichage btn btn-secondary" value="Afficher tous les utilisateurs" onclick="pullusers('all')"></input><?php if ($_SESSION['type'] == "Administrateur"){ ?>

    <button class="btn btn-success" id="addUser"  value="Ajouter" data-toggle="modal" data-target="#addUserModal">Ajouter</button><?php } ?>

    <table id="tblUsers" class="table table-striped table-bordered">
        <thead class="thead-dark">
        <th scope="col">#</th>
        <th scope="col">Adresse courriel</th>
        <th scope="col">Nom</th>
        <th scope="col">Prénom</th>
        <th scope="col">Type</th>
        <th scope="col">Actions</th>
        </thead>
        <tbody class="table-light">
        </tbody>
    </table>
    <table id="tblProjets" class="table table-striped table-bordered">
        <thead class="thead-dark">
        <th scope="col">#</th>
        <th scope="col">Adresse courriel</th>
        <th scope="col">Nom</th>
        <th scope="col">Prénom</th>
        <th scope="col">Type</th>
        <th scope="col">Actions</th>
        </thead>
        <tbody class="table-light">
        </tbody>
    </table>



    <div class="modal fade" id="addUserModal" tabindex="-1" role="dialog" aria-labelledby="addUserModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="addUserModalLabel">Ajouter un utilisateur</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">

            <form id="form_adduser" method="post">
              <div class="form-group">
                <label id="frmAddUser" for="courriel">Adresse courriel</label><input id="courriel" class="form-control" type="text" value="">
                <label id="frmAddUser" for="nom">Nom</label><input id="nom" class="form-control" type="text" value="">
                <label id="frmAddUser" for="prenom">Prénom</label><input id="prenom" class="form-control" type="text" value="">
              </div>
              <div class="form-group">
                <label id="frmAddUser">Type</label><select id="type" class="form-control">
                  <option value="Administrateur">Administrateur</option>
                  <option value="Enseignant">Enseignant/Accompagnateur</option>
                  <option value="Enseignant">Agent de soutien/Accompagnateur</option>
                  <option value="Enseignant">Professionnel/Accompagnateur</option>
                  <option value="Enseignant">Cadre/Accompagnateur</option>
                  <option value="Enseignant">Autres/Accompagnateur</option>
                </select>
              </div>

              <div class="form-group">
                <label id="frmAddUser" for="password">Mot de passe</label><input id="password" class="form-control" type="text" value="" readonly>
                <br>
                <input id="generate" class="btn btn-secondary" type="button" value="Générer un mot de passe">
              </div>
            </form>

          </div>
          <div class="modal-footer">
            <input id="submitAddUser" type="submit" form="form_adduser" class="btn btn-primary" value="Créer">
          </div>
        </div>
      </div>
    </div>


    <div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="confirmModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                    <button id="confirmBT" type="button" class="btn btn-danger" data-dismiss="modal">Oui</button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="resetModal" tabindex="-1" role="dialog" aria-labelledby="resetModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="resetModalLabel">Mot de passe réinitialisé</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Mot de passe réinitialisé ; le nouveau mot de passe pour l'utilisateur <span
                                id="userReset"></span> est: <span id="tempPass"></span></p>
                    <p>Veuillez le garder en mémoire ou directement l'envoyer au propriétaire du compte car vous ne
                        pourrez plus le voir par la suite.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="successerrorModal" tabindex="-1" role="dialog" aria-labelledby="successerrorModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="successerrorModalLabel">Mot de passe réinitialisé</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
                </div>
            </div>
        </div>
    </div>


</div><?php
} else {
        echo "<br><br><h3>Vous n'avez pas les autorisations nécessaires pour accéder à cette page.</h3>";
    }?>
<div id="overlay"></div>
</body>
</html>
