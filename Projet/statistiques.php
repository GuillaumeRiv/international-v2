<?php

require 'php/BD.inc.php';?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>statistiques de projets</title>
     <link rel="stylesheet" href="css/gestion_projets.css">

     <!-- section impression -->
      <style>
      .table_alignement{
        width:50%;
     }

     @media print{
       #btn_imprimer,#btRetour,#btDeconnexion,#hello,#projetTitle,#totalstats,.hide_stat_selection{ /*tblProjets hide selection table*/
          display:none;
       }
       #logo{
          height: 9vh;
          width: 115vw !important;
       }
     }
     </style>
<script>
function imprimer_la_page() {
  
  window.print();
}
</script>


<!-- Resources -->
<!-- <script src="https://www.amcharts.com/lib/4/core.js"></script>
<script src="https://www.amcharts.com/lib/4/charts.js"></script>
<script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>  -->

<!-- Chart code -->
<!-- <script>
var test = 125;
am4core.ready(function() {

// Themes begin
am4core.useTheme(am4themes_animated);
// Themes end

// Create chart instance
var chart = am4core.create("", am4charts.PieChart); //here_table

// Set data
var selected;
var types = [{
  type: "Fossil Energy",
  percent: 70,
  color: chart.colors.getIndex(0),
  subs: [{
    type: "Oil",
    percent: test
  }, {
    type: "Coal",
    percent: 35
  }, {
    type: "Nuclear",
    percent: 20
  }]
}, {
  type: "Green Energy",
  percent: 30,
  color: chart.colors.getIndex(1),
  subs: [{
    type: "Hydro",
    percent: 15
  }, {
    type: "Wind",
    percent: 10
  }, {
    type: "Other",
    percent: 5
  }]
}];

// Add data
chart.data = generateChartData();

// Add and configure Series
var pieSeries = chart.series.push(new am4charts.PieSeries());
pieSeries.dataFields.value = "percent";
pieSeries.dataFields.category = "type";
pieSeries.slices.template.propertyFields.fill = "color";
pieSeries.slices.template.propertyFields.isActive = "pulled";
pieSeries.slices.template.strokeWidth = 0;

function generateChartData() {
  var chartData = [];
  for (var i = 0; i < types.length; i++) {
    if (i == selected) {
      for (var x = 0; x < types[i].subs.length; x++) {
        chartData.push({
          type: types[i].subs[x].type,
          percent: types[i].subs[x].percent,
          color: types[i].color,
          pulled: true
        });
      }
    } else {
      chartData.push({
        type: types[i].type,
        percent: types[i].percent,
        color: types[i].color,
        id: i
      });
    }
  }
  return chartData;
}

pieSeries.slices.template.events.on("hit", function(event) {
  if (event.target.dataItem.dataContext.id != undefined) {
    selected = event.target.dataItem.dataContext.id;
  } else {
    selected = "undefined";
  }
  chart.data = generateChartData();
});

}); // end am4core.ready()
</script> -->


<script>
$(document).ready(function() {
 $("button").click(function(){
   //if($("button").text !== 'Afficher les statistiques individuelles' or $("button").text !== 'Afficher les statistiques générales'){
    alert($(this).text()); // nom de projet
    alert(this.id);  // id du projet
    var variable = $(this).text();
    var id = this.id;

    $("#here_table").toggle();


      var url = "php/stats/test.php";

      $.post(url,{variable:variable,id:id}).done(function(data){
        alert("POST was successful" + data.id_demandes);
      
      
      });


      /*$.ajax({
      url: 'php/stats/test.php',
      type: 'POST',
      data: { variable : variable, id : id},
      success: function(data) {  
            //alert(data);
            selected_pj_stat(data);
      }
      });*/
   //}
      /*  $('.getbtn').on( 'click', function(){
          var pid = $(this).data('pid');
          $.get( "php/stats/test.php", { ID: pid })
            .done(function( dat ) {
              data = $.parseJSON(dat);
              selected_pj_stat();
              if(data){
                $('#prod_id').val(data.id);
                $('#product_name').val(data.product_name);
                $('#price').val(data.price);
                $('#category').val(data.category);
            }
          });
      });*/

});
});

function selected_pj_stat(data){  
  var test = data;
        $('#here_table').append( '<tr><td>' + test + '</td><td>' + data + '</td></tr>' ); //test
}
</script> 
<!-- <script>
$(document).ready(function() {
  var rowCount = $('#par_projet tr').length;
  
  for(i=0;i<rowcount;i++){
  $("#pj" + i).click(function(){
    $("#here_table").toggle();
  });
  }
});
</script> -->


<script>
function rdy(){
  $("#here_table").hide();
  $("#hideTotalStats").hide();
  $("#justhidethisfornow").hide();
  $("#projetTitle").hide();
}

function affichage_general(){
         $("#totalstats").text(function(i,text){
           $("#tblProjets").toggle();
           $("#hideTotalStats").toggle();
           return text === 'Afficher les statistiques générales' ? 'Afficher les statistiques individuelles' : 'Afficher les statistiques générales'
         });
}

</script>
</head>
<body onload="rdy();">
  

  <button id="btRetour"type="button" class="btn btn-secondary col-md-1"  onclick="window.location.href='main.php'"><i id="iRetour"class="fa fa-arrow-left"></i> Retour</button><?php
  if (isset($_SESSION['type']) && ($_SESSION['type'] == "Administrateur")||($_SESSION['type'] == "Enseignant")) {?>
    <div class="midSection">

    <h1 class="text-white">Statistiques</h1>
    <br><br>
    <button id="btn_imprimer" onclick="imprimer_la_page()" class="btn btn-primary float-right">Imprimer</button>
    <button id="totalstats" onclick="affichage_general()" class="btn btn-primary float-left">Afficher les statistiques générales</button>
    <br><br>

       <table id="tblProjets" class="table table-striped hide_stat_selection">
       <thead  class="thead-dark">
       <th scope="col">Par projet</th>
       <th scope="col">Par année</th>
       <th scope="col">Par groupe ou technique</th>
       </thead>
       <tbody class="table-light">

       <tr>
       <td>
      <table id="par_projet" name="table_projet" class="table table-striped"><?php
 

      $request="SELECT * FROM `projets`";
      $sth = $conn->query($request);
      $pj_result=$sth->fetchAll(PDO::FETCH_ASSOC);
      foreach($pj_result as $row){                                                  
      echo '<tr data-pid ="pj-'.$row['ID'].'"><td><button id ="pj-'.$row['ID'].'" class="getbtn" onMouseOver=this.style.color="#0069d9" onMouseOut=this.style.color="#000" style="background: transparent;border:none;">'.
      $row['nom_projet'].'</button></td></tr>';
      }?>
      </table>
      </td>

      <td>
      <table class="table table-striped"><?php
      $request1="SELECT distinct year(dateP) FROM `projets` where 1 order by dateP asc;";
      $sth = $conn->query($request1);
      $year_result=$sth->fetchAll(PDO::FETCH_ASSOC);
      $id=0;
      foreach($year_result as $row){
      $id++;
      echo '<tr id="ann-'.$id.'"><td><button onMouseOver=this.style.color="#0069d9" onMouseOut=this.style.color="#000" style="background: transparent;border:none;">'.
      $row['year(dateP)'].'</button></td></tr>';
      }?>
      </table>
      </td>


      <td>
      <table><?php //a revoir une fois que les formulaires des demandes sont fini
      $request2="SELECT distinct programme FROM `demandes` WHERE etat = '1'";
      $sth = $conn->query($request2);
      $group_result=$sth->fetchAll(PDO::FETCH_ASSOC);
      $id=0;
      foreach($group_result as $row){
      $id++;
      echo '<tr id="gr-'.$id.'"><td style="width:50%;"><button onMouseOver=this.style.color="#0069d9" onMouseOut=this.style.color="#000" style="background: transparent;border:none;">'.
      $row['programme'].'</button></td></tr>';
      }?>
      </table>
      </td>
      </tr>

      

     </tbody>
   </table>
   <!-- test -->
<!--<div id="here_table" style="background-color:white;"> </div>-->
<table id="here_table" class="table table-striped" style="background-color:white;">
   <thead class="thead-dark">
        <th scope="col">#</th>
       <th scope="col">Mobilité étudiante (individuelle)</th>
 </thead>
 <tbody class="table-light"><?php
      
      print_r($_POST);
      if(isset($_POST["id"])){
        $id = $_POST["id"];
        echo 'alert('.$id.')';
        echo $_POST;
      }
    
      /*foreach($pj_result as $row){                                               
      echo '<tr data-pid ="pj-'.$row['ID'].'"><td><button id ="pj-'.$row['ID'].'" class="getbtn" onMouseOver=this.style.color="#0069d9" onMouseOut=this.style.color="#000" style="background: transparent;border:none;">'.
      $row['nom_projet'].'</button></td></tr>';
      }*/

      
      /* //Mobilité étudiante (individuelle)
      //Nombre de projets acceptés par type de projet = sessions/ stage / etc
      $requestt="SELECT count(*) FROM projets"; //on a pas de catégorie autres que prof pour l'instant'
      $sth = $conn->query($requestt);
      $result=$sth->fetchColumn();
      echo '<tr><td class="table_alignement"> Nombre de projets acceptés par type de projet = <br> sessions / stage / etc </td>'.'<td class="table_alignement">'.$result.'</td> </tr>';

      //Nombre d'étudiants ayant participés (par type)'
      $requestt1="SELECT count(*) FROM utilisateurs WHERE type='e'"; // doit avoir une variable pour savoir quelle étudiants ont participé ou non aux final.
      $sth = $conn->query($requestt1);
      $result=$sth->fetchColumn();
      echo '<tr><td> Nombre d'."'".'étudiants ayant participés (par type)'.' </td>'.'<td>'.$result.'</td> </tr>';

      //Nombre d'enseignants responsables'
      $requestt3="SELECT count(*) + (SELECT count(*) FROM demandes_accompagnateurs)FROM utilisateurs WHERE type='p'"; //on a pas de catégorie autres que prof pour l'instant'
      //"SELECT count(*) FROM utilisateurs WHERE type='p' union SELECT count(*) FROM demandes_accompagnateurs"
      $sth = $conn->query($requestt3);
      $result=$sth->fetchColumn();
      echo '<tr><td> Nombre d'."'".'enseignants responsables'.'</td>'.'<td>'.$result.'</td> </tr>';

        //Genre des  étudiants participants
      $requestt6="SELECT count(*) FROM formulaires_etudiants WHERE sexe='Aucun'"; // ou autres dépendament de se qui est sauvegarder du formulaire
      $sth = $conn->query($requestt6);
      $result=$sth->fetchColumn();
       echo '<tr><td> Nombre d'."'".'étudiants'.'</td>'.'<td>'.$result.'</td> </tr>';

      $requestt7="SELECT count(*) FROM formulaires_etudiants WHERE sexe='Homme'";
      $sth = $conn->query($requestt7);
      $result=$sth->fetchColumn();
       echo '<tr><td> Nombre d'."'".'homme'.'</td>'.'<td>'.$result.'</td> </tr>';

      $requestt8="SELECT count(*) FROM formulaires_etudiants WHERE sexe='Femme'";
      $sth = $conn->query($requestt8);
      $result=$sth->fetchColumn();
       echo '<tr><td> Nombre de femme</td>'.'<td>'.$result.'</td> </tr>';

       //nombre de participants par type de projet

       //nombre de participants par programme d'étude (et par type de projet)'

       //Nombre de participants par destination (et par type de projet)
*/?>
</tbody>
</table>














<div id="hideTotalStats">

<table id="tblProjets" class="table table-striped">
      <thead class="thead-dark">
        <th scope="col">#</th>
        <th scope="col">Mobilité étudiante (Groupe)</th>
      </thead>
      <tbody class="table-light"><?php

      //Mobilité étudiante groupe
      //Nombre de projets
      $request1="SELECT count(*) FROM `projets`";
      $sth = $conn->query($request1);
      $result=$sth->fetchColumn();
      echo  '<tr><td class="table_alignement"> Nombre de projets'.'</td>'.'<td class="table_alignement">'.$result.'</td> </tr>';

      //nombre de projet acceptés
      $request2="SELECT count(*) FROM `demandes` WHERE etat= 1";
      $sth = $conn->query($request2);
      $result=$sth->fetchColumn();
      echo '<tr><td> Nombre de projets acceptés'.'</td>'.'<td>'.$result.'</td> </tr>';

      //nombre de projet refusés
      $request3="SELECT count(*) FROM `demandes` WHERE etat= 2";
      $sth = $conn->query($request3);
      $result=$sth->fetchColumn();
      echo '<tr><td> Nombre de projets refusés'.'</td>'.'<td>'.$result.'</td> </tr>';
      //nombre de projet soustenus ??
      $request="SELECT count(*) FROM `demandes`";
      $sth = $conn->query($request);
      $result=$sth->fetchColumn();
      echo '<tr><td> Nombre de demande de projet'.'</td>'.'<td>'.$result.'</td> </tr>';

      //nombre d'étudiants ayant participés'   (tous les étudiants pour tous les projets pour le moment.)
      $request4="SELECT count(*) FROM usr_projet_info";
      $sth = $conn->query($request4);
      $result=$sth->fetchColumn();
       echo '<tr><td> Nombre d'."'".'étudiants ayant participés'.'</td>'.'<td>'.$result.'</td> </tr>';
      //Nombre d'enseignants accompagnateurs'
      $request5="SELECT count(*) FROM utilisateurs where type='p'";
      $sth = $conn->query($request5);
      $result=$sth->fetchColumn();
       echo '<tr><td> Nombre d'."'".'enseignants accompagnateurs'.'</td>'.'<td>'.$result.'</td> </tr>';

      //calculer les prof + les étu selon leur genre.
      //Genre des  étudiants participants
      $request6="SELECT count(*) FROM formulaires_etudiants WHERE sexe='Aucun'"; // ou autres dépendament de se qui est sauvegarder du formulaire
      $sth = $conn->query($request6);
      $result=$sth->fetchColumn();
       echo '<tr><td> Nombre d'."'".'étudiants'.'</td>'.'<td>'.$result.'</td> </tr>';

      $request7="SELECT count(*) FROM formulaires_etudiants WHERE sexe='Homme'";
      $sth = $conn->query($request7);
      $result=$sth->fetchColumn();
       echo '<tr><td> Nombre d'."'".'homme'.'</td>'.'<td>'.$result.'</td> </tr>';

      $request8="SELECT count(*) FROM formulaires_etudiants WHERE sexe='Femme'";
      $sth = $conn->query($request8);
      $result=$sth->fetchColumn();
       echo '<tr><td> Nombre de femme</td>'.'<td>'.$result.'</td> </tr>';
     
      //Genre des prof participants (il n'y a pas de table dans la bd pour les différentier')
      $request9="SELECT count(*) FROM ? WHERE Autre";
      echo '<tr><td>Genre des  prof: autre </td>'.'<td>'.$result.'</td> </tr>';
      $request10="SELECT count(*) FROM ? WHERE Homme";
      echo '<tr><td>Genre des  prof: Homme </td>'.'<td>'.$result.'</td> </tr>';
      $request11="SELECT count(*) FROM ? WHERE Femme";
      echo '<tr><td>Genre des  prof: Femme </td>'.'<td>'.$result.'</td> </tr>';

      //Nombre de participants par projet (par catégorie)
      $request12="SELECT categorie, count(*) AS num FROM demandes GROUP BY categorie";
      $sth = $conn->query($request12);
      $result=$sth->fetchALL(\PDO::FETCH_ASSOC);
      foreach($result as $row){
       echo '<tr><td>'.$row['categorie'].'</td>'.'<td>'.$row['num'].'</td> </tr>';
      }


      /*$request13="SELECT categorie FROM demandes GROUP BY categorie UNION (SELECT count(*) as num FROM usr_projet_info WHERE projetID=1) ";
      $sth = $conn->query($request13);
      $result=$sth->fetchALL(\PDO::FETCH_ASSOC);
      foreach($result as $row){
       echo '<tr><td>'.$row['categorie'].'</td>'.'<td>'.$row['0'].'</td> </tr>';
      }*/



      //Nombre de participants par programme d'études (par catégorie)

      //Nombre de participants par destination (par catégorie)?>



      </tbody>
    </table>

</div>



<div id="justhidethisfornow">

<br> <br>
<table id="tblProjets" class="table table-striped">
      <thead class="thead-dark">
        <th scope="col">#</th>
        <th scope="col">Mobilité étudiante (individuelle)</th>
      </thead>
      <tbody class="table-light"><?php

      //Mobilité étudiante (individuelle)
      //Nombre de projets acceptés par type de projet = sessions/ stage / etc
      $requestt="SELECT count(*) FROM projets"; //on a pas de catégorie autres que prof pour l'instant'
      $sth = $conn->query($requestt);
      $result=$sth->fetchColumn();
      echo '<tr><td class="table_alignement"> Nombre de projets acceptés par type de projet = <br> sessions / stage / etc </td>'.'<td class="table_alignement">'.$result.'</td> </tr>';

      //Nombre d'étudiants ayant participés (par type)'
      $requestt1="SELECT count(*) FROM utilisateurs WHERE type='e'"; // doit avoir une variable pour savoir quelle étudiants ont participé ou non aux final.
      $sth = $conn->query($requestt1);
      $result=$sth->fetchColumn();
      echo '<tr><td> Nombre d'."'".'étudiants ayant participés (par type)'.' </td>'.'<td>'.$result.'</td> </tr>';

      //Nombre d'enseignants responsables'
      $requestt3="SELECT count(*) + (SELECT count(*) FROM demandes_accompagnateurs)FROM utilisateurs WHERE type='p'"; //on a pas de catégorie autres que prof pour l'instant'
      //"SELECT count(*) FROM utilisateurs WHERE type='p' union SELECT count(*) FROM demandes_accompagnateurs"
      $sth = $conn->query($requestt3);
      $result=$sth->fetchColumn();
      echo '<tr><td> Nombre d'."'".'enseignants responsables'.'</td>'.'<td>'.$result.'</td> </tr>';

        //Genre des  étudiants participants
      $requestt6="SELECT count(*) FROM formulaires_etudiants WHERE sexe='Aucun'"; // ou autres dépendament de se qui est sauvegarder du formulaire
      $sth = $conn->query($requestt6);
      $result=$sth->fetchColumn();
       echo '<tr><td> Nombre d'."'".'étudiants'.'</td>'.'<td>'.$result.'</td> </tr>';

      $requestt7="SELECT count(*) FROM formulaires_etudiants WHERE sexe='Homme'";
      $sth = $conn->query($requestt7);
      $result=$sth->fetchColumn();
       echo '<tr><td> Nombre d'."'".'homme'.'</td>'.'<td>'.$result.'</td> </tr>';

      $requestt8="SELECT count(*) FROM formulaires_etudiants WHERE sexe='Femme'";
      $sth = $conn->query($requestt8);
      $result=$sth->fetchColumn();
       echo '<tr><td> Nombre de femme</td>'.'<td>'.$result.'</td> </tr>';

       //nombre de participants par type de projet

       //nombre de participants par programme d'étude (et par type de projet)'

       //Nombre de participants par destination (et par type de projet)?>

 </tbody>
    </table>








    <br> <br>
<table id="tblProjets" class="table table-striped">
      <thead class="thead-dark">
        <th scope="col">#</th>
        <th scope="col">Mobilité enseignante et du personnel</th>
      </thead>
      <tbody class="table-light"><?php

      //Mobilité enseignante et du personnel
      //Nombre de projets déposés par catégorie de personnel
      $requesttt="SELECT count(*) FROM demandes"; //on a pas de catégorie autres que prof pour l'instant'
      $sth = $conn->query($requesttt);
      $result=$sth->fetchColumn();
      echo '<tr><td class="table_alignement"> Nombre de projets déposés par catégorie de personnel </td>'.'<td class="table_alignement">'.$result.'</td> </tr>';

      //Nombre de projets accepté par catégorie de personnel
      $requesttt2="SELECT count(*) FROM demandes WHERE etat=1"; // Same
      $sth = $conn->query($requesttt2);
      $result=$sth->fetchColumn();
      echo '<tr><td> Nombre de projets acceptés par catégorie de personnel </td>'.'<td>'.$result.'</td> </tr>';

      //Nombre de participants par catégorie de personnel ??
      $requesttt3="SELECT count(*) FROM utilisateurs WHERE type='p'";
      $sth = $conn->query($requesttt3);
      $result=$sth->fetchColumn();
      echo '<tr><td> Nombre de participants par catégorie de personnel (enseignants) </td>'.'<td>'.$result.'</td> </tr>';
      $requesttt4="SELECT count(*) FROM utilisateurs WHERE type='e'";
      $sth = $conn->query($requesttt4);
      $result=$sth->fetchColumn();
      echo '<tr><td> Nombre de projets acceptés par catégorie de personnel (étudiants) </td>'.'<td>'.$result.'</td> </tr>';

      //Genre des participants par cagétorie de personnel (les genres ne sont pas là)
      $requesttt5="SELECT count(*) FROM ? WHERE Autre"; //+ les catégories
      $requesttt6="SELECT count(*) FROM ? WHERE Homme";
      $requesttt7="SELECT count(*) FROM ? WHERE Femme";

        //Nombre de participants par destination (par catégorie de personnel)

        //Nombre de participants par programme d'études (par catégrie de personnel)'?>


 </tbody>
    </table>

    <br> <br>
<table id="tblProjets" class="table table-striped">
      <thead class="thead-dark">
        <th scope="col">#</th>
        <th scope="col">Projet de coopération</th>
      </thead>
      <tbody class="table-light"><?php
      //Nombre de participants par catégorie de personnel
      $requesttt3="SELECT count(*) FROM utilisateurs WHERE type='p'";  //toujours aucune catégorie de personnel
      $sth = $conn->query($requesttt3);
      $result=$sth->fetchColumn();
      echo '<tr><td class="table_alignement"> Nombre de participants par catégorie de personnel (enseignants) </td>'.'<td class="table_alignement">'.$result.'</td> </tr>';
      $requesttt4="SELECT count(*) FROM utilisateurs WHERE type='e'";
      $sth = $conn->query($requesttt4);
      $result=$sth->fetchColumn();
      echo '<tr><td> Nombre de projets acceptés par catégorie de personnel (étudiants) </td>'.'<td>'.$result.'</td> </tr>';

      //Genre des participants par catégorie de personnel



      //Nombre de participants par destination par catégorie de personnel

      //Nombre de participants par projet par catégorie de personnel

      //Nombre de participants par programme d'études par catégorie de personnel?>
 </tbody>
    </table>


</div><?php
  } else {
    echo "<br><br><h3>Vous n'avez pas les autorisations nécessaires pour accéder à cette page.</h3>";
}?>


</body>
</html>
