<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link rel="stylesheet" href= "css/usr_create_account.css" type="text/css">
  <link rel="icon" href="./img/favicon.jpg" sizes="16x16" type="image/png">
  <title>Création de compte</title>
</head>
<body>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

  <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>

<header>
    <img id="logo" src="img/iconcegep.jpg" alt="Icone du cegep">
</header>

  <div class="container">
    <div class="row" id="login-container">
      <div class="col-10 offset-1">
        <form id="frm_register" method="POST" class="needs-validation" novalidate>
          <div id="errorAlert" class="alert alert-danger" role="alert">
            Erreur de connexion à la base de donnée
          </div>

          <div class="form-group has-feedback">
              <label id="lbCode" for="tbCode" >Code du projet</label>
              <input type="text" class="form-control" id="tbCode" onchange="checkCode()" required>
              <div class="invalid-feedback">
                Veuillez entrer un code de projet valide
              </div>
            </div>

            <div class="form-group has-feedback">
                <label id="lbCourriel" for="tbCourriel" >Adresse courriel</label>
                <input type="email" class="form-control" id="tbCourriel" onchange="checkCourriel()" required>
                <div class="invalid-feedback">
                  Veuillez entrer une adresse courriel valide
                </div>
              </div>

              <div class="form-group has-feedback">
                  <label id="lbPrenom" for="tbPrenom" >Prénom</label>
                  <input type="text" class="form-control" id="tbPrenom" onchange="checkPrenom()" required>
                  <div class="invalid-feedback">
                    Veuillez entrer un prénom
                  </div>
                </div>

                <div class="form-group has-feedback">
                    <label id="lbNom" for="tbNom" >Nom</label>
                    <input type="text" class="form-control" id="tbNom" onchange="checkNom()" required>
                    <div class="invalid-feedback">
                      Veuillez entrer un nom
                    </div>
                  </div>

            <div class="form-group has-feedback">
              <label id="lbPassword" for="tbPassword">Mot de passe</label>
              <input type="password" class="form-control" id="tbPassword" onchange="checkPassword()" required>
              <div class="invalid-feedback">
                Veuillez entrer un mot de passe
              </div>
            </div>

            <div class="form-group has-feedback">
              <label id="lbConfirmPassword" for="tbConfirmPassword">Confirmer le mot de passe</label>
              <input type="password" class="form-control" id="tbConfirmPassword" onchange="checkConfirmPassword()" required>
              <div class="invalid-feedback">
                Veuillez confirmer le mot de passe
              </div>
            </div>

          <div>
            <input class="btn btn-secondary" id="btCreateAccount" type="submit" value="Créer un compte"></input>
            <input class="btn btn-secondary" id="btAnnuler" onclick="window.location.href='connection.php'" type="button" value="Annuler"></input>
          </div>

        </form>
      </div>
    </div>
  </div>
  <div id="overlay"></div>

  <script src="js/global.js"></script>
  <script src="js/usr_create_account.js"></script>
  <div id="overlay"></div>

</body>
</html>
