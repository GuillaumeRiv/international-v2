<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href= "css/mdpOublier.css" type="text/css">
    <title>Récupération du mot de passe</title>
    <link rel="icon" href="img/favicon.jpg" sizes="16x16" type="image/png">
</head>
<body>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script src="js/global.js"></script>

  <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>

<header>
    <img id="logo" src="img/iconcegep.jpg" alt="Icone du cegep">
</header><?php
require 'php/BD.inc.php';?>
<div class="container">
  <div class="row" id="login-container">
    <div class="col-10 offset-1">
        <form id="frm_connexion" class="center" method="POST">
            <div class="form-group mx-auto">
              <label id="lbCourriel" for="tbCourriel" >Entrez l'adresse courriel du compte</label>
              <input type="text" class="form-control" name="tbCourriel" id="tbCourriel">
            </div>

            <div>
              <input type="submit" class="btn btn-secondary" name="btEnvoyer" id="btEnvoyer" value="Envoyer"></input><?php
                if( isset($_POST['btEnvoyer'])){
                  if(!empty($_POST['tbCourriel'])){
                  $email = $_POST['tbCourriel'];
                  $sql = "SELECT COUNT(courriel) as found FROM utilisateurs where courriel ='".$email."';";
                  $req = $conn->prepare($sql);
                  $req->execute();
                  $found = $req->fetch();

                  if ($found['found'] == 1){

                    function randomPassword() {
                      $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
                      $pass = array();
                      $alphaLength = strlen($alphabet) - 1;
                      for ($i = 0; $i < 8; $i++) {
                        $n = rand(0, $alphaLength);
                        $pass[] = $alphabet[$n];
                      }
                      return implode($pass);
                      }

                      $nouveauMdp =  randomPassword();
                      $nouveauMdp2 = hash('SHA256', $nouveauMdp);
                      $sql = "UPDATE utilisateurs set password = '".$nouveauMdp2."' where courriel ='".$_POST['tbCourriel']."';";
                      $req = $conn->prepare($sql);
                      $req->execute();


                      $msg= "Voici votre nouveau mot de passe temporaire:".$nouveauMdp."\nVeuillez le changer le plutôt possible";
                      $msg = wordwrap($msg,70);
                      $subject = "Changement de mot de passe";
                      mail($email, $subject, $msg);
                      echo "<script>alert(\"Votre nouveau mot de passe temporaire a été envoyé à l'adresse courriel inscrite.\")</script>";
                  }
                  else{
                    echo "<script>alert(\"Cette addresse courriel est invalide ou inexistante.\")</script>";
                  }


                }
                else{
                  echo "<script>alert(\"Veuillez inscrire l'adresse courriel de votre compte.\")</script>";
                }
              }?>
              <input type="button" class="btn btn-secondary" id="btAnnuler" onClick="location.href='connection.php';" value="Annuler"></input>
            </div>
        </form>
    </div>
  </div>
  <br>
</div>

<div id="result"></div>
<div id="overlay"></div>
<script src="js/connection.js"></script>
</body>
</html>
