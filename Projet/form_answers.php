<!Doctype html>
<html lang="eng">
    <head>
        <meta charset="UTF-8">
        <title>Formulaire | Cégep International</title>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href= "css/header.css" type="text/css">
        <link rel="stylesheet" href= "css/profil.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    </head>
    <body><?php
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }

        if (empty($_SESSION["courriel"])) {
            header('Location: connection.php');
        }  

        include "header.php";?>
        <button id="btRetour"type="button" class="btn btn-secondary col-md-1"  onclick="javascript:history.go(-1)"><i id="iRetour"class="fa fa-arrow-left"></i> Retour</button>

        <div class="container">
            <div class="row">
                <div id="form" class="needs-validation ml-auto gray-background col s12" style="padding-bottom: 550px" novalidate>

                    <!-- Information du formulaire -->
                    <div class="row">
                        <div class="col-md-12 align-center">
                            <div class="form-group card-background">
                                <h1 id="titre_form"></h1>
                                <h4 id="desc_form"></h4>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div id="overlay"></div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
        <script src="./js/form_answers.js"></script>
        <link rel="stylesheet" href="css/form_generated.css">

    </body>
</html>
