<table id="etutable">
  <tr>
    <td class="etutd">
    <a href="?page=formulaire">
        <button class="sectionA">
          <img class="iconSection" src="img/formIcon.png" alt="IconeFormulaire">
          <span>Formulaire</span>
        </button>
      </a>
    </td>
    <td class="etutd">
      <a href="?page=projets">
        <button class="sectionA">
          <img class="iconSection" src="img/projetIcon.png" alt="IconeProjet">
          <span>Projets</span>
        </button>
      </a>
    </td>
  </tr>

  <tr>
    <td class="etutd">
      <a href="?page=profil">
        <button class="sectionA">
          <img class="iconSection" src="img/profileIcon.png" alt="IconePréférences">
          <span>Profil</span>
        </button>
      </a>
    </td>
    <td class="etutd">
      <a href="?page=appreciation">
      <button class="sectionA">
        <img class="iconSection" src="img/appreciationIcon.png" alt="IconeProfileS">
        <span>Bilan du séjour</span>
      </button>
      </a>
    </td>
  </tr>
  <tr>
    <td class="etutd">
      <a href="?page=tableau_de_bord">
        <button class="sectionA">
          <img class="iconTableau" src="img/iconTableau.png" alt="IconeTableau">
          <span>Tableau de bord</span>
        </button>
      </a>
    </td>
  </tr>
</table>
<div id="overlay"></div>
<script type="text/javascript">
if ($(window).width() < 880) {
  $(".sectionE").find("span").css('font-size', '18px');
} else {
  $(".sectionE").find("span").css('font-size', '');
}

if (matchMedia) {
  const mq = window.matchMedia("(min-width: 880px)");
  mq.addListener(WidthChange);
  WidthChange(mq);
}


function WidthChange(mq) {
  if (mq.matches) {
    $(".sectionE").find("span").css('font-size', '');
  } else {
    $(".sectionE").find("span").css('font-size', '18px');
  }

}
</script>
