<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="css/formulairedyn.css">
</head>
<body>
  <script src="js/formulairedyn.js" charset="utf-8"></script>
  <button id="btRetour"type="button" class="btn btn-secondary col-md-1"  onclick="window.location.href='main.php'"><i id="iRetour"class="fa fa-arrow-left"></i> Retour</button><?php
  if (isset($_SESSION['type']) && ($_SESSION['type'] == "Administrateur")) {
?>
    <div class="midSection">

      <br><br>
      <h1 class="text-white">Modifier la liste des vaccins</h1>
      <br><br>

      <i class="far fa-plus-square float-right" id="addVaccin" value="Ajouter" data-toggle="modal" data-target="#addVaccinModal"><label id="lbladd"> Ajouter</label></i>

      <table id="tblVaccins" class="table table-striped">
        <thead class="thead-dark">
          <th scope="col">#</th>
          <th scope="col">Nom</th>
          <th scope="col">Activé pour projet sélectionné</th>
          <th scope="col">Action</th>
        </thead>
        <tbody class="table-light">
        </tbody>
      </table>

        <!-- <div class="col-md-6"><input id="addVaccin" class="btn btn-primary float-right" type="button" value="Ajouter un vaccin" data-toggle="modal" data-target="#addVaccinModal"></div> -->


    <div class="modal fade" id="deleteVaccinModal" tabindex="-1" role="dialog" aria-labelledby="deleteVaccinModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="deleteVaccinModalLabel">Supprimer le vaccin ?</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p>Voulez-vous vraiment supprimer le vaccin ?</p>
          </div>
          <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
            <button id="deleteVaccinBt" type="button" class="btn btn-danger">Oui</button>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="addVaccinModal" tabindex="-1" role="dialog" aria-labelledby="addVaccinModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="addVaccinModalLabel"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <label for="addVaccinNom">Entrer le nom du vaccin à ajouter: </label>
            <input id="addVaccinNom" class="form-control" type="text" value="">
          </div>
          <div class="modal-footer">
            <button id="addVaccinBt" type="button" class="btn btn-primary">Ok</button>
          </div>
        </div>
      </div>
    </div>
<?php
  } else {
    echo "<br><br><h3>Vous n'avez pas les autorisations nécessaires pour accéder à cette page.</h3>";
}
?>
<div id="overlay"></div>
</body>
</html>
