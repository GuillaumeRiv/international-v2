<!DOCTYPE html>
<html>
<head>

  <link rel="stylesheet" href="css/gestion_projets.css">
</head>
<body><?php
  require "php/BD.inc.php";?>
  <script src="js/gestion_projets.js" charset="utf-8"></script>

  </script>
  <button id="btRetour"type="button" class="btn btn-secondary col-md-1"  onclick="window.location.href='main.php'"><i id="iRetour"class="fa fa-arrow-left"></i> Retour</button><?php
  if (isset($_SESSION['type']) && ($_SESSION['type'] == "Administrateur")||($_SESSION['type'] == "Enseignant") ||($_SESSION['type'] == "Étudiant"))  {?>
    <div class="midSection">

    <h1 class="text-white">Gestion des projets</h1>
    <br><br>
    <input type="button" id="affProjets" class="btAffichage btn btn-secondary" value="Afficher tous les projets actifs"></input>
    <input type="button" id="affProjetsD" class="btSelectAffichage btn btn-secondary" value="Afficher tous les projets inactifs"></input>
    <table id="tblProjets" class="table table-striped table-bordered">
      <thead class="thead-dark">
        <th scope="col">#</th>
        <th scope="col">Nom</th>
        <th scope="col">Date de départ</th>
        <th scope="col">Code de Projet</th>
        <th scope="col">Actions</th>
      </thead>
      <tbody class="table-light">
      </tbody>
    </table>
    <table id="tblProjetsD" class="table table-striped table-bordered">
      <thead class="thead-dark">
        <th scope="col">#</th>
        <th scope="col">Nom</th>
        <th scope="col">Date de départ</th>
        <th scope="col">Code de Projet</th>
        <th scope="col">Actions</th>
      </thead>
      <tbody class="table-light">
      </tbody>
    </table>
    <form id="formAddProjet" method="POST">
    <input style="margin-right:10px" name="btAddProjet" class='btn btn-primary addprojet' type='submit' for="formAddProjet" value='Participer à un autre projet'>
    <input  type="text" id="tbAddProjet" name="tbAddProjet" class="form-control col-md-3 addprojet" placeholder="Code de projet">
    </form><?php
}
  else {
    echo "<br><br><h3>Vous n'avez pas les autorisations nécessaires pour accéder à cette page.</h3>";
}?>
<div id="overlay"></div>
</body>
</html>
