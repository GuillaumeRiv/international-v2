$( document ).ready(function() {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const idForm = urlParams.get('idForm');

    $.ajax(
    {
        type: "POST",
        url: 'php/form_creation/ajaxForm_generated.php',
        data: {'idForm' : idForm},
        dataType: 'json',
        error: function (result) {
            alert(result.responseText)
        },
        success: function (data) {
            $('#titre_form').val(data.titre);
            $('#desc_form').val(data.description);

            var toappend = "";
            var button = "";
            var choixReponse = "";
            var input = "";
            var option = "";
            file = "";
            for (question in data.questions) {
                //x += "<h1>" + data.questions[question].question + "</h1>";
                // for (j in data.questions[question].models) {
                //     x += myObj.cars[i].models[j];
                // }

                switch (data.questions[question].type) {
                    case "ShortText":
                        input = '<input class="input-field form-control" type="text" name="fname" placeholder="Réponse courte" disabled>';
                        break;
                    case "LongText":
                        input = '<textarea class="materialize-textarea input-field form-control" data-length="120" placeholder="Réponse longue" disabled></textarea>';
                        break;
                    case "Nud":
                        input = '<input type="number" value="1" disabled/>';
                        break;
                    case "Date":
                        input = '<input type="Date" value="1" disabled/>';
                        break;
                    case "Radio":
                        input = "";
                        for (choix_reponse in data.questions[question].choix_reponse) {
                            if (data.questions[question].type == 'Radio') {

                                if ((data.questions[question].choix_reponse.length - 1) == choix_reponse) {
                                    if (choix_reponse != 0) {
                                        choixReponse = '<span id="deleteOption_container_' + (generatedQuestionId) + '" class="deleteOption">' +
                                                        '<a class="btn-floating waves-effect waves-light red" onclick="deleteOption(this)">'+
                                                        '<i class="material-icons">delete</i>' +
                                                        '</a>' +
                                                        '</span>' +
                                                        '<span id="addOption_container_' + (generatedQuestionId) + '" class="deleteOption">' +
                                                        '<a id="addOptionButton_' + (generatedQuestionId) + '" onclick="addOption(this)" class="btn-floating waves-effect waves-light blue">'+
                                                        '<i class="material-icons">add</i>' +
                                                        '</a>' +
                                                        '</span>';
                                    } else choixReponse =  '<span id="addOption_container_' + (generatedQuestionId) + '" class="deleteOption">' +
                                                           '<a id="addOptionButton_' + (generatedQuestionId) + '" onclick="addOption(this)" class="btn-floating waves-effect waves-light blue">'+
                                                           '<i class="material-icons">add</i>' +
                                                           '</a>' +
                                                           '</span>';
                                } else {
                                    if (choix_reponse != 0) {
                                        choixReponse = '<span id="deleteOption_container_' + (generatedQuestionId) + '" class="deleteOption">' +
                                                        '<a class="btn-floating waves-effect waves-light red" onclick="deleteOption(this)">'+
                                                        '<i class="material-icons">delete</i>' +
                                                        '</a>' +
                                                        '</span>';
                                    } else choixReponse = "";
                                }

                                input +=
                                    '<div id="optionRadio_' + (generatedOptionId) +'">' +
                                        '<label style="margin-bottom: 11.5px">' +
                                            '<input name="group1" type="Radio" disabled/>' +
                                            '<span>' +
                                                '<input id="option_' + (generatedOptionId) + '_' + (generatedQuestionId) +'" class="input-field form-control" type="text" name="fname" placeholder="Option" value="'+ data.questions[question].choix_reponse[choix_reponse].choix_reponse +'">'+
                                            '</span>' +
                                        '</label>' +
                                        choixReponse +
                                    '</div>';
                            }
                        
                          generatedOptionId++;  
                        }
                        break;
                    case "Checkbox":
                        input = "";
                        for (choix_reponse in data.questions[question].choix_reponse) {
                            if (data.questions[question].type == 'Checkbox') {

                                if ((data.questions[question].choix_reponse.length - 1) == choix_reponse) {
                                    if (choix_reponse != 0) {
                                        choixReponse = '<span id="deleteOption_container_' + (generatedQuestionId) + '" class="deleteOption">' +
                                                        '<a class="btn-floating waves-effect waves-light red" onclick="deleteOption(this)">'+
                                                        '<i class="material-icons">delete</i>' +
                                                        '</a>' +
                                                        '</span>' +
                                                        '<span id="addOption_container_' + (generatedQuestionId) + '" class="deleteOption">' +
                                                        '<a id="addOptionButton_' + (generatedQuestionId) + '" onclick="addOption(this)" class="btn-floating waves-effect waves-light blue">'+
                                                        '<i class="material-icons">add</i>' +
                                                        '</a>' +
                                                        '</span>';
                                    } else choixReponse =  '<span id="addOption_container_' + (generatedQuestionId) + '" class="deleteOption">' +
                                                           '<a id="addOptionButton_' + (generatedQuestionId) + '" onclick="addOption(this)" class="btn-floating waves-effect waves-light blue">'+
                                                           '<i class="material-icons">add</i>' +
                                                           '</a>' +
                                                           '</span>';
                                } else {
                                     if (choix_reponse != 0) {
                                        choixReponse = '<span id="deleteOption_container_' + (generatedQuestionId) + '" class="deleteOption">' +
                                                        '<a class="btn-floating waves-effect waves-light red" onclick="deleteOption(this)">'+
                                                        '<i class="material-icons">delete</i>' +
                                                        '</a>' +
                                                        '</span>';
                                    } else choixReponse = "";
                                }


                                console.log(data.questions[question]);
                                input +=
                                    '<div id="optionCheckbox_' + (generatedOptionId) +'">' +
                                        '<label style="margin-bottom: 11.5px">' +
                                            '<input name="group1" type="Checkbox" disabled/>' +
                                            '<span>' +
                                                '<input id="option_' + (generatedOptionId) + '_' + (generatedQuestionId) +'" class="input-field form-control" type="text" name="fname" placeholder="Option" value="'+ data.questions[question].choix_reponse[choix_reponse].choix_reponse +'">'+
                                            '</span>' +
                                        '</label>' +
                                        choixReponse +
                                    '</div>';
                            }

                            generatedOptionId++; 
                        }
                        break;
                    case "Select":
                        input = "";
                        for (choix_reponse in data.questions[question].choix_reponse) {
                            if (data.questions[question].type == 'Select') {

                                if ((data.questions[question].choix_reponse.length - 1) == choix_reponse) {
                                    if (choix_reponse != 0) {
                                        choixReponse = '<span id="deleteOption_container_' + (generatedQuestionId) + '" class="deleteOption">' +
                                                        '<a class="btn-floating waves-effect waves-light red" onclick="deleteOption(this)">'+
                                                        '<i class="material-icons">delete</i>' +
                                                        '</a>' +
                                                        '</span>' +
                                                        '<span id="addOption_container_' + (generatedQuestionId) + '" class="deleteOption">' +
                                                        '<a id="addOptionButton_' + (generatedQuestionId) + '" onclick="addOption(this)" class="btn-floating waves-effect waves-light blue">'+
                                                        '<i class="material-icons">add</i>' +
                                                        '</a>' +
                                                        '</span>';
                                    } else choixReponse =  '<span id="addOption_container_' + (generatedQuestionId) + '" class="deleteOption">' +
                                                           '<a id="addOptionButton_' + (generatedQuestionId) + '" onclick="addOption(this)" class="btn-floating waves-effect waves-light blue">'+
                                                           '<i class="material-icons">add</i>' +
                                                           '</a>' +
                                                           '</span>';
                                } else {
                                     if (choix_reponse != 0) {
                                        choixReponse = '<span id="deleteOption_container_' + (generatedQuestionId) + '" class="deleteOption">' +
                                                        '<a class="btn-floating waves-effect waves-light red" onclick="deleteOption(this)">'+
                                                        '<i class="material-icons">delete</i>' +
                                                        '</a>' +
                                                        '</span>';
                                    } else choixReponse = "";
                                }


                                console.log(data.questions[question]);
                                input +=
                                    '<div id="optionSelect_' + (++generatedOptionId) +'">' +
                                    '<label>' +
                                        '<span>' +
                                            '<input id="option_' + (generatedOptionId) + '_' + (generatedQuestionId) +'" class="input-field form-control" type="text" name="fname" placeholder="Option" value="'+ data.questions[question].choix_reponse[choix_reponse].choix_reponse +'">'+
                                        '</span>' +
                                    '</label>' +
                                    choixReponse +
                                '</div>';
                            }

                            generatedOptionId++; 
                        }
                        break;
                    case "File":
                        let file;

                        if (data.questions[question].fileUploaded != null) {
                            file = data.questions[question].fileUploaded.substring(13, data.questions[question].fileUploaded.length);
                        } else file = "";

                        input = '<div class="file-field input-field">' +
                                '<div class="btn btn-primary">' +
                                    '<span>Téléverser</span>'+
                                    '<input type="file">'+
                                '</div>' +
                                '<div class="file-path-wrapper">' +
                                    '<input class="file-path validate" type="text" value="'+ file +'">' +
                                '</div>' +
                            '</div>' +
                            '<div class="file-field input-field">' +
                                '<div class="btn" disabled>' +
                                    '<span>Télécharger</span>'+
                                    '<input type="file">'+
                                '</div>' +
                                '<div class="file-path-wrapper">' +
                                    '<input class="file-path validate" type="text" disabled>' +
                                '</div>' +
                            '</div>';
                        break;
                    default:
                        alert('erreur');
                        break;

                }

                if ((data.questions.length - 1) == question) {
                    if (question != 0) {
                        button = '<a id="deleteQuestion" class="fontButton ml-auto" href="#" onClick="deleteQuestion(this)"><i class="fa fa-trash" aria-hidden="true"></i></a>' +
                                 '<a id="addQuestion" class="fontButton" href="#" onClick="addQuestion()"><i class="fa fa-plus" aria-hidden="true"></i></a>';
                    } else button = '<a id="addQuestion" class="fontButton ml-auto" href="#" onClick="addQuestion()"><i class="fa fa-plus" aria-hidden="true"></i></a>';
                } else {
                    if (question != 0) {
                         button = '<a id="deleteQuestion" class="fontButton ml-auto" href="#" onClick="deleteQuestion(this)"><i class="fa fa-trash" aria-hidden="true"></i></a>';
                    }
                }

                toappend +=
                    '<div id="question_' + (generatedQuestionId) + '" class="row">' +
                                        '<div class="col-md-12 align-center">' +
                                            '<div class="form-group card-background">' +
                                                '<div class="row">' +
                                                    '<div class="col-md-8">' +
                                                        '<input id="questionTitle_' + (generatedQuestionId) + '" class="input-field form-control" type="text" name="fname" placeholder="Question..." value="'+ data.questions[question].question +'">' +
                                                    '</div>' +
                                                    '<div class="col-md-4">' +
                                                        '<div class="input-field">' +
                                                            '<select class="question-type" id="optionSelector_' + (generatedQuestionId) + '" onChange="setQuestionType(this)" value="Nud">' +
                                                                '<option value="ShortText">Réponse courte</option>' +
                                                                '<option value="LongText">Réponse longue</option>' +
                                                                '<option value="Nud">Numérique</option>' +
                                                                '<option value="Radio">Choix multiples</option>' +
                                                                '<option value="Checkbox">Cases à cocher</option>' +
                                                                '<option value="Select">Liste déroulante</option>' +
                                                                '<option value="Date">Date</option>' +
                                                                '<option value="File">Fichier</option>' +
                                                            '</select>' +
                                                        '</div>' +
                                                    '</div>' +
                                                '</div>' +
                                                '<div id="container_' + (generatedQuestionId) + '" class="row">' +
                                                    '<div class="col-md-12">' +
                                                       input +
                                                    '</div>' +
                                                '</div>' +

                                                '<div class="toolButton row">' +
                                                    button +
                                                '</div>' +
                                            '</div>' +
                                        '</div>' +
                                '</div>';

                input = "";
                generatedQuestionId++;
            }

            $('#form').append(toappend);
            $('#form').append('<div id="saveButton" class="text-center"><button class="btn btn-primary center-block" type="button" onclick="saveForm()">Enregistrer</button></div>');
            $('select').formSelect();

            generatedQuestionId = 1;
            for (question in data.questions) {
                var select = $('#optionSelector_' + (generatedQuestionId++));
                select.find('option[value="'+ data.questions[question].type +'"]').prop('selected', true);
                select.formSelect();
             }



/*
            
*/






            //$('#titre_form').attr("placeholder", data.titre);
            //alert(data.titre)
        }
    });

});
