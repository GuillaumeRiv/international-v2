var errorAlert = $("#errorAlert");
errorAlert.hide();

var tbCode = $('#tbCode');
var tbPrenom = $('#tbPrenom');
var tbNom = $('#tbNom');
var tbCourriel = $('#tbCourriel');
var tbPassword = $('#tbPassword');
var tbConfirmPassword = $('#tbConfirmPassword');

var btCreateAccount = $('#btCreateAccount');
var form = $('frm_register');


tbCode.change(function(){
checkCode();
});


tbPrenom.change(function(){
  checkPrenom();
})
tbNom.change(function(){
  checkNom();
})

tbCourriel.change(function(){
checkCourriel();
});


tbPassword.change(function() {
checkPassword();
});


tbConfirmPassword.change(function(){
  checkConfirmPassword();
});


//TODO
//FAIRE LA VALIDATION QUE LE CODE DE VALIDATION ENTRER EXISTE
function checkCode()
{
  if(tbCode.val().trim() == "")
  {
    tbCode.removeClass("is-valid");
    tbCode.addClass("is-invalid");
    return false;
  }

    tbCode.removeClass("is-invalid");
    tbCode.addClass("is-valid");
    return true;

}

function checkPrenom()
{
  if(tbPrenom.val().trim() =="")
  {
    tbPrenom.removeClass("is-valid");
    tbPrenom.addClass("is-invalid");
    return false;
  }

      tbPrenom.removeClass("is-invalid");
    tbPrenom.addClass("is-valid");
    return true;

}

function checkNom()
{
  if(tbNom.val().trim() =="")
  {
    tbNom.removeClass("is-valid");
    tbNom.addClass("is-invalid");
    return false;
  }

      tbNom.removeClass("is-invalid");
    tbNom.addClass("is-valid");
    return true;

}

//TODO
//FAIRE LA VALIDATION SI LE Courriel EXISTE
function checkCourriel()
{
  if(tbCourriel.val().trim() =="")
  {
    tbCourriel.removeClass("is-valid");
    tbCourriel.addClass("is-invalid");
    return false;
  }

      tbCourriel.removeClass("is-invalid");
    tbCourriel.addClass("is-valid");
    return true;

}

function checkPassword()
{
    if(tbPassword.val().trim() =="")
  {
    tbPassword.removeClass("is-valid");
    tbPassword.addClass("is-invalid");
    return false;
  }

    tbPassword.removeClass("is-invalid");
    tbPassword.addClass("is-valid");
    return true;

}

function checkConfirmPassword()
{
    if((tbConfirmPassword.val().trim() !="")&&(tbPassword.val() == tbConfirmPassword.val()))
    {
      tbConfirmPassword.removeClass("is-invalid");
      tbConfirmPassword.addClass("is-valid");
      return true;
    }

      tbConfirmPassword.removeClass("is-valid");
      tbConfirmPassword.addClass("is-invalid");
      return false;
}

function resetCourrielAndPassword()
{
  $('#tbCourriel').val('');
  tbCourriel.removeClass("is-valid");
  tbCourriel.addClass("is-invalid");

  $('#tbPassword').val('');
  tbPassword.removeClass("is-valid");
  tbPassword.addClass("is-invalid");

  $('#tbConfirmPassword').val('');
  tbConfirmPassword.removeClass("is-valid");
  tbConfirmPassword.addClass("is-invalid");

}

function isEmail(email)
{
  var pat = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  return (pat.test(email));
}


$('#frm_register').submit(function(){
      errorAlert.hide();
      var prenom = Fonction.escapeHtml($('#tbPrenom').val());
      var nom = Fonction.escapeHtml($('#tbNom').val());
			var courriel = Fonction.escapeHtml($('#tbCourriel').val());
			var passwd = Fonction.escapeHtml($('#tbPassword').val());
			var passConf = Fonction.escapeHtml($('#tbConfirmPassword').val());
			var code = Fonction.escapeHtml($('#tbCode').val());



       if( !checkCode()||!checkPrenom()||!checkNom()||!checkCourriel()||!checkPassword()||!checkConfirmPassword() )
         {
                  checkCode();
                  checkPrenom();
                  checkNom();
                  checkCourriel();
                  checkPassword();
                  checkConfirmPassword();
                  event.preventDefault();
                  event.stopPropagation();
                  return false;
         }
         alert(courriel);
                        $.post('php/register.php',{courriel:courriel, prenom:prenom, nom:nom, passwd:passwd, passConf:passConf, code:code},function(data){
                          if (data == "success"){
                            $('#tbPrenom').val('');
                            $('#tbNom').val('');
                            $('#tbCourriel').val('');
                            $('#tbPassword').val('');
                            $('#tbConfirmPassword').val('');
                            $('#tbCode').val('');
                            window.location = "connection.php";
                          }
                          else if (data == "error"){
                                $('#tbCode').val('');
                                tbCode.removeClass("is-valid");
                                tbCode.addClass("is-invalid");

                          }
                          else if(data == "errorUserExists"){
                                resetCourrielAndPassword();
                                alert("Cette adresse courriel est déjà utilisé");
                          }
                          else {
                              errorAlert.show();
                          }
                });
  return false;
});
