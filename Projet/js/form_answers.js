$( document ).ready(function() {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const idForm = urlParams.get('idForm');
    const idUser = urlParams.get('idUser');
    const idDemande = urlParams.get('idDemande');

    $.ajax(
        {
            type: "POST",
            url: 'php/form_creation/ajaxForm_answers.php',
            data: {'idForm': idForm, 'idUser': idUser, 'idDemande': idDemande},
            dataType: 'json',
            error: function (result) {
                console.log(result.responseText);
            },
            success: function (data) {
                console.log(data);

                $('#titre_form').append(data.titre);
                $('#desc_form').append(data.description);

                var toappend = "";
                var input = "";

                for (question in data.questions) {

                    switch (data.questions[question].type) {
                        case "ShortText":
                            input = '<p>' + data.questions[question].reponse + '</p>';
                            break;
                        case "LongText":
                            input = '<p>' + data.questions[question].reponse + '</p>';
                            break;
                        case "Nud":
                            input = '<p>' + data.questions[question].reponse + '</p>';
                            break;
                        case "Radio":
                            input = '<p>' + data.questions[question].reponse + '</p>';
                            break;
                        case "Checkbox":
                            input = '<p>' + data.questions[question].reponse + '</p>';
                            break;
                        case "Select":
                            input =
                                '<div class="input-field col s12">' +
                                    '<p>' + data.questions[question].reponse + '</p>' +
                                '</div>';
                            break;
                        case "Date":
                            input = '<p>' + data.questions[question].reponse + '</p>';
                            break;
                            case "File":
                            if (data.questions[question].fileUploaded != null) {
                                file =
                                    '<div class="btn btn-primary" style = "margin-bottom:10px;padding:.375rem .75rem; height: 3rem;line-height: 3rem;">' +
                                        "<a style='color:white' href='/international-v2/uploaded_files/" + data.questions[question].fileUploaded + "' download='/international-v2/uploaded_files/" + data.questions[question].fileUploaded + "'>" + ' Télécharger </a>' +
                                    "</div>"
                            }
                            else {
                                file = "";
                            }

                            input =
                                file +
                                '<div class="file-field input-field">' +
                                    '<div class="btn btn-primary">' +
                                        '<span>Téléverser</span>'+
                                        '<input name='+ data.questions[question].id_question +' type="file">'+
                                    '</div>' +
                                    '<div class="file-path-wrapper">' +
                                        '<input name='+ data.questions[question].id_question +' class="file-path validate" type="text">' +
                                    '</div>' +
                                '</div>';
                            break;
                        default:
                            alert('erreur');
                            break;
                    }

                    toappend +=
                        '<div class="row">' +
                            '<div class="col-md-12 align-center">' +
                                '<div class="form-group card-background">' +

                                    '<div class="row">' +
                                        '<div class="col-md-12">' +
                                            '<h4>'+data.questions[question].question+'</h4>' +
                                        '</div>' +
                                    '</div>' +

                                    '<div class="row">' +
                                        '<div class="col-md-12">' +
                                            input +
                                        '</div>' +
                                    '</div>' +

                                '</div>' +
                            '</div>' +
                        '</div>'

                    input = "";
                }
                $('#form').append(toappend);
            }
        });
});
