var cbPolitiques = $("#cbPolitiques");
var cbReglements = $("#cbReglements");
var btConfirmerEngagement = $("#btConfirmerEngagement");
var btAnnulerEngagement = $("#btAnnulerEngagement");
var tbNom = $("#tbNom");
var tbPrenom = $("#tbPrenom");
var tbCourriel = $("#tbCourriel");
var tbSexe = $("#tbSexe");
var tbDateNaissance = $("#tbDateNaissance");
var compagnieassmal = $("#compagnieassmal");
var numassmal = $("#numassmal");
var adressecompagniemal = $("#adressecompagniemal");
var telcompagniemal = $("#telcompagniemal");
var telurgcompagniemal = $("#telurgcompagniemal");
var courrielcompagniemal = $("#courrielcompagniemal");
var compagnieassbag = $("#compagnieassbag");
var numassbag = $("#numassbag");
var adressecompagniebag = $("#adressecompagniebag");
var telcompagniebag = $("#telcompagniebag");
var courrielcompagniebag = $("#courrielcompagniebag");
var adresseamb = $("#adresseamb");
var telamb = $("#telamb");
var courrielamb = $("#courrielamb");


var maintenant = new Date();
var dd = maintenant.getDate();
var mm = maintenant.getMonth()+1;
var yyyy = maintenant.getFullYear();
if(dd<10){
  dd='0'+dd
}
if(mm<10){
  mm='0'+mm
}
maintenant = yyyy+'-'+mm+'-'+dd;
document.getElementById("tbDateNaissance").setAttribute("max", maintenant);



function checkPolitiques(){
  if (cbPolitiques.is(':checked')){
    $("#cbPolitiques").removeClass("is-invalid");
    $("#cbPolitiques").addClass("is-valid");
    return true;
  }
  $("#cbPolitiques").removeClass("is-valid");
  $("#cbPolitiques").addClass("is-invalid");
  return false;
}

function checkReglements(){
  if (cbReglements.is(':checked')){
    $("#cbReglements").removeClass("is-invalid");
    $("#cbReglements").addClass("is-valid");
    return true;
  }
  $("#cbReglements").removeClass("is-valid");
  $("#cbReglements").addClass("is-invalid");
  return false;
}





function checkEngagement(){

  if (cbPolitiques.is(':checked') && cbReglements.is(':checked')){

    $("#submit").prop('disabled', false);
    btConfirmerEngagement.prop('disabled', false);
  }
  else {btConfirmerEngagement.prop('disabled', true);
  $("#submit").prop('disabled', false);

}

}

function uncheckEngagement(){
  if (cbPolitiques.is('checked'))
  cbPolitiques.prop('checked', false);
  if (cbReglements.is('checked'))
  cbReglements.prop('checked', false);

  btConfirmerEngagement.prop('disabled', true);
}


function checkDateNaissance(){

  if(tbDateNaissance.val() != "" && tbDateNaissance.val() <= maintenant){

    tbDateNaissance.removeClass("is-invalid");
    tbDateNaissance.addClass("is-valid");
    return true;
  }
  tbDateNaissance.removeClass("is-valid");
  tbDateNaissance.addClass("is-invalid");
  return false;
}

/* COMPAGNIE ASSURANCE MALADIE */
function checkNomAssMal(){
  if($("#compagnieassmal").val() != "" ){
    $("#compagnieassmal").removeClass("is-invalid");
    $("#compagnieassmal").addClass("is-valid");
    return true;
  }
  $("#compagnieassmal").removeClass("is-valid");
  $("#compagnieassmal").addClass("is-invalid");
  return false;
}

function checkNumAssMal(){
  if($("#numassmal").val() != "" ){
    $("#numassmal").removeClass("is-invalid");
    $("#numassmal").addClass("is-valid");
    return true;
  }
  $("#numassmal").removeClass("is-valid");
  $("#numassmal").addClass("is-invalid");
  return false;
}

function checkAdresseAssMal(){
  if($("#adressecompagniemal").val() != "" ){
    $("#adressecompagniemal").removeClass("is-invalid");
    $("#adressecompagniemal").addClass("is-valid");
    return true;
  }
  $("#adressecompagniemal").removeClass("is-valid");
  $("#adressecompagniemal").addClass("is-invalid");
  return false;
}

function checkCourrielAssMal(){
  if($("#courrielcompagniemal").val() != "" && isEmail($("#courrielcompagniemal").val())){
    $("#courrielcompagniemal").removeClass("is-invalid");
    $("#courrielcompagniemal").addClass("is-valid");
    return true;
  }
  $("#courrielcompagniemal").removeClass("is-valid");
  $("#courrielcompagniemal").addClass("is-invalid");
  return false;
}


/* COMPAGNIE ASSURANCE BAGAGES */

function checkNomAssBag(){
  if($("#compagnieassbag").val() != "" ){
    $("#compagnieassbag").removeClass("is-invalid");
    $("#compagnieassbag").addClass("is-valid");
    return true;
  }
  $("#compagnieassbag").removeClass("is-valid");
  $("#compagnieassbag").addClass("is-invalid");
  return false;
}

function checkNumAssBag(){
  if($("#numassbag").val() != "" ){
    $("#numassbag").removeClass("is-invalid");
    $("#numassbag").addClass("is-valid");
    return true;
  }
  $("#numassbag").removeClass("is-valid");
  $("#numassbag").addClass("is-invalid");
  return false;
}

function checkAdresseAssBag(){
  if($("#adressecompagniebag").val() != "" ){
    $("#adressecompagniebag").removeClass("is-invalid");
    $("#adressecompagniebag").addClass("is-valid");
    return true;
  }
  $("#adressecompagniebag").removeClass("is-valid");
  $("#adressecompagniebag").addClass("is-invalid");
  return false;
}

function checkCourrielAssBag(){
  if($("#courrielcompagniebag").val() != "" && isEmail($("#courrielcompagniebag").val())){
    $("#courrielcompagniebag").removeClass("is-invalid");
    $("#courrielcompagniebag").addClass("is-valid");
    return true;
  }
  $("#courrielcompagniebag").removeClass("is-valid");
  $("#courrielcompagniebag").addClass("is-invalid");
  return false;
}


/* AMBASSADE CANADA */

function checkAdresseAmb(){
  if($("#adresseamb").val() != "" ){
    $("#adresseamb").removeClass("is-invalid");
    $("#adresseamb").addClass("is-valid");
    return true;
  }
  $("#adresseamb").removeClass("is-valid");
  $("#adresseamb").addClass("is-invalid");
  return false;
}


function checkCourrielAmb(){
  if($("#courrielamb").val() != "" && isEmail($("#courrielamb").val())){
    $("#courrielamb").removeClass("is-invalid");
    $("#courrielamb").addClass("is-valid");
    return true;
  }
  $("#courrielamb").removeClass("is-valid");
  $("#courrielamb").addClass("is-invalid");
  return false;
}




function checkNom(){
  if(tbNom.val() != "" && lettreSeulement(tbNom.val().trim())){
    tbNom.removeClass("is-invalid");
    tbNom.addClass("is-valid");
    return true;
  }
  tbNom.removeClass("is-valid");
  tbNom.addClass("is-invalid");
  return false;
}


function checkPrenom(){

  if(tbPrenom.val() != "" && lettreSeulement(tbPrenom.val().trim())){
    tbPrenom.removeClass("is-invalid");
    tbPrenom.addClass("is-valid");
    return true;
  }
  tbPrenom.removeClass("is-valid");
  tbPrenom.addClass("is-invalid");
  return false;

}

function checkCourriel(){

  if($("#tbCourriel").val() != "" && isEmail($("#tbCourriel").val())){
    $("#tbCourriel").removeClass("is-invalid");
    $("#tbCourriel").addClass("is-valid");
    return true;
  }
  $("#tbCourriel").removeClass("is-valid");
  $("#tbCourriel").addClass("is-invalid");
  return false;
}


function checkAdresse(e){
  if(e.val() != ""){
    e.removeClass("is-invalid");
    e.addClass("is-valid");
    return true;
  }
  else {
    e.removeClass("is-valid");
    e.addClass("is-invalid");
  }
}


function checkCoorDate(e){
  if(e.val() != ""){
    e.removeClass("is-invalid");
    e.addClass("is-valid");
    return true;
  }
  else {
    e.removeClass("is-valid");
    e.addClass("is-invalid");
  }
}


function checkTelephone(e){
  if(e.val() != "" && NumeroTelCheck(e.val().trim())){
    e.removeClass("is-invalid");
    e.addClass("is-valid");
    return true;
  }
  e.removeClass("is-valid");
  e.addClass("is-invalid");
  return false;
}

function NumeroTelCheck(tel){
  var pat = new RegExp("^[(][0-9]{3}[)][0-9]{3}[-][0-9]{4}$");
  return (pat.test(tel));

}

function isEmail(email)
{
  var pat = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  return (pat.test(email));
}

function lettreSeulement(str)
{
  var pat = new RegExp("^([a-zA-Z.,' -])*$");
  return (pat.test(str));
}

$("#tbNom").change(function(){
  checkNom();
});
$("#tbPrenom").change(function(){
  checkPrenom();
});

// TABLEAU COORDONNÉES ÉTUDIANTS
var ctr1=1;
function ajouterCoorEtu()
{
  ctr1++;


  var tr = document.createElement("TR");

  var td1 = document.createElement("TD");
  td1.style.width = "25%";
  var input1 = document.createElement("INPUT");
  input1.setAttribute("id","tbCoorEtuDu"+ctr1);
  input1.attributes.type="text";
  input1.style.width = "100%";
  input1.classList.add("inputtable");
  input1.classList.add("form-control");
  input1.classList.add("CoorEtuDu");
  input1.required = true;
  var div1 = document.createElement("DIV");
  div1.classList.add("invalid-feedback");
  div1.innerText = "Veuillez entrer une date ou supprimer la ligne";

  var td2 = document.createElement("TD");
  td2.style.width = "25%";
  var input2 = document.createElement("INPUT");
  input2.setAttribute("id","tbCoorEtuAu"+ctr1);
  input2.attributes.type="text";
  input2.style.width = "100%";
  input2.classList.add("inputtable");
  input2.classList.add("form-control");
  input2.classList.add("CoorEtuAu");
  input2.required = true;
  var div2 = document.createElement("DIV");
  div2.classList.add("invalid-feedback");
  div2.innerText = "Veuillez entrer une date ou supprimer la ligne";

  var td3 = document.createElement("TD");
  td3.style.width = "25%";
  var input3 = document.createElement("INPUT");
  input3.setAttribute("id","tbAdresseEtu"+ctr1);
  input3.attributes.type="text";
  input3.style.width = "100%";
  input3.classList.add("inputtable");
  input3.classList.add("form-control");
  input3.classList.add("AdresseEtu");
  input3.required = true;
  var div3 = document.createElement("DIV");
  div3.classList.add("invalid-feedback");
  div3.innerText = "Veuillez entrer une adresse ou supprimer la ligne";

  var td4 = document.createElement("TD");
  td4.style.width = "25%";
  var input4 = document.createElement("INPUT");
  input4.setAttribute("id","tbTelephoneEtu"+ctr1);
  input4.attributes.type="text";
  input4.setAttribute("placeholder","(123)456-7890");
  input4.style.width = "100%";
  input4.classList.add("inputtable");
  input4.classList.add("form-control");
  input4.classList.add("TelephoneEtu");
  input4.required = true;
  var div4 = document.createElement("DIV");
  div4.classList.add("invalid-feedback");
  div4.innerText = "Veuillez entrer un numéro de téléphone ou supprimer la ligne";

  td1.appendChild(input1);
  td1.appendChild(div1);
  tr.appendChild(td1);

  td2.appendChild(input2);
  td2.appendChild(div2);
  tr.appendChild(td2);

  td3.appendChild(input3);
  td3.appendChild(div3);
  tr.appendChild(td3);

  td4.appendChild(input4);
  td4.appendChild(div4);
  tr.appendChild(td4);



  document.getElementById("TBODYcooretu").appendChild(tr);

}

function supprimerCoorEtu()
{

  var nb = document.getElementById('tablecooretu').rows.length;
  if (nb > 2)
  {
    ctr1--;
    document.getElementById('tablecooretu').deleteRow(-1);
  }
}

// TABLEAU COORDONNÉES RESPONSABLES
var ctr2=1;
function ajouterCoorResp()
{
  ctr2++;


  var tr = document.createElement("TR");

  var td1 = document.createElement("TD");
  td1.style.width = "25%";
  var input1 = document.createElement("INPUT");
  input1.setAttribute("id","tbCoorRespDu"+ctr2);
  input1.attributes.type="text";
  input1.style.width = "100%";
  input1.classList.add("inputtable");
  input1.classList.add("form-control");
  input1.classList.add("CoorRespDu");
  input1.required = true;
  var div1 = document.createElement("DIV");
  div1.classList.add("invalid-feedback");
  div1.innerText = "Veuillez entrer une date ou supprimer la ligne";

  var td2 = document.createElement("TD");
  td2.style.width = "25%";
  var input2 = document.createElement("INPUT");
  input2.setAttribute("id","tbCoorRespAu"+ctr2);
  input2.attributes.type="text";
  input2.style.width = "100%";
  input2.classList.add("inputtable");
  input2.classList.add("form-control");
  input2.classList.add("CoorRespAu");
  input2.required = true;
  var div2 = document.createElement("DIV");
  div2.classList.add("invalid-feedback");
  div2.innerText = "Veuillez entrer une date ou supprimer la ligne";

  var td3 = document.createElement("TD");
  td3.style.width = "25%";
  var input3 = document.createElement("INPUT");
  input3.setAttribute("id","tbAdresseResp"+ctr2);
  input3.attributes.type="text";
  input3.style.width = "100%";
  input3.classList.add("inputtable");
  input3.classList.add("form-control");
  input3.classList.add("AdresseResp");
  input3.required = true;
  var div3 = document.createElement("DIV");
  div3.classList.add("invalid-feedback");
  div3.innerText = "Veuillez entrer une adresse ou supprimer la ligne";

  var td4 = document.createElement("TD");
  td4.style.width = "25%";
  var input4 = document.createElement("INPUT");
  input4.setAttribute("id","tbTelephoneResp"+ctr2);
  input4.attributes.type="text";
  input4.setAttribute("placeholder","(123)456-7890");
  input4.style.width = "100%";
  input4.classList.add("inputtable");
  input4.classList.add("form-control");
  input4.classList.add("TelephoneResp");
  input4.required = true;
  var div4 = document.createElement("DIV");
  div4.classList.add("invalid-feedback");
  div4.innerText = "Veuillez entrer un numéro de téléphone ou supprimer la ligne";

  td1.appendChild(input1);
  td1.appendChild(div1);
  tr.appendChild(td1);

  td2.appendChild(input2);
  td2.appendChild(div2);
  tr.appendChild(td2);

  td3.appendChild(input3);
  td3.appendChild(div3);
  tr.appendChild(td3);

  td4.appendChild(input4);
  td4.appendChild(div4);
  tr.appendChild(td4);



  document.getElementById("TBODYcoorresp").appendChild(tr);

}

function supprimerCoorResp()
{

  var nb = document.getElementById('tablecoorresp').rows.length;
  if (nb > 2)
  {
    ctr2--;
    document.getElementById('tablecoorresp').deleteRow(-1);
  }
}


// TABLEAU COORDONNÉES PROCHE
var ctr3=1;
function ajouterCoorProche()
{
  ctr3++;


  var tr = document.createElement("TR");

  var td1 = document.createElement("TD");
  td1.style.width = "25%";
  var input1 = document.createElement("INPUT");
  input1.setAttribute("id","tbCoorProDu"+ctr3);
  input1.attributes.type="text";
  input1.style.width = "100%";
  input1.classList.add("inputtable");
  input1.classList.add("form-control");
  input1.classList.add("CoorProDu");
  input1.required = true;
  var div1 = document.createElement("DIV");
  div1.classList.add("invalid-feedback");
  div1.innerText = "Veuillez entrer une date ou supprimer la ligne";

  var td2 = document.createElement("TD");
  td2.style.width = "25%";
  var input2 = document.createElement("INPUT");
  input2.setAttribute("id","tbCoorProAu"+ctr3);
  input2.attributes.type="text";
  input2.style.width = "100%";
  input2.classList.add("inputtable");
  input2.classList.add("form-control");
  input2.classList.add("CoorProAu");
  input2.required = true;
  var div2 = document.createElement("DIV");
  div2.classList.add("invalid-feedback");
  div2.innerText = "Veuillez entrer une date ou supprimer la ligne";

  var td3 = document.createElement("TD");
  td3.style.width = "25%";
  var input3 = document.createElement("INPUT");
  input3.setAttribute("id","tbAdressePro"+ctr3);
  input3.attributes.type="text";
  input3.style.width = "100%";
  input3.classList.add("inputtable");
  input3.classList.add("form-control");
  input3.classList.add("AdressePro");
  input3.required = true;
  var div3 = document.createElement("DIV");
  div3.classList.add("invalid-feedback");
  div3.innerText = "Veuillez entrer une adresse ou supprimer la ligne";

  var td4 = document.createElement("TD");
  td4.style.width = "25%";
  var input4 = document.createElement("INPUT");
  input4.setAttribute("id","tbTelephonePro"+ctr3);
  input4.attributes.type="text";
  input4.setAttribute("placeholder","(123)456-7890");
  input4.style.width = "100%";
  input4.classList.add("inputtable");
  input4.classList.add("form-control");
  input4.classList.add("TelephonePro");
  input4.required = true;
  var div4 = document.createElement("DIV");
  div4.classList.add("invalid-feedback");
  div4.innerText = "Veuillez entrer un numéro de téléphone ou supprimer la ligne";

  td1.appendChild(input1);
  td1.appendChild(div1);
  tr.appendChild(td1);

  td2.appendChild(input2);
  td2.appendChild(div2);
  tr.appendChild(td2);

  td3.appendChild(input3);
  td3.appendChild(div3);
  tr.appendChild(td3);

  td4.appendChild(input4);
  td4.appendChild(div4);
  tr.appendChild(td4);



  document.getElementById("TBODYcoorproche").appendChild(tr);

}

function supprimerCoorProche()
{

  var nb = document.getElementById('tablecoorproche').rows.length;
  if (nb > 2)
  {
    ctr3--;
    document.getElementById('tablecoorproche').deleteRow(-1);
  }
}



$("#tbNom").change(function(){
  checkNom();
});
$("#tbPrenom").change(function(){
  checkPrenom();
});
$("#tbCourriel").change(function(){
  checkCourriel();
});
$("#tbDateNaissance").change(function(){
  checkDateNaissance();
});



$("table").delegate(".CoorEtuDu", "change", function(){
  checkCoorDate($(this));

});
$("table").delegate(".CoorEtuAu", "change", function(){
  checkCoorDate($(this));

});
$("table").delegate(".AdresseEtu", "change", function(){
  checkAdresse($(this));
});
$("table").delegate(".TelephoneEtu", "change", function(){
  checkTelephone($(this));

});

$("table").delegate(".CoorRespDu", "change", function(){
  checkCoorDate($(this));

});
$("table").delegate(".CoorRespAu", "change", function(){
  checkCoorDate($(this));

});
$("table").delegate(".AdresseResp", "change", function(){
  checkAdresse($(this));
});
$("table").delegate(".TelephoneResp", "change", function(){
  checkTelephone($(this));

});
$("table").delegate(".CoorProDu", "change", function(){
  checkCoorDate($(this));

});
$("table").delegate(".CoorProAu", "change", function(){
  checkCoorDate($(this));

});
$("table").delegate(".AdressePro", "change", function(){
  checkAdresse($(this));
});
$("table").delegate(".TelephonePro", "change", function(){
  checkTelephone($(this));

});




$("#compagnieassmal").change(function(){
  checkNomAssMal();
});
$("#numassmal").change(function(){
  checkNumAssMal();
});
$("#adressecompagniemal").change(function(){
  checkAdresseAssMal();
});
$("#telcompagniemal").change(function(){
  checkTelephone($(this));
});
$("#telurgcompagniemal").change(function(){
  checkTelephone($(this));
});
$("#courrielcompagniemal").change(function(){
  checkCourrielAssMal();
});



$("#compagnieassbag").change(function(){
  checkNomAssBag();
});
$("#numassbag").change(function(){
  checkNumAssBag();
});
$("#adressecompagniebag").change(function(){
  checkAdresseAssBag();
});
$("#telcompagniebag").change(function(){
  checkTelephone($(this));
});
$("#courrielcompagniebag").change(function(){
  checkCourrielAssBag();
});


$("#adresseamb").change(function(){
  checkAdresseAmb();
});
$("#telamb").change(function(){
  checkTelephone($(this));
});
$("#courrielamb").change(function(){
  checkCourrielAmb();
});

$("#btConfirmerEngagement").click(function(){
  $("#submit").prop("disabled", false);
});


$("#form").submit(function(){



  var arCoorDuEtu = [], arCoorAuEtu = [], arCoorAdEtu = [], arCoorTelEtu = [];
  var arCoorDuRes = [], arCoorAuRes = [], arCoorAdRes = [], arCoorTelRes = [];
  var arCoorDuPro = [], arCoorAuPro = [], arCoorAdPro = [], arCoorTelPro = [];
  var arVaccin = [], arValeurVaccin = [];

  for ( var ctrEtu = 1 ; ctrEtu <= ctr1; ctrEtu++){

    if ( checkCoorDate($("#tbCoorEtuDu"+ctrEtu))){
      arCoorDuEtu.push($("#tbCoorEtuDu"+ctrEtu).val());}
      else{
        alert("Il y a eu une erreur lors de l'envoi de la demande. Veuillez vérifier que tous les champs soient bien remplis puis réessayer. (Coordonnées de l'étudiant)");

        event.preventDefault();
        event.stopPropagation();
return false;
      }

      if ( checkCoorDate($("#tbCoorEtuAu"+ctrEtu))){
        arCoorAuEtu.push($("#tbCoorEtuAu"+ctrEtu).val());}
        else{
          alert("Il y a eu une erreur lors de l'envoi de la demande. Veuillez vérifier que tous les champs soient bien remplis puis réessayer. (Coordonnées de l'étudiant)");

          event.preventDefault();
          event.stopPropagation();
return false;
        }

        if ( checkAdresse($("#tbAdresseEtu"+ctrEtu))){
          arCoorAdEtu.push($("#tbAdresseEtu"+ctrEtu).val());}
          else{
            alert("Il y a eu une erreur lors de l'envoi de la demande. Veuillez vérifier que tous les champs soient bien remplis puis réessayer. (Coordonnées de l'étudiant)");

            event.preventDefault();
            event.stopPropagation();
return false;
          }

          if ( checkTelephone($("#tbTelephoneEtu"+ctrEtu))){
            arCoorTelEtu.push($("#tbTelephoneEtu"+ctrEtu).val());}
            else{
              alert("Il y a eu une erreur lors de l'envoi de la demande. Veuillez vérifier que tous les champs soient bien remplis puis réessayer. (Coordonnées de l'étudiant)");
              event.preventDefault();
              event.stopPropagation();
              return false;
            }

          }
          for ( var ctrResp = 1 ; ctrResp <= ctr2; ctrResp++){

            if ( checkCoorDate($("#tbCoorResDu"+ctrResp))){
              arCoorDuRes.push($("#tbCoorResDu"+ctrResp).val());}
              else{
                alert("Il y a eu une erreur lors de l'envoi de la demande. Veuillez vérifier que tous les champs soient bien remplis puis réessayer. (Coordonnées du responsable)");

                event.preventDefault();
                event.stopPropagation();
            return false;
              }
              if ( checkCoorDate($("#tbCoorResAu"+ctrResp))){
                arCoorAuRes.push($("#tbCoorResAu"+ctrResp).val());}
                else{
                  alert("Il y a eu une erreur lors de l'envoi de la demande. Veuillez vérifier que tous les champs soient bien remplis puis réessayer. (Coordonnées du responsable)");

                  event.preventDefault();
                  event.stopPropagation();
              return false;
                }
                if ( checkAdresse($("#tbAdresseResp"+ctrResp))){
                  arCoorAdRes.push($("#tbAdresseResp"+ctrResp).val());}
                  else{
                    alert("Il y a eu une erreur lors de l'envoi de la demande. Veuillez vérifier que tous les champs soient bien remplis puis réessayer. (Coordonnées du responsable)");

                    event.preventDefault();
                    event.stopPropagation();
                return false;
                  }
                  if ( checkAdresse($("#tbTelephoneResp"+ctrResp))){
                      arCoorTelRes.push($("#tbTelephoneResp"+ctrResp).val());}
                    else{
                      alert("Il y a eu une erreur lors de l'envoi de la demande. Veuillez vérifier que tous les champs soient bien remplis puis réessayer. (Coordonnées du responsable)");

                      event.preventDefault();
                      event.stopPropagation();
                  return false;
                    }

          }
          for ( var ctrPro = 1 ; ctrPro <= ctr3; ctrPro++){
            if ( checkCoorDate($("#tbCoorProDu"+ctrPro))){
              arCoorDuPro.push($("#tbCoorProDu"+ctrPro).val());}
              else{
                alert("Il y a eu une erreur lors de l'envoi de la demande. Veuillez vérifier que tous les champs soient bien remplis puis réessayer. (Coordonnées du proche)");

                event.preventDefault();
                event.stopPropagation();
            return false;
              }

              if ( checkCoorDate($("#tbCoorProAu"+ctrPro))){
                arCoorAuPro.push($("#tbCoorProAu"+ctrPro).val());}
                else{
                  alert("Il y a eu une erreur lors de l'envoi de la demande. Veuillez vérifier que tous les champs soient bien remplis puis réessayer. (Coordonnées du proche)");

                  event.preventDefault();
                  event.stopPropagation();
              return false;
                }

                if ( checkCoorDate($("#tbAdressePro"+ctrPro))){
                    arCoorAdPro.push($("#tbAdressePro"+ctrPro).val());}
                  else{
                    alert("Il y a eu une erreur lors de l'envoi de la demande. Veuillez vérifier que tous les champs soient bien remplis puis réessayer. (Coordonnées du proche)");

                    event.preventDefault();
                    event.stopPropagation();
                return false;
                  }
                  if ( checkCoorDate($("#tbTelephonePro"+ctrPro))){
                        arCoorTelPro.push($("#tbTelephonePro"+ctrPro).val());}
                    else{
                      alert("Il y a eu une erreur lors de l'envoi de la demande. Veuillez vérifier que tous les champs soient bien remplis puis réessayer. (Coordonnées du proche)");

                      event.preventDefault();
                      event.stopPropagation();
                  return false;
                    }
          }

          var ctrTotVac = $("#nbrvaccin").val();
          for ( var ctrvac = 1 ; ctrvac <= ctrTotVac; ctrvac++){
                      if($("#vaccin"+ctrvac+"Oui").is(':checked')){
                      arVaccin.push($("#vaccin"+ctrvac+"Oui").attr("name"));
                      arValeurVaccin.push($("#vaccin"+ctrvac+"Oui").val());
                    }
                      if($("#vaccin"+ctrvac+"Non").is(':checked')){
                      arVaccin.push($("#vaccin"+ctrvac+"Non").attr("name"));
                      arValeurVaccin.push($("#vaccin"+ctrvac+"Non").val());
                    }

          }


          var reglements = $("#cbReglements").val();
          var politiques = $("#cbPolitiques").val();

          if ($("#taSante").val() == ""){
            $("#taSante").val("Aucun") ;
          }
          if ($("#taMedications").val() == ""){
            $("#taMedications").val("Aucune");
          }
          if ($("#taAllergies").val() == ""){
            $("#taAllergies").val("Aucune");
          }
alert('je suis avant le post');
alert(arVaccin.toString());
alert(arValeurVaccin.toString());

          $.post("php/requetesqlFormulaireEtudiants.php", {
            arCoorDuEtu:arCoorDuEtu.toString(),
            arCoorAuEtu:arCoorAuEtu.toString(),
            arCoorAdEtu:arCoorAdEtu.toString(),
            arCoorTelEtu:arCoorTelEtu.toString(),
            arCoorDuRes:arCoorDuRes.toString(),
            arCoorAuRes:arCoorAuRes.toString(),
            arCoorAdRes:arCoorAdRes.toString(),
            arCoorTelRes:arCoorTelRes.toString(),
            arCoorDuPro:arCoorDuPro.toString(),
            arCoorAuPro:arCoorAuPro.toString(),
            arCoorAdPro:arCoorAdPro.toString(),
            arCoorTelPro:arCoorTelPro.toString(),
            nom:tbNom.val(),
            prenom:tbPrenom.val(),
            courriel:tbCourriel.val(),
            sexe:tbSexe.val(),
            datenaissance:tbDateNaissance.val(),
            reglements:reglements,
            politiques:politiques,
            compagnieassmal:compagnieassmal.val(),
            numassmal:numassmal.val(),
            adressecompagniemal:adressecompagniemal.val(),
            telcompagniemal:telcompagniemal.val(),
            telurgcompagniemal:telurgcompagniemal.val(),
            courrielcompagniemal:courrielcompagniemal.val(),
            compagnieassbag:compagnieassbag.val(),
            numassbag:numassbag.val(),
            adressecompagniebag:adressecompagniebag.val(),
            telcompagniebag:telcompagniebag.val(),
            courrielcompagniebag:courrielcompagniebag.val(),
            adresseamb:adresseamb.val(),
            telamb:telamb.val(),
            courrielamb:courrielamb.val(),
            taSante:$("#taSante").val(),
            taMedications:$("#taMedications").val(),
            taAllergies:$("#taAllergies").val(),
            arVaccin:arVaccin.toString(),
            arValeurVaccin:arValeurVaccin.toString()
            ,},function(data){

              if (data == "success")
              {
                alert("Votre demande a bien été envoyée.");
              }
              else {
                alert(data);
                alert("Il y a eu une erreur lors de l'envoi de la demande. Veuillez vérifier que tous les champs soient bien remplis puis réessayer. (RETOUR PHP)");
              }
            });
            return false;
          } ) ;
