var selectedProject = "";
var selectedUser = "";
var selectedDemande = "";
var titreProj = "";
var userType = "";

$(document).ready(function() {

    $.ajax({
      url: 'php/gestion_demandes/getusertype.php',
      type: 'POST',
      success: function(data){
        userType = data;
      }
    });

  function remplirTbl(arr){
    $("#tblDemandes tbody").html("");

    if(arr.destination.length == 0)
    {
      var tr = $("<tr></tr>");
      var th = $("<td scope='row' colspan='4'></th>").text("Aucune demande trouvé.");

      tr.append(th);

      $("#tblDemandes tbody").append(tr);
    }
    else
      {
        for(var ctr = 0; ctr < arr.destination.length; ctr++)
        {
          var tr = $("<tr></tr>");
          var th = $("<th scope='row'></th>").text(ctr+1);
          var td = $("<td style='width: 20%'></td>").text(arr.destination[ctr]);
          var td1 = $("<td style='width: 20%'></td>").text(arr.user[ctr]);
          var td2 = $("<td style='width: 45%'></td>").attr('id', arr.id[ctr]).attr('class', arr.destination[ctr]).attr('data-user', arr.id_user[ctr]).attr('data-demande', arr.id_demande[ctr]);
          var td3 = $("<td style='width: 15%'></td>");
          td3.css("text-align", "center");

            switch(arr.etat[ctr])
            {
              case '0' :
                  td3.text("En Attente");
                  td3.css("background-color","gray");
                  td3.css("color","white");
                  break;
              case '1' :
                  td3.text("Accepté");
                  td3.css("background-color","#28a745");
                  td3.css("color","white");
                  break;
              case '2' :
                  td3.text("Refusé");
                  td3.css("background-color","#dc3545");
                  td3.css("color","white");
                  break;
              case '3' :
                  td3.text("Non-envoyé");
                  break;
              default :
                  td3.text("État de la demande inconnu");
                  break;

            }


          var bt0 = $("<input class='btn btn-primary BTview' type='button' value='Voir la demande'>");

          if(userType == "Administrateur")
          {
            var bt = $("<input id='butAccept"+ctr+"' class='btn btn-success BTacccept' type='button' value='Accepter la demande' data-toggle='modal' data-target='#acceptModal'>");
            if(arr.etat[ctr] != 2)
            {
               var bt1 = $("<input id='butRefus"+ctr+"' class='btn btn-danger BTrefuse' type='button' value='Refuser la demande' data-toggle='modal' data-target='#refuseModal'>");
            }
            else if(arr.etat[ctr] == 2)
            {
              var bt1 = $("<input id='butRaisonRefus"+ctr+"' class='btn btn-danger BTraisonrefuse' type='button' value='Raison de refus' data-toggle='modal' data-target='#raisonrefuseModal'>");
            }

          td2.append(bt0, bt, bt1);

          tr.append(th, td, td1, td2,td3);
          $("#tblDemandes tbody").append(tr);

                if(arr.etat[ctr] == 1)
          {
              $("#butAccept"+ctr).hide();
              $("#butRefus"+ctr).hide();
          }


          }
          else if(userType == "Enseignant")
          {
            if(arr.etat[ctr] == 2)
              {
                var bt = $("<input id='butRaisonRefus"+ctr+"' class='btn btn-danger BTraisonrefuse' type='button' value='Raison de refus' data-toggle='modal' data-target='#raisonrefuseModal'>");
                td2.append(bt0, bt);
              }
              else
              {
                td2.append(bt0);
              }

          tr.append(th, td, td1, td2,td3);
          $("#tblDemandes tbody").append(tr);
          }

        }
    }

  }


  function pulldemandes(){
    $.ajax({
      url: 'php/gestion_demandes/pulldemande.php',
      type: 'POST',
      success: function(data){
        var reponse = $.parseJSON(data);
        remplirTbl(reponse);
        $('#tblDemandes').DataTable({
          "oLanguage":{
  "sEmptyTable":     "Aucune donnée disponible dans le tableau",
  "sInfo":           "Affichage de l'élément _START_ à _END_ sur _TOTAL_ éléments",
  "sInfoEmpty":      "Affichage de l'élément 0 à 0 sur 0 élément",
  "sInfoFiltered":   "(filtré à partir de _MAX_ éléments au total)",
  "sInfoPostFix":    "",
  "sInfoThousands":  ",",
  "sLengthMenu":     "Afficher _MENU_ éléments",
  "sLoadingRecords": "Chargement...",
  "sProcessing":     "Traitement...",
  "sSearch":         "Recherche :",
  "sZeroRecords":    "Aucun élément correspondant trouvé",
   "oPaginate": {
    "sFirst":    "Premier",
    "sLast":     "Dernier",
    "sNext":     "Suivant",
    "sPrevious": "Précédent"
  },
  "oAria": {
    "sSortAscending":  ": activer pour trier la colonne par ordre croissant",
    "sSortDescending": ": activer pour trier la colonne par ordre décroissant"
  },
  "select": {
          "rows": {
            "_": "%d lignes sélectionnées",
            "0": "Aucune ligne sélectionnée",
            "1": "1 ligne sélectionnée"
          }
  }
}
});
      }
    });
  }

  pulldemandes();

    $("#acceptBt").click(function(event) {
    acceptDemande();


  });

$("#refuseBt").click(function(event) {
    refusDemande();
});

$('#btRefCancel').click(function(){
  $('#msgRefus').val("");
});

  function acceptDemande()
  {
    var codeProj = Fonction.random_password_generate(5, 7);
        $.ajax({
      url: 'php/gestion_demandes/acceptdemande.php',
      type: 'POST',
      data: {id: selectedProject, codeProj: codeProj},
      success: function(data){
        if (data == "success"){
          pulldemandes();
        }
        else{
        alert("erreur lors de l'acceptation de la demande " + data);
        }
      }
    });
  }

    function refusDemande() {
      var refus_message = $('#msgRefus').val();

      $.ajax({
          url: 'php/gestion_demandes/refusdemande.php',
          type: 'POST',
          data: {id_demande: selectedProject, msgrefus: refus_message},
          success: function(data){
            if (data == "success"){
              pulldemandes();
              $('#msgRefus').val("");
            }
            else
            {
             $('#msgRefus').val("");
            alert("erreur lors du refus de la demande");
            }
          }
      });
  }

  function voirDemande()
  {
    window.location.href='form_answers.php?idForm=' + selectedProject + "&idUser=" + selectedUser + "&idDemande=" + selectedDemande;
  }

  function setReason()
  {
          $.ajax({
      url: 'php/gestion_demandes/getraison.php',
      type: 'POST',
      data: {id: selectedProject},
      success: function(data){
        if(data != "error")
        {
            $('#raisonrefuseModal').find(".modal-body").html("<textarea id='msgRefus' rows='7' cols='55'>" + data +"</textarea>");
        }
        else{
          alert("Erreur survenu lors de la visualisation de la demande.");
        }
      }
    });
  }

  function fillLocalStorage(response)
  {
    localStorage.setItem("projetDocsID", response.projetDocsID);
    localStorage.setItem("nom", response.Nom);
    localStorage.setItem("prenom", response.Prenom);
    localStorage.setItem("adresse", response.adresse);
    localStorage.setItem("programme", response.programme);
    localStorage.setItem("destination", response.destination);
    localStorage.setItem("dateD", response.dateD);
    localStorage.setItem("dateR", response.dateR);
    localStorage.setItem("precisionDestination", response.precisionDestination);
    localStorage.setItem("destination", response.destination);
    localStorage.setItem("type", response.type);
    localStorage.setItem("description", response.description);
    localStorage.setItem("etudiants", response.etudiants);
    localStorage.setItem("liberation", response.liberation);
    localStorage.setItem("financement", response.financement);
    localStorage.setItem("recrutement", response.recrutement);
    localStorage.setItem("strategies", response.strategies);
    localStorage.setItem("activites", response.activites);
    localStorage.setItem("dateactivites", response.dateactivites);
    localStorage.setItem("acc_nom", response.acc_nom);
    localStorage.setItem("acc_prenom", response.acc_prenom);
    localStorage.setItem("acc_telephone", response.acc_telephone);
    localStorage.setItem("acc_courriel", response.acc_courriel);

  }

    $("tbody").delegate('.BTview', 'click', function(event) {
    selectedProject = $(event.target).parent("td").attr('id');
    selectedUser = $(event.target).parent("td").attr('data-user');
    selectedDemande = $(event.target).parent("td").attr('data-demande');
    voirDemande();
  });


    $("tbody").delegate('.BTacccept', 'click', function(event) {
    selectedProject = $(event.target).parent("td").attr('data-demande');
    titreProj =  $(event.target).parent("td").attr('class');
    $('#acceptModalLabel').text("Accepter la demande");
    $('#acceptModal').find(".modal-body").html("<p>Voulez-vouz vraiment accepter la demande '" + titreProj + "' ?</p>" +
                                                "<p> Cette action est définitive. </p>");
  });

    $("tbody").delegate('.BTrefuse', 'click', function(event) {
    selectedProject = $(event.target).parent("td").attr('data-demande');
    $('#refuseModalLabel').text("Refuser la demande");
  });

  $("tbody").delegate('.BTraisonrefuse', 'click', function(event){
    selectedProject = $(event.target).parent("td").attr('data-demande');
    $('#raisonrefuseModalLabel').text("Raison du refus de la demande");
    setReason();
  });

});
