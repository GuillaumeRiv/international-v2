        //Déclaration des variables globales.
        var generatedOptionId = 1; //Variable pour générer l'id des options.
        var generatedQuestionId = 1; //variable pour générer l'id des questions
        var generatedFileId = 1; //Variable pour générer l'id des files
        var typeForm = "Demande"; //Le type de formulaire

         $(document).ready(function(){
            // Ne pas toucher
            $('select').formSelect();
        });

        //Ajout des options pour les questions à choix (Choix multiples, cases à cocher, liste déroulante)
        function addOption(e){
            containerId = $(e).parent().parent().parent().parent().attr('id'); //Get le container id de la question présentement select.
            type = $(e).parent().parent().parent().parent().parent().children().children().eq(1).children().children().children().eq(3).val();
            $("#addOptionButton_" + containerId).remove();
            $("#addOption_" + containerId).remove();

            switch (type) {
                //Choix multiples et cases à cocher.
                case "Radio":
                case "Checkbox":
                    toappend = '<div id="option' + (type) + '_' + (++generatedOptionId) +'">' +
                                    '<label>' +
                                        '<input name="group1" type="' + (type) + '" disabled/>' +
                                        '<span>' +
                                            '<input id="option_' + (generatedOptionId) + '_' + (containerId) +'" class="input-field form-control" type="text" name="fname" placeholder="Option">'+
                                        '</span>' +
                                    '</label>' +
                                    '<span id="deleteOption_' + (containerId) + '" class="deleteOption">' +
                                        '<a class="btn-floating waves-effect waves-light red" onclick="deleteOption(this)">'+
                                            '<i class="material-icons">delete</i>' +
                                        '</a>' +
                                    '</span>' +
                                    '<span id="addOption_' + (containerId) + '" class="addOption">' +
                                    '<a id="addOptionButton_' + (containerId) + '" onclick="addOption(this)" class="btn-floating waves-effect waves-light blue">'+
                                        '<i class="material-icons">add</i>' +
                                    '</a>' +
                                    '</span>' +
                                '</div>';
                    break;
                //Liste déroulante.
                case "Select":
                     toappend = '<div id="option' + (type) + '_' + (++generatedOptionId) +'">' +
                                    '<label>' +
                                        '<span>' +
                                            '<input id="option_' + (generatedOptionId) + '_' + (containerId) +'" class="input-field form-control" type="text" name="fname" placeholder="Option">'+
                                        '</span>' +
                                    '</label>' +
                                    '<span id="deleteOption_' + (containerId) + '" class="deleteOption">' +
                                        '<a class="btn-floating waves-effect waves-light red" onclick="deleteOption(this)">'+
                                            '<i class="material-icons">delete</i>' +
                                        '</a>' +
                                    '</span>' +
                                    '<span id="addOption_' + (containerId) + '" class="addOption">' +
                                    '<a id="addOptionButton_' + (containerId) + '" onclick="addOption(this)" class="btn-floating waves-effect waves-light blue">'+
                                        '<i class="material-icons">add</i>' +
                                    '</a>' +
                                    '</span>' +
                                '</div>';
                    break;
                //Toute autre options.
                default:
                    alert("Une erreur est survenue...");
                    break;
            }


            $('#'+containerId).children().append(toappend);
        }

        //Suppression des options pour les questions à choix (Choix multiples, cases à cocher, liste déroulante)
        function deleteOption(e){
            containerId = $(e).parent().parent().parent().parent().attr('id'); //Get le container id de la question présentement select.
            optionId = $(e).parent().parent().attr('id'); //L'id de l'option à supprimer.
            prevId =  $(e).parent().parent().prev().attr('id'); //L'id de l'option juste au dessus de l'option à supprimer.

            toappend = '<span id="addOption_' + (containerId) + '" class="addOption">' +
                            '<a id="addOptionButton" onclick="addOption(this)" class="btn-floating waves-effect waves-light blue">' +
                                '<i class="material-icons">add</i>' +
                            '</a>' +
                        '</span>';

            // vérifier si la ligne supprimée est la dernière
            if ($("#"+optionId).is(':last-child')) {
                // si oui, on ajoute le plus à ligne précédente
                $("#"+prevId).append(toappend);
            }

            $( "#"+optionId ).remove();
        }

         //Fonction servant à ajouter une question.
            function addQuestion() {
               $('#addQuestion').remove();

               toappend = '<div id="question_' + (++generatedQuestionId) + '" class="row">' +
                                        '<div class="col-md-12 align-center">' +
                                            '<div class="form-group card-background">' +
                                                '<div class="row">' +
                                                    '<div class="col-md-8">' +
                                                        '<input id="questionTitle_' + (generatedQuestionId) + '" class="input-field form-control" type="text" name="fname" placeholder="Question...">' +
                                                    '</div>' +
                                                    '<div class="col-md-4">' +
                                                        '<div class="input-field">' +
                                                            '<select class="question-type" id="optionSelector_' + (generatedQuestionId) + '" onChange="setQuestionType(this)">' +
                                                                '<option value="ShortText">Réponse courte</option>' +
                                                                '<option value="LongText">Réponse longue</option>' +
                                                                '<option value="Nud">Numérique</option>' +
                                                                '<option value="Radio">Choix multiples</option>' +
                                                                '<option value="Checkbox">Cases à cocher</option>' +
                                                                '<option value="Select">Liste déroulante</option>' +
                                                                '<option value="Date">Date</option>' +
                                                                '<option value="File">Fichier</option>' +
                                                            '</select>' +
                                                        '</div>' +
                                                    '</div>' +
                                                '</div>' +
                                                '<div id="container_' + (generatedQuestionId) + '" class="row">' +
                                                    '<div class="col-md-12">' +
                                                        '<input class="input-field form-control" type="text" name="fname" placeholder="Réponse courte" disabled>' +
                                                    '</div>' +
                                                '</div>' +

                                                '<div class="toolButton row">' +
                                                    '<a id="deleteQuestion" class="fontButton ml-auto" href="#" onClick="deleteQuestion(this)"><i class="fa fa-trash" aria-hidden="true"></i></a>' +
                                                    '<a id="addQuestion" class=" fontButton" href="#" onClick="addQuestion()"><i class="fa fa-plus" aria-hidden="true"></i></a>' +
                                                '</div>' +
                                            '</div>' +
                                        '</div>' +
                                '</div>';


                $('#form>#saveButton').before(toappend);
                $('select').formSelect();
            }


            function deleteQuestion(e){
            questionId = $(e).parent().parent().parent().parent().attr('id'); //L'id de la question à supprimer.
            prevId =  $(e).parent().parent().parent().parent().prev().attr('id'); //L'id de la question juste au dessus de la question à supprimer.

            if (prevId != "question_1") {
                toappend = '<a id="addQuestion" class="fontButton" href="#" onClick="addQuestion()"><i class="fa fa-plus" aria-hidden="true"></i></a>';
            } else {
                toappend = '<a id="addQuestion" class="fontButton ml-auto" href="#" onClick="addQuestion()"><i class="fa fa-plus" aria-hidden="true"></i></a>';
            }


            // vérifier si la ligne supprimée est la dernière
            if ($("#"+questionId).is(':nth-last-child(2)')) {
                // si oui, on ajoute le plus à ligne précédente
                $("#"+prevId+">div>div>div.toolButton").append(toappend);
            }

            $( "#"+questionId ).remove();
        }

        //Fonction servant à changer le type de question.
        function setQuestionType(e) {
                containerId = $(e).parent().parent().parent().parent().parent().children().eq(1).attr('id'); //Get le container id de la question présentement select.
                optionType = e.value;  //Get le type de question.
                toappend = ""; //Le code html à générer.
                generatedOptionId = 1; //L'id de l'option à générer.


                 $('#'+containerId).children().empty();

                //Switch selon la question choisi
                switch (e.value) {
                    case "ShortText":
                        toappend = '<input class="input-field form-control" type="text" name="fname" placeholder="Réponse courte" disabled>';
                        break;
                    case "LongText":
                        toappend = '<textarea id="textarea2" class="materialize-textarea input-field form-control" data-length="120" placeholder="Réponse longue" disabled></textarea>';
                        break;
                    case "Nud":
                        toappend = '<input type="number" value="1" disabled/>';
                        break;
                    case "Radio":
                    case "Checkbox":
                        toappend =  '<div id="option' + (e.value) + '_' + (generatedOptionId) +'">' +
                                        '<label style="margin-bottom: 11.5px">' +
                                            '<input name="group1" type="' + (e.value) + '" disabled/>' +
                                            '<span>' +
                                                '<input id="option_' + (generatedOptionId) + '_' + (containerId) +'" class="input-field form-control" type="text" name="fname" placeholder="Option">'+
                                            '</span>' +
                                        '</label>' +
                                        '<span id="addOption_' + (containerId) + '" class="addOption">' +
                                        '<a id="addOptionButton_' + (containerId) + '" onclick="addOption(this)" class="btn-floating waves-effect waves-light blue">'+
                                            '<i class="material-icons">add</i>' +
                                        '</a>' +
                                        '</span>' +
                                    '</div>';
                        break;
                    case "Select":
                        toappend =  '<div id="option' + (e.value) + '_' + (generatedOptionId) +'">' +
                                        '<label>' +
                                            '<span>' +
                                                '<input id="option_' + (generatedOptionId) + '_' + (containerId) +'" class="input-field form-control" type="text" name="fname" placeholder="Option">'+
                                            '</span>' +
                                        '</label>' +
                                        '<span id="addOption_' + (containerId) + '" class="addOption">' +
                                        '<a id="addOptionButton_' + (containerId) + '" onclick="addOption(this)" class="btn-floating waves-effect waves-light blue">'+
                                            '<i class="material-icons">add</i>' +
                                        '</a>' +
                                        '</span>' +
                                    '</div>';
                        break;
                    case "Date":
                        toappend =  '<div id="option' + (e.value) + '_' + (generatedOptionId) +'">' +
                                        '<label>' +
                                            '<span>' +
                                                '<input class="input-field form-control" type="date" name="fname" placeholder="Option" disabled>'+
                                            '</span>' +
                                        '</label>' +
                                    '</div>';
                        break;
                    case "File":
                        toappend =  '<div id="option' + (e.value) + '_' + (generatedOptionId) +'">' +
                                        '<div class="file-field input-field">' +
                                            '<div class="btn btn-primary">' +
                                                '<span>Téléverser</span>'+
                                                '<input name="fileToUpload_' + (generatedFileId++) + '" class="fileToUpload" type="file">'+
                                            '</div>' +
                                            '<div class="file-path-wrapper">' +
                                                '<input class="file-path validate" type="text">' +
                                            '</div>' +
                                        '</div>' +
                                        '<div class="file-field input-field">' +
                                            '<div class="btn" disabled>' +
                                                '<span>Télécharger</span>'+
                                                '<input type="file">'+
                                            '</div>' +
                                            '<div class="file-path-wrapper">' +
                                                '<input class="file-path validate" type="text" disabled>' +
                                            '</div>' +
                                        '</div>' +
                                    '</div>';
                        break;
                  default:
                    alert("error");
                    break;
                }

                $('#'+containerId).children().append(toappend);
        }


        //Envoie le formulaire ce sauvegarder dans la database.
        function saveForm() {
            if (regex()) {
                let questions = new Array();

                $('[id^=questionTitle_').each(function(index,data) {
                    let id_question = data.id.match(/\d+/g);
                    let question;
                    let options = new Array();
                    let titre = data.value;
                    let type = $('#optionSelector_'+id_question).val();

                    if (type == 'Radio' || type == 'Checkbox' || type == 'Select') {
                        $('#container_'+id_question).children().find('div').each(function(index, data2) {
                            if (type == 'Select') {
                                options.push($('#'+data2.id).children().children().children().val());
                            }
                            else {
                                options.push($('#'+data2.id).children().children().eq(1).children().val());
                            }

                        })
                    }

                    question = {
                        titre_question: titre,
                        type_question: type,
                        options: options
                    }

                    questions.push(question)
                });

                let formulaire = {
                    titre : $('#titre_form').val(),
                    description : $('#desc_form').val(),
                    questions : questions,
                    typeForm : typeForm
                }

                var formulaire_json = JSON.stringify(formulaire);
                var lastInsertID;
                var okay = true;

                $('.fileToUpload').each(function(){
                    var extension = $( this ).val().replace(/^.*\./, '');
                    if (extension != "pdf" && extension != "txt" && extension != "docx" && extension != "xlsx" && extension != "ppt" && extension != "pptx" && extension != "") {
                        okay = false;
                    }
                })

                if (okay) {
                $.ajax(
                    {
                        type: "POST",
                        url: "php/form_creation/ajaxForm_creation.php",
                        dataType: 'json',
                        data: {'vals' : formulaire_json},
                        error: function(result){
                            alert(result.responseText);
                        },
                        success: function(result){
                            lastInsertID = result['message'];
                        }
                    }).done( function( data ) {
                        uploadFile();
                });

                } else {
                    alert('Extension de fichier non valide!');
                }


                //Envoie des fichiers si existant
                function uploadFile() {
                        if ($('.fileToUpload').length) {
                                var form = $('#form')[0];
                                var formData = new FormData(form);
                                formData.append('ID', lastInsertID);

                                $.ajax(
                                {
                                    type: "POST",
                                    contentType: false,
                                    processData: false,
                                    cache: false,
                                    url: 'php/form_creation/fileUpload.php',
                                    dataType: 'json',
                                    data: formData,
                                    error: function (result) {
                                        alert(result.responseText);
                                    },
                                    success: function (result) {

                                    }
                                });
                        }
                        window.location.replace("main.php?page=formulaire");
                }
            }
        }


        //Vérifie les champs avec des regex.
        function regex() {
            var confirmation = true; //Variable retournée disant si les champs sont ok ou non.

            //Vérifie tous les champs s'ils sont conforme.
            $('input[type=text]').each(function(){
                if ($(this).attr('id') != null) {
                  if ($(this).attr('id') != "desc_form" && $(this).attr('id') != "destination"){
                    //Vérifie si les champs sont vide
                    if ($(this).val() == "") {
                        $(this).addClass('invalid-input');
                        confirmation = false;
                    } else {
                        $(this).removeClass('invalid-input');
                    }
                  }
                }
            })

            return confirmation;
        }


        function typeFormChange(e) {
            
            switch(e.value) {
                case "Demande":
                    $('#Demande_form').show();
                    typeForm = e.value;
                break;
                case "Mobilite":
                    $('#Demande_form').hide();
                    typeForm = e.value;
                break;
            }

        }
