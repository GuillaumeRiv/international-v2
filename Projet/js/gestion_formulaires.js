$(document).ready(function() {
    //Variables globales.
    var selectedProjet = "";
    var idProjet = -1;
    var ajaxResponse;
    var pullTypeGlobal;
    var modalAction;
    var idForm;
  
  //Fonction servant à remplir le tableau des formulaires.
  function remplirTbl(arr, pullType){
    $("#tblFormulaires tbody").html("");
    $("#tblMobilite tbody").html("");
    pullTypeGlobal = pullType;

    //Vérifie pour voir si l'array de formulaire est vide.
    if(arr.title.length == 0)
    {
      var tr = $("<tr></tr>");
      var th = $("<td scope='row' colspan='4'></th>").text("Aucun formulaire trouvé.");

      tr.append(th);

      $("#tblFormulaires tbody").append(tr);
      $("#tblMobilite tbody").append(tr);
    }
    else
    {   

        //Boucle dans l'array et vérifie chaque formulaire (modifiable, sélectionné)
        for(var ctr = 0; ctr < arr.id_formulaire.length; ctr++)
        { 
          
          var tr = $("<tr></tr>");
          var th = $("<th scope='row'></th>").text(ctr+1);
          var td = $("<td style='width: 20%'></td>").text(arr.title[ctr]);
          var td2 = $("<td style='width: 35%'></td>").text(arr.description[ctr]);
          var td3;
          var td4 = $("<td id="+ arr.id_formulaire[ctr] + "style='width: 40%'></td>").attr('title', arr.title[ctr]).attr("id", arr.id_formulaire[ctr]).attr("data-tblid", ctr+1);
          var icon;
          var btnModifier;
          var btnSelect;
          var btnSupprimer;
          var modalTarget;

          if (arr.utilized[ctr] == 0) {
            td3 = $("<td style='width: 10%'></td>").text("Oui");
            btnModifier = $("<input class='btn btn-primary BTModif' type='button' data-toggle='modal' data-target='#confirmModal' value='Modifier'>");
          } else {
            modalTarget = null;
            btnModifier = null;
            td3 = $("<td style='width: 10%'></td>").text("Non");
          }

          if (pullType == "Demande") {
            if (arr.selected[ctr] == 1) {
              icon = $("<i class='fas fa-check fa-2x' aria-hidden='true' style='color:green; margin-left:1vw;'></i>");
              btnSelect = $("<input class='btn btn-primary BTSelect' type='button' value='Sélectionner' data-toggle='modal' data-target='#confirmModal' disabled>");
              btnSupprimer = $("<input class='btn btn-danger BTSuppr' type='button' value='Supprimer' data-toggle='modal' data-target='#confirmModal' disabled>");
            }  else {
              icon = $("<i class='fas fa-check fa-2x' aria-hidden='true' style='color:green; margin-left:1vw; visibility:hidden;'></i>");
              btnSelect = $("<input class='btn btn-primary BTSelect' type='button' value='Sélectionner' data-toggle='modal' data-target='#confirmModal'>");
              btnSupprimer = $("<input class='btn btn-danger BTSuppr' type='button' value='Supprimer' data-toggle='modal' data-target='#confirmModal'>");
            }
          } else if (pullType == "Mobilite") {
            projetIsSet().done(function(data1){
              if (data1) {
              checkFormulaire(arr.id_formulaire[ctr]).done(function(data2){
                if (data2) {
                  icon = $("<i class='fas fa-check fa-2x' aria-hidden='true' style='color:green; margin-left:1vw;'></i>");
                  btnSelect = $("<input class='btn btn-primary BTSelect' type='button' value='Sélectionner' data-toggle='modal' data-target='#confirmModal' disabled>");
                  btnSupprimer = $("<input class='btn btn-danger BTSuppr' type='button' value='Supprimer' data-toggle='modal' data-target='#confirmModal' disabled>");
                } else {
                  btnSelect = $("<input class='btn btn-primary BTSelect' type='button' value='Sélectionner' data-toggle='modal' data-target='#confirmModal'>");
                  icon = $("<i class='fas fa-check fa-2x' aria-hidden='true' style='color:green; margin-left:1vw; visibility:hidden;'></i>");
                  btnSupprimer = $("<input class='btn btn-danger BTSuppr' type='button' value='Supprimer' data-toggle='modal' data-target='#confirmModal'>");
                }
              });
              } else {
                 btnSelect = $("<input class='btn btn-primary BTSelect' type='button' value='Sélectionner' data-toggle='modal' data-target='#confirmModal' disabled>");
                 icon = $("<i class='fas fa-check fa-2x' aria-hidden='true' style='color:green; margin-left:1vw; visibility:hidden;'></i>");
                 btnSupprimer = $("<input class='btn btn-danger BTSuppr' type='button' value='Supprimer' data-toggle='modal' data-target='#confirmModal'>");
              }
            });
          }

          td4.append(btnSelect, btnModifier,btnSupprimer, icon);

          tr.append(th, td, td2, td3, td4);

          if (pullType == "Demande"){ 
            $("#tblFormulaires tbody").append(tr);
          } else $("#tblMobilite tbody").append(tr);
          
          $("#iRetour").show();
        }
    }
  }

  //Fonction servant à récupérer les formulaires dans la base de données selon le type de formulaire.
  function pullFormulaires(pullType){
    $.ajax({
      url: 'php/gestion_formulaires/pullFormulaires.php',
      type: 'POST',
      data: {pullType: pullType},
      success: function(data){
        var reponse = $.parseJSON(data);
        if(pullType=="Demande")
        {
          $("#affMobilite").removeClass("btAffichage");
          $("#affMobilite").addClass("btSelectAffichage");
          $("#affDemande").removeClass("btSelectAffichage");
          $("#affDemande").addClass("btAffichage");
          $("#tblFormulaires").show();
          $("#tblFormulaires_wrapper").show();
          $("#tblMobilite").hide();
          $("#tblMobilite_wrapper").hide();
        }
        else if(pullType=="Mobilite")
        {
          $("#affDemande").removeClass("btAffichage");
          $("#affDemande").addClass("btSelectAffichage");
          $("#affMobilite").removeClass("btSelectAffichage");
          $("#affMobilite").addClass("btAffichage");
          $("#tblFormulaires").hide();
          $("#tblFormulaires_wrapper").hide();
          $("#tblMobilite").show();
          $("#tblMobilite_wrapper").show();
        }
        remplirTbl(reponse, pullType);
        if (! $.fn.dataTable.isDataTable('#tblFormulaires') && pullType == "Demande"){
          $('#tblFormulaires').DataTable({
                    "oDestroy":"true",
                    "oLanguage":{
                                      "sEmptyTable":     "Aucune donnée disponible dans le tableau",
                                      "sInfo":           "Affichage de l'élément _START_ à _END_ sur _TOTAL_ éléments",
                                      "sInfoEmpty":      "Affichage de l'élément 0 à 0 sur 0 élément",
                                      "sInfoFiltered":   "(filtré à partir de _MAX_ éléments au total)",
                                      "sInfoPostFix":    "",
                                      "sInfoThousands":  ",",
                                      "sLengthMenu":     "Afficher _MENU_ éléments",
                                      "sLoadingRecords": "Chargement...",
                                      "sProcessing":     "Traitement...",
                                      "sSearch":         "Recherche :",
                                      "sZeroRecords":    "Aucun élément correspondant trouvé",
                                      "oPaginate": {
                                      "sFirst":    "Premier",
                                      "sLast":     "Dernier",
                                      "sNext":     "Suivant",
                                      "sPrevious": "Précédent"
                                      },
                                      "oAria": {
                                      "sSortAscending":  ": activer pour trier la colonne par ordre croissant",
                                      "sSortDescending": ": activer pour trier la colonne par ordre décroissant"
                                      },
                                      "select": {
                                                "rows": {
                                                  "_": "%d lignes sélectionnées",
                                                  "0": "Aucune ligne sélectionnée",
                                                  "1": "1 ligne sélectionnée"
                                                }
                                              }
          }
          });
        }
        else{

        }
        if (! $.fn.dataTable.isDataTable('#tblMobilite') && pullType == "Mobilite"){
          $('#tblMobilite').DataTable({
                    "oDestroy":"true",
                    "oLanguage":{
                                      "sEmptyTable":     "Aucune donnée disponible dans le tableau",
                                      "sInfo":           "Affichage de l'élément _START_ à _END_ sur _TOTAL_ éléments",
                                      "sInfoEmpty":      "Affichage de l'élément 0 à 0 sur 0 élément",
                                      "sInfoFiltered":   "(filtré à partir de _MAX_ éléments au total)",
                                      "sInfoPostFix":    "",
                                      "sInfoThousands":  ",",
                                      "sLengthMenu":     "Afficher _MENU_ éléments",
                                      "sLoadingRecords": "Chargement...",
                                      "sProcessing":     "Traitement...",
                                      "sSearch":         "Recherche :",
                                      "sZeroRecords":    "Aucun élément correspondant trouvé",
                                      "oPaginate": {
                                      "sFirst":    "Premier",
                                      "sLast":     "Dernier",
                                      "sNext":     "Suivant",
                                      "sPrevious": "Précédent"
                                      },
                                      "oAria": {
                                      "sSortAscending":  ": activer pour trier la colonne par ordre croissant",
                                      "sSortDescending": ": activer pour trier la colonne par ordre décroissant"
                                      },
                                      "select": {
                                                "rows": {
                                                  "_": "%d lignes sélectionnées",
                                                  "0": "Aucune ligne sélectionnée",
                                                  "1": "1 ligne sélectionnée"
                                                }
                                              }
          }
          });
        }
        else{

        }
      }
    });
  }

  //Fonction vérifiant si le formulaire est présentement associé à un projet.
  function checkFormulaire(idFormulaire) {
    return $.ajax({
            url: 'php/gestion_formulaires/checkFormulaire.php',
            type: 'POST',
            async: false,
            data: {idFormulaire: idFormulaire},
            success: function(data){

            }
          });
  }

//Fonction vérifiant si un projet à été sélectionné.
function projetIsSet(){
  return $.ajax({
          url: 'php/gestion_formulaires/projetIsSet.php',
          type: 'POST',
          async: false,
  });
}

//Fonction de select, d'ajout, de modification et de suppression de formulaire.
function selectFormulaire(idFormulaire, typeFormulaire) {
  $.ajax({
    url: 'php/gestion_formulaires/selectFormulaire.php',
    type: 'POST',
    async: false,
    data: {idFormulaire: idFormulaire, typeFormulaire: typeFormulaire},
  });
}

function addFormulaire(idFormulaire) {
  window.location.replace("./form_creation.php");
}

function modifyFormulaire(idFormulaire) {
  window.location.replace("./form_modify.php?idForm=" + idFormulaire);
}

function deleteFormulaire(idFormulaire, typeFormulaire) {
    $.ajax({
    url: 'php/gestion_formulaires/deleteFormulaire.php',
    type: 'POST',
    async: false,
    data: {idFormulaire: idFormulaire, typeFormulaire: typeFormulaire},
  });
}


  //Pull les formulaires de type demande lors du premier chargement de la page.
  pullFormulaires('Demande');


  //Les triggers des boutons menu.
  $("#affDemande").on("click", function(){
    pullFormulaires("Demande");
  });

  $("#affMobilite").on("click", function(){
    pullFormulaires("Mobilite");
  });


  //Les modals et leurs messages.
  $("#confirmBT").click(function(event) {
    if (modalAction == "Select") {
      selectFormulaire(idForm, pullTypeGlobal);
      pullFormulaires(pullTypeGlobal);

    } else if (modalAction == "Add") {
      addFormulaire();

    } else if (modalAction == "Delete") {
      deleteFormulaire(idForm, pullTypeGlobal);
      pullFormulaires(pullTypeGlobal);

    } else if (modalAction == "Modif") {
      modifyFormulaire(idForm)
    }
  });

  $("tbody").delegate('.BTSelect', 'click', function(event) {
    selectedForm = $(event.target).parent("td").attr('data-tblid');
    idForm = $(event.target).parent("td").attr('id');
    modalAction = "Select";
    $('#confirmModalLabel').text("Confirmer la sélection");
    $('#confirmModal').find(".modal-body").html("<p>Une fois le formulaire #" + selectedForm + " sélectionné, il ne pourrra plus être modifié, êtes-vous sur de vouloir continuer?</p>");
  });

  $("body").delegate('.BTAdd', 'click', function(event) {
    selectedForm = $(event.target).parent("td").attr('data-tblid');
    idForm = $(event.target).parent("td").attr('id');
    modalAction = "Add";
    $('#confirmModalLabel').text("Confirmer l'ajout d'un formulaire");
    $('#confirmModal').find(".modal-body").html("<p>Voulez-vous vraiment ajouter un formulaire?</p>");
  });

  $("tbody").delegate('.BTSuppr', 'click', function(event) {
    selectedForm = $(event.target).parent("td").attr('data-tblid');
    idForm = $(event.target).parent("td").attr('id');
    modalAction = "Delete";
    $('#confirmModalLabel').text("Confirmer la suppression");
    $('#confirmModal').find(".modal-body").html("<p>Voulez-vous vraiment supprimer le formulaire #" + selectedForm + " ?</p>");
  });

  $("tbody").delegate('.BTModif', 'click', function(event) {
    selectedForm = $(event.target).parent("td").attr('data-tblid');
    idForm = $(event.target).parent("td").attr('id');
    modalAction = "Modif";
    $('#confirmModalLabel').text("Confirmer la modification");
    $('#confirmModal').find(".modal-body").html("<p>Voulez-vous vraiment modifier le formulaire #" + selectedForm + " ?</p>");
  });

});
