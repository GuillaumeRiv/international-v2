$(document).ready(function() {
  var selectedVaccin = "";
  var projID = "";

  $.ajax({
    url: 'php/formulairedyn/getprojid.php',
    type: 'POST',
    success: function(data) {
      projID = data;
    }
  }).done(function() {
    pullVaccins();
  });


  function remplirVaccins(arr) {
    $("#tblVaccins tbody").html("");
    var arrVaccinStates;

    $.ajax({
      url: 'php/formulairedyn/pullvaccinstates.php',
      type: 'POST',
      success: function(data) {
        if (projID != -1)
          arrVaccinStates = $.parseJSON(data);
      }
    }).done(function() {

      if (arr.vaccin.length == 0) {
        var tr = $("<tr></tr>");
        var th = $("<td scope='row' colspan='4'></th>").text("Aucun vaccin trouvé.");

        tr.append(th);

        $("#tblVaccins tbody").append(tr);
      } else {
        for (var ctr = 0; ctr < arr.vaccin.length; ctr++) {
          var tr = $("<tr></tr>");
          var th = $("<th scope='row'></th>").text(ctr + 1);
          var td = $("<td style='width: 40%'></td>").text(arr.vaccin[ctr]);
          var td0 = $("<td id='' style='width: 35%'></td>");
          var td1 = $("<td id='" + arr.id_vaccin[ctr] + "' style='width: 35%'></td>");

          var bt = $("<input class='btn btn-danger BTsuppr' type='button' value='Supprimer le vaccin'>");
          var check = $("<input id='" + arr.id_vaccin[ctr] + "' class='form-check-input' type='checkbox' style='margin-left: 5vw;'>");


          if (projID == "-1") {
            check.attr('disabled', true);
          } else {
            check.removeAttr('disabled');
          }

          if(arrVaccinStates != null && arrVaccinStates.length != 0){
            arrVaccinStates.forEach(function(item, index) {
              if (item.vaccin_id == arr.id_vaccin[ctr]){
                check.prop('checked', true);
              }
            });
          }

          td1.append(bt);
          td0.append(check);

          tr.append(th, td, td0, td1);
          $("#tblVaccins tbody").append(tr);
        }
      }
    });


  }

  function pullVaccins() {
    $.ajax({
      url: 'php/formulairedyn/pullvaccins.php',
      type: 'POST',
      success: function(data) {
        vaccins = $.parseJSON(data);
        remplirVaccins(vaccins);
      }
    });
  }


  $("#tblVaccins tbody").delegate('.BTsuppr', 'click', function(event) {
    selectedVaccin = $(event.target).parent('td').attr('id');
    $("#deleteVaccinModal").modal('toggle');
  });


  $("#deleteVaccinBt").click(function(event) {
    $.ajax({
      url: 'php/formulairedyn/deletevaccin.php',
      type: 'POST',
      data: {
        id: selectedVaccin
      },
      success: function(data) {
        if (data == "success") {
          alert("Vaccin supprimé avec succès");
          location.reload();
        } else {
          alert("Erreur lors de la suppression du vaccin");
        }
      }
    });

  });

  $("#addVaccinBt").click(function(event) {
    var nom = $.trim($("#addVaccinNom").val());

    if (nom == "") {
      alert("Veuillez entrer un nom");
      event.preventDefault();
      event.stopPropagation();
    } else {
      $.ajax({
        url: 'php/formulairedyn/addvaccin.php',
        type: 'POST',
        data: {
          nom: nom
        },
        success: function(data) {
          if (data == "success") {
            alert("Vaccin ajouté avec succès");
            location.reload();
          } else {
            alert("Erreur lors de l'ajout du vaccin");
          }
        }
      });
    }
  });

  $("#tblVaccins tbody").delegate('input[type=checkbox]', 'change', function(event) {
    var box = $(event.target);

    var checkVaccinId = box.attr('id');
    var etat = "";

    if (box.is(":checked")) {
      etat = "1";
    } else {
      etat = "0";
    }

    $.ajax({
      url: 'php/formulairedyn/changevaccinetat.php',
      type: 'POST',
      data: {
        id_vaccin: checkVaccinId,
        etat: etat
      }
    });


  });

});
