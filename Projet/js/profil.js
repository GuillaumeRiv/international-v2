$(document).ready(function() {

  $("#form_changepass").submit(function(event) {

    var oldpass = $("#oldPass").val();
    var newpass = $("#newPass").val();
    var newpass1 = $("#newPass1").val();

    if (!(oldpass == "" || newpass == "" || newpass1 == "")) {
      if (newpass == newpass1) {
              $.ajax({
                url: 'php/profil/changepass.php',
                type: 'POST',
                data: {
                  newpass: newpass,
                  oldpass: oldpass
                },
                success: function(data) {
                  if (data == "success") {
                    alert("Votre mot de passe a bien été changé");
                    $('#resetModal').modal('hide');
                  }
                  else if (data == "error_user_notfound") {
                    alert("Error: Mot de passe invalide");
                  }
                  else {
                    alert(data);
                    alert("Erreur lors du changement de mot de passe");
                  }
                }
              });

      } else {
        alert("Les deux champs du nouveau mot de passe doivent correspondre");
      }
    } else {
      alert("Veuillez remplir tous les champs");
    }

    return false;

  });



  $("#form_changeCourriel").submit(function(event) {
    var newCourriel = $("#newCourriel").val();
    var confirmCourriel = $("#confirmCourriel").val();
    var password = $("#passwordC").val();

    if( newCourriel.trim() !="" && confirmCourriel.trim() != "" && password.trim()!="")
    {
      if(newCourriel == confirmCourriel)
      {
          $.ajax({
            type: 'POST',
            url: 'php/profil/changecourriel.php',
            data: {newCourriel : newCourriel , password : password},
            success: function(data) {
             if(data =="success")
             {
              alert("Votre adresse courriel a bien été changé");
              $('#resetCourrielModal').modal('hide');
             }
             else
             {
               alert(data);
             }
            }
          });
      }
      else
      {
        alert("Les deux champs avec l'adresse courriel ne sont pas identiques.")
      }
    }
    else
    {
      alert("Veuillez remplir tous les champs!")
    }

    return false;

  });

  $("#form_changeInfo").submit(function(event) {
    var newPrenom = $("#newPrenom").val();
    var newNom = $("#newNom").val();
    var passwordI = $("#passwordI").val();
    if( newPrenom.trim() !="" && newNom.trim() != "" && passwordI.trim()!="")
    {
          $.ajax({
            type: 'POST',
            url: 'php/profil/changeinfo.php',
            data: {newPrenom : newPrenom , newNom : newNom, passwordI: passwordI},
            success: function(data) {
             if(data =="success")
             {
              alert("Vos informations ont bien été modifiées.");
              $('#resetInfoModal').modal('hide');
             }
             else
             {
               alert(data);
             }
            }
          });
    }
    else
    {
      alert("Veuillez remplir tous les champs!")
    }

    return false;

  });

});
