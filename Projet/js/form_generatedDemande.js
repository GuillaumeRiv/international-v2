var idFormulaire;

$( document ).ready(function() {

    $.ajax({
        type: "GET",
        url: 'php/form_creation/getSelectedFormDemande.php',
    }).done(function(data){
        idFormulaire = data;
        getForm(data);
    });
});


function getForm(idForm) {
        $.ajax(
    {
        type: "POST",
        url: 'php/form_creation/ajaxForm_generated.php',
        dataType: 'json',
        data: {idForm: idForm},
        error: function (result) {
            alert(result.responseText)
        },
        success: function (data) {
            $('#titre_form').append(data.titre);
            $('#desc_form').append(data.description);

            var toappend = "";
            var input = "";
            var option = "";
            file = "";
            for (question in data.questions) {

                switch (data.questions[question].type) {
                    case "ShortText":
                        input = '<input name='+ data.questions[question].id_question +' class="input-field form-control formDyn" type="text" name="fname" placeholder="Votre réponse">';
                        break;
                    case "LongText":
                        input = '<textarea name='+ data.questions[question].id_question +' class="materialize-textarea input-field form-control formDyn" data-length="120" placeholder="Réponse longue"></textarea>';
                        break;
                    case "Nud":
                        input = '<input name='+ data.questions[question].id_question +' type="number" value="1" class="formDyn" />';
                        break;
                    case "Radio":
                        input = "";
                        for (choix_reponse in data.questions[question].choix_reponse) {
                            if (data.questions[question].type == 'Radio') {
                                var value = encodeURIComponent(data.questions[question].choix_reponse[choix_reponse].choix_reponse)
                                input +=
                                    '<div>' +
                                        '<label style="margin-bottom: 11.5px">' +
                                            '<input name='+ data.questions[question].id_question +' type="radio" value='+ value +' class="formDyn" />' +
                                                '<span>' +
                                                    data.questions[question].choix_reponse[choix_reponse].choix_reponse +
                                                '</span>' +
                                        '</label>' +

                                    '</div>';
                            }
                        }
                        break;
                    case "Checkbox":
                        input = "";
                        for (choix_reponse in data.questions[question].choix_reponse) {
                            if (data.questions[question].type == 'Checkbox') {
                                var value = encodeURIComponent(data.questions[question].choix_reponse[choix_reponse].choix_reponse)
                                input +=
                                    '<div>' +
                                        '<label style="margin-bottom: 11.5px">' +
                                            '<input name=' + data.questions[question].id_question +' type="checkbox" class="filled-in formDyn" value='+ value +' />' +
                                                '<span>' +
                                                     data.questions[question].choix_reponse[choix_reponse].choix_reponse +
                                                '</span>' +
                                        '</label>' +
                                    '</div>';
                            }
                        }
                        break;
                    case "Select":
                        input = "";
                        for (choix_reponse in data.questions[question].choix_reponse) {
                            if (data.questions[question].type == 'Select') {
                                var value = encodeURIComponent(data.questions[question].choix_reponse[choix_reponse].choix_reponse)
                                option += '<option name='+ data.questions[question].id_question +' value='+ value +'>'+ data.questions[question].choix_reponse[choix_reponse].choix_reponse +'</option>'
                            }
                        }
                        input =
                            '<div name='+ data.questions[question].id_question +' class="input-field col s12">' +
                                    '<select name='+ data.questions[question].id_question +'>' +
                                        option +
                                    '</select>' +
                            '</div>';
                        break;
                    case "Date":
                        input =
                            '<div>' +
                                '<label>' +
                                    '<span>' +
                                        '<input name='+ data.questions[question].id_question +' class="input-field form-control formDyn" type="date" name="fname">'+
                                    '</span>' +
                                '</label>' +
                            '</div>';
                        break;
                    case "File":
                        if (data.questions[question].fileUploaded != null) {
                            file =
                                '<div class="btn btn-primary" style = "margin-bottom:10px;padding:.375rem .75rem; height: 3rem;line-height: 3rem;">' +
                                    "<a style='color:white' href='/international-v2/uploaded_files/" + data.questions[question].fileUploaded + "' download='/international-v2/uploaded_files/" + data.questions[question].fileUploaded + "'>" + ' Télécharger </a>' +
                                "</div>"
                        }
                        else {
                            file = "";
                        }

                        input =
                            file +
                            '<div class="file-field input-field">' +
                                '<div class="btn btn-primary">' +
                                    '<span>Téléverser</span>'+
                                    '<input name='+ data.questions[question].id_question +' type="file" class="formDyn">'+
                                '</div>' +
                                '<div class="file-path-wrapper">' +
                                    '<input name='+ data.questions[question].id_question +' class="file-path validate formDyn" type="text">' +
                                '</div>' +
                            '</div>';
                        break;
                    default:
                        alert('erreur');
                        break;

                }

                toappend +=
                    '<div class="row">' +
                        '<div class="col-md-12 align-center">' +
                            '<div class="form-group card-background">' +

                                '<div class="row">' +
                                    '<div class="col-md-12">' +
                                        '<h4>'+data.questions[question].question+'</h4>' +
                                    '</div>' +
                                '</div>' +

                                '<div class="row">' +
                                    '<div class="col-md-12">' +
                                        input +
                                    '</div>' +
                                '</div>' +

                            '</div>' +
                        '</div>' +
                    '</div>'

                input = "";
            }

            $('#form').append(toappend);
            $('#form').append('<div id="saveButton" class="text-center"><button class="btn btn-primary center-block" type="button" onclick="sendData()">Enregistrer</button></div>');
            $('select').formSelect();
        }
    });
}

function sendData() {

    let reponse_choix = new Array()
    let answers = new Array();
    let prevNames = new Array();
    let okay = true;
    let radio_checked = false;
    checkbox_checked = false;

    if($('#dateD').val().replace(/-/g,'/') > $('#dateR').val().replace(/-/g,'/')) {
        $('#dateD').addClass('invalid-input');
        $('#dateR').addClass('invalid-input')
        okay = false;
    } else {
        $('#dateD').removeClass('invalid-input');
        $('#dateR').removeClass('invalid-input');
    }

    $('input.formDyn, textarea').each(function(index,data) {
        let question = data.name;
        let reponse = data.value;

        if (!($(data).is('input:radio') || $(data).is('input:checkbox') || $(data).is('input:file'))) {
            if(!$(data).is('[readonly]')){
                let extension = $( this ).val().replace(/^.*\./, '');
                if (extension == "exe") {
                    $(this).removeClass('valid');
                    $(this).addClass('invalid-input');
                    okay = false;
                }
                else if (reponse != "") {
                    let answer = {
                        question: question,
                        reponse: reponse
                    }
                    answers.push(answer);
                    $(this).removeClass('invalid-input');
                }
                else {
                    okay = false;
                    $(this).addClass('invalid-input');
                }
            }
        }
        else if ($(data).is('input:radio')) {
            if ($(data).is(":checked")) {
                radio_checked = true;

                let answer = {
                    question: data.name,
                    reponse: decodeURIComponent(data.value)
                }
                answers.push(answer);
            }
        }
        else if ($(data).is('input:checkbox')) {
            if ($(data).is(":checked")) {
                checkbox_checked = true;

                let name = data.name;

                if(prevNames.includes(name) == false){
                    $("input[name='" +name+ "']").each(function(index,data) {
                        if ($(data).is(":checked")) {
                            reponse_choix.push(decodeURIComponent(data.value))
                        }
                    })
                    let answer = {
                        question: data.name,
                        reponse: reponse_choix
                    }
                    answers.push(answer);
                }

                reponse_choix = new Array()
                prevNames.push(name);
            }
        }

    });

    $('select').each(function(index,data) {
        let answer = {
            question : data.name,
            reponse : decodeURIComponent(data.value)
        }
        answers.push(answer);
    });

    var answers_json = JSON.stringify(answers);
    console.log(answers_json);


    if ($('input[type=checkbox]').length) {
        if (checkbox_checked==false) {
            okay = false;
        }
    }

    if ($('input[type=radio]').length) {
        if (radio_checked==false) {
            okay = false;
        }
    }

    if (okay) {
        $.ajax(
        {
            type: "POST",
            url: 'php/form_creation/ajaxForm_saveUserResponse.php',
            dataType: 'json',
            data: {'vals' : answers_json, 'idForm': idFormulaire, 'destination': $('#destination').val(), 'dateD': $('#dateD').val(), 'dateR': $('#dateR').val()},
            error: function (result) {
                alert(result.responseText)
            },
            success: function (data) {

            }
        }).done(function(data){
            uploadFile();
        });
    }
}

//Envoie des fichiers si existant
function uploadFile() {
                var form = $('#form')[0];
                var formData = new FormData(form);

                $.ajax(
                {
                    type: "POST",
                    contentType: false,
                    processData: false,
                    cache: false,
                    url: 'php/form_creation/fileUpload_answer.php',
                    dataType: 'json',
                    data: formData,
                    error: function (result) {
                        console.log(result.responseText);
                    },
                    success: function (data) {
                        alert("Succès 2")
                    }
                });
        window.location.replace("javascript:history.go(-1)");
}
