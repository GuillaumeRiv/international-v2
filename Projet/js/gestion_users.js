function pullusers(pulltype) {
      $.ajax({
      url: 'php/gestion_users/pulluser.php',
      type: 'POST',
      data: {pulltype: pulltype},
      success: function(data) {
        var reponse = JSON.parse(data);
        if(pulltype=="all")
        {
          $("#affProjet").removeClass("btAffichage");
          $("#affProjet").addClass("btSelectAffichage");
          $("#affTous").removeClass("btSelectAffichage");
          $("#affTous").addClass("btAffichage");
          $("#tblProjets").hide();
          $("#tblProjets_wrapper").hide();
          $("#tblUsers").show();
          $("#tblUsers_wrapper").show();

        }
        else if(pulltype=="projet")
        {
          $("#affProjet").removeClass("btSelectAffichage");
          $("#affProjet").addClass("btAffichage");
          $("#affTous").removeClass("btAffichage");
          $("#affTous").addClass("btSelectAffichage");
          $("#tblUsers").hide();
          $("#tblUsers_wrapper").hide();
          $("#tblProjets").show();
          $("#tblProjets_wrapper").show();
        }
        remplirTbl(reponse, pulltype);
        if (! $.fn.dataTable.isDataTable('#tblUsers') && pulltype == "all"){
          $('#tblUsers').DataTable({
                    "oDestroy":"true",
                    "oLanguage":{
                                      "sEmptyTable":     "Aucune donnée disponible dans le tableau",
                                      "sInfo":           "Affichage de l'élément _START_ à _END_ sur _TOTAL_ éléments",
                                      "sInfoEmpty":      "Affichage de l'élément 0 à 0 sur 0 élément",
                                      "sInfoFiltered":   "(filtré à partir de _MAX_ éléments au total)",
                                      "sInfoPostFix":    "",
                                      "sInfoThousands":  ",",
                                      "sLengthMenu":     "Afficher _MENU_ éléments",
                                      "sLoadingRecords": "Chargement...",
                                      "sProcessing":     "Traitement...",
                                      "sSearch":         "Recherche :",
                                      "sZeroRecords":    "Aucun élément correspondant trouvé",
                                      "oPaginate": {
                                      "sFirst":    "Premier",
                                      "sLast":     "Dernier",
                                      "sNext":     "Suivant",
                                      "sPrevious": "Précédent"
                                      },
                                      "oAria": {
                                      "sSortAscending":  ": activer pour trier la colonne par ordre croissant",
                                      "sSortDescending": ": activer pour trier la colonne par ordre décroissant"
                                      },
                                      "select": {
                                                "rows": {
                                                  "_": "%d lignes sélectionnées",
                                                  "0": "Aucune ligne sélectionnée",
                                                  "1": "1 ligne sélectionnée"
                                                }
                                              }
          }
          });
        }
        else{

        }
        if (! $.fn.dataTable.isDataTable('#tblProjets') && pulltype == "projet"){
          $('#tblProjets').DataTable({
                    "oDestroy":"true",
                    "oLanguage":{
                                      "sEmptyTable":     "Aucune donnée disponible dans le tableau",
                                      "sInfo":           "Affichage de l'élément _START_ à _END_ sur _TOTAL_ éléments",
                                      "sInfoEmpty":      "Affichage de l'élément 0 à 0 sur 0 élément",
                                      "sInfoFiltered":   "(filtré à partir de _MAX_ éléments au total)",
                                      "sInfoPostFix":    "",
                                      "sInfoThousands":  ",",
                                      "sLengthMenu":     "Afficher _MENU_ éléments",
                                      "sLoadingRecords": "Chargement...",
                                      "sProcessing":     "Traitement...",
                                      "sSearch":         "Recherche :",
                                      "sZeroRecords":    "Aucun élément correspondant trouvé",
                                      "oPaginate": {
                                      "sFirst":    "Premier",
                                      "sLast":     "Dernier",
                                      "sNext":     "Suivant",
                                      "sPrevious": "Précédent"
                                      },
                                      "oAria": {
                                      "sSortAscending":  ": activer pour trier la colonne par ordre croissant",
                                      "sSortDescending": ": activer pour trier la colonne par ordre décroissant"
                                      },
                                      "select": {
                                                "rows": {
                                                  "_": "%d lignes sélectionnées",
                                                  "0": "Aucune ligne sélectionnée",
                                                  "1": "1 ligne sélectionnée"
                                                }
                                              }
          }
          });
        }
        else{

        }
      }
    });
  }


  function remplirTbl(arr, pulltype) {
    $("#tblUsers tbody").html("");
    $("#tblProjets tbody").html("");

    if(arr.courriel.length == 0)
    {
      var tr = $("<tr></tr>");
      var th = $("<td id='row1' scope='row'></th>");
      var td = $("<td style='width: 18%'></td>");
      var tdNom = $("<td style='width: 18%'></td>");
      var tdPrenom = $("<td style='width: 18%'></td>");
      var td1 = $("<td></td>").text(arr.type[ctr]);
      var td2 = $("<td style='width: 45%'></td>");
      tr.append(th, td, tdNom, tdPrenom, td1, td2);
      $("#tblUsers tbody").append(tr);
      $("#tblProjets tbody").append(tr);
    }
    else
    {
      for (var ctr = 0; ctr < arr.courriel.length; ctr++) {
        var tr = $("<tr></tr>");
        var th = $("<th scope='row'></th>").text(ctr + 1);
        var td = $("<td style='width: 13%'></td>").text(arr.courriel[ctr]);
        var tdNom = $("<td style='width: 13%'></td>").text(arr.nom[ctr]);
        var tdPrenom = $("<td style='width: 13%'></td>").text(arr.prenom[ctr]);
        var td1 = $("<td></td>").text(arr.type[ctr]);
        var td2 = $("<td style='width: 57%'></td>").attr('id', arr.courriel[ctr]);


        if (arr.courriel[ctr] != arr.currentUser) {
          var bt =  $("<input class='btn btn-primary BTreset' type='button' value='Réinitialiser mot de passe' data-toggle='modal' data-target='#confirmModal'>");

          var bt2 = $("<input class='btn btn-success BTview' type='button' value='Voir le formulaire'>");

          var icon = $("<button id=confirmation onclick='confirmationParticipation()' class='fas fa-check fa-1x' aria-hidden='true' style='color:green; margin-left:1vw; opacity:0.3; background: transparent; border: none;'></button>");
          var icon2 = $("<button id=annulation onclick='confirmationParticipation()' class='fas fa-times fa-1x' aria-hidden='true' style='color:red; margin-left:1vw; opacity:0.3; background: transparent; border: none;'></button>");


          if(arr.actif[ctr] == 1)
          {
            var bt1 = $("<input class='btn btn-danger BTsuppr' type='button' id='desactiver_"+ ctr +"' value='Désactiver' data-toggle='modal' data-target='#confirmModal' style='visibility: visible;'>");
            var btActif = $("<input class='btn btn-success BTActive' type='button' id='activer_"+ ctr +"' value='Activer' data-toggle='modal' data-target='#confirmModal' style='visibility: hidden;'>");
          }
          else  {
            var bt1 = $("<input class='btn btn-danger BTsuppr' type='button' id='desactiver_"+ ctr +"' value='Désactiver' data-toggle='modal' data-target='#confirmModal' style='visibility: hidden;'>");
            var btActif = $("<input class='btn btn-success BTActive' type='button' id='activer_"+ ctr +"' value='Activer' data-toggle='modal' data-target='#confirmModal' style='visibility: visible;'>");
          }
        }

        if (pulltype == "all"){
          td2.append(bt, btActif, bt1);
          tr.append(th, td, tdNom, tdPrenom, td1, td2);
          $("#tblUsers tbody").append(tr);
        }
        if (pulltype == "projet"){
          td2.append(bt,bt2,btActif, bt1);
          tr.append(th,td,tdNom, tdPrenom, td1,td2);
          $("#tblProjets tbody").append(tr);
        }


    }
  }
}
$(document).ready(function() {
  var selectedUser = "";
  var reset = true;
  var action = "";

  pullusers('projet');


  function resetUser(courriel) {
    var tempPass = Fonction.random_password_generate(8, 12);

    $.ajax({
      url: 'php/gestion_users/resetuser.php',
      type: 'POST',
      data: {
        courriel: courriel,
        tempPass: tempPass
      },
      success: function(data) {
        if (data == "success") {
          $("#userReset").text(selectedUser);
          $("#tempPass").text(tempPass);

          $("#resetModal").modal('show');
        } else {
          $("#successerrorModalLabel").text("Erreur");
          $("#successerrorModal").find(".modal-body").html("<p>erreur lors de la réinitialisation du mot de passe de l'utilisateur ''" + selectedUser + "''</p>");
          $("#successerrorModal").modal('show');
        }
      }
    });
  }

  function delUser(courriel) {
    $.ajax({
      url: 'php/gestion_users/deluser.php',
      type: 'POST',
      data: {
        courriel: courriel
      },
      success: function(data) {
        if (data == "success") {
          $("#successerrorModalLabel").text("Succès");
          $("#successerrorModal").find(".modal-body").html("<p>L'utilisateur " + selectedUser + " désactiver avec succès</p>");
          $("#successerrorModal").modal('show');
          pullusers();
        } else {
          $("#successerrorModalLabel").text("Erreur");
          $("#successerrorModal").find(".modal-body").html("<p>Erreur lors de la désactivation de l'utilisateur ''" + selectedUser + "''</p>");
          $("#successerrorModal").modal('show');
        }
      }
    });
  }

  function actifUser(courriel) {
    $.ajax({
      url: 'php/gestion_users/actifuser.php',
      type: 'POST',
      data: {
        courriel: courriel
      },
      success: function(data) {
        if (data == "success") {
          $("#successerrorModalLabel").text("Succès");
          $("#successerrorModal").find(".modal-body").html("<p>L'utilisateur " + selectedUser + " a été activer avec succès</p>");
          $("#successerrorModal").modal('show');
          pullusers();
          //location.reload();
        } else {
          $("#successerrorModalLabel").text("Erreur");
          $("#successerrorModal").find(".modal-body").html("<p>Erreur lors de l'activation de l'utilisateur ''" + selectedUser + "''</p>");
          $("#successerrorModal").modal('show');
        }
      }
    });
  }

  function voirFormulaire() {
    $.ajax({
      url: 'php/gestion_users/voirformulaire.php',
      type: 'POST',
      data: {
        id: selectedUser
      },
      success: function(data) {
        var reponse = $.parseJSON(data);
        fillForm(reponse);
        window.location.href='?page=viewformulaire';
      }
    });
  }


  function fillForm(response){
    localStorage.setItem("formNom", response.nom);
    localStorage.setItem("formPrenom", response.prenom);
    localStorage.setItem("formCourriel", response.courriel);
    localStorage.setItem("formSexe", response.sexe);
    localStorage.setItem("formDateN", response.datenaissance);
    localStorage.setItem("formEtuDu", response.form_coor_etuDu.join());
    localStorage.setItem("formEtuAu", response.form_coor_etuAu.join());
    localStorage.setItem("formEtuAd", response.form_coor_etuAd.join());
    localStorage.setItem("formEtuTel", response.form_coor_etuTel.join());
    localStorage.setItem("formResDu", response.form_coor_resDu.join());
    localStorage.setItem("formResAu", response.form_coor_resAu.join());
    localStorage.setItem("formResAd", response.form_coor_resAd.join());
    localStorage.setItem("formResTel", response.form_coor_resTel.join());
    localStorage.setItem("formProDu", response.form_coor_proDu.join());
    localStorage.setItem("formProAu", response.form_coor_proAu.join());
    localStorage.setItem("formProAd", response.form_coor_proAd.join());
    localStorage.setItem("formProTel", response.form_coor_proTel.join());
    localStorage.setItem("formAssMalNom", response.form_ass_malNom);
    localStorage.setItem("formAssMalNum", response.form_ass_malNum);
    localStorage.setItem("formAssMalAd", response.form_ass_malAd);
    localStorage.setItem("formAssMalTel", response.form_ass_malTel);
    localStorage.setItem("formAssMalTelU", response.form_ass_malTelU);
    localStorage.setItem("formAssMalCourriel", response.form_ass_malCourriel);
    localStorage.setItem("formAssBagNom", response.form_ass_bagNom);
    localStorage.setItem("formAssBagNum", response.form_ass_bagNum);
    localStorage.setItem("formAssBagAd", response.form_ass_bagAd);
    localStorage.setItem("formAssBagTel", response.form_ass_bagTel);
    localStorage.setItem("formAssBagCourriel", response.form_ass_bagCourriel);
    localStorage.setItem("formAmbAd", response.form_ambAd);
    localStorage.setItem("formAmbTel", response.form_ambTel);
    localStorage.setItem("formAmbCourriel", response.form_ambCourriel);
    localStorage.setItem("formSanteEtat", response.form_santeEtat);
    localStorage.setItem("formSanteMed", response.form_santeMed);
    localStorage.setItem("formSanteAl", response.form_santeAl);
    localStorage.setItem("formVaccinsId", response.form_vaccinsId.join());
    localStorage.setItem("formVaccinsVal", response.form_vaccinsVal.join());

  }

  function verifierParticipation(){
      var conf = document.getElementById("confirmation").style.opacity;
      //var ann = document.getElementById("annulation").style.opacity;

      $.ajax({
      url: 'php/gestion_users/confirmuser.php',
      type: 'POST',
      data: { id: selectedUser, conf: conf},
      success: function(data) {
      if(data == "conf")
      document.getElementById("confirmation").style.opacity = "1.0";
      }
    });
  }

   function confirmationParticipation(){
      var conf = document.getElementById("confirmation").style.opacity;
      //var ann = document.getElementById("annulation").style.opacity;

      $.ajax({
      url: 'php/gestion_users/confirmuser.php',
      type: 'POST',
      data: { id: selectedUser, conf: conf},
      success: function(data) {
      if(data == "conf")
      document.getElementById("confirmation").style.opacity = "1.0";
      }
    });
  }

  $("#confirmBT").click(function(event) {

    if (action == "reset")
    {
      resetUser(selectedUser);
    }
    else if(action == "desactiver")
    {
      delUser(selectedUser);
    }
    else {
      actifUser(selectedUser);
    }
  });


  $('#resetModal').on('hidden.bs.modal', function() {
    selectedUser = "";
  })

  $('#successerrorModal').on('hidden.bs.modal', function() {
    selectedUser = "";
  })

  $('#resetModal').on('hidden.bs.modal', function() {
    $("#userReset").text("");
    $("#tempPass").text("");
  })

  $("tbody").delegate('.BTreset', 'click', function(event) {
    selectedUser = $(event.target).parent("td").attr('id');
    action = "reset";
    $('#confirmModalLabel').text("Confirmer la réinitialisation");
    $('#confirmModal').find(".modal-body").html("<p>Voulez-vouz vraiment réinitialiser le mot de passe de l'utilisateur ''" + selectedUser + "'' ?</p>");
  });

  $("tbody").delegate('.BTsuppr', 'click', function(event) {
    selectedUser = $(event.target).parent("td").attr('id');
    action = "desactiver";
    $('#confirmModalLabel').text("Confirmer la désactivation");
    $('#confirmModal').find(".modal-body").html("<p>Voulez-vouz vraiment désactiver l'utilisateur ''" + selectedUser + "'' ?</p>");
  });

  $("tbody").delegate('.BTActive', 'click', function(event) {
    selectedUser = $(event.target).parent("td").attr('id');
    action = "activer";
    $('#confirmModalLabel').text("Confirmer l'activation");
    $('#confirmModal').find(".modal-body").html("<p>Voulez-vouz vraiment activer l'utilisateur ''" + selectedUser + "'' ?</p>");
  });

  $("tbody").delegate('.BTview', 'click', function(event) {
    selectedUser = $(event.target).parent("td").attr('id');
    voirFormulaire();
  });


  $("#generate").click(function(event) {
    var pass = Fonction.random_password_generate(8,10);

    $("#password").val(pass);

  });


  $("#form_adduser").submit(function(event) {
    var pass = $("#password").val();
    var courriel = $("#courriel").val();
    var nom = $("#nom").val();
    var prenom = $("#prenom").val();
    var type = $("#type option:selected").val();

    if (courriel.trim() == "") {
      $("#successerrorModalLabel").text("Erreur");
      $("#successerrorModal").find(".modal-body").html("<p>Veuillez entrer un nom d'utilisateur</p>");
      $("#successerrorModal").modal('show');

      event.preventDefault();
      event.stopPropagation();
      return false;
    } else if (pass.trim() == "") {
      $("#successerrorModalLabel").text("Erreur");
      $("#successerrorModal").find(".modal-body").html("<p>Veuillez générer un mot de passe</p>");
      $("#successerrorModal").modal('show');

      event.preventDefault();
      event.stopPropagation();
      return false;
    } else if (nom.trim() == "") {
      $("#successerrorModalLabel").text("Erreur");
      $("#successerrorModal").find(".modal-body").html("<p>Veuillez entrer le nom de l'utilisateur</p>");
      $("#successerrorModal").modal('show');

      event.preventDefault();
      event.stopPropagation();
      return false;
    } else if (prenom.trim() == "") {
      $("#successerrorModalLabel").text("Erreur");
      $("#successerrorModal").find(".modal-body").html("<p>Veuillez entrer le prénom de l'utilisateur</p>");
      $("#successerrorModal").modal('show');

      event.preventDefault();
      event.stopPropagation();
      return false;
    } else {

      $.ajax({
        url: 'php/gestion_users/adduser.php',
        type: 'POST',
        data: {
          courriel: courriel,
          nom:  nom,
          prenom: prenom,
          pass: pass,
          type: type
        },
        success: function(data) {
          if (data == "success") {
            $("#addUserModal").modal('toggle');
            $("#successerrorModalLabel").text("Succès");
            $("#successerrorModal").find(".modal-body").html("<p>L'utilisateur a été créé</p>");
            $("#successerrorModal").modal('show');
            setTimeout(function(){
              location.reload();
            }, 3000);

          } else if (data == "error_user_exists") {
            $("#successerrorModalLabel").text("Erreur");
            $("#successerrorModal").find(".modal-body").html("<p>Ce nom d'utilisateur a déjà été attribué, veuillez réessayer</p>");
            $("#successerrorModal").modal('show');

            setTimeout(function(){
              location.reload();
            }, 3000);
          } else {
            alert(data);
          $("#addUserModal").modal('toggle');
            $("#successerrorModalLabel").text("Erreur");
            $("#successerrorModal").find(".modal-body").html("<p>Erreur lors de l'ajout de l'utilisateur</p>");
            $("#successerrorModal").modal('show');

            setTimeout(function(){
              location.reload();
            }, 3000);
          }
        }
      });

    }

    return false;
  });


});
