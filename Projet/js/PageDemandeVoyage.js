var tbNom = $("#tbNom");
var tbPrenom = $("#tbPrenom");
var tbAdresse = $("#tbAdresse");
var lsProgramme = $("#lsProgramme");
var lsDestination = $("#lsDestination");
var tbDateDepart = $("#tbDateDepart");
var tbDateRetour = $("#tbDateRetour");
var tbDestinationPrecision = $("#tbDestinationPrecision");
var tbTitreProjet = $("#tbTitreProjet");
var tbTypeProjet = $("#tbTypeProjet");
var taDescription = $("#taDescription");
var taEtudiants = $("#taEtudiants");

var maintenant = new Date();
var dd = maintenant.getDate();
var mm = maintenant.getMonth() + 1;
var yyyy = maintenant.getFullYear();
if (dd < 10) {
  dd = '0' + dd
}
if (mm < 10) {
  mm = '0' + mm
}
maintenant = yyyy + '-' + mm + '-' + dd;
document.getElementById("tbDateDepart").setAttribute("min", maintenant);
document.getElementById("tbDateRetour").setAttribute("min", maintenant);

function checkNom() {
  if (tbNom.val() != "" && lettreSeulement(tbNom.val().trim())) {
    tbNom.removeClass("is-invalid");
    tbNom.addClass("is-valid");
    return true;
  }
  tbNom.removeClass("is-valid");
  tbNom.addClass("is-invalid");
  return false;
}


function checkPrenom() {

  if (tbPrenom.val() != "" && lettreSeulement(tbPrenom.val().trim())) {
    tbPrenom.removeClass("is-invalid");
    tbPrenom.addClass("is-valid");
    return true;
  }
  tbPrenom.removeClass("is-valid");
  tbPrenom.addClass("is-invalid");
  return false;

}


function checkAdresse() {

  if (tbAdresse.val() != "" && isEmail(tbAdresse.val())) {
    tbAdresse.removeClass("is-invalid");
    tbAdresse.addClass("is-valid");
    return true;
  }
  tbAdresse.removeClass("is-valid");
  tbAdresse.addClass("is-invalid");
  return false;


}

function checkProgramme() {
  if (lsProgramme.val() != "") {
    lsProgramme.removeClass("is-invalid");
    lsProgramme.addClass("is-valid");
    return true;
  }
  lsProgramme.removeClass("is-valid");
  lsProgramme.addClass("is-invalid");
  return false;

}

function checkDestination() {

  if (lsDestination.val() != "") {
    lsDestination.removeClass("is-invalid");
    lsDestination.addClass("is-valid");
    return true;
  }
  lsDestination.removeClass("is-valid");
  lsDestination.addClass("is-invalid");
  return false;

}

function checkDateD() {

  if (tbDateDepart.val() != "") {

    tbDateDepart.removeClass("is-invalid");
    tbDateDepart.addClass("is-valid");
    return true;
  }
  tbDateDepart.removeClass("is-valid");
  tbDateDepart.addClass("is-invalid");
  return false;


}

function checkDateR() {

  if (tbDateRetour.val() != "") {
    tbDateRetour.removeClass("is-invalid");
    tbDateRetour.addClass("is-valid");
    return true;
  }
  tbDateRetour.removeClass("is-valid");
  tbDateRetour.addClass("is-invalid");
  return false;

}




function checkDestinationP() {

  if (tbDestinationPrecision.val() != "" && lettreSeulement(tbDestinationPrecision.val().trim())) {
    tbDestinationPrecision.removeClass("is-invalid");
    tbDestinationPrecision.addClass("is-valid");
    return true;
  }
  tbDestinationPrecision.removeClass("is-valid");
  tbDestinationPrecision.addClass("is-invalid");
  return false;

}

function checkTitreProjet() {

  if (tbTitreProjet.val() != "") {
    tbTitreProjet.removeClass("is-invalid");
    tbTitreProjet.addClass("is-valid");
    return true;
  }
  tbTitreProjet.removeClass("is-valid");
  tbTitreProjet.addClass("is-invalid");
  return false;

}


function checkDescription() {

  if (taDescription.val() != "") {
    taDescription.removeClass("is-invalid");
    taDescription.addClass("is-valid");
    return true;
  }
  taDescription.removeClass("is-valid");
  taDescription.addClass("is-invalid");
  return false;

}

function checkEtudiants() {
  if (taEtudiants.val() != "") {
    taEtudiants.removeClass("is-invalid");
    taEtudiants.addClass("is-valid");
    return true;
  }
  taEtudiants.removeClass("is-valid");
  taEtudiants.addClass("is-invalid");
  return false;
}

function checkAccNom(e) {
  if (e.val() != "" && lettreSeulement(e.val().trim())) {
    e.removeClass("is-invalid");
    e.addClass("is-valid");
    return true;
  }
  e.removeClass("is-valid");
  e.addClass("is-invalid");
  return false;
}

function checkAccPrenom(e) {
  if (e.val() != "" && lettreSeulement(e.val().trim())) {
    e.removeClass("is-invalid");
    e.addClass("is-valid");
    return true;
  }
  e.removeClass("is-valid");
  e.addClass("is-invalid");
  return false;
}

function checkAccTel(e) {
  if (e.val() != "" && NumeroTelCheck(e.val().trim())) {
    e.removeClass("is-invalid");
    e.addClass("is-valid");
    return true;
  }
  e.removeClass("is-valid");
  e.addClass("is-invalid");
  return false;
}

function checkAccCourriel(e) {

  if (e.val() != "" && isEmail(e.val())) {
    e.removeClass("is-invalid");
    e.addClass("is-valid");
    return true;
  }
  e.removeClass("is-valid");
  e.addClass("is-invalid");
  return false;


}

function checkActivite(e) {
  if (e.val() != "") {
    e.removeClass("is-invalid");
    e.addClass("is-valid");
    return true;
  }
  e.removeClass("is-valid");
  e.addClass("is-invalid");
  return false;
}

function checkActDate(e) {
  if (e.val() != "") {
    e.removeClass("is-invalid");
    e.addClass("is-valid");
    return true;
  }
  e.removeClass("is-valid");
  e.addClass("is-invalid");
  return false;
}





function NumeroTelCheck(tel) {
  var pat = new RegExp("^[(][0-9]{3}[)][0-9]{3}[-][0-9]{4}$");
  return (pat.test(tel));

}

function lettreSeulement(str) {
  var pat = new RegExp("^([a-zA-Z.,' -])*$");
  return (pat.test(str));
}

function isEmail(email) {
  var pat = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  return (pat.test(email));
}



var ctr1 = 1;

function ajouterLigneActivite() {
  ctr1++;


  var tr = document.createElement("TR");
  var td1 = document.createElement("TD");
  td1.style.width = "70%";
  var input1 = document.createElement("INPUT");
  input1.setAttribute("id", "tbActivite" + ctr1);
  input1.attributes.type = "text";
  input1.style.width = "100%";
  input1.classList.add("inputtable");
  input1.classList.add("form-control");
  input1.classList.add("activite");
  input1.required = true;
  var div1 = document.createElement("DIV");
  div1.classList.add("invalid-feedback");
  div1.innerText = "Veuillez entrer une activité ou supprimer la ligne";

  var td2 = document.createElement("TD");
  td2.style.width = "30%";
  var input2 = document.createElement("INPUT");
  input2.setAttribute("id", "tbDateActivite" + ctr1);
  input2.attributes.type = "text";
  input2.style.width = "100%";
  input2.classList.add("inputtable");
  input2.classList.add("form-control");
  input2.classList.add("actdate");
  input2.required = true;
  var div2 = document.createElement("DIV");
  div2.classList.add("invalid-feedback");
  div2.innerText = "Veuillez entrer une date ou supprimer la ligne";


  td1.appendChild(input1);
  td1.appendChild(div1);
  tr.appendChild(td1);

  td2.appendChild(input2);
  td2.appendChild(div2);
  tr.appendChild(td2);

  document.getElementById("TBODYactivite").appendChild(tr);

}

function supprimerLigneActivite() {
  ctr1--;

  var nb = document.getElementById('tableActivite').rows.length;
  if (nb > 2) {
    document.getElementById('tableActivite').deleteRow(-1);

  }
}

var ctr2 = 2;

function ajouterLigneAccompagnateur() {
  ctr2++;


  var tr = document.createElement("TR");

  var td1 = document.createElement("TD");
  td1.style.width = "25%";
  var input1 = document.createElement("INPUT");
  input1.setAttribute("id", "tbAccNom" + ctr2);
  input1.attributes.type = "text";
  input1.style.width = "100%";
  input1.classList.add("inputtable");
  input1.classList.add("form-control");
  input1.classList.add("nom");
  input1.required = true;
  var div1 = document.createElement("DIV");
  div1.classList.add("invalid-feedback");
  div1.innerText = "Veuillez entrer un nom ou supprimer la ligne";

  var td2 = document.createElement("TD");
  td2.style.width = "25%";
  var input2 = document.createElement("INPUT");
  input2.setAttribute("id", "tbAccPrenom" + ctr2);
  input2.attributes.type = "text";
  input2.style.width = "100%";
  input2.classList.add("inputtable");
  input2.classList.add("form-control");
  input2.classList.add("prenom");
  input2.required = true;
  var div2 = document.createElement("DIV");
  div2.classList.add("invalid-feedback");
  div2.innerText = "Veuillez entrer un prénom ou supprimer la ligne";

  var td3 = document.createElement("TD");
  td3.style.width = "25%";
  var input3 = document.createElement("INPUT");
  input3.setAttribute("id", "tbAccTelephone" + ctr2);
  input3.attributes.type = "text";
  input3.setAttribute("placeholder", "(123)456-7890");
  input3.style.width = "100%";
  input3.classList.add("inputtable");
  input3.classList.add("form-control");
  input3.classList.add("telephone");
  input3.required = true;
  var div3 = document.createElement("DIV");
  div3.classList.add("invalid-feedback");
  div3.innerText = "Veuillez entrer un numéro de téléphone ou supprimer la ligne";

  var td4 = document.createElement("TD");
  td4.style.width = "25%";
  var input4 = document.createElement("INPUT");
  input4.setAttribute("id", "tbAccCourriel" + ctr2);
  input4.attributes.type = "text";
  input4.style.width = "100%";
  input4.classList.add("inputtable");
  input4.classList.add("form-control");
  input4.classList.add("courriel");
  input4.required = true;
  var div4 = document.createElement("DIV");
  div4.classList.add("invalid-feedback");
  div4.innerText = "Veuillez entrer une adresse courriel ou supprimer la ligne";


  td1.appendChild(input1);
  td1.appendChild(div1);
  tr.appendChild(td1);

  td2.appendChild(input2);
  td2.appendChild(div2);
  tr.appendChild(td2);

  td3.appendChild(input3);
  td3.appendChild(div3);
  tr.appendChild(td3);

  td4.appendChild(input4);
  td4.appendChild(div4);
  tr.appendChild(td4);

  document.getElementById("TBODYaccompagnateur").appendChild(tr);

}

function supprimerLigneAccompagnateur() {
  ctr2--;

  var nb = document.getElementById('tableAccompagnateur').rows.length;
  if (nb > 3) {
    document.getElementById('tableAccompagnateur').deleteRow(-1);
  }
}

var listeProgrammes = ["Techniques de design d’intérieur", "Techniques de la documentation", "Techniques d’hygiène dentaire", "Techniques de diététique", "Techniques de soins infirmiers",
  "Techniques de travail social", "Techniques policières", "DEC-Bac en informatique", "Techniques de génie mécanique", "Techniques de l’informatique", "Techniques de procédés industriels",
  "Technologie de l’architecture", "Technologie de l’électronique", "Technologie de l’électronique industrielle", "Technologie de la mécanique du bâtiment (Génie du bâtiment)",
  "Technologie de la mécanique industrielle (maintenance)", "Technologie du génie civil", "Technologie du génie industriel", "Technologie du génie métallurgique – Contrôle des matériaux",
  "Technologie du génie métallurgique – Fabrication mécanosoudée", "Technologie du génie métallurgique – Procédés de transformation", "DEC-Bac en logistique", "DEC-Bac en marketing",
  "DEC-Bac en sciences comptables", "Gestion de commerces", "Techniques de comptabilité et de gestion", "Techniques de la logistique du transport"
];
listeProgrammes.forEach(loadProgrammes);

function loadProgrammes(item, index) {
  document.getElementById("listeProgramme").innerHTML += "<option value='" + item + "'>";

}

var listeDestination = ["Afghanistan", "Afrique du Sud", "Algérie", "Allemagne", "Andorre", "Angola", "Anguilla", "Antarctique", "Antigua-et-Barbuda", "Antilles néerlandaises", "Arabie saoudite",
  "Argentine", "Arménie", "Aruba", "Australie", "Autriche", "Azerbaïdjan", "Bénin", "Bahamas", "Bahreïn", "Bangladesh", "Barbade", "Belau", "Belgique", "Belize", "Bermudes", "Bhoutan", "Biélorussie",
  "Birmanie", "Bolivie", "Bosnie-Herzégovine", "Botswana", "Brésil", "Brunei", "Bulgarie", "Burkina Faso", "Burundi", "Côte d'Ivoire", "Cambodge", "Cameroun", "Canada", "Cap-Vert", "Chili", "Chine",
  "Chypre", "Colombie", "Comores", "Congo", "Corée du Nord", "Corée du Sud", "Costa Rica", "Croatie", "Cuba", "Danemark", "Djibouti", "Dominique", "Égypte", "Émirats arabes unis", "Équateur", "Érythrée",
  "Espagne", "Estonie", "États-Unis", "Éthiopie", "Finlande", "France", "Géorgie", "Gabon", "Gambie", "Ghana", "Gibraltar", "Grèce", "Grenade", "Groenland", "Guadeloupe", "Guam", "Guatemala", "Guinée",
  "Guinée équatoriale", "Guinée-Bissao", "Guyana", "Guyane française", "Haïti", "Honduras", "Hong Kong", "Hongrie", "Ile Bouvet", "Ile Christmas", "Ile Norfolk", "Iles Cayman", "Iles Cook", "Iles Féroé",
  "Iles Falkland", "Iles Fidji", "Iles Géorgie du Sud et Sandwich du Sud", "Iles Heard et McDonald", "Iles Marshall", "Iles Pitcairn", "Iles Salomon ", "Iles Svalbard et Jan Mayen",
  "Iles Turks-et-Caicos", "Iles Vierges américaines", "Iles Vierges britanniques", "Iles des Cocos (Keeling)", "Iles mineures éloignées des États-Unis", "Inde", "Indonésie", "Iran", "Iraq", "Irlande",
  "Islande", "Israël", "Italie", "Jamaïque", "Japon", "Jordanie", "Kazakhstan", "Kenya", "Kirghizistan", "Kiribati", "Koweït", "Laos", "Lesotho", "Lettonie", "Liban", "Liberia", "Libye", "Liechtenstein",
  "Lituanie", "Luxembourg", "Macao", "Madagascar", "Malaisie", "Malawi", "Maldives", "Mali", "Malte", "Mariannes du Nord", "Maroc", "Martinique", "Maurice", "Mauritanie", "Mayotte", "Mexique", "Micronésie",
  "Moldavie", "Monaco", "Mongolie", "Montserrat", "Mozambique", "Népal", "Namibie", "Nauru", "Nicaragua", "Niger", "Nigeria", "Nioué", "Norvège", "Nouvelle-Calédonie", "Nouvelle-Zélande", "Oman", "Ouganda",
  "Ouzbékistan", "Pérou", "Pakistan", "Panama", "Papouasie-Nouvelle-Guinée", "Paraguay", "Pays-Bas", "Philippines", "Pologne", "Polynésie française", "Porto Rico", "Portugal", "Qatar", "République centrafricaine",
  "République démocratique du Congo", "République dominicaine", "République tchèque", "Réunion", "Roumanie", "Royaume-Uni", "Russie", "Rwanda", "Sénégal", "Sahara occidental",
  "Saint-Christophe-et-Niévès", "Saint-Marin", "Saint-Pierre-et-Miquelon", "Saint-Siège", "Saint-Vincent-et-les-Grenadines", "Sainte-Hélène", "Sainte-Lucie", "Salvador", "Samoa", "Samoa américaines",
  "Sao Tomé-et-Principe", "Seychelles", "Sierra Leone", "Singapour", "Slovénie", "Slovaquie", "Somalie", "Soudan", "Sri Lanka", "Suède", "Suisse", "Suriname", "Swaziland", "Syrie", "Taïwan", "Tadjikistan",
  "Tanzanie", "Tchad", "Terres australes françaises", "Territoire britannique de l'Océan Indien", "Thaïlande", "Timor Oriental", "Togo", "Tokélaou", "Tonga", "Trinité-et-Tobago", "Tunisie",
  "Turkménistan", "Turquie", "Tuvalu", "Ukraine", "Uruguay", "Vanuatu", "Venezuela", "Vietnam", "Wallis-et-Futuna", "Yémen", "Yougoslavie", "Zambie", "Zimbabwe", "ex-République yougoslave de Macédoine"
];
listeDestination.forEach(loadDestination);

function loadDestination(item, index) {

  document.getElementById("listeDestination").innerHTML += "<option value='" + item + "'>";


}

$("#tbNom").change(function() {
  checkNom();
});
$("#tbPrenom").change(function() {
  checkPrenom();
});
$("#tbAdresse").change(function() {
  checkAdresse();
});
$("#lsProgramme").change(function() {
  checkProgramme();
});
$("#lsDestination").change(function() {
  checkDestination();
});
$("#tbDateDepart").change(function() {

  document.getElementById("tbDateRetour").setAttribute("min", $("#tbDateDepart").val());
  checkDateD();
});
$("#tbDateRetour").change(function() {
  document.getElementById("tbDateDepart").setAttribute("max", $("#tbDateRetour").val());

  checkDateR();
});
$("#tbDestinationPrecision").change(function() {
  checkDestinationP();
});
$("#tbTitreProjet").change(function() {
  checkTitreProjet();
});
$("#taDescription").change(function() {
  checkDescription();
});
$("#taEtudiants").change(function() {
  checkEtudiants();
});

$("table").delegate(".nom", "change", function() {
  checkAccNom($(this));

});
$("table").delegate(".prenom", "change", function() {
  checkAccPrenom($(this));

});
$("table").delegate(".telephone", "change", function() {
  checkAccTel($(this));
});
$("table").delegate(".courriel", "change", function() {
  checkAccCourriel($(this));

});
$("table").delegate(".activite", "change", function() {
  checkActivite($(this));

});
$("table").delegate(".actdate", "change", function() {
  checkActDate($(this));
});








$("#form").submit(function() {

  var file = $("#fileUpload");
  var fileFormData = new FormData();
  fileFormData.append("file", file.prop('files')[0]);


  var aractivite = [];
  var ardateactivite = [];
  var arnomacc = [];
  var arpreacc = [];
  var artelacc = [];
  var arcouacc = [];

  var liberation = $("input[name=liberation]:checked").val();
  var acceptation = $("input[name=acceptation]:checked").val();
  var recrutement = $("input[name=recrutement]:checked").val();


  for (var ctract = 1; ctract <= ctr1; ctract++) {
    if (checkActivite($("#tbActivite" + ctract)) && checkActDate($("#tbDateActivite"+ctract)) ) {
    aractivite.push($("#tbActivite" + ctract).val());
    ardateactivite.push($("#tbDateActivite" + ctract).val());}
    else{
      alert("Il y a eu une erreur lors de l'envoi de la demande. Veuillez vérifier que tous les champs soient bien remplis puis réessayer.");
      event.preventDefault();
      event.stopPropagation();
      return false;
  }
}

  for (var ctracc = 1; ctracc <= ctr2; ctracc++) {
    if(checkAccNom($("#tbAccNom"+ctracc)) && checkAccPrenom($("#tbAccPrenom"+ctracc)) && checkAccTel($("#tbAccTelephone"+ctracc)) && checkAccCourriel($("#tbAccCourriel"+ctracc)) ){
    arnomacc.push($("#tbAccNom" + ctracc).val());
    arpreacc.push($("#tbAccPrenom" + ctracc).val());
    artelacc.push($("#tbAccTelephone" + ctracc).val());
    arcouacc.push($("#tbAccCourriel" + ctracc).val());
  }
else{
  alert("Il y a eu une erreur lors de l'envoi de la demande. Veuillez vérifier que tous les champs soient bien remplis puis réessayer.");
  event.preventDefault();
  event.stopPropagation();
  return false;
}
}

  $.ajax({
      url: 'php/uploadfile.php',
      method: "POST",
      data: fileFormData,
      contentType: false,
      cache: false,
      processData: false,
      success: function(data) {
        if (data == "-1") {
          projID = -1;
        }
        else if (data.indexOf("error") >= 0) {
          alert(data);
          projID = -1;
        } else {
          projID = parseInt(data);
        }
      }
    })
    .done(function() {

      $.ajax({
        url: 'php/requetesqldemande.php',
        type: 'POST',
        data: {
          aractivite: aractivite.toString(),
          ardateactivite: ardateactivite.toString(),
          arnomacc: arnomacc.toString(),
          arpreacc: arpreacc.toString(),
          artelacc: artelacc.toString(),
          arcouacc: arcouacc.toString(),
          nom: tbNom.val(),
          prenom: tbPrenom.val(),
          adresse: tbAdresse.val(),
          programme: lsProgramme.val(),
          destination: lsDestination.val(),
          dateD: tbDateDepart.val(),
          dateR: tbDateRetour.val(),
          precisionDestination: tbDestinationPrecision.val(),
          titreProjet: tbTitreProjet.val(),
          typeProjet: tbTypeProjet.val(),
          description: taDescription.val(),
          etudiants: taEtudiants.val(),
          liberation: liberation,
          acceptation: acceptation,
          recrutement: recrutement,
          strategies: $("#taRecrutement").val(),
          projID: projID
        },
        success: function(data) {
          if (data == "success") {
            alert("Votre demande a bien été envoyée");
            window.location.href="?page=demande";
          } else {
            alert(data);
          }
        }
      });
    });




  return false;
});
