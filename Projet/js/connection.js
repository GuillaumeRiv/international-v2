var connexion_alert = $('#connexion_alert');
connexion_alert.hide();
var alert_message = $('#alert_message');
var courriel = $('#tbCourriel');
var password = $('#tbPassword');

var btConnexion = $('#btConnexion');

courriel.change(function(){
    checkCourriel();
});

password.change(function(){
    checkPassword();
})

btConnexion.click(function(){
    checkFields();
})

function checkFields()
{
     checkCourriel();
     checkPassword();
}

function checkCourriel()
{
    if((courriel.val().trim() !=""))
    {
      courriel.removeClass("is-invalid");
      courriel.addClass("is-valid");
      hideErrorMessage();
      return true;
    }

      courriel.removeClass("is-valid");
      courriel.addClass("is-invalid");
      showErrorMessage();
      return false;
}

function checkPassword()
{
    if((password.val().trim() !="")&&(password.val() != null))
    {
      password.removeClass("is-invalid");
      password.addClass("is-valid");
      hideErrorMessage();
      return true;
    }

      password.removeClass("is-valid");
      password.addClass("is-invalid");
      showErrorMessage();
      return false;
}




function showErrorMessage(){
  alert_message.text("Veuillez remplir tous les champs");
  connexion_alert.show();
}

function hideErrorMessage(){
  connexion_alert.hide();
}


$('#frm_connexion').submit(function(){
			var courriel = Fonction.escapeHtml($('#tbCourriel').val());
			var passwd = Fonction.escapeHtml($('#tbPassword').val());

      if(checkCourriel() && checkPassword())
      {
        $.post('php/login.php',{usr:courriel,passwd:passwd},function(data){
          $('#tbCourriel').val('');
          $('#tbPassword').val('');

          if (data == "success"){
            hideErrorMessage();
            window.location = "main.php";
          }
          else if(data=="error") {
            showErrorMessage();
            checkCourriel();
            checkPassword();
            alert_message.text("Adresse courriel ou mot de passe invalide");
          }
          else{
            showErrorMessage();
            alert_message.text("Erreur de connexion à la base de donnée");
          }
        });
      }
  return false;
});
