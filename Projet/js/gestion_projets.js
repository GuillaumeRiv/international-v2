$(document).ready(function() {
    var selectedProjet = "";
    var idProjet = -1;
    var type = "";
     pullType();

$("#affProjets").click(function(){
  $("#affProjets").removeClass("btSelectAffichage");
  $("#affProjets").addClass("btAffichage");
  $("#affProjetsD").removeClass("btAffichage");
  $("#affProjetsD").addClass("btSelectAffichage");
  $("#tblProjetsD").hide();
  $("#tblProjetsD_wrapper").hide();
  $("#tblProjets").show();
  $("#tblProjets_wrapper").show();
});
$("#affProjetsD").click(function(){
  $("#affProjetsD").removeClass("btSelectAffichage");
  $("#affProjetsD").addClass("btAffichage");
  $("#affProjets").removeClass("btAffichage");
  $("#affProjets").addClass("btSelectAffichage");
  $("#tblProjets").hide();
  $("#tblProjets_wrapper").hide();
  $("#tblProjetsD").show();
  $("#tblProjetsD_wrapper").show();
});

  function remplirTbl(arr){
    $("#tblProjets tbody").html("");
    if(arr.nom.length == 0)
    {
      var tr = $("<tr></tr>");
      var th = $("<td scope='row' colspan='5'></th>").text("Aucun projet trouvé.");
      tr.append(th);
      $("#tblProjets tbody").append(tr);

    }
    else
    {
        for(var ctr = 0; ctr < arr.id.length; ctr++)
        {
          var tr = $("<tr></tr>");
          var th = $("<th scope='row'></th>").text(ctr+1);
          var td = $("<td style='width: 30%'></td>").text(arr.nom[ctr]);
          var td1 = $("<td style='width: 15%'></td>").text(arr.dateD[ctr]);
          var td2 = $("<td style='width: 15%'></td>").text(arr.code[ctr]);
          var td3 = $("<td id="+ arr.id[ctr] + "style='width: 40%'></td>").attr('nom', arr.nom[ctr]).attr("id", arr.id[ctr]);

          var bt = $("<input class='btn btn-primary BTselection' type='button' value='Sélectionner le projet'>");

          if (type == "Administrateur"){
            var bt2= $("<input class='btn btn-danger BTdesactiver' type='button' value='Désactiver'>");}
            else{ var bt2 = $("<span></span>")}


          var icon = $("<i class='fas fa-check fa-2x' aria-hidden='true' style='color:green; margin-left:1vw;'></i>");

          td3.append(bt,bt2,icon);

          tr.append(th, td,td1,td2,td3);
          $("#tblProjets tbody").append(tr);
          $("i").hide();
          $("#iRetour").show();
        }
        checkProjet();
        $('#tblProjets').DataTable({
          "oLanguage":{
	"sEmptyTable":     "Aucune donnée disponible dans le tableau",
	"sInfo":           "Affichage de l'élément _START_ à _END_ sur _TOTAL_ éléments",
	"sInfoEmpty":      "Affichage de l'élément 0 à 0 sur 0 élément",
	"sInfoFiltered":   "(filtré à partir de _MAX_ éléments au total)",
	"sInfoPostFix":    "",
	"sInfoThousands":  ",",
	"sLengthMenu":     "Afficher _MENU_ éléments",
	"sLoadingRecords": "Chargement...",
	"sProcessing":     "Traitement...",
	"sSearch":         "Recherche :",
	"sZeroRecords":    "Aucun élément correspondant trouvé",
   "oPaginate": {
		"sFirst":    "Premier",
		"sLast":     "Dernier",
		"sNext":     "Suivant",
		"sPrevious": "Précédent"
	},
	"oAria": {
		"sSortAscending":  ": activer pour trier la colonne par ordre croissant",
		"sSortDescending": ": activer pour trier la colonne par ordre décroissant"
	},
	"select": {
        	"rows": {
         		"_": "%d lignes sélectionnées",
         		"0": "Aucune ligne sélectionnée",
        		"1": "1 ligne sélectionnée"
        	}
	}
}
});
    }
  }

  function remplirTblD(arr){
    $("#tblProjetsD tbody").html("");
    if(arr.nom.length == 0)
    {
      var tr = $("<tr></tr>");
      var th = $("<td scope='row' colspan='5'></th>").text("Aucun projet trouvé.");

      tr.append(th);
      $("#tblProjetsD tbody").append(tr);

    }
    else
    {
        for(var ctr = 0; ctr < arr.id.length; ctr++)
        {
          var tr = $("<tr></tr>");
          var th = $("<th scope='row'></th>").text(ctr+1);
          var td = $("<td style='width: 30%'></td>").text(arr.nom[ctr]);
          var td1 = $("<td style='width: 15%'></td>").text(arr.dateD[ctr]);
          var td2 = $("<td style='width: 15%'></td>").text(arr.code[ctr]);
          var td3 = $("<td id="+ arr.id[ctr] + "style='width: 40%'></td>").attr('nom', arr.nom[ctr]).attr("id", arr.id[ctr]);
          if (type == "Administrateur"){
          var bt2= $("<input class='btn btn-success BTactiver' type='button' value='Activer'>");}
          else{
            var bt2= $("<p>Aucune action disponible</p>");}

          var icon = $("<i class='fas fa-check fa-2x' aria-hidden='true' style='color:green; margin-left:1vw;'></i>");

          td3.append(bt2,icon);

          tr.append(th, td,td1,td2,td3);
          $("#tblProjetsD tbody").append(tr);
          $("i").hide();
          $("#iRetour").show();
        }
        checkProjet();
        $('#tblProjetsD').DataTable({
          "oLanguage":{
  "sEmptyTable":     "Aucune donnée disponible dans le tableau",
  "sInfo":           "Affichage de l'élément _START_ à _END_ sur _TOTAL_ éléments",
  "sInfoEmpty":      "Affichage de l'élément 0 à 0 sur 0 élément",
  "sInfoFiltered":   "(filtré à partir de _MAX_ éléments au total)",
  "sInfoPostFix":    "",
  "sInfoThousands":  ",",
  "sLengthMenu":     "Afficher _MENU_ éléments",
  "sLoadingRecords": "Chargement...",
  "sProcessing":     "Traitement...",
  "sSearch":         "Recherche :",
  "sZeroRecords":    "Aucun élément correspondant trouvé",
   "oPaginate": {
    "sFirst":    "Premier",
    "sLast":     "Dernier",
    "sNext":     "Suivant",
    "sPrevious": "Précédent"
  },
  "oAria": {
    "sSortAscending":  ": activer pour trier la colonne par ordre croissant",
    "sSortDescending": ": activer pour trier la colonne par ordre décroissant"
  },
  "select": {
          "rows": {
            "_": "%d lignes sélectionnées",
            "0": "Aucune ligne sélectionnée",
            "1": "1 ligne sélectionnée"
          }
  }
}
});
$("#tblProjetsD").hide();
$("#tblProjetsD_wrapper").hide();
    }
}



  function pullProjets(){
    $.ajax({
      url: 'php/gestion_projets/pullprojets.php',
      type: 'POST',
      success: function(data){
        var reponse = $.parseJSON(data);
        $("#affProjets").removeClass("btSelectAffichage");
        $("#affProjets").addClass("btAffichage");
        remplirTbl(reponse);
      }
    });
  }
  function pullProjetsD(){
    $.ajax({
      url: 'php/gestion_projets/pullprojetsD.php',
      type: 'POST',
      success: function(data){
        var reponse = $.parseJSON(data);
        remplirTblD(reponse);
      }
    });
  }
  function pullType(){
    $.ajax({
      url: 'php/gestion_projets/pullType.php',
      type: 'POST',
      success: function(data){
          $("#tblProjetsD").hide();
        pullProjets();
        pullProjetsD();
        if (data == "Administrateur" || data == "Enseignant" || data == "Étudiant" ){
          type = data;
        }
        else{
          alert(data);
        }
      }
    });
  }

  function checkProjet()
  {
    $.ajax({
      url: 'php/gestion_projets/checkprojets.php',
      type: 'POST',
      success: function(data){
        if(data !="empty")
        {
           $("#" + data).children("i").show();
          $("#" + data).children("input").prop("disabled", true);
        }

      }
    });
  }



    $("tbody").delegate('.BTselection', 'click', function(event) {
        selectedProjet = $(event.target).parent("td").attr('nom');
        idProjet = $(event.target).parent("td").attr("id");
        $("i").hide();
        $("#iRetour").show();
        $("input").prop("disabled", false);
        $(event.target).prop("disabled", true);
        $(event.target).parent("td").children("i").show();
        $(event.target).parent("td").children("input").prop("disabled", true);
        $('#selectModalLabel').text("Sélection de projet");
        $('#selectModal').find(".modal-body").html("<p>Projet ''" + selectedProjet + "'' a été sélectionné !</p>");
        $('#selectModal').modal('toggle');
        $("#projetTitle").text(selectedProjet);
        $.ajax({
          url: 'php/gestion_projets/setprojetname.php',
          type: 'POST',
          data: {nom: selectedProjet, idProjet:idProjet},
          success: function(data){
            if(data == "error")
            {
              alert("Une erreur est survenu.");
            }
          }
        });
  });

  $("tbody").delegate('.BTdesactiver', 'click', function(event) {
      selectedProjet = $(event.target).parent("td").attr('nom');
      idProjet = $(event.target).parent("td").attr("id");
      $.ajax({
        url: 'php/gestion_projets/desactiver_projet.php',
        type: 'POST',
        data: {nom: selectedProjet, idProjet:idProjet},
        success: function(data){
          if (data == "success"){
            location.reload();
          }
          else alert("Une erreur est survenu.");
        }
      });
});
$("tbody").delegate('.BTactiver', 'click', function(event) {
    selectedProjet = $(event.target).parent("td").attr('nom');
    idProjet = $(event.target).parent("td").attr("id");
    $.ajax({
      url: 'php/gestion_projets/activer_projet.php',
      type: 'POST',
      data: {nom: selectedProjet, idProjet:idProjet},
      success: function(data){
        if (data == "success"){
          location.reload();
        }
        else alert("Une erreur est survenu.");
      }
    });
});


function checkCodeProjet(code){
  if (code.trim() == ""){
    $("#tbAddProjet").removeClass("is-valid");
      $("#tbAddProjet").addClass("is-invalid");
    return false;
  }

      $("#tbAddProjet").removeClass("is-invalid");
      $("#tbAddProjet").addClass("is-valid");
    return true;

};

$("#formAddProjet").submit(function(){
  if(checkCodeProjet($("#tbAddProjet").val())){
  $.post('php/gestion_projets/ajout_projet.php',{tbAddProjet:$("#tbAddProjet").val()},function(data){
alert(data);
    if (data == "success"){

    }
    else if(data=="Vous participer déjà à ce projet") {
      alert(data);
    }
    else if(data == "Code de projet invalide"){
      alert(data);
    }
  }    );

}
else{event.preventDefault();
event.stopPropagation();}

}  );



});
