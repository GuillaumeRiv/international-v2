<header class="container-fluid">
<div class="row">
  <div class="col-md-5 col-sm-2 col-2">
      <a href="main.php">
          <img id="logo" src="img/iconcegep.jpg" alt="Icone du cegep">
      </a>
  </div>
    <div class="form-inline">
      <div class="form-group">
        <span class="fas fa-user" id="hello"> Bonjour, <?= $_SESSION['prenom'], ' ', $_SESSION['nom'] ?> </span>
        </div>
      <div class="form-group">
        <button class="btn btn-secondary btn-block" id="btDeconnexion" type="button" onclick="window.location.href='connection.php'" style="font-size: large;">Déconnexion</button>
      </div>
    </div>
 </div>
</header>


      <script type="text/javascript"><?php

 if (session_status() == PHP_SESSION_NONE) {
     session_start();
 }

   require 'php/BD.inc.php';

   $stmt = $conn->prepare("SELECT * from usr_projet_info where userID = :userID;");
   $stmt->execute(array(':userID' => $_SESSION['userID']));
   $projID = $stmt->fetchAll();
   $projctr = count($projID); ?>
<?php
  if ($projctr >= 1 || $_SESSION['type'] == "Administrateur") {
      if (!empty($_SESSION["nomprojet"])) {?>
    $("#projetTitle").html("<?=$_SESSION["nomprojet"]?>");<?php
      } else {?>
     $("#projetTitle").html('Aucun projet n\'a été sélectionné');<?php
      }
  } elseif ($projctr == 0) {?>
    $("#projetTitle").html('Aucun projet n\'a été créé');<?php
  }

$conn = null;?>

$(document).ready(function() {
  $(".dropdown-item").click(function(event) {
    $(".dropdown-toggle").text($(this).text());
  });

if($(window).width() < 1185)
{
  $("#logo").attr('src', 'img/iconcegepmin.jpg');
  $("#logo").width(53);
  $("#btDeconnexion").html("<i class='fas fa-sign-out-alt'></i>");
}
else{
  $("#logo").attr('src', 'img/iconcegep.jpg');
  $("#logo").width('auto');
  $("#btDeconnexion").html('Déconnexion');
}

if (matchMedia) {
const mq = window.matchMedia("(min-width: 1185px)");
mq.addListener(WidthChange);
WidthChange(mq);
}


function WidthChange(mq) {
if (mq.matches) {
  $("#logo").attr('src', 'img/iconcegep.jpg');
  $("#logo").width('auto');
  $("#btDeconnexion").html('Déconnexion');
} else {
  $("#logo").attr('src', 'img/iconcegepmin.jpg');
  $("#logo").width(53);
  $("#btDeconnexion").html("<i class='fas fa-sign-out-alt'></i>");
}

}

});


    </script>
