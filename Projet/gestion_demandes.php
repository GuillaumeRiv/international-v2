<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title></title>

  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="css/gestion_demandes.css">
</head>
<body>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

  <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
  
  <script src="js/gestion_demandes.js" charset="utf-8"></script>

  <button id="btRetour"type="button" class="btn btn-secondary col-md-1"  onclick="window.location.href='main.php'"><i class="fas fa-arrow-left"></i> Retour</button><?php
  if (isset($_SESSION['type']) && ($_SESSION['type'] == "Administrateur")||($_SESSION['type'] == "Enseignant")) {?>
    <div class="midSection">

    <h1 class="text-white">Gestion des demandes</h1>
    <br><br><?php
      if($_SESSION['type']=='Enseignant')
      {?>
    <div class="row">
      <a href="?page=demande&option=new" style="text-decoration:none;">
        <input id="btCreerDemande" class="btn btn-primary col-md-2" type="button" name="" value="Créer une demande">
      </a>
    </div><?php
      }?>

    <table id="tblDemandes" class="table table-striped table-bordered">
      <thead class="thead-dark">
        <th scope="col">#</th>
        <th scope="col">Destination</th>
        <th scope="col">Utilisateur</th>
        <th scope="col">Actions</th>
        <th scope="col">État</th>
      </thead>
      <tbody class="table-light">
      </tbody>
    </table>

        <div class="modal fade" id="acceptModal" tabindex="-1" role="dialog" aria-labelledby="acceptModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="acceptModalLabel"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
            <button id="acceptBt" type="button" class="btn btn-success" data-dismiss="modal">Oui</button>
          </div>
        </div>
      </div>
    </div>


    <div class="modal fade" id="refuseModal" tabindex="-1" role="dialog" aria-labelledby="refuseModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="refuseModalLabel">Refuser la demande</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
               <p>Veuillez entrer la raison du refus de la demande (Optionnel)</p>
              <textarea id="msgRefus" rows="7" cols="55"></textarea >
          </div>
          <div class="modal-footer">
            <button id="btRefCancel" type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
            <button id="refuseBt" type="button" class="btn btn-danger" data-dismiss="modal">Refuser</button>
          </div>
        </div>
      </div>

    </div>

    <div class="modal fade" id="raisonrefuseModal" tabindex="-1" role="dialog" aria-labelledby="raisonrefuseModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="raisonrefuseModalLabel">Refuser la demande</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
          </div>
          <div class="modal-footer">
            <button id="btRefCancel" type="button" class="btn btn-secondary" data-dismiss="modal">Retour</button>
          </div>
        </div>
      </div>
    </div>
    <div id="overlay"></div><?php

  } else {
    echo "<br><br><h3>Vous n'avez pas les autorisations nécessaires pour accéder à cette page.</h3>";
}?>
<div id="overlay"></div>
</body>
</html>
