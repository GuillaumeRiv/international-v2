<table id="admintable">
  <tr>
    <td class="admintd">
      <a href="?page=formulaire">
        <button class="sectionA">
          <img class="iconSection" src="img/formIcon.png" alt="IconeFormulaire">
          <span>Formulaires</span>
        </button>
      </a>
    </td>
    <td class="admintd">
      <a href="?page=demande">
        <button class="sectionA">
          <img class="iconSection" src="img/demandeIcon.png" alt="IconeFormulaire">
          <span>Demandes</span>
        </button>
      </a>
    </td>
  </tr>
  <tr>
    <td class="admintd">
      <a href="?page=profil">
        <button class="sectionA">
          <img class="iconSection" src="img/profileIcon.png" alt="IconeProfileS">
          <span>Profil</span>
        </button>
      </a>
    </td>
    <td class="admintd">
      <a href="?page=projets">
        <button class="sectionA">
          <img class="iconSection" src="img/projetIcon.png" alt="IconeProjet">
          <span>Projets</span>
        </button>
      </a>
    </td>
  </tr>

  <tr>
    <td class="admintd">
      <a href="?page=appreciation_admin">
        <button class="sectionA">
          <img class="iconSection" src="img/appreciationIcon.png" alt="IconePréférences">
          <span>Bilan du séjour</span>
        </button>
      </a>
    </td>
    <td class="admintd">
      <a href="?page=gestion_users">
        <button class="sectionA">
          <img class="iconSection" src="img/groupIcon.png" alt="IconePréférences">
          <span>Utilisateurs</span>
        </button>
      </a>
    </td>
  </tr>
   <tr>
    <td class="admintd">
      <a href="?page=statistiques">
        <button class="sectionA">
          <img class="iconSection" src="img/statsIcon.png" alt="IconeStatistiques">
          <span>Statistiques</span>
        </button>
      </a>
    </td>
   <td class="admintd">
     <a href="?page=tableau_de_bord">
       <button class="sectionA">
         <img class="iconTableau" src="img/iconTableau.png" alt="IconeTableau">
         <span>Tableau de bord</span>
       </button>
     </a>
   </td>
 </tr>
</table>
<div id="overlay"></div>
<script type="text/javascript">
if ($(window).width() < 880) {
  $(".sectionA").find("span").css('font-size', '18px');
} else {
  $(".sectionA").find("span").css('font-size', '');
}

if (matchMedia) {
  const mq = window.matchMedia("(min-width: 880px)");
  mq.addListener(WidthChange);
  WidthChange(mq);
}


function WidthChange(mq) {
  if (mq.matches) {
    $(".sectionA").find("span").css('font-size', '');
  } else {
    $(".sectionA").find("span").css('font-size', '18px');
  }

}
</script>
