<?php

require '../BD.inc.php';

if(isset($_POST['id'])){

  $id = $_POST['id'];

  $sql = "SELECT raison_refus FROM etat_demande WHERE id_demande = :id ";
    $stmt = $conn->prepare($sql);
    $stmt->execute(array(':id' => $id));
    $raison = $stmt->fetch();

  echo $raison['raison_refus'];
}
else {
  echo 'error';
}

$conn = null;

?>
