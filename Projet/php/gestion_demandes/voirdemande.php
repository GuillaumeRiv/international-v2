<?php

require '../BD.inc.php';

if(isset($_POST['id'])){

    $id = $_POST['id'];
 
    $data = array();

    $activites = array();
    $dateactivites = array();

    $acc_nom = array();
    $acc_prenom = array();
    $acc_telephone = array();
    $acc_courriel = array();
    

    //INFORMATIONS GÉNÉRALES
  $sql="SELECT projetDocsID, Nom, Prenom, adresse, programme, destination, dateD, dateR, precisionDestination, titre, categorie , description, etudiants, liberation, financement, recrutement, strategies  FROM demandes WHERE ID=:demandeID;";
    $stmt = $conn->prepare($sql);
    $stmt->execute(array(':demandeID' => $id));
    $demande = $stmt->fetch();

     $projetDocsID = $demande['projetDocsID'];
     $Nom = $demande['Nom'];
     $Prenom = $demande['Prenom'];
     $adresse = $demande['adresse'];
     $programme = $demande['programme'];
     $destination = $demande['destination'];
     $dateD = $demande['dateD'];
     $dateR = $demande['dateR'];
     $precisionDestination = $demande['precisionDestination'];
     $titre = $demande['titre'];
     $type = $demande['categorie'];
     $description = $demande['description'];
     $etudiants = $demande['etudiants'];
     $liberation = $demande['liberation'];
     $financement = $demande['financement'];
     $recrutement = $demande['recrutement'];
     $strategies = $demande['strategies'];


$data['projetDocsID'] = $projetDocsID;
$data['Nom'] = $Nom;
$data['Prenom'] = $Prenom;
$data['adresse'] = $adresse;
$data['programme'] = $programme;
$data['destination'] = $destination;
$data['dateD'] = $dateD;
$data['dateR'] = $dateR;
$data['precisionDestination'] = $precisionDestination;
$data['titre'] = $titre;
$data['type'] = $type;
$data['description'] = $description;
$data['etudiants'] = $etudiants;
$data['liberation'] = $liberation;
$data['financement'] = $financement;
$data['recrutement'] = $recrutement;
$data['strategies'] = $strategies;

    // ACTIVITES
    $sql="SELECT activites,dates FROM demandes_activites WHERE id_demande=:demandeID;";
    $stmt = $conn->prepare($sql);
    $stmt->execute(array(':demandeID' => $id));
    $sqlactivites = $stmt->fetchAll(\PDO::FETCH_ASSOC);

    foreach ($sqlactivites as $sqlactivite)
    {
      array_push($activites, $sqlactivite["activites"]);
      array_push($dateactivites, $sqlactivite["dates"]);
    }

    $data["activites"] = $activites;
    $data["dateactivites"] = $dateactivites;

    //ACCOMPAGNATEURS
    $sql="SELECT nom, prenom, telephone, courriel FROM demandes_accompagnateurs WHERE id_demande=:demandeID;";
    $stmt = $conn->prepare($sql);
    $stmt->execute(array(':demandeID' => $id));
    $sqlaccs = $stmt->fetchAll(\PDO::FETCH_ASSOC);

    foreach ($sqlaccs as $sqlacc)
    {
      array_push($acc_nom, $sqlacc["nom"]);
      array_push($acc_prenom, $sqlacc["prenom"]);
      array_push($acc_telephone, $sqlacc["telephone"]);
      array_push($acc_courriel, $sqlacc["courriel"]);
    }

    $data["acc_nom"] = $acc_nom;
    $data["acc_prenom"] = $acc_prenom;
    $data["acc_telephone"] = $acc_telephone;
    $data["acc_courriel"] = $acc_courriel;


echo json_encode($data);
}
else
{
  echo "error";
}
$conn = null;

