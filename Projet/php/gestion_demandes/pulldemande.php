<?php

require '../BD.inc.php';

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

$data = array();
$id = array();
$user = array();
$etat = array();
$idUser = array();
$destination = array();
$idDemande = array();

if ($_SESSION['type'] == 'Administrateur') {

$sql = "SELECT id_demande, id_formulaire, id_user, (SELECT CONCAT(u.prenom,' ',u.nom) as nom FROM utilisateurs u WHERE u.ID = id_user) as user, etat, destination FROM etat_demande";

    foreach($conn->query($sql) as $row){
      array_push($id, $row['id_formulaire']);
      array_push($user, $row['user']);
      array_push($idUser, $row['id_user']);
      array_push($etat, $row['etat']);
      array_push($destination, $row['destination']);
      array_push($idDemande, $row['id_demande']);
    }

    $data['id'] = $id;
    $data['user'] = $user;
    $data['etat'] = $etat;
    $data['id_user'] = $idUser;
    $data['destination'] = $destination;
    $data['id_demande']= $idDemande;
}
else if($_SESSION['type'] == 'Enseignant'){
  $sql = "SELECT id_demande, id_formulaire, id_user, (SELECT CONCAT(u.prenom,' ',u.nom) as nom FROM utilisateurs u WHERE u.ID = id_user) as user, etat, destination FROM etat_demande WHERE id_user = :userId";

    $stmt = $conn->prepare($sql);
    $stmt->execute(array(':userId' => $_SESSION['userID']));
    $demandes = $stmt->fetchAll(\PDO::FETCH_ASSOC);

    foreach($demandes as $row){
      array_push($id, $row['id_formulaire']);
      array_push($user, $row['user']);
      array_push($idUser, $row['id_user']);
      array_push($etat, $row['etat']);
      array_push($destination, $row['destination']);
      array_push($idDemande, $row['id_demande']);
    }


    $data['id'] = $id;
    $data['user'] = $user;
    $data['etat'] = $etat;
    $data['id_user'] = $idUser;
    $data['destination'] = $destination;
    $data['id_demande']= $idDemande;
}



echo json_encode($data);

$conn = null;
