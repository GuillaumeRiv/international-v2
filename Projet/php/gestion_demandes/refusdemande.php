<?php

require '../BD.inc.php';

if(isset($_POST['id_demande'])){

  $id = $_POST['id_demande'];
  if(isset($_POST['msgrefus']))
  {
     $msgrefus = $_POST['msgrefus'];
  }
  else{
      $msgrefus = "";
  }

  $a = 2;

  $sql="UPDATE etat_demande SET etat=:etat, raison_refus=:raison WHERE id_demande=:id_demande";
  $stmt = $conn->prepare($sql);
  $stmt->bindParam(':etat', $a);
  $stmt->bindParam(':raison', $msgrefus);
  $stmt->bindParam(':id_demande', $id);
  $stmt->execute();
  echo 'success';
}
else {
  echo 'error';
}

$conn = null;
