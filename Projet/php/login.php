<?php

require 'BD.inc.php';

if (session_status() == PHP_SESSION_NONE) {
  session_start();
}
session_regenerate_id(true);

if (!empty(trim($_POST['usr'])) && !empty(trim($_POST['passwd'])))
{
  $courriel = $_POST['usr'];
  $passwd = hash('SHA256', $_POST['passwd']);

  $sql = "SELECT ID,nom,prenom,type,actif FROM utilisateurs where courriel = LOWER(:courriel) and password = :passwd";

  $stmt = $conn->prepare($sql);

  $stmt->execute(array(':courriel' => $courriel, ':passwd' => $passwd));

  $result = $stmt->fetch();

  if ($result)
  {
    $userID = $result['ID'];
    $type = $result['type'];
    $nom = $result['nom'];
    $prenom = $result['prenom'];
    $actif = $result['actif'];

    $_SESSION['userID'] = $userID;
    $_SESSION['courriel'] = $courriel;
    $_SESSION['nom'] = $nom;
    $_SESSION['prenom'] = $prenom;
    $_SESSION['type'] = $type;
    $_SESSION['actif'] = $actif;


    if($_SESSION['actif'] == 0)
    {
      echo "error";
    }

    if($_SESSION["type"] == 'Étudiant'){
      $sql = "SELECT projetID FROM usr_projet_info where userID = :id";
      $stmt = $conn->prepare($sql);
      $stmt->execute(array(':id' => $userID));
      $result = $stmt->fetch();

      $_SESSION['idprojet'] = $result['projetID'];

      $sql = "SELECT nom_projet FROM projets where ID = :id";
      $stmt = $conn->prepare($sql);
      $stmt->execute(array(':id' => $_SESSION['idprojet']));
      $result = $stmt->fetch();

      $_SESSION['nomprojet']= $result['nom_projet'];

    }

    echo "success";
  }
  else{
    echo "error";
  }
}
else{
  echo "error";
}

$conn = null;
