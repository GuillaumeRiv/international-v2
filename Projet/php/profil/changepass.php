<?php

require '../BD.inc.php';

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

if (isset($_POST['newpass']) && isset($_POST['oldpass'])) {
  $oldpass = hash('SHA256', $_POST['oldpass']);
  $sql = "SELECT COUNT(*) AS count FROM utilisateurs where id = :id and password = :oldpass;";
  $stmt = $conn->prepare($sql);
  $stmt->execute(array(':id' => $_SESSION['userID'], ":oldpass" => $oldpass));
  $found = $stmt->fetch();

  if($found['count'] == 1){
  $pass = hash('SHA256', $_POST['newpass']);
  $sql = "UPDATE utilisateurs set password = :pass where id = :id";
  $stmt = $conn->prepare($sql);
  $stmt->execute(array(':pass' => $pass, ':id' => $_SESSION['userID']));
  echo "success";
  }
  else{
    echo "error_user_notfound";
  }
} else {
        echo "error_bd";
    }

$conn = null;
