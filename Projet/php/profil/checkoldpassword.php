<?php

require '../BD.inc.php';

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

if (isset($_POST['oldpass'])){

  $pass = hash('SHA256', $_POST['oldpass']);

  $sql = "SELECT * as count from utilisateurs where ID = :id and password = :pass";
  $stmt = $conn->prepare($sql);
  $stmt->execute(array(':id' => $_SESSION['userID'], ':pass' => $pass));

  $user = $stmt->fetch();

  if ($user['count'] == 1)
    echo 'success';
  else
    echo 'error_user_notfound';
}
else {
  echo 'error_empty_field';
}

$conn = null;
