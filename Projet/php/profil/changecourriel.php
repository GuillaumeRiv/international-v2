<?php

require '../BD.inc.php';

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

if (isset($_POST['newCourriel']) && isset($_POST['password'])) {
  $password = hash('SHA256', $_POST['password']);
  $sql = "SELECT COUNT(*) AS count FROM utilisateurs where id = :id and password = :password;";
  $stmt = $conn->prepare($sql);
  $stmt->execute(array(':id' => $_SESSION['userID'], ":password" => $password));
  $found = $stmt->fetch();

  if($found['count'] == 1){
  $sql = "UPDATE utilisateurs set courriel = :courriel where id = :id;";
  $stmt = $conn->prepare($sql);
  $stmt->execute(array(':courriel' => $_POST['newCourriel'], ':id' => $_SESSION['userID']));
  echo "success";
  }
  else{
    echo "error_user_notfound";
  }
} else {
        echo "error_bd";
    }

$conn = null;
