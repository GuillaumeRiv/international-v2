<?php

require '../BD.inc.php';

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
if (isset($_POST['newPrenom']) && isset($_POST['newNom']) && isset($_POST['passwordI'])) {

  $passwordI = hash('SHA256', $_POST['passwordI']);
  $sql = "SELECT COUNT(*) AS count FROM utilisateurs where id = :id and password = :passwordI;";
  $stmt = $conn->prepare($sql);
  $stmt->execute(array(':id' => $_SESSION['userID'], ":passwordI" => $passwordI));
  $found = $stmt->fetch();

  if($found['count'] == 1){
    $prenom = $_POST['newPrenom'];
    $nom = $_POST['newNom'];

  $sql = "UPDATE utilisateurs set prenom = :prenom, nom = :nom where id = :id";
  $stmt = $conn->prepare($sql);
  $stmt->execute(array(':prenom' => $prenom, ':nom' => $nom, ':id' => $_SESSION['userID']));
  echo "success";
  }
  else{
    echo "error_user_notfound";
  }
} else {
        echo "error_bd";
    }

$conn = null;
