<?php

require '../BD.inc.php';

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

if (isset($_POST['newcourriel']) && isset($_POST['password'])) {

    $pass = hash('SHA256', $_POST['password']);

    $sql = "SELECT * FROM utilisateurs WHERE ID= :id AND `password` = :pass";
    $stmt = $conn->prepare($sql);
    $stmt->execute(array(":id" => $_SESSION['userID'], ":pass" => $pass));
    $rowcount = $stmt->fetch();

    $stmtExist = $conn->prepare("SELECT count(*) from utilisateurs where courriel = LOWER(:courriel)");
    $stmtExist->execute(array(':courriel' => $_POST['newcourriel']));
    $userCtr = $stmtExist->fetchColumn();

    if($userCtr > 0)
    {
      echo "error_user_exists";
    }
    elseif($rowcount)
    {
        $sql = "UPDATE utilisateurs set courriel = :courriel where ID = :id";
        $stmt = $conn->prepare($sql);
        if ($stmt->execute(array(':courriel' => $_POST['newcourriel'], ':id' => $_SESSION['userID']))) {
            echo "success";
        } else {
            echo "error_bd";
        }
    }
    else
    {
        echo "error_wrong_password";
    }
} else {
    echo "error_empty_field";
}

$conn = null;
