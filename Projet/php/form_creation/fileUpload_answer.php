<?php
    require '../BD.inc.php';
    $ctr = 0;
    

    foreach($_FILES as $name => $value) {

        if(isset($value)){

            $errors= array();
            $file_name = $value['name'];
            $file_size = $value['size'];
            $file_tmp =  $value['tmp_name'];
            $file_type=  $value['type'];
            $tmp=explode('.',$file_name);
            $file_ext=strtolower(end($tmp));

            $extensions= array("pdf", "txt", "docx", "xlsx", "ppt", "pptx");

            if(in_array($file_ext,$extensions) == false){
                $errors[]="Le type d'un des fichiers n'est pas valide.";
            }

            if($file_size > 2097152){
                $errors[]="La taille d'un des fichiers dépasse 2 MB";
            }

            if(empty($errors)==true){
                $final_file_name=uniqid().$file_name;

                $sql = "UPDATE utilisateur_reponse SET reponse = :file WHERE id_question = :id";
                $stmt = $conn->prepare($sql);
                $stmt->bindParam(':file', $final_file_name);
                $stmt->bindParam(':id', $name);
                $stmt->execute();

                move_uploaded_file($file_tmp,"../../../uploaded_files/".$final_file_name);
            }
        }

        $ctr = $ctr + 1;
    }
