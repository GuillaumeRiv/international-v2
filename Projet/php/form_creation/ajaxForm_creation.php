<?php
    require '../BD.inc.php';

    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }

    $data = $_POST['vals'];
    $formulaire = json_decode($data, true);
    $last_id_formulaire;
    $last_id_question;
    $last_id_question_array = array();

    $sql_formulaire = "INSERT INTO formulaire (title, description, type) values (:title, :description, :type)";
    $stmt_formulaire = $conn->prepare($sql_formulaire);
    $stmt_formulaire->bindParam(':title', $formulaire['titre']);
    $stmt_formulaire->bindParam(':description', $formulaire['description']);
    $stmt_formulaire->bindParam(':type', $formulaire['typeForm']);
    $stmt_formulaire->execute();
    $last_id_formulaire = $conn->lastInsertId();

    foreach($formulaire['questions'] as $question) {
        $sql_question = "INSERT INTO question (id_formulaire, question, type) values (:id_formulaire, :question, :type)";
        $stmt_question = $conn->prepare($sql_question);
        $stmt_question->bindParam(':id_formulaire', $last_id_formulaire);
        $stmt_question->bindParam(':question', $question['titre_question']);
        $stmt_question->bindParam(':type', $question['type_question']);
        $stmt_question->execute();
        $last_id_question = $conn->lastInsertId();

        if ($question['type_question'] == "File") {
            array_push($last_id_question_array, $last_id_question);
        }
        

        if ($question['type_question'] == 'Radio' || $question['type_question'] == 'Checkbox' || $question['type_question'] == 'Select') {
            foreach($question['options'] as $option) {
                $sql_option = "INSERT INTO choix_reponse (id_question, choix_reponse) values (:id_question, :choix_reponse)";
                $stmt_option = $conn->prepare($sql_option);
                $stmt_option->bindParam(':id_question', $last_id_question);
                $stmt_option->bindParam(':choix_reponse', $option);
                $stmt_option->execute();
            }
        }
    }


    $user_arr=array(
        "message" => $last_id_question_array
    );

    print_r(json_encode($user_arr));
?>
