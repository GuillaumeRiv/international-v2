<?php
require '../BD.inc.php';

$id_user = $_POST['idUser'];
$idForm = $_POST['idForm'];
$idDemande = $_POST['idDemande']; //Mobilite = 0

$query = "SELECT * FROM formulaire WHERE id_formulaire= :idForm";
$stmt = $conn->prepare($query);
$stmt->execute(array(':idForm' => $idForm));
$num = $stmt->rowCount();

if ($num > 0) {

    // form array
    $form_arr=array();
    $form_arr["form"]=array();

    // question array
    $question_arr=array();
    $question_arr["question"]=array();

    $choix_reponse_arr=array();
    $choix_reponse_arr["choix_reponse"]=array();

    // output data of each row
    while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        extract($row);

        $query2 = "SELECT * FROM question WHERE id_formulaire= " . $id_formulaire;
        $stmt2 = $conn->prepare($query2);
        $stmt2->execute();
        $num2 = $stmt2->rowCount();

        if ($num2 > 0) {
            while($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
                extract($row2);

                $query3 = "SELECT * FROM choix_reponse WHERE id_question=" . $id_question;
                $stmt3 = $conn->prepare($query3);
                $stmt3->execute();
                $num3 = $stmt3->rowCount();
                $choix_reponse_arr["choix_reponse"]=array();

                if ($num3 > 0) {
                    while ($row3 = $stmt3->fetch(PDO::FETCH_ASSOC)) {
                        extract($row3);
                        $choix_reponse_item=array(
                            "id_choix_reponse" => $id_choix_reponse,
                            "id_question" => $id_question,
                            "choix_reponse" => $choix_reponse
                        );
                        array_push($choix_reponse_arr["choix_reponse"], $choix_reponse_item);
                    }
                }

                $query4 = "SELECT * FROM utilisateur_reponse WHERE id_question=" . $id_question . " AND id_user=" . $id_user;
                $stmt4 = $conn->prepare($query4);
                $stmt4->execute();
                $num4 = $stmt4->rowCount();

                if ($num4 > 0) {
                    while ($row4 = $stmt4->fetch(PDO::FETCH_ASSOC)) {
                        extract($row4);
                        $user_answer = $reponse;
                    }
                }

                $question_item=array(
                    "id_question" => $id_question,
                    "id_formulaire" => $id_formulaire,
                    "question" => $question,
                    "type" => $type,
                    "fileUploaded" => $fileUploaded,
                    "choix_reponse" => $choix_reponse_arr["choix_reponse"],
                    "reponse" => $user_answer
                );
                array_push($question_arr["question"], $question_item);
            }
        }

        $form_item=array(
            "id_formulaire" => $id_formulaire,
            "titre" => $title,
            "description" => $description,
            "deleted" => $deleted,
            "created_at" => $created_at,
            "updated_at" => $updated_at,
            "deleted_at" => $deleted_at,
            "questions" => $question_arr["question"]
        );
        array_push($form_arr["form"], $form_item);
    }
    echo json_encode($form_item);
    }
else {
    echo json_encode(array());
}

?>
