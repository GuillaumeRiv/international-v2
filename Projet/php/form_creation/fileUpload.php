<?php
    require '../BD.inc.php';

    $id_chaine = $_POST['ID'];
    $id_arr = explode(',', $id_chaine);
    $ctr = 0;
    $user_arr=array(
                "message" => $_FILES['fileToUpload_1']['type']
            );


    foreach($_FILES as $file) {
        $id = $id_arr[$ctr];

        if(isset($file)){
            $errors= array();
            $file_name = $file['name'];
            $file_size = $file['size'];
            $file_tmp =  $file['tmp_name'];
            $file_type=  $file['type'];
            $tmp=explode('.',$file_name);
            $file_ext=strtolower(end($tmp));

            $extensions= array("pdf", "txt", "docx", "xlsx", "ppt", "pptx");

            if(in_array($file_ext,$extensions) == false){
                $errors[]="Le type d'un des fichiers n'est pas valide.";
            }

            if($file_size > 2097152){
                $errors[]="La taille d'un des fichiers dépasse 2 MB";
            }

            if(empty($errors)==true){
                $final_file_name=uniqid().$file_name;

                $sql = "UPDATE question SET fileUploaded = :file WHERE id_question = :id";
                $stmt = $conn->prepare($sql);
                $stmt->bindParam(':file', $final_file_name);
                $stmt->bindParam(':id', $id);
                $stmt->execute();

                $user_arr=array(
                "message" => $id
            );
                

                move_uploaded_file($file_tmp,"../../../uploaded_files/".$final_file_name);
            } 
        }

        $ctr = $ctr + 1;
    }


    print_r(json_encode($user_arr));            
?>