<?php
    require '../BD.inc.php';

    $last_id_demande = 0;

    
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }

    $sql_form = "SELECT type FROM formulaire WHERE id_formulaire = :idForm";
    $stmt_form = $conn->prepare($sql_form);
    $stmt_form->bindParam(':idForm', $_POST['idForm']);
    $stmt_form->execute();
    $typeForm = $stmt_form->fetch(PDO::FETCH_ASSOC);

    if ($typeForm['type'] == "Demande") {
        $sql_etat = "INSERT INTO etat_demande values (0, :idForm, :idUser, :destination, :dateD, :dateR, 0)";
        $stmt_etat = $conn->prepare($sql_etat);
        $stmt_etat->bindParam(':idForm', $_POST['idForm']);
        $stmt_etat->bindParam(':idUser', $_SESSION['userID']);
        $stmt_etat->bindParam(':destination', $_POST['destination']);
        $stmt_etat->bindParam(':dateD', $_POST['dateD']);
        $stmt_etat->bindParam(':dateR', $_POST['dateR']);
        $stmt_etat->execute();
        $last_id_demande = $conn->lastInsertId();
    }

    $data = $_POST['vals'];
    $answers = json_decode($data, true);
    $sql_answer = "INSERT INTO utilisateur_reponse (id_user, id_question, id_demande, reponse) values (:id_user, :id_question, :id_demande, :reponse)";
    $a = $_SESSION['userID'];

    foreach($answers as $answer) {

            if (gettype($answer['reponse']) == "array") {

                $json_answer=json_encode($answer['reponse']);

                $stmt_answer = $conn->prepare($sql_answer);
                $stmt_answer->bindParam(':id_user', $a);
                $stmt_answer->bindParam(':id_question', $answer['question']);
                $stmt_answer->bindParam(':id_demande', $last_id_demande);
                $stmt_answer->bindParam(':reponse', $json_answer);
                $stmt_answer->execute();
            }
            else {
                $stmt_answer = $conn->prepare($sql_answer);
                $stmt_answer->bindParam(':id_user', $a);
                $stmt_answer->bindParam(':id_question', $answer['question']);
                $stmt_answer->bindParam(':id_demande', $last_id_demande);
                $stmt_answer->bindParam(':reponse', $answer['reponse']);
                $stmt_answer->execute();
            }

    }

    echo json_encode(array());

?>
