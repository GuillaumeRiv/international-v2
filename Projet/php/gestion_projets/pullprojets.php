<?php

require '../BD.inc.php';

  if (session_status() == PHP_SESSION_NONE) {
      session_start();
  }

$data = array();
$id = array();
$nom = array();
$code = array();
$dateD = array();

if ($_SESSION['type'] == 'Administrateur') {
    $sql = "SELECT p.ID as ID, p.id_demandes as id_demandes, p.nom_projet as nom_projet, p.codeProj as codeProj, de.dateD as dateD from projets p INNER JOIN demandes de ON de.ID = p.id_demandes where actif = 1";

    foreach($conn->query($sql) as $row){
      array_push($id, $row['ID']);
      array_push($nom, $row['nom_projet']);
      array_push($code, $row['codeProj']);
      array_push($dateD, $row['dateD']);
    }

}
else if ($_SESSION['type'] == 'Enseignant' || $_SESSION['type'] == 'Étudiant')
{
  $sql = "SELECT p.ID as ID, p.id_demandes as id_demandes, p.nom_projet as nom_projet, p.codeProj as codeProj, de.dateD as dateD from projets p
  INNER JOIN demandes de ON de.ID = p.id_demandes
  INNER JOIN usr_projet_info upi ON upi.projetID = p.ID
  where p.actif = 1 AND upi.userID = '".$_SESSION['userID']."';";
foreach ($conn->query($sql) as $row) {
              array_push($id, $row['ID']);
              array_push($nom, $row['nom_projet']);
              array_push($code, $row['codeProj']);
              array_push($dateD, $row['dateD']);
    }
}


$data['id'] = $id;
$data['nom'] = $nom;
$data['code'] = $code;
$data['dateD'] = $dateD;

echo json_encode($data);

$conn = null;
