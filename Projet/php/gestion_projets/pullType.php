<?php

require '../BD.inc.php';

  if (session_status() == PHP_SESSION_NONE) {
      session_start();
  }

    $sql = "SELECT type from utilisateurs where ID = :id;";
    $stmt = $conn->prepare($sql);
    $stmt->execute(array(':id' => $_SESSION['userID']));
    $data = $stmt->fetch();
    echo $data['type'];

$conn = null;
