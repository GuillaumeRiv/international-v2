<?php

require 'BD.inc.php';

if (!empty(trim($_POST['courriel'])) && !empty(trim($_POST['prenom'])) && !empty(trim($_POST['nom'])) && !empty(trim($_POST['passwd'])) && !empty(trim($_POST['code'])))
{
  $courriel = $_POST['courriel'];
  $prenom = $_POST['prenom'];
  $nom = $_POST['nom'];
  $passwd = hash('SHA256', $_POST['passwd']);
  $code = $_POST['code'];

  $sql = "SELECT codeProj FROM projets where codeProj = :code";

  $stmt = $conn->prepare($sql);
  $stmt->execute(array(':code' => $code));

  $result = $stmt->fetch();

  $stmtExist = $conn->prepare("SELECT count(*) from utilisateurs where courriel = LOWER(:courriel)");
  $stmtExist->execute(array(':courriel' => $courriel));
  $userCtr = $stmtExist->fetchColumn();

  if ($result && $userCtr == 0)
  {
    $sql = "INSERT INTO utilisateurs(courriel, nom, prenom, password, type) value(LOWER(:courriel), :nom, :prenom, :passwd, :type)";

    $stmt = $conn->prepare($sql);

    $stmt->execute(array(':courriel' => $courriel, ':nom' => $nom, ':prenom' => $prenom, ':passwd' => $passwd, ':type' => 'Étudiant'));




    if ($stmt->rowCount() > 0)
    {
      $sql = "SELECT ID from utilisateurs where courriel = LOWER(:courriel)";

      $stmt = $conn->prepare($sql);
      $stmt->execute(array(':courriel' => $courriel));

      $result = $stmt->fetch();
      $userID = $result['ID'];



      $sql = "SELECT ID from projets where codeProj = :code";

      $stmt = $conn->prepare($sql);
      $stmt->execute(array(':code' => $code));

      $result = $stmt->fetch();
      $projID = $result['ID'];

      $sql = "INSERT INTO usr_projet_info values(:userID, :projID)";

      $stmt = $conn->prepare($sql);
      $stmt->execute(array(':userID' => $userID, 'projID' => $projID));

      echo "success";
    }
    else
    echo "errorBD";
  }
  else if ($userCtr > 0)
  {
    echo "errorUserExists";
  }
  else
  {
    echo "error";
  }
}

$conn = null;
