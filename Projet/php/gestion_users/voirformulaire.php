<?php

require '../BD.inc.php';

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

if (isset($_POST['id'])) {
    $data = array();
    $user = $_POST['id'];
    $projid = $_SESSION['idprojet'];

    $sql = "SELECT * from utilisateurs where courriel = :user;";
    $stmt = $conn->prepare($sql);
    $stmt->execute(array(':user' => $user));

    $userid = $stmt->fetch();
    $userid = $userid['ID'];

    $sql = "SELECT * from formulaires_etudiants where userid = :userid and projetid = :projid order by date_creation desc LIMIT 1;";
    $stmt = $conn->prepare($sql);
    $stmt->execute(array(':userid' => $userid, ':projid' => $projid));

    $formetu = $stmt->fetch();

    $formid = $formetu['id_formulaire'];

    $data['nom'] = $formetu['nom'];
    $data['prenom'] = $formetu['prenom'];
    $data['courriel'] = $formetu['courriel'];
    $data['sexe'] = $formetu['sexe'];
    $data['datenaissance'] = $formetu['datenaissance'];

// formulaire coor urgence etu
    $dateDu = array();
    $dateAu = array();
    $adresse = array();
    $telephone = array();

    $sql = "SELECT * from form_coor_etu where id_formulaire = :formid;";
    $stmt = $conn->prepare($sql);
    $stmt->execute(array(':formid' => $formid));

    while ($row = $stmt->fetch()) {
        array_push($dateDu, $row['dateDu']);
        array_push($dateAu, $row['dateAu']);
        array_push($adresse, $row['adresse']);
        array_push($telephone, $row['telephone']);
    }

    $data['form_coor_etuDu'] = $dateDu;
    $data['form_coor_etuAu'] = $dateAu;
    $data['form_coor_etuAd'] = $adresse;
    $data['form_coor_etuTel'] = $telephone;


// formulaire responsable etu
    $dateDu = array();
    $dateAu = array();
    $adresse = array();
    $telephone = array();

    $sql = "SELECT * from form_coor_res where id_formulaire = :formid;";
    $stmt = $conn->prepare($sql);
    $stmt->execute(array(':formid' => $formid));

    while ($row = $stmt->fetch()) {
        array_push($dateDu, $row['dateDu']);
        array_push($dateAu, $row['dateAu']);
        array_push($adresse, $row['adresse']);
        array_push($telephone, $row['telephone']);
    }

    $data['form_coor_resDu'] = $dateDu;
    $data['form_coor_resAu'] = $dateAu;
    $data['form_coor_resAd'] = $adresse;
    $data['form_coor_resTel'] = $telephone;


// formulaire coor proche etu
  $dateDu = array();
  $dateAu = array();
  $adresse = array();
  $telephone = array();

  $sql = "SELECT * from form_coor_pro where id_formulaire = :formid;";
  $stmt = $conn->prepare($sql);
  $stmt->execute(array(':formid' => $formid));

  while ($row = $stmt->fetch()) {
    array_push($dateDu, $row['dateDu']);
    array_push($dateAu, $row['dateAu']);
    array_push($adresse, $row['adresse']);
    array_push($telephone, $row['telephone']);
  }

  $data['form_coor_proDu'] = $dateDu;
  $data['form_coor_proAu'] = $dateAu;
  $data['form_coor_proAd'] = $adresse;
  $data['form_coor_proTel'] = $telephone;


// coor assurance maladie

  $sql = "SELECT * from form_ass_mal where id_formulaire = :formid;";
  $stmt = $conn->prepare($sql);
  $stmt->execute(array(':formid' => $formid));

  $assmal = $stmt->fetch();

  $data['form_ass_malNom'] = $assmal['nom'];
  $data['form_ass_malNum'] = $assmal['num'];
  $data['form_ass_malAd'] = $assmal['adresse'];
  $data['form_ass_malTel'] = $assmal['tel'];
  $data['form_ass_malTelU'] = $assmal['telurg'];
  $data['form_ass_malCourriel'] = $assmal['courriel'];


  // coor assurance bagages
  $sql = "SELECT * from form_ass_bag where id_formulaire = :formid;";
  $stmt = $conn->prepare($sql);
  $stmt->execute(array(':formid' => $formid));

  $assbag = $stmt->fetch();

  $data['form_ass_bagNom'] = $assbag['nom'];
  $data['form_ass_bagNum'] = $assbag['num'];
  $data['form_ass_bagAd'] = $assbag['adresse'];
  $data['form_ass_bagTel'] = $assbag['tel'];
  $data['form_ass_bagNom'] = $assbag['nom'];
  $data['form_ass_bagCourriel'] = $assbag['courriel'];



  //coor ambassade

  $sql = "SELECT * from form_amb where id_formulaire = :formid;";
  $stmt = $conn->prepare($sql);
  $stmt->execute(array(':formid' => $formid));

  $amb = $stmt->fetch();

  $data['form_ambAd'] = $amb['adresse'];
  $data['form_ambTel'] = $amb['tel'];
  $data['form_ambCourriel'] = $amb['courriel'];


//santé

$sql = "SELECT * from form_sante where id_formulaire = :formid;";
$stmt = $conn->prepare($sql);
$stmt->execute(array(':formid' => $formid));

$sante = $stmt->fetch();

$data['form_santeEtat'] = $sante['etat_sante'];
$data['form_santeMed'] = $sante['medications'];
$data['form_santeAl'] = $sante['allergies'];



// vaccins
    $idvac = array();
    $valvac = array();

    $sql = "SELECT * from form_vaccins where id_formulaire = :formid;";
    $stmt = $conn->prepare($sql);
    $stmt->execute(array(':formid' => $formid));

    while ($row = $stmt->fetch()) {
        array_push($idvac, $row['id_vaccin']);
        array_push($valvac, $row['valeur_vaccin']);
    }

    $data['form_vaccinsId'] = $idvac;
    $data['form_vaccinsVal'] = $valvac;


    echo json_encode($data);

} else {
    echo "error";
}

$conn = null;
