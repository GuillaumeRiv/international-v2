<?php

require '../BD.inc.php';
if (session_status() == PHP_SESSION_NONE)
{
    session_start();
}

if (isset($_POST['courriel']))
{
    $courriel = $_POST['courriel'];
    $sql="SELECT ID FROM utilisateurs WHERE courriel = :courriel;";
    $stmt = $conn->prepare($sql);
    $stmt->execute(array(':courriel' => $courriel));

    $user = $stmt->fetch();

    if(!empty($user['ID']))
    {
      $sql= "UPDATE utilisateurs SET actif = 1 WHERE ID = :id;";
      $stmt = $conn->prepare($sql);
      $stmt->execute(array(':id' => $user['ID']));
      echo 'success';
    }
}
else { echo 'error'; }

$conn = null;
