<?php

require '../BD.inc.php';


if (isset($_POST['courriel']) && isset($_POST['nom']) && isset($_POST['prenom']) && isset($_POST['pass']) && isset($_POST['type'])) {
    $courriel = $_POST['courriel'];
    $nom = $_POST['nom'];
    $prenom = $_POST['prenom'];
    $pass = hash('SHA256', $_POST['pass']);
    $type = $_POST['type'];
    $actif = 1;


    $stmtExist = $conn->prepare("SELECT count(*) from utilisateurs where courriel = LOWER(:courriel)");
    $stmtExist->execute(array(':courriel' => $courriel));
    $userCtr = $stmtExist->fetchColumn();

    if ($userCtr == 0) {
        $sql = "INSERT INTO utilisateurs(courriel, nom, prenom, password, type, actif) values(LOWER(:courriel), :nom, :prenom, :password, :type, :actif)";
        $stmt = $conn->prepare($sql);
        $stmt->execute(array(':courriel' => $courriel, ':nom' => $nom, ':prenom' => $prenom, ':password' => $pass, ':type' => $type, ':actif' => $actif));

        echo "success";
    } else {
        echo "error_user_exists";
    }
} else {
    echo "error";
}

$conn = null;
