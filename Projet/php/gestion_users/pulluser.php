<?php

require '../BD.inc.php';

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

if(isset($_POST["pulltype"]))
{
    $pulltype = $_POST["pulltype"];
    $data = array();
    $courriel = array();
    $nom = array();
    $prenom = array();
    $types = array();
    $actif = array();
    if($pulltype == "all")
    {

            if ($_SESSION['type'] == 'Enseignant') {
                $sql = "SELECT * from usr_projet_info where userID = :userID;";
                $stmt = $conn->prepare($sql);
                $stmt->execute(array(':userID' => $_SESSION['userID']));
                $projIDs = $stmt->fetchAll(\PDO::FETCH_ASSOC);

                foreach ($projIDs as $projID) {
                    $sql = "SELECT * from usr_projet_info where projetID = :projID and userID != :userID;";
                    $stmt = $conn->prepare($sql);
                    $stmt->execute(array(':projID' => $projID['projetID'], ':userID' => $_SESSION['userID']));
                    $userListe = $stmt->fetchAll(\PDO::FETCH_ASSOC);

                    foreach ($userListe as $user) {
                        $sql = "SELECT courriel, nom, prenom, type,actif from utilisateurs where ID = :id order by courriel";
                        $stmt = $conn->prepare($sql);
                        $stmt->execute(array(':id' => $user['userID']));

                        $selectedUser = $stmt->fetch();

                        array_push($courriel, $selectedUser['courriel']);
                        array_push($nom, $selectedUser['nom']);
                        array_push($prenom, $selectedUser['prenom']);
                        array_push($types, $selectedUser['type']);
                        array_push($actif, $selectedUser['actif']);
                    }
                }
            } else if ($_SESSION['type'] == 'Administrateur') {
                $sql = "SELECT courriel, nom, prenom, type, actif from utilisateurs order by courriel";
                foreach ($conn->query($sql) as $row) {
                    array_push($courriel, $row['courriel']);
                    array_push($nom, $row['nom']);
                    array_push($prenom, $row['prenom']);
                    array_push($types, $row['type']);
                    array_push($actif, $row['actif']);
                }
            }
    }
    else if($pulltype == "projet")
    {
        if ($_SESSION['type'] == 'Enseignant') {
            if(!empty($_SESSION['idprojet']))
            {
                $sql = "SELECT userID from usr_projet_info where projetID=:projetid and userID != :userID;";
                $stmt = $conn->prepare($sql);
                $stmt->execute(array(':projetid' => $_SESSION['idprojet'], ':userID' => $_SESSION['userID']));
                $userIds = $stmt->fetchAll(\PDO::FETCH_ASSOC);

                foreach($userIds as $userId)
                    {
                       $sql = "SELECT courriel, nom, prenom, type, actif from utilisateurs where ID = :id order by courriel";
                       $stmt = $conn->prepare($sql);
                        $stmt->execute(array(':id' => $userId['userID']));
                        $userinfo = $stmt->fetch();

                            array_push($courriel, $userinfo['courriel']);
                            array_push($nom, $userinfo['nom']);
                            array_push($prenom, $userinfo['prenom']);
                            array_push($types, $userinfo['type']);
                            array_push($actif, $userinfo['actif']);
                    }
            }
        } else if($_SESSION['type'] == 'Administrateur') {
            if(!empty($_SESSION['idprojet']))
            {
                $sql = "SELECT userID from usr_projet_info where projetID=:projetid";
                $stmt = $conn->prepare($sql);
                $stmt->execute(array(':projetid' => $_SESSION['idprojet']));
                $userIds = $stmt->fetchAll(\PDO::FETCH_ASSOC);

                foreach($userIds as $userId)
                    {
                        $sql = "SELECT courriel, nom, prenom, type, actif from utilisateurs where ID = :id order by courriel";
                        $stmt = $conn->prepare($sql);
                        $stmt->execute(array(':id' => $userId['userID']));
                        $userinfo = $stmt->fetch();

                        array_push($courriel, $userinfo['courriel']);
                        array_push($nom, $userinfo['nom']);
                        array_push($prenom, $userinfo['prenom']);
                        array_push($types, $userinfo['type']);
                        array_push($actif, $userinfo['actif']);
                    }
            }
        }
    }

$data['courriel'] = $courriel;
$data['nom'] = $nom;
$data['prenom'] = $prenom;
$data['type'] = $types;
$data['actif'] = $actif;
$data['currentUser'] = $_SESSION['courriel'];

echo json_encode($data);
}
$conn = null;
