<?php

require '../BD.inc.php';

if (isset($_POST['courriel']) && isset($_POST['tempPass'])) {
    $courriel = $_POST['courriel'];
    $tempPass = hash('SHA256', $_POST['tempPass']);

    $sql="UPDATE utilisateurs SET password = :password WHERE courriel = :courriel;";
    $stmt = $conn->prepare($sql);
    $stmt->execute(array(':courriel' => $courriel, ':password' => $tempPass));

    echo 'success';
} else {
    echo 'error';
}

$conn = null;
