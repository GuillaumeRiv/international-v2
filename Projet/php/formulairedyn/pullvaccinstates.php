<?php

require '../BD.inc.php';

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

$idproj = $_SESSION['idprojet'];

$sql = "SELECT * from proj_vaccins where proj_id = :projid;";
$stmt = $conn->prepare($sql);
$stmt->execute(array(':projid' => $idproj));

$arr = $stmt->fetchAll(PDO::FETCH_ASSOC);

echo json_encode($arr);

$conn = null;
