<?php

require '../BD.inc.php';

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

if ($_POST['etat'] == "0"){
  $sql = "DELETE FROM proj_vaccins where proj_id = :projid and vaccin_id = :vaccinid;";
} else {
  $sql = "INSERT INTO proj_vaccins(proj_id, vaccin_id) values(:projid, :vaccinid);";
}

$stmt = $conn->prepare($sql);
$stmt->execute(array(':projid' => $_SESSION['idprojet'], ':vaccinid' => $_POST['id_vaccin']));

$conn = null;
