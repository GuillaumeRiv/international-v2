<?php

require '../BD.inc.php';

$data = array();
$vaccin = array();
$id_vaccin = array();

$sql = "SELECT * from vaccins";

foreach($conn->query($sql) as $row){
  array_push($id_vaccin, $row['id_vaccin']);
  array_push($vaccin, $row['vaccin']);
}

$data['vaccin'] = $vaccin;
$data['id_vaccin'] = $id_vaccin;

echo json_encode($data);

$conn = null;
