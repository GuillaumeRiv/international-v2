<?php
require '../BD.inc.php';

  if (session_status() == PHP_SESSION_NONE) {
      session_start();
  }

if (isset($_POST["idFormulaire"]) && isset($_POST["typeFormulaire"])) {
    if ($_SESSION['type'] == 'Administrateur') {
        if ($_POST["typeFormulaire"] == "Demande") {
            $sql = "UPDATE formulaire SET deleted = 1 WHERE id_formulaire = :idFormulaire";
            $stmt = $conn->prepare($sql);
            $stmt->execute(array(':idFormulaire' => $_POST['idFormulaire']));

        } else if ($_POST["typeFormulaire"] == "Mobilite") {
            $sql = "UPDATE formulaire SET deleted = 1 WHERE id_formulaire = :idFormulaire";
            $stmt = $conn->prepare($sql);
            $stmt->execute(array(':idFormulaire' => $_POST['idFormulaire']));

            $sql2 = "UPDATE formulaire_projet SET actif = 0 WHERE id_formulaire = :idFormulaire";
            $stmt2 = $conn->prepare($sql2);
            $stmt2->execute(array(':idFormulaire' => $_POST['idFormulaire']));

        } else if ($_POST["typeFormulaire"] == "Vaccin" && isset($_SESSION['idprojet'])) {
            //PARTIE VACCIN À COMPLÉTER.


        }
    }
}

$conn = null;

