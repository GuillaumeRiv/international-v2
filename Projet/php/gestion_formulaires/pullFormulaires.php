<?php

require '../BD.inc.php';

  if (session_status() == PHP_SESSION_NONE) {
      session_start();
  }

  if(isset($_POST["pullType"])) {

    $pullType = $_POST["pullType"];
    $data = array();
    $id_formulaire = array();
    $title = array();
    $description = array();
    $utilized = array();
    $selected = array();

    if ($_SESSION['type'] == 'Administrateur') {
        $sql = "SELECT * FROM formulaire WHERE deleted = 0 AND type = :type";
        $stmt = $conn->prepare($sql);
        $stmt->execute(array(':type' => $pullType));
        $formArr = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        foreach($formArr as $row){
          array_push($id_formulaire, $row['id_formulaire']);
          array_push($title, $row['title']);
          array_push($description, $row['description']);
          array_push($utilized, $row['utilized']);
          array_push($selected, $row['selected']);
        }
    }

    $data['id_formulaire'] = $id_formulaire;
    $data['title'] = $title;
    $data['description'] = $description;
    $data['utilized'] = $utilized;
    $data['selected'] = $selected;

    echo json_encode($data);

  }

$conn = null;


