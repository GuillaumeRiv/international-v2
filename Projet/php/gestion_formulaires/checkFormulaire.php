<?php
require '../BD.inc.php';

  if (session_status() == PHP_SESSION_NONE) {
      session_start();
  }

  if(isset($_POST["idFormulaire"]) && isset($_SESSION['idprojet'])) {

    $idFormulaire = $_POST["idFormulaire"];

    if ($_SESSION['type'] == 'Administrateur') {
        $sql = "SELECT * FROM formulaire_projet WHERE actif = 1 AND id_formulaire = :id_formulaire AND id_projet = :id_projet";
        $stmt = $conn->prepare($sql);
        $stmt->execute(array(':id_projet' => $_SESSION['idprojet'], ':id_formulaire' => $idFormulaire));
        $formArr = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        if (sizeof($formArr) != 0) {
            echo true;
        } else echo false;
        
    }
  } else echo false;

$conn = null;


