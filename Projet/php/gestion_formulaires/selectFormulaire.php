<?php
require '../BD.inc.php';

  if (session_status() == PHP_SESSION_NONE) {
      session_start();
  }

if (isset($_POST["idFormulaire"]) && isset($_POST["typeFormulaire"])) {
    if ($_SESSION['type'] == 'Administrateur') {
        if ($_POST["typeFormulaire"] == "Demande") {
            $sql = "UPDATE formulaire SET selected = 0 WHERE type = 'Demande'";
            $stmt = $conn->prepare($sql);
            $stmt->execute();

            $sql2 = "UPDATE formulaire SET selected = 1, utilized = 1 WHERE type = 'Demande' AND id_formulaire = :idFormulaire";
            $stmt2 = $conn->prepare($sql2);
            $stmt2->execute(array(':idFormulaire' => $_POST["idFormulaire"]));

        } else if ($_POST["typeFormulaire"] == "Mobilite" && isset($_SESSION['idprojet'])) {
            $sql = "UPDATE formulaire_projet SET actif = 0 WHERE id_projet = :idProjet";
            $stmt = $conn->prepare($sql);
            $stmt->execute(array(':idProjet' => $_SESSION['idprojet']));

            $sql2 = "SELECT * FROM formulaire_projet WHERE id_formulaire = :idFormulaire AND id_projet = :idProjet";
            $stmt2 = $conn->prepare($sql2);
            $stmt2->execute(array(':idFormulaire' => $_POST['idFormulaire'], ':idProjet' => $_SESSION['idprojet']));
            $formArr = $stmt2->fetchAll(\PDO::FETCH_ASSOC);

            if (sizeof($formArr) == 0) {
                $sql3 = "INSERT INTO formulaire_projet VALUES (:idFormulaire, :idProjet, 1)";
                $stmt3 = $conn->prepare($sql3);
                $stmt3->execute(array(':idFormulaire' => $_POST['idFormulaire'], ':idProjet' => $_SESSION['idprojet']));

            } else {
                $sql3 = "UPDATE formulaire_projet SET actif = 1 WHERE id_formulaire = :idFormulaire AND id_projet = :idProjet";
                $stmt3 = $conn->prepare($sql3);
                $stmt3->execute(array(':idFormulaire' => $_POST['idFormulaire'], ':idProjet' => $_SESSION['idprojet']));
            }
            

            $sql4 = "UPDATE formulaire SET utilized = 1 WHERE type = 'Mobilite' AND id_formulaire = :idFormulaire";
            $stmt4 = $conn->prepare($sql4);
            $stmt4->execute(array(':idFormulaire' => $_POST["idFormulaire"]));

        } 
    }
}

$conn = null;


