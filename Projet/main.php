<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

if (empty($_SESSION["courriel"])) {
    header('Location: connection.php');
}?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">

  <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>

    <link rel="icon" href="img/favicon.jpg" sizes="16x16" type="image/png">
  <title>Cégep de Trois-Rivières - Bureau des communications et des affaires institutionnelles</title>
  <!--
    =============  CSS ==================================
   -->
    <link rel="stylesheet" href= "css/main_prof.css" type="text/css">
    <link rel="stylesheet" href= "css/main_etu.css" type="text/css">
    <link rel="stylesheet" href= "css/main_admin.css" type="text/css">
    <link rel="stylesheet" href="css/PageDemandeVoyage.css">
    <link rel="stylesheet" href= "css/header.css" type="text/css">
    <link rel="stylesheet" href= "css/appreciation.css" type="text/css">
    <link rel="stylesheet" href= "css/apperciation_admin.css" type="text/css">
    <link rel="stylesheet" href= "css/profil.css">


</head>
<body>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

  <script src="js/global.js" charset="utf-8"></script>
  <script src="js/main.js" charset="utf-8"></script><?php include "header.php";?>

    <div class="toast" data-autohide="false">
      <div class="toast-header">
        <strong class="mr-auto text-primary">Projet sélectionné</strong>
      </div>
      <div>
        <div class="projetNom">
          <span id="projetTitle"></span>
        </div>
      </div>
    </div>
  </div>
  <script><?php
        if ($projctr >= 1 || $_SESSION['type'] == "Administrateur") {
            if (!empty($_SESSION["nomprojet"])) {?>
          $("#projetTitle").html("<?=$_SESSION["nomprojet"]?>");<?php
            } else {?>
           $("#projetTitle").html('Aucun projet n\'a été sélectionné');<?php
            }
        } elseif ($projctr == 0) {?>
          $("#projetTitle").html('Aucun projet n\'a été créé');<?php
        }

      $conn = null;?>
  </script>

  <script>
      $(document).ready(function(){
        $('.toast').toast('show');
      });
  </script><?php

/* ===== SECTION ADMIN ====== */

if ($_SESSION["type"] == 'Administrateur') {
    if (isset($_GET['page'])) {
        switch ($_GET['page']) {
          case 'demande':
              include 'gestion_demandes.php';
              break;

          case 'gestion_users':
              include 'gestion_users.php';
              break;

         case 'statistiques':
           include 'statistiques.php';
           break;

           case 'tableau_de_bord':
             include 'tableau_de_bord.php';
             break;

         case 'viewdemande':
            include 'form_answers.php';
            break;
         case 'projets':
            include 'gestion_projets.php';
            break;

         case 'profil':
            include 'profil.php';
            break;
         case 'viewformulaire':
            include 'FormulaireEtudiants.php';
            break;

         case 'formulaire':
           include 'gestion_formulaires.php';
           break;

           case 'appreciation_admin':
                include 'appreciation_admin.php';
                 break;

            case 'gestion_formulaires':
                include 'gestion_formulaires.php';
                 break;

          default:
              include 'main_admin.php';

      }
    } else {
        include 'main_admin.php';
    }
}

/* ===== SECTION Enseignant ====== */

if ($_SESSION["type"] == 'Enseignant') {
    if (isset($_GET['page'])) {
        switch ($_GET['page']) {
        case 'demande':
              if (isset($_GET['option']) && $_GET['option'] == "new") {
                  include 'form_generatedDemande.php';
              } else {
                  include 'gestion_demandes.php';
              }
              break;

          case 'gestion_users':
              include 'gestion_users.php';
              break;

         case 'viewdemande':
            include 'PageDemandeVoyage.php';
            break;

         case 'projets':
            include 'gestion_projets.php';
            break;

            case 'tableau_de_bord':
              include 'tableau_de_bord.php';
              break;

         case 'profil':
            include 'profil.php';
            break;
         case 'viewformulaire':
            include 'FormulaireEtudiants.php';
            break;
        case 'appreciation':
            include 'appreciation.php';
        break;

        case 'formulaire':
            if(isset($_SESSION['idprojet']))
            {
                include 'form_generatedMobilite.php';
            }
            else{
                echo '<button id="btRetour"type="button" class="btn btn-secondary col-md-1"  onclick="javascript:history.go(-1)"><i id="iRetour"class="fa fa-arrow-left"></i> Retour</button>';
                echo "<h1 style='text-align: center; color: white;'>Veuillez sélectionner un projet!</h1>";
                echo '<div id="overlay"></div>';
            }

        break;

          default:
              include 'main_prof.php';

      }
    } else {
        include 'main_prof.php';
    }
}

/* ===== SECTION ETU ====== */

if ($_SESSION["type"] == 'Étudiant') {
    if (isset($_GET['page'])) {
        switch ($_GET['page']) {

        case 'formulaire':
              include 'FormulaireEtudiants.php';
              break;
        case 'appreciation':
              include 'appreciation.php';
              break;
        case 'profil':
              include 'profil.php';
              break;
        case 'tableau_de_bord':
          include 'tableau_de_bord.php';
          break;
          case 'projets':
             include 'gestion_projets.php';
             break;

          default:
              include 'main_etu.php';

      }
    } else {
        include 'main_etu.php';
    }
}?>
</body>
</html>
