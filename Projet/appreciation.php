<button id="btRetour"type="button" class="btn btn-secondary col-md-1"  onclick="window.location.href='main.php'"><i class="fas fa-arrow-left"></i> Retour</button>
<div class="midSectAppreciation col-md-8 text-center">
    <h1 class="centerText">Bilan d'appréciation</h1>
    <br>
    <p class="centerText" id="infoText"></p>
    <a href="" target="_blank">
              <input id="fileDownload" class="btn btn-secondary" type="button" value="Télécharger le fichier">
    </a>
    <br>
    <br>
</div>
<div id="overlay"></div>
<script src="js/appreciation.js"></script>

<script>
$("#fileDownload").parent("a").parent("div").removeClass('d-none');

$.ajax({
  url: 'php/getbilandocpath.php',
  type: 'POST',
  data: {
    docid: 1
  },
  success: function(data) {
    if (data != "error")
    {
        $("#fileDownload").parent("a").attr('href', data);
    }
    else {
      $("#fileDownload").parent("a").parent("div").addClass('d-none');
      alert("Erreur lors de la récupération du fichier lié à la demande");
    }
  }
});</script>
