<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title></title>
    <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="css/tableau_de_bord.css">
    <button id="btRetour"type="button" class="btn btn-secondary col-md-1"  onclick="window.location.href='main.php'"><i class="fas fa-arrow-left"></i> Retour</button>
</head>
<body><?php
require "php/BD.inc.php";?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
<script src="js/global.js" charset="utf-8"></script>
<script src="js/tableau_de_bord.js" charset="utf-8"></script><?php
if (isset($_SESSION['type']) && ($_SESSION['type'] == "Administrateur" || $_SESSION['type'] == "Enseignant" || $_SESSION['type'] == "Étudiant")) {?>
<div class="midSection">
    <h1 class="text-white">Tableau de bord (<?php if (!empty($_SESSION["nomprojet"])){
      echo $_SESSION["nomprojet"];?>
      )
    </h1><?php
    $sql = "SELECT * FROM demandes de
    INNER JOIN projets p ON p.id_demandes = de.ID
    WHERE p.ID =  '".$_SESSION['idprojet']."' ;";
    $req = $conn->prepare($sql);
    $req->execute();
    $info = $req->fetch();


    $sql = "SELECT COUNT(*) AS count1 FROM demandes_accompagnateurs da
    INNER JOIN demandes de ON de.ID = da.id_demande
    INNER JOIN projets p ON p.id_demandes = de.ID
    WHERE p.ID =  '".$_SESSION['idprojet']."' ;";
    $req = $conn->prepare($sql);
    $req->execute();
    $count1 = $req->fetch();


    $sql = "SELECT COUNT(*) AS count2 FROM usr_projet_info upi
    INNER JOIN utilisateurs u ON u.ID = upi.userID
    WHERE u.type = 'Etudiant' AND upi.projetID = '".$_SESSION['idprojet']."'; ";
    $req = $conn->prepare($sql);
    $req->execute();
    $count2 = $req->fetch();?>
      <table class="table table-striped">
          <thead class="thead-dark">
          <th scope="col"><?php echo $info['titre']?></th>
          <th scope="col"></th>
          </thead>
          <tbody class="table-light">

            <tr scope="row"><td>Programme d'études</td><td><?php echo $info['programme'];?></td></tr>
            <tr scope="row"><td>Destination (Pays,Villes)</td><td><?php echo $info['destination'].", ".$info['precisionDestination'];?></td></tr>
            <tr scope="row"><td>Date de départ</td><td><?php echo $info['dateD'];?></td></tr>
            <tr scope="row"><td>Date de retour</td><td><?php echo $info['dateR'];?></td></tr>
            <tr scope="row"><td>Nombres d'accompagnateurs</td><td><?php echo $count1['count1'];?></td></tr>
            <tr scope="row"><td>Nombre d'étudiants</td><td><?php echo $count2['count2'];?></td></tr>

          </tbody>
      </table>
      <br><br>

      <h1 class="text-white">Étudiants participants au projet</h1>
      <table class="table table-striped">
          <thead class="thead-dark">
          <th scope="col">Nom</th>
          <th scope="col">Prénom</th>
          </thead>
          <tbody class="table-light"><?php

            $sql = "SELECT u.nom, u.prenom FROM utilisateurs u
            INNER JOIN usr_projet_info upi ON upi.userID = u.ID
              WHERE u.type = 'Etudiant' AND upi.projetID = '".$_SESSION['idprojet']."'; ";
            $req = $conn->prepare($sql);
            $req->execute();
            $found = $req->fetch();
            if($found > 0){
                $result = $conn->query($sql);
                if($result -> rowcount() > 0){
                  while($row = $result->fetch()){?>
                      <tr scope="row"><td><?php echo $row['nom']?></td><td><?php echo $row['prenom']?></td></tr><?php
                  }
                }?>
              </tbody>
            </table><?php }
            else{ ?>
              </tbody>
              </table>
              <h2 class="text-white">Il n'y a aucun élève relié à ce projet.</h2><?php
            }?>
    <br><br>
    <h1 class="text-white">Accompagnateur(s) participant(s) au projet</h1>
    <table class="table table-striped">
        <thead class="thead-dark">
        <th scope="col">Nom</th>
        <th scope="col">Prénom</th>
        </thead>
        <tbody class="table-light"><?php
            $sql = "SELECT da.nom AS nom, da.prenom AS prenom FROM demandes_accompagnateurs da
            INNER JOIN demandes de ON de.ID = da.id_demande
            INNER JOIN projets p ON p.id_demandes = de.ID
            WHERE p.ID ='".$_SESSION['idprojet']."';";
          $req = $conn->prepare($sql);
          $req->execute();
          $found = $req->fetch();
          if($found > 0){
              $result = $conn->query($sql);
              if($result -> rowcount() > 0){
                while($row = $result->fetch()){?>
                    <tr scope="row"><td><?php echo $row['nom']?></td><td><?php echo $row['prenom']?></td></tr><?php
                }
              }?>
            </tbody>
          </table><?php }
          else{ ?>
            </tbody>
            </table>
            <h2 class="text-white">Il n'y a aucun accompagnateur relié à ce projet.</h2><?php
            }?>

    <br><br>
    <h1 class="text-white">Liste des vaccins</h1>
    <table class="table table-striped">
        <thead class="thead-dark">
        <th scope="col">Vaccins</th>
        </thead>
        <tbody class="table-light"><?php

            $sql = "SELECT vaccin FROM vaccins v
            INNER JOIN proj_vaccins pv ON pv.vaccin_id = v.id_vaccin
            WHERE proj_id ='".$_SESSION['idprojet']."';";
          $req = $conn->prepare($sql);
          $req->execute();
          $found = $req->fetch();
          if($found > 0){
              $result = $conn->query($sql);
              if($result -> rowcount() > 0){
                while($row = $result->fetch()){?>
                    <tr scope="row"><td><?php echo $row['vaccin']?></td></tr><?php
                }
              }?>
            </tbody>
          </table><?php }
          else{ ?>
            </tbody>
            </table>
            <h2 class="text-white">Il n'y a aucun vaccin relié à ce projet.</h2><?php
            }?>
            <br><br>
            <h1 class="text-white">Liste d'activité(s)</h1>
            <table class="table table-striped">
                <thead class="thead-dark">
                <th scope="col">Activité(s)</th>
                <th scope="col">Date(s) ou Période(s)</th>
                </thead>
                <tbody class="table-light"><?php

                    $sql = "SELECT * FROM demandes_activites da
                    INNER JOIN demandes de ON de.ID = da.id_demande
                    INNER JOIN projets p ON p.id_demandes = de.ID
                    WHERE p.ID ='".$_SESSION['idprojet']."';";
                  $req = $conn->prepare($sql);
                  $req->execute();
                  $found = $req->fetch();
                  if($found > 0){
                      $result = $conn->query($sql);
                      if($result -> rowcount() > 0){
                        while($row = $result->fetch()){?>
                            <tr scope="row"><td><?php echo $row['activites']?></td><td><?php echo $row['dates']?></td></tr><?php
                        }
                      }?>
                    </tbody>
                  </table><?php }
                  else{ ?>
                    </tbody>
                    </table>
                    <h2 class="text-white">Il n'y a aucun vaccin relié à ce projet.</h2><?php
                    }?><?php
    }
    else { echo "Aucun projet sélectionné)";}?><?php
} else {
        echo "<br><br><h3>Vous n'avez pas les autorisations nécessaires pour accéder à cette page.</h3>";
    }?>
<div id="overlay"></div>
</body>
</html>
