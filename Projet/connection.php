<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href= "css/connection.css" type="text/css">
    <title>Page de connexion</title>
    <link rel="icon" href="img/favicon.jpg" sizes="16x16" type="image/png">
</head>
<body>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script src="js/global.js"></script>

  <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>

<header>
    <img id="logo" src="img/iconcegep.jpg" alt="Icone du cegep">`

</header>


<div class="container">
  <div class="row" id="login-container">
    <div class="col-10 offset-1">
        <form id="frm_connexion" class="center" method="POST">
          <div class="form-group mx-auto">
              <div id="connexion_alert" class="alert alert-danger" role="alert">
                      <div id="alert_message">

                      </div>
                </div>
          </div>

            <div class="form-group mx-auto">
              <label id="lbCourriel" for="tbCourriel" >Adresse courriel</label>
              <input type="text" class="form-control" id="tbCourriel">
            </div>

            <div class="form-group mx-auto">
              <label id="lbPassword" for="tbPassword">Mot de passe</label>
              <input type="password" class="form-control" id="tbPassword">
              <a href="mdpOublier.php" style="color:white; text-decoration: underline;">Mot de passe oublié?</a>
              <br>
              <label id ="lblMajuscule">*Attention au(x) majuscule(s)</label>
            </div>

            <div>
              <input type="submit" class="btn btn-secondary" id="btConnexion" value="Connexion"></input>
              <input type="button" class="btn btn-secondary" id="btCreateAccount" onClick="location.href='usr_create_account.php';" value="Création de compte"></input>
            </div>
        </form>
    </div>
  </div>
  <br>
</div>

<div id="result"></div>
<div id="overlay"></div>
<script src="js/connection.js"></script>
</body>
</html>
