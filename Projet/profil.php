<!DOCTYPE html>
<html>

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Profil</title>
    <!--[if lt IE 9]>
  <script src = "http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  </head>

  <body>
    <script src="js/profil.js" charset="utf-8"></script>
    <button id="btRetour"type="button" class="btn btn-secondary col-md-1"  onclick="window.location.href='main.php'"><i class="fas fa-arrow-left"></i> Retour</button>

    <div class="midSection">


    <div class="midSectAppreciation col-md-8 text-center">
      <h1 id="headProfil" class="float-left">Profil</h1>
      <br>
      <br>
      <br>
      <br>
      <input id="resetCourriel" class="btn btn-primary col-md-8" type="button" value="Modifier votre adresse courriel" data-toggle='modal' data-target='#resetCourrielModal'>
      <br>
      <br>
      <br>
      <input id="resetPass" class="btn btn-primary col-md-8" type="button" value="Modifier votre mot de passe" data-toggle='modal' data-target='#resetModal'>
      <br>
      <br>
      <br>
      <input id="resetInfo" class="btn btn-primary col-md-8" type="button" value="Modifier votre Nom et Prénom" data-toggle='modal' data-target='#resetInfoModal'>
      <br>
      <br>
      <br>
    </div>



      <div class="modal fade" id="resetModal" tabindex="-1" role="dialog" aria-labelledby="resetModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="resetModalLabel">Changement de mot de passe</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form id="form_changepass" method="post">

                <label for="oldPass">Mot de passe actuel :</label>
                <input id="oldPass" class="form-control" type="password">
                <label for="newPass">Nouveau mot de passe :</label>
                <input id="newPass" class="form-control" type="password">
                <label for="newPass1">Confirmer le nouveau mot de passe :</label>
                <input id="newPass1" class="form-control" type="password">

              </form>
            </div>
            <div class="modal-footer">
            <input type="submit" form="form_changepass" class="btn btn-success" value="Valider"></input>
              <button type="button" class="btn btn-secondary pull-left" data-dismiss="modal">Annuler</button>
            </div>
          </div>
        </div>
      </div>


      <div class="modal fade" id="resetCourrielModal" tabindex="-1" role="dialog" aria-labelledby="resetModalCourrielLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="resetCourrielModalLabel">Changement d'adresse courriel</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form id="form_changeCourriel" method="post">

                <label for="newCourriel">Nouvel adresse courriel :</label>
                <input id="newCourriel" class="form-control" type="text">
                <label for="confirmCourriel">Confirmer le nouvel adresse courriel :</label>
                <input id="confirmCourriel" class="form-control" type="text">
                <label for="passwordC">Mot de passe :</label>
                <input id="passwordC" class="form-control" type="password">

              </form>
            </div>
            <div class="modal-footer">
            <input type="submit" form="form_changeCourriel" class="btn btn-success" value="Valider"></input>
              <button type="button" class="btn btn-secondary pull-left" data-dismiss="modal">Annuler</button>
            </div>
          </div>
        </div>
      </div>


      <div class="modal fade" id="resetInfoModal" tabindex="-1" role="dialog" aria-labelledby="resetModalInfoLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="resetInfoModalLabel">Changement de nom ou/et prénom</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form id="form_changeInfo" method="post">

                <label for="newPrenom">Nouveau prénom :</label>
                <input id="newPrenom" class="form-control" type="text">
                <label for="newNom">Nouveau nom :</label>
                <input id="newNom" class="form-control" type="text">
                <label for="passwordI">Mot de passe :</label>
                <input id="passwordI" class="form-control" type="password">

              </form>
            </div>
            <div class="modal-footer">
            <input type="submit" form="form_changeInfo" class="btn btn-success" value="Valider"></input>
              <button type="button" class="btn btn-secondary pull-left" data-dismiss="modal">Annuler</button>
            </div>
          </div>
        </div>
      </div>


    </div>
<div id="overlay"></div>
  </body>

</html>
